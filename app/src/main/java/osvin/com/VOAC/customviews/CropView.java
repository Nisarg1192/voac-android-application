package osvin.com.VOAC.customviews;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

//import com.theartofdev.edmodo.cropper.CropImageView;

import com.library.view.cropper.CropImageView;

import osvin.com.VOAC.R;
import osvin.com.VOAC.util.Debug;
import osvin.com.VOAC.base.MyApplication;


public class CropView extends AppCompatActivity implements CropImageView.OnSetImageUriCompleteListener,
        CropImageView.OnCropImageCompleteListener {

    private CropImageView mCropImageView;
    private Button btn_back,btn_ok;

    private Uri imageUri;
    private Bitmap cropped;
    private int ratio_type, height, width;
    private MyApplication amcApplication;

    public static final String RATIO_TYPE = "ratio_type";
    public static final String IMAGE_URI = "image_uri";

    public static final int SQUARE = 3;
    public static final int LANDSCAPE_RECTANGLE = 1;
    public static final int PORTRAIT_RECTANGLE = 2;
    public static final int FREE_STYLE = 4;

    public static final int PORTRAIT_X_CORD = 8;
    public static final int PORTRAIT_Y_CORD = 10;

    public static final int LANDSCAPE_X_CORD = 10;
    public static final int LANDSCAPE_Y_CORD = 6;

    public static final int SQUARE_X_CORD = 10;
    public static final int SQUARE_Y_CORD = 10;

    public static final int PORTRAIT_HEIGHT = 1000;
    public static final int PORTRAIT_WIDTH = 800;

    public static final int LANDSCAPE_HEIGHT = 800;
    public static final int LANDSCAPE_WIDTH = 1000;

    public static final int SQUARE_HEIGHT = 1000;
    public static final int SQUARE_WIDTH = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.crop_activity);

        amcApplication = (MyApplication) getApplicationContext();

        getDataFromBundle();

        init();

        setCropImageView();

        //SetViews.set_title_to_actionbar(R.drawable.ic_arrow_left_white, getString(R.string.title_crop_image), mContext, true);
    }

    void getDataFromBundle() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            ratio_type = extras.getInt(RATIO_TYPE);
            imageUri = Uri.parse(getIntent().getStringExtra(IMAGE_URI));
        }
    }

    void init() {

        //loader = findViewById(R.id.loader);
        mCropImageView = findViewById(R.id.CropImageView);
        btn_back = findViewById(R.id.btn_back);
        btn_ok = findViewById(R.id.btn_ok);

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ratio_type != FREE_STYLE)
                    mCropImageView.getCroppedImageAsync(width, height);
                else
                    mCropImageView.getCroppedImageAsync();
            }
        });
    }

    void setCropImageView() {
        mCropImageView.setOnSetImageUriCompleteListener(this);
        mCropImageView.setOnCropImageCompleteListener(this);
        mCropImageView.setFixedAspectRatio(true);
        mCropImageView.setImageUriAsync(imageUri);
        mCropImageView.setShowProgressBar(false);


        if (ratio_type == PORTRAIT_RECTANGLE) {
            height = PORTRAIT_HEIGHT;
            width = PORTRAIT_WIDTH;
            mCropImageView.setFixedAspectRatio(true);
            mCropImageView.setAspectRatio(PORTRAIT_X_CORD, PORTRAIT_Y_CORD);
        } else if (ratio_type == LANDSCAPE_RECTANGLE) {
            height = LANDSCAPE_HEIGHT;
            width = LANDSCAPE_WIDTH;
            mCropImageView.setFixedAspectRatio(true);
            mCropImageView.setAspectRatio(LANDSCAPE_X_CORD, LANDSCAPE_Y_CORD);
        } else if (ratio_type == SQUARE) {
            height = SQUARE_HEIGHT;
            width = SQUARE_WIDTH;
            mCropImageView.setFixedAspectRatio(true);
            mCropImageView.setAspectRatio(SQUARE_X_CORD, SQUARE_Y_CORD);
        } else if (ratio_type == FREE_STYLE) {
            mCropImageView.setFixedAspectRatio(false);
        }
    }

    @Override
    public void onSetImageUriComplete(CropImageView view, Uri uri, Exception error) {
    }

    @SuppressLint("StaticFieldLeak")
    class CropImageAsync extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //showLoader();
        }

        @Override
        protected Void doInBackground(Void... params) {
            amcApplication.Photo = cropped;
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            //hideLoader();
            setResult(RESULT_OK, new Intent());
            finish();
        }
    }

    @Override
    public void onCropImageComplete(CropImageView view, CropImageView.CropResult result) {
        handleCropResult(result);
    }

    private void handleCropResult(CropImageView.CropResult result) {
        if (result.getError() == null) {

            Bitmap bitmap = result.getBitmap();
            if (bitmap != null) {
                cropped = bitmap;
                new CropImageAsync().execute();
            }
        } else {
            Debug.trace("AIC Failed to crop image", result.getError().getLocalizedMessage());
        }
    }
}