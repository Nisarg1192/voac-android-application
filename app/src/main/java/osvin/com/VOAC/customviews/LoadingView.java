package osvin.com.VOAC.customviews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.content.ContextCompat;

import osvin.com.VOAC.R;


/**
 * A simple rotating spinner with custom attributes to set duration of the rotation
 * <p>
 * Created by stoyan on 4/24/17.
 */
public class LoadingView extends AppCompatImageView {
    Context context;

    /**
     * The default duration of the rotation animation {@link #mRotateAnim}
     */
    private static final int DEFAULT_ROTATION_DURATION_MS = 1000;

    /**
     * The duration of the the rotation animation {@link #mRotateAnim},
     *
     * @see #DEFAULT_ROTATION_DURATION_MS
     */
    private int mRotationDuration = DEFAULT_ROTATION_DURATION_MS;

    /**
     * The rotation animation that we apply to this view
     */
    private Animation mRotateAnim;

    /**
     * The state of whether this view. True if this is animating and visible
     */
    private boolean mIsRunning;

    public LoadingView(Context context) {
        super(context);
        this.context = context;
        init(null);
    }

    public LoadingView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init(attrs);
    }

    public LoadingView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        init(attrs);
    }

    /**
     * Initialize all of the images and animations as well as apply attributes if set
     *
     * @param attrs
     */
    private void init(@Nullable AttributeSet attrs) {
        Drawable drawable = getResources().getDrawable(R.drawable.loading_spinner);
        drawable.setColorFilter(new PorterDuffColorFilter(ContextCompat.getColor(context, R.color.red_light), PorterDuff.Mode.MULTIPLY));
        setImageDrawable(drawable);

        if (isInEditMode()) {
            return;
        }

        if (attrs != null) {
            TypedArray a = getContext().getTheme().obtainStyledAttributes(attrs,
                    R.styleable.LoadingView, 0, 0);
            try {
                mRotationDuration = a.getInt(R.styleable.LoadingView_rotation_duration_ms, DEFAULT_ROTATION_DURATION_MS);
            } finally {
                a.recycle();
            }
        }

        mRotateAnim = AnimationUtils.loadAnimation(getContext(), R.anim.loading_spinner_rotate);
        start();
    }


    /**
     * Starts the animation and makes the view visible
     */
    public void start() {
        if (mIsRunning) {//already running
            return;
        }
        mRotateAnim.setDuration(mRotationDuration);
        setVisibility(View.VISIBLE);
        startAnimation(mRotateAnim);
        mIsRunning = true;
    }
}
