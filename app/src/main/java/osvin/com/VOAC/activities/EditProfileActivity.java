package osvin.com.VOAC.activities;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.app.ActivityCompat;

import com.crowdfire.cfalertdialog.CFAlertDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.List;

import osvin.com.VOAC.R;
import osvin.com.VOAC.api.NetworkManager;
import osvin.com.VOAC.base.BaseActivity;
import osvin.com.VOAC.customviews.CropView;
import osvin.com.VOAC.util.BitmapUtils;
import osvin.com.VOAC.util.Constants;
import osvin.com.VOAC.util.CustomDialog;
import osvin.com.VOAC.util.Debug;
import osvin.com.VOAC.util.DialogUtils;
import osvin.com.VOAC.imageloader.ImageUtils;
import osvin.com.VOAC.base.MyApplication;
import osvin.com.VOAC.util.PermissionClass;
import osvin.com.VOAC.util.Utils;
import osvin.com.VOAC.util.ValidationUtils;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;

public class EditProfileActivity extends BaseActivity implements View.OnClickListener {

    private Button btn_update_profile, btn_back;
    private EditText et_name, et_email_address;
    AppCompatImageView iv_profile_pic;

    private File ImageFile;

    String profileImageName = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        init();
    }

    private void init() {

        et_name = findViewById(R.id.et_name);
        et_email_address = findViewById(R.id.et_email_address);
        btn_update_profile = findViewById(R.id.btn_update_profile);
        btn_back = findViewById(R.id.btn_back);
        iv_profile_pic = findViewById(R.id.iv_profile_pic);

        et_name.setText(Utils.getPrefDataString(this, Constants.USER_NAME));
        et_email_address.setText(Utils.getPrefDataString(this, Constants.USER_EMAIL));

        btn_update_profile.setOnClickListener(this);
        btn_back.setOnClickListener(this);
        iv_profile_pic.setOnClickListener(this);

        if (!Utils.getPrefDataString(this, Constants.USER_PROFILE_IMAGE_URL).isEmpty()) {
            ImageUtils.loadCircle(Utils.getPrefDataString(this, Constants.USER_PROFILE_IMAGE_URL), this, R.drawable.ic_add_profile_pic, iv_profile_pic);
        }

        profileImageName = Utils.getPrefDataString(this, Constants.USER_PROFILE_IMAGE_URL);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btn_update_profile:
                validate_Data();
                break;
            case R.id.btn_back:
                onBackPressed();
                break;
            case R.id.iv_profile_pic:
                selectImage();
                break;
        }
    }

    public void validate_Data() {
        if (!ValidationUtils.ValidateEditText(et_name)) {
            CFAlertDialog.Builder builder = new CFAlertDialog.Builder(this)
                    .setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT)
                    .setMessage("Please enter name.")
                    .addButton("Ok", -1, -1, CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.END, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            builder.show();
        } else if (!ValidationUtils.ValidateEditText(et_email_address)) {
            CFAlertDialog.Builder builder = new CFAlertDialog.Builder(this)
                    .setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT)
                    .setMessage("Please enter email address.")
                    .addButton("Ok", -1, -1, CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.END, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            et_email_address.setText("");
                        }
                    });
            builder.show();
        } else if (!ValidationUtils.isValidEmail(et_email_address)) {
            CFAlertDialog.Builder builder = new CFAlertDialog.Builder(this)
                    .setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT)
                    .setMessage("Please enter valid email address.")
                    .addButton("Ok", -1, -1, CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.END, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            et_email_address.setText("");
                        }
                    });
            builder.show();
        } else {
            if (Utils.checkInternetConnection(this)) {
                CustomDialog.showLoadDialog(this);
                String cookie = Utils.getPrefDataString(this, Constants.USER_COOKIE);
                NetworkManager.getInstance().getNonce("user", "update_profile", cookie, new NetworkManager.DataObserver() {
                    @Override
                    public void onSuccess(JSONObject response) {

                        try {
                            String status = response.getString("status");
                            if (status.toLowerCase().equals("ok")) {
                                String nonce = response.getString("nonce");

                                if (ImageFile != null) {
                                    uploadProfileImage(nonce);
                                } else {
                                    updateProfileApi(nonce, "");
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            CustomDialog.hideLoadDialog();
                        }
                    }

                    @Override
                    public void onFailure(String error, int errorCode) {
                        CustomDialog.hideLoadDialog();
                        // Debug.trace("Register", error);
                        CFAlertDialog.Builder builder = new CFAlertDialog.Builder(EditProfileActivity.this)
                                .setDialogStyle(CFAlertDialog.CFAlertStyle.BOTTOM_SHEET)
                                .setMessage("Update Profile Failed, Please try later.")
                                .addButton("Cancel", -1, -1, CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.END, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        builder.show();
                    }
                });
            } else {
                CFAlertDialog.Builder builder = new CFAlertDialog.Builder(EditProfileActivity.this)
                        .setDialogStyle(CFAlertDialog.CFAlertStyle.BOTTOM_SHEET)
                        .setMessage("No Internet Connection.")
                        .addButton("Ok", -1, -1, CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.END, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                builder.show();
            }

        }
    }

    void uploadProfileImage(String nonce) {
        NetworkManager.getInstance().uploadProfileImage(ImageFile, new NetworkManager.DataObserver() {
            @Override
            public void onSuccess(JSONObject response) {
                CustomDialog.hideLoadDialog();
                Debug.trace("Upload", response.toString());

                try {

                    String status = response.getString("status");
                    if (status.toLowerCase().equals("ok")) {

                        updateProfileApi(nonce, response.getString("user_profile"));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String error, int errorCode) {
                CustomDialog.hideLoadDialog();
//                                        Debug.trace("Register", error);
                CFAlertDialog.Builder builder = new CFAlertDialog.Builder(EditProfileActivity.this)
                        .setDialogStyle(CFAlertDialog.CFAlertStyle.BOTTOM_SHEET)
                        .setMessage("Update Profile Failed, Please try later.")
                        .addButton("Cancel", -1, -1, CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.END, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                builder.show();
            }
        });
    }

    void updateProfileApi(String nonce, String user_profile) {
        NetworkManager.getInstance().updateProfile(Utils.getPrefDataString(EditProfileActivity.this, Constants.USER_ID), et_name.getText().toString(), et_email_address.getText().toString(), nonce, Utils.getPrefDataString(EditProfileActivity.this, Constants.USER_COOKIE), user_profile, new NetworkManager.DataObserver() {
            @Override
            public void onSuccess(JSONObject response) {
                CustomDialog.hideLoadDialog();
                //Debug.trace("Register", response.toString());

                try {

                    String status = response.getString("status");
                    if (status.toLowerCase().equals("ok")) {

                        if (response.has("ResponseData")) {
                            JSONObject jsonObj = response.getJSONObject("ResponseData");

                            Utils.setPrefDataString(EditProfileActivity.this, Constants.USER_NAME, jsonObj.getString("user_nicename"));
                            Utils.setPrefDataString(EditProfileActivity.this, Constants.USER_EMAIL, jsonObj.getString("user_email"));
                        }
                        setResult(RESULT_OK, new Intent());
                        finish();
                    } else {

                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(String error, int errorCode) {
                CustomDialog.hideLoadDialog();
//                                        Debug.trace("Register", error);
                CFAlertDialog.Builder builder = new CFAlertDialog.Builder(EditProfileActivity.this)
                        .setDialogStyle(CFAlertDialog.CFAlertStyle.BOTTOM_SHEET)
                        .setMessage("Update Profile Failed, Please try later.")
                        .addButton("Cancel", -1, -1, CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.END, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                builder.show();
            }
        });


    }


    void selectImage() {
        final Dialog dialog = DialogUtils.getCustomDialog(this, R.layout.cl_video_media_option, true);
        dialog.findViewById(R.id.iv_cross_media_selection).setOnClickListener((View v) -> dialog.dismiss());

        dialog.findViewById(R.id.tv_camera).setOnClickListener((View v) -> {
            dialog.dismiss();
            checkCameraPermission();
        });

        dialog.findViewById(R.id.tv_gallery).setOnClickListener((View v) -> {
            dialog.dismiss();
            checkGalleryPermission();
        });
    }

    void checkCameraPermission() {

        if (checkPermissionForCamera()) {
            EasyImage.openCamera(this, 0);
        } else {
            requestPermissionCamera();
        }
    }

    void checkGalleryPermission() {

        if (checkPermissionForGallery()) {
            EasyImage.openGallery(this, 0);
        } else {
            requestPermissionGallery();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case Constants.REQUEST_CODE_CAMERA:

                if (PermissionClass.checkPermission(permissions)) {

                    checkCameraPermission();

                } else {

                    if (!ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)
                            || !ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        alertDialogDeny(getString(R.string.camera_permission));
                    } else {
                        alertDialog(Constants.CHECK_CAMERA_PERMISSION, true);
                    }

                }
                break;

            case Constants.REQUEST_CODE_GALLERY:
                if (PermissionClass.checkPermission(permissions)) {

                    checkGalleryPermission();

                } else {
                    if (!ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        alertDialogDeny(getString(R.string.gallery_permission));

                    } else {
                        alertDialog(Constants.CHECK_GALLERY_PERMISSION, true);
                    }
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {

            if (requestCode == Constants.REQUEST_CODE_CROP_IMAGE) {

                iv_profile_pic.setImageBitmap(BitmapUtils.getCircleBitmap(MyApplication.getInstance().Photo));
                ImageFile = BitmapUtils.getFileFromBitmap(this, MyApplication.getInstance().Photo);

            } else {
                EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
                    @Override
                    public void onImagesPicked(@NonNull List<File> imageFiles, EasyImage.ImageSource source, int type) {

                        Uri imageUri = Uri.fromFile(imageFiles.get(0));

                        startActivityForResult(new Intent(EditProfileActivity.this, CropView.class)
                                .putExtra(CropView.IMAGE_URI, imageUri.toString())
                                .putExtra(CropView.RATIO_TYPE, CropView.SQUARE), Constants.REQUEST_CODE_CROP_IMAGE);
                    }
                });
            }
        }
    }
}
