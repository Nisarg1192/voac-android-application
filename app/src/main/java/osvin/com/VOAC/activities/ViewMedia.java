package osvin.com.VOAC.activities;

import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import osvin.com.VOAC.R;
import osvin.com.VOAC.base.BaseActivity;
import osvin.com.VOAC.util.Constants;
import osvin.com.VOAC.imageloader.ImageUtils;


public class ViewMedia extends BaseActivity {

    private ProgressBar progressBar;
    private Button btn_back;
    private ImageView iv_media_image;
    private SimpleExoPlayer exoPlayer;
    private ConstraintLayout cl_video_view;
    private ExoPlayer.EventListener listener;

    private String media_url, media_type;
    public static final String KEY_MEDIA_URL = "media_url";
    public static final String KEY_MEDIA_TYPE = "media_type";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_media);

        init();

        getData();

        setView();

        /*SetViews.set_title_to_actionbar(R.drawable.ic_arrow_left_white
                , Constants.DEFAULT_STRING
                , mContext
                , true);*/
    }

    void getData() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            media_url = extras.getString(KEY_MEDIA_URL);
            media_type = extras.getString(KEY_MEDIA_TYPE);
        }
    }

    void init() {
        progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);
        cl_video_view = findViewById(R.id.cl_video_view);
        iv_media_image = findViewById(R.id.iv_media_image);
        btn_back = findViewById(R.id.btn_back);

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    void setView() {

        if (media_type.equalsIgnoreCase(Constants.IMAGE)) {

            cl_video_view.setVisibility(View.GONE);
            iv_media_image.setVisibility(View.VISIBLE);

            ImageUtils.setImage(iv_media_image, media_url, this, R.color.textColorPrimaryLight);

        } else if (media_type.equalsIgnoreCase(Constants.VIDEO) || media_type.equalsIgnoreCase(Constants.AUDIO)) {

            iv_media_image.setVisibility(View.GONE);
            cl_video_view.setVisibility(View.VISIBLE);

            initExoPlayer();

            loadVideo();
        }
    }

    private void initExoPlayer() {

//        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter.Builder(this).build();
        TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveTrackSelection.Factory();
        TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);

        exoPlayer = ExoPlayerFactory.newSimpleInstance(this, trackSelector);
        PlayerView exoPlayerView = findViewById(R.id.exoPlayer);
        exoPlayerView.setPlayer(exoPlayer);

        listener = new ExoPlayer.EventListener() {

            @Override
            public void onTracksChanged(TrackGroupArray trackGroupArray, TrackSelectionArray trackSelectionArray) {
                if (trackGroupArray.length > 0) {
                    progressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onLoadingChanged(boolean b) {
                progressBar.setVisibility(b ? View.VISIBLE : View.GONE);
            }

            @Override
            public void onPlayerStateChanged(boolean b, int i) {
            }

            @Override
            public void onPlayerError(ExoPlaybackException e) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(ViewMedia.this, "Can't play this video.", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {
            }
        };
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (exoPlayer != null)
            exoPlayer.release();
    }

    private void loadVideo() {

        if (checkInternetConnection()) {

            progressBar.setVisibility(View.VISIBLE);

            final DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(this,
                    Util.getUserAgent(this, "VOICE OF AFRICAN CHILD"), null);
            final ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();

            if (!media_url.contains("http"))
                media_url = Constants.BASE_URL + media_url;

            ProgressiveMediaSource.Factory videoSource = new ProgressiveMediaSource.Factory(dataSourceFactory, extractorsFactory);
            MediaSource mediaSource = videoSource.createMediaSource(Uri.parse(media_url));
            exoPlayer.addListener(listener);
            exoPlayer.setPlayWhenReady(true);
            exoPlayer.prepare(mediaSource);

        } else {
            showSnackBar(getString(R.string.str_no_internet_connection), false, null);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}