package osvin.com.VOAC.activities;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import osvin.com.VOAC.R;
import osvin.com.VOAC.base.ExitActivity;
import osvin.com.VOAC.fragments.addpost.AddPostFragment;
import osvin.com.VOAC.fragments.addpost.CameraFragment;
import osvin.com.VOAC.fragments.feed.FeedFragment;
import osvin.com.VOAC.fragments.home.HomeFragment;
import osvin.com.VOAC.fragments.home.ProfileFragment;
import osvin.com.VOAC.fragments.settings.SettingFragment;
import osvin.com.VOAC.models.ArticleModel;
import osvin.com.VOAC.models.PostModel;
import osvin.com.VOAC.util.Constants;

public class HomeActivity extends AppCompatActivity {

    @BindView(R.id.bottom_bar)
    protected BottomNavigationView bottomNavigationBar;
    private FragmentTransaction fragmentTransaction;
    private FragmentManager fragmentManager;
    public Fragment currentFragment;


    public List<PostModel> posts_list = new ArrayList<>();
    public List<ArticleModel> article_list = new ArrayList<>();
    public boolean loading = true;
    public boolean isRefresh = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        fragmentManager = getSupportFragmentManager();
        initializeUI();
    }

    private void initializeUI() {

        bottomNavigationBar = findViewById(R.id.bottom_bar);

        bottomNavigationBar.setOnNavigationItemSelectedListener(item -> {

            switch (item.getItemId()) {
                case R.id.menu_home:
                    if (!(currentFragment instanceof HomeFragment))
                        pushFragment(HomeFragment.newInstance(), true, true, null);
                    break;

                case R.id.menu_feed:
                    if (!(currentFragment instanceof FeedFragment))
                        pushFragment(FeedFragment.newInstance(), true, true, null);
                    break;

                case R.id.menu_add_post:
                    if (!(currentFragment instanceof CameraFragment) && !(currentFragment instanceof AddPostFragment)) {
                        //                            showBottomSheetDialog();
                        pushCameraFragment(Constants.POST_TYPE_POST);
                    }
                    return false;

                case R.id.menu_profile:
                    if (!(currentFragment instanceof ProfileFragment))
                        pushFragment(ProfileFragment.newInstance(), true, true, null);
                    break;

                case R.id.menu_setting:
                    if (!(currentFragment instanceof SettingFragment))
                        pushFragment(SettingFragment.newInstance(), true, true, null);
                    break;
            }
            return true;
        });

        pushFragment(HomeFragment.newInstance(), true, true, null);
    }


    public BottomNavigationView getBottomNavigationBar() {
        return bottomNavigationBar;
    }

    /**
     * Push the fragment in the view provided in activity's xml layout
     * It uses FragmentManager to add fragment in current view
     *
     * @param fragment       object of fragment to be added in view
     * @param addToBackStack to define if fragment wants to keep in stack of fragments
     * @param shouldAnimate  to determine if the fragment add with animation or not
     * @param bundle         extras values or data pass to fragment from current view
     */
    public void pushFragment(Fragment fragment, boolean addToBackStack, boolean shouldAnimate, Bundle bundle) {

        try {
            fragmentTransaction = fragmentManager.beginTransaction();

            Fragment fragmentFromBackstack = fragmentManager.findFragmentByTag(fragment.getClass().getCanonicalName());

            if (fragmentFromBackstack == null) {

                if (bundle != null) {
                    fragment.setArguments(bundle);
                }

                if (currentFragment != null) {
                    Fragment currentFragmentInBackStack = fragmentManager.findFragmentByTag(currentFragment.getClass().getCanonicalName());
                    if (currentFragmentInBackStack == null) {

                        fragmentTransaction.remove(currentFragment);
                    } else {
                        fragmentTransaction.hide(currentFragment);
                    }
                }

                if (addToBackStack) {
                    fragmentTransaction.add(R.id.main_container, fragment, fragment.getClass().getCanonicalName());
                } else {
                    if (fragment.isAdded()) {
                        fragmentTransaction.remove(fragment);
                        fragmentTransaction.add(R.id.main_container, fragment);
                    } else {
                        fragmentTransaction.add(R.id.main_container, fragment);
                    }
                }

                currentFragment = fragment;
//                fragmentTransaction.show(fragment);
                // Commit the transaction
                fragmentTransaction.commitAllowingStateLoss();

            } else {

                if (currentFragment != null) {
                    Fragment currentFragmentInBackStack = fragmentManager.findFragmentByTag(currentFragment.getClass().getCanonicalName());
                    if (currentFragmentInBackStack == null) {
                        fragmentTransaction.remove(currentFragment);

                    } else {
                        fragmentTransaction.hide(currentFragment);

                    }
                }

                currentFragment = fragmentFromBackstack;
                if (bundle != null) {
                    currentFragment.setArguments(bundle);
                }
                fragmentTransaction.show(currentFragment);
                // Commit the transaction
                fragmentTransaction.commitAllowingStateLoss();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Pop back the fragment from stack, if when there is only one fragment in stack then it will finish
     * the current activity
     */
    public void popBackFragment() {

        try {

            int allFragmentsCount = fragmentManager.getFragments().size();

            if (allFragmentsCount > 1) {

                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.remove(fragmentManager.getFragments().get(allFragmentsCount - 1));
                fragmentTransaction.commitNow();

                allFragmentsCount = fragmentManager.getFragments().size();
                Fragment fragment = fragmentManager.getFragments().get(allFragmentsCount - 1);
                Fragment currentFragmentInBackStack = fragmentManager.findFragmentByTag(fragment.getClass().getCanonicalName());
                if (currentFragmentInBackStack == null) {

                    popBackFragment();
                } else {
                    currentFragment = fragment;
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.show(currentFragment);
                    fragmentTransaction.commitNow();

                    setNavigationBar(currentFragment);
                }
            } else {
                finish();
                ExitActivity.exitApplication(this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setNavigationBar(Fragment currentFragment) {

        if (currentFragment instanceof HomeFragment) {
            bottomNavigationBar.getMenu().getItem(0).setChecked(true);
        } else if (currentFragment instanceof FeedFragment) {
            bottomNavigationBar.getMenu().getItem(1).setChecked(true);
        } else if (currentFragment instanceof CameraFragment) {
            bottomNavigationBar.getMenu().getItem(2).setChecked(true);
        } else if (currentFragment instanceof ProfileFragment) {
            bottomNavigationBar.getMenu().getItem(3).setChecked(true);
        } else if (currentFragment instanceof SettingFragment) {
            bottomNavigationBar.getMenu().getItem(4).setChecked(true);
        }
    }

    /*Remove Fragments until provided Fragment class*/
    public void removeFragmentUntil(Class<?> fragmentClass) {

        try {
            int allFragmentsCount = fragmentManager.getFragments().size();

            if (allFragmentsCount > 0) {
                /*Note: To eliminate pop menu fragments and push base menu fragment animation effect at a same times*/

                for (int i = allFragmentsCount; i > 0; i--) {

                    Fragment fragment = fragmentManager.getFragments().get(i - 1);

                    if (Objects.requireNonNull(fragment.getClass().getCanonicalName()).equals(fragmentClass.getCanonicalName())) {
                        currentFragment = fragment;
                        fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.show(currentFragment);
                        fragmentTransaction.commitNow();
                        setNavigationBar(currentFragment);
                        break;
                    } else {
                        fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.remove(fragment);
                        fragmentTransaction.commitNow();
                    }
                }

            } else
                finish();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*Remove Fragments until provided Fragment class*/
//    public void removeFragmentUntil(Class<?> fragmentClass) {
//
//        try {
//            int backStackCountMain = fragmentManager.getBackStackEntryCount();
////            int backStackCountMain = fragmentManager.getFragments().size();
//            if (backStackCountMain > 1) {
//                /*Note: To eliminate pop menu fragments and push base menu fragment animation effect at a same times*/
//                //MyApplication.disableFragmentAnimations = true;
//                int backStackCount = backStackCountMain;
//                for (int i = 0; i < backStackCountMain; i++) {
//                    FragmentManager.BackStackEntry backEntry = fragmentManager.getBackStackEntryAt(backStackCount - 1);
//                    String str = backEntry.getName();
//                    Fragment fragment = fragmentManager.findFragmentByTag(str);
//                    if (fragment.getClass().getCanonicalName().equals(fragmentClass.getCanonicalName())) {
//                        currentFragment = fragment;
//                        break;
//                    } else
//                        fragmentManager.popBackStack();
//
//                    backStackCount--;
//                }
//
//            } else
//                finish();
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    /**
     * Push the fragment in the view provided in activity's xml layout
     * It uses FragmentManager to add fragment in current view
     *
     * @param fragment       object of fragment to be added in view
     * @param addToBackStack to define if fragment wants to keep in stack of fragments
     * @param shouldAnimate  to determine if the fragment add with animation or not
     * @param bundle         extras values or data pass to fragment from current view
     */
//    public void pushFragment(Fragment fragment, boolean addToBackStack, boolean shouldAnimate, Bundle bundle) {
//
//        try {
//            currentFragment = fragment;
//
//            if (bundle != null) {
//                fragment.setArguments(bundle);
//            }
//
//            fragmentTransaction = fragmentManager.beginTransaction();
//
//            if (shouldAnimate) {
////                fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_out_left, R.anim.enter_from_left, R.anim.exit_out_right);
////                fragmentTransaction.setCustomAnimations(R.animator.card_flip_right_in, R.animator.card_flip_right_out, R.animator.card_flip_left_in, R.animator.card_flip_left_out);
//            } else {
////                fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_out_left, R.anim.enter_from_left, R.anim.exit_out_right);
//            }
//
//            if (addToBackStack) {
//                fragmentTransaction.addToBackStack(fragment.getClass().getCanonicalName());
//            }
//
//            // Replace whatever is in the fragment_container dataView with this fragment,
//            // and add the transaction to the back stack so the user can navigate back
//            fragmentTransaction.replace(R.id.main_container, fragment, fragment.getClass().getCanonicalName());
//
//            // Commit the transaction
//            fragmentTransaction.commitAllowingStateLoss();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    /**
     * Pop back the fragment from stack, if when there is only one fragment in stack then it will finish
     * the current activity
     */
//    public void popBackFragment() {
//
//        try {
//            int backStackCount = fragmentManager.getBackStackEntryCount();
//
//            if (backStackCount > 1) {
//
//                FragmentManager.BackStackEntry backStackEntry = fragmentManager.getBackStackEntryAt(backStackCount - 2);
//
//                String className = backStackEntry.getName();
//
//                currentFragment = fragmentManager.findFragmentByTag(className);
//
//                fragmentManager.popBackStack();
//            } else {
//                finish();
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
    public void pushCameraFragment(int postType) {
//        if (!(currentFragment instanceof CameraFragment)) {
        Bundle bundle = new Bundle();
        bundle.putInt(Constants.POST_TYPE, postType);
        pushFragment(CameraFragment.newInstance(), false, true, bundle);
//        }
    }

    @Override
    public void onBackPressed() {
        popBackFragment();
    }
}
