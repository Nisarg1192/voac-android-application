package osvin.com.VOAC.activities;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import osvin.com.VOAC.R;
import osvin.com.VOAC.models.TrusteeModel;

public class TrustDetailActivity extends AppCompatActivity {

    TrusteeModel trusteeModel;
    ImageView imageView;
    TextView nameView, titleView, descriptionView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trust_detail);

        trusteeModel = getIntent().getParcelableExtra("trusteeModel");
        imageView = findViewById(R.id.image_view);
        Glide.with(this.getApplicationContext()).load(trusteeModel.photo).into(imageView);

        nameView = findViewById(R.id.name_view);
        titleView = findViewById(R.id.title_view);
        descriptionView = findViewById(R.id.description_view);

        Button btnClose = findViewById(R.id.btn_close);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        nameView.setText(trusteeModel.name);
        titleView.setText(trusteeModel.title);
        descriptionView.setText(trusteeModel.description);

    }
}
