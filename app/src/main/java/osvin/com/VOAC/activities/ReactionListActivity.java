package osvin.com.VOAC.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.tabs.TabLayout;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import osvin.com.VOAC.R;
import osvin.com.VOAC.adapter.AdpReactionList;
import osvin.com.VOAC.api.NetworkManager;
import osvin.com.VOAC.base.BaseActivity;
import osvin.com.VOAC.models.ReactionUserModel;
import osvin.com.VOAC.util.Constants;
import osvin.com.VOAC.util.DialogUtils;

public class ReactionListActivity extends BaseActivity implements View.OnClickListener {

    @BindView(R.id.btn_back)
    ImageButton btnBack;
    @BindView(R.id.appBar_layout)
    AppBarLayout appBarLayout;
    @BindView(R.id.tab_layout)
    TabLayout tabLayout;
    @BindView(R.id.rvReactions)
    RecyclerView rvReactions;
    @BindView(R.id.rootView)
    LinearLayout rootView;
    @BindView(R.id.tvNoData)
    AppCompatTextView tvNoData;

    private AdpReactionList adpReactionList;
    private List<ReactionUserModel> reactionUserList;
    private List<Integer> reactionPosition;
    private String postId = "0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reaction_list);
        ButterKnife.bind(this);

        initViews();
    }

    private void initViews() {

        if (getIntent().hasExtra(Constants.POST_ID)) {
            postId = getIntent().getStringExtra(Constants.POST_ID);
            if (postId.equals("0")) {
                DialogUtils.oneButtonDialog(ReactionListActivity.this, getString(R.string.ERROR_MSG), v -> finish()).show();
                return;
            }
        }

        reactionUserList = new ArrayList<>();
        rvReactions.setLayoutManager(new LinearLayoutManager(ReactionListActivity.this));
        setListeners();
        getReactionCount();
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.btn_back:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    private void setAdapter() {

        if (adpReactionList == null) {
            adpReactionList = new AdpReactionList(ReactionListActivity.this, reactionUserList, this);
            rvReactions.setAdapter(adpReactionList);
        } else {
            adpReactionList.refreshList(reactionUserList);
        }
    }

    private void setListeners() {
        btnBack.setOnClickListener(this);
    }

    private void getPostReactionsUser() {
        Map<String, String> requestBody = new HashMap<>();
        requestBody.put("insecure", Constants.INSECURE);
        requestBody.put("rid", String.valueOf(reactionPosition.get(tabLayout.getSelectedTabPosition())));
        requestBody.put("pid", postId);

        NetworkManager.getInstance().post(ReactionListActivity.this, true, Constants.GET_REACTION_USER, requestBody, new NetworkManager.DataObserver() {
            @Override
            public void onSuccess(JSONObject response) {

                setReactionList(response);
            }

            @Override
            public void onFailure(String error, int errorCode) {
                showSnackBarWithAction(rootView, error, getString(R.string.str_ok), true, v -> finish());
            }
        });
    }

    private void setReactionList(JSONObject response) {

        if (response.optString("status").equals("ok") && response.optInt("Responsecode") == 200) {

            JSONArray reactionArray = response.optJSONArray("reaction");

            for (int i = 0; i < reactionArray.length(); i++) {

                JSONObject reactionObj = reactionArray.optJSONObject(i);

                ReactionUserModel reactionUserModel = new ReactionUserModel();
                reactionUserModel.setProfilePhoto(reactionObj.optString("profile_pic"));
                reactionUserModel.setUserName(reactionObj.optString("username"));

                reactionUserList.add(reactionUserModel);
            }

            setAdapter();
        }
    }

    private void getReactionCount() {

        Map<String, String> requestBody = new HashMap<>();
        requestBody.put("insecure", Constants.INSECURE);
        requestBody.put("pid", postId);

        NetworkManager.getInstance().post(ReactionListActivity.this, true, Constants.POST_REACTION_COUNT, requestBody, new NetworkManager.DataObserver() {
            @Override
            public void onSuccess(JSONObject response) {

                setTabs(response);

                if (reactionPosition != null && !reactionPosition.isEmpty()) {
                    getPostReactionsUser();
                }
            }

            @Override
            public void onFailure(String error, int errorCode) {
                showSnackBarWithAction(rootView, error, getString(R.string.str_ok), true, v -> finish());
            }
        });
    }

    private void setTabs(JSONObject response) {

        if (response.optString("status").equals("ok") && response.optInt("Responsecode") == 200) {

            JSONArray reaction = response.optJSONArray("reaction");
            if (reaction.length() > 0) {
                rvReactions.setVisibility(View.VISIBLE);
                tvNoData.setVisibility(View.GONE);
                reactionPosition = new ArrayList<>();
                for (int i = 0; i < reaction.length(); i++) {
                    JSONObject reactionObj = reaction.optJSONObject(i);
                    reactionPosition.add(Integer.parseInt(reactionObj.optString("reaction_id")));

                    TabLayout.Tab tab = tabLayout.newTab();
                    switch (Integer.parseInt(reactionObj.optString("reaction_id"))) {

                        case Constants.REACTION_LIKE:
                            tab.setIcon(R.drawable.ic_fb_like);
                            break;

                        case Constants.REACTION_LOVE:
                            tab.setIcon(R.drawable.ic_fb_love);
                            break;

                        case Constants.REACTION_HAHA:
                            tab.setIcon(R.drawable.ic_fb_haha);
                            break;

                        case Constants.REACTION_SOCKED:
                            tab.setIcon(R.drawable.ic_fb_shocked);
                            break;

                        case Constants.REACTION_SAD:
                            tab.setIcon(R.drawable.ic_fb_sad);
                            break;

                        case Constants.REACTION_ANGRY:
                            tab.setIcon(R.drawable.ic_fb_angry);
                            break;
                    }

                    tab.setText(reactionObj.optString("reaction_count"));
                    boolean isSelected = false;
                    if (i == 0) {
                        isSelected = true;
                    }
                    tabLayout.addTab(tab, isSelected);
                }

                tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                    @Override
                    public void onTabSelected(TabLayout.Tab tab) {
                        reactionUserList.clear();
                        setAdapter();
                        getPostReactionsUser();
                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {

                    }

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {

                    }
                });

            } else {
                rvReactions.setVisibility(View.GONE);
                tvNoData.setVisibility(View.VISIBLE);
            }

        } else {

            showSnackBarWithAction(rootView, getString(R.string.ERROR_MSG), getString(R.string.str_ok), true, v -> finish());
        }
    }
}
