package osvin.com.VOAC.activities;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.crowdfire.cfalertdialog.CFAlertDialog;

import org.json.JSONException;
import org.json.JSONObject;

import osvin.com.VOAC.R;
import osvin.com.VOAC.api.NetworkManager;
import osvin.com.VOAC.util.Constants;
import osvin.com.VOAC.util.CustomDialog;
import osvin.com.VOAC.util.Debug;
import osvin.com.VOAC.util.DialogUtils;
import osvin.com.VOAC.util.Utils;
import osvin.com.VOAC.util.ValidationUtils;

public class ChangePasswordActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btn_update, btn_back;
    private EditText et_newPassword, et_confirmPassword;
    private Dialog dialog;
    private String new_pass, confirm_pass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        init();
    }

    private void init() {

        btn_update = findViewById(R.id.btn_update);
        btn_back = findViewById(R.id.btn_back);
        et_newPassword = findViewById(R.id.et_new_password);
        et_confirmPassword = findViewById(R.id.et_confirm_password);
        btn_update.setOnClickListener(this);
        btn_back.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btn_update:
                validate_Data();
                break;
            case R.id.btn_back:
                onBackPressed();
                break;
            case R.id.tv_ok:
                if (dialog != null && dialog.isShowing())
                    dialog.dismiss();
                finish();
                break;
        }
    }

    public void validate_Data() {

        new_pass = et_newPassword.getText().toString().trim();
        confirm_pass = et_confirmPassword.getText().toString().trim();

        if (!ValidationUtils.ValidateEditText(et_newPassword)) {
            CFAlertDialog.Builder builder = new CFAlertDialog.Builder(this)
                    .setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT)
                    .setMessage("" + getString(R.string.error_new_password))
                    .addButton("Ok", -1, -1, CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.END, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            builder.show();
        } else if (ValidationUtils.editTextLength(et_newPassword) < 6) {
            CFAlertDialog.Builder builder = new CFAlertDialog.Builder(this)
                    .setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT)
                    .setMessage("" + getString(R.string.error_min_password))
                    .addButton("Ok", -1, -1, CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.END, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            builder.show();
        } else if (!ValidationUtils.ValidateEditText(et_confirmPassword)) {
            CFAlertDialog.Builder builder = new CFAlertDialog.Builder(this)
                    .setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT)
                    .setMessage("" + getString(R.string.error_confirm_password))
                    .addButton("Ok", -1, -1, CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.END, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            builder.show();
        } else if (!confirm_pass.equalsIgnoreCase(new_pass)) {
            CFAlertDialog.Builder builder = new CFAlertDialog.Builder(this)
                    .setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT)
                    .setMessage("" + getString(R.string.error_match_password))
                    .addButton("Ok", -1, -1, CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.END, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            builder.show();
        } else {


            if (Utils.checkInternetConnection(this)) {
                CustomDialog.showLoadDialog(this);
                //String cookie = Utils.getPrefDataString(this, Constants.USER_COOKIE);
                NetworkManager.getInstance().getNonce("user", "change_password", "", new NetworkManager.DataObserver() {
                    @Override
                    public void onSuccess(JSONObject response) {

                        try {
                            String status = response.getString("status");
                            if (status.toLowerCase().equals("ok")) {
                                String nonce = response.getString("nonce");

                                NetworkManager.getInstance().changePassword(Utils.getPrefDataString(ChangePasswordActivity.this, Constants.USER_ID), new_pass,confirm_pass, nonce,  new NetworkManager.DataObserver() {
                                    @Override
                                    public void onSuccess(JSONObject response) {
                                        CustomDialog.hideLoadDialog();
                                        Debug.trace("Register", response.toString());

                                        try {

                                            String status = response.getString("status");
                                            if (status.toLowerCase().equals("ok")) {

                                                dialog = DialogUtils.oneButtonDialog(ChangePasswordActivity.this, getString(R.string.password_changed), ChangePasswordActivity.this);
                                            } else {

                                            }


                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }


                                    }

                                    @Override
                                    public void onFailure(String error, int errorCode) {
                                        CustomDialog.hideLoadDialog();
                                     //   Debug.trace("Register", error);
                                        CFAlertDialog.Builder builder = new CFAlertDialog.Builder(ChangePasswordActivity.this)
                                                .setDialogStyle(CFAlertDialog.CFAlertStyle.BOTTOM_SHEET)
                                                .setMessage("Update Profile Failed, Please try later.")
                                                .addButton("Cancel", -1, -1, CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.END, new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        dialog.dismiss();
                                                    }
                                                });
                                        builder.show();
                                    }
                                });
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            CustomDialog.hideLoadDialog();
                        }
                    }

                    @Override
                    public void onFailure(String error, int errorCode) {
                        CustomDialog.hideLoadDialog();
                      //  Debug.trace("Register", error);
                        CFAlertDialog.Builder builder = new CFAlertDialog.Builder(ChangePasswordActivity.this)
                                .setDialogStyle(CFAlertDialog.CFAlertStyle.BOTTOM_SHEET)
                                .setMessage("Update Profile Failed, Please try later.")
                                .addButton("Cancel", -1, -1, CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.END, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        builder.show();
                    }
                });
            } else {
                CFAlertDialog.Builder builder = new CFAlertDialog.Builder(ChangePasswordActivity.this)
                        .setDialogStyle(CFAlertDialog.CFAlertStyle.BOTTOM_SHEET)
                        .setMessage("No Internet Connection.")
                        .addButton("Ok", -1, -1, CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.END, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                builder.show();
            }

        }
    }
}
