package osvin.com.VOAC.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatImageButton;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.appbar.AppBarLayout;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import osvin.com.VOAC.R;
import osvin.com.VOAC.adapter.PostListAdapter;
import osvin.com.VOAC.api.NetworkManager;
import osvin.com.VOAC.base.BaseActivity;
import osvin.com.VOAC.interfaces.EndlessRecyclerViewScrollListener;
import osvin.com.VOAC.models.MediaModel;
import osvin.com.VOAC.models.PostModel;
import osvin.com.VOAC.util.Constants;
import osvin.com.VOAC.util.CustomDialog;
import osvin.com.VOAC.util.Debug;
import osvin.com.VOAC.util.PhotoFullPopupWindow;
import osvin.com.VOAC.util.Utils;

public class PostListActivity extends BaseActivity implements View.OnClickListener {

    @BindView(R.id.btn_back)
    AppCompatImageButton btnBack;
    @BindView(R.id.appBar_layout)
    AppBarLayout appBarLayout;
    @BindView(R.id.rvPosts)
    RecyclerView rvPosts;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.no_data)
    TextView noData;

    private PostListAdapter postAdapter;
    private ArrayList<PostModel> posts_list = new ArrayList<>();
    private EndlessRecyclerViewScrollListener scrollListener;
    private GridLayoutManager gridLayoutManager;
    private String user_id;
    private int TOTAL_RECORD_PER_POST = 10;
    private boolean loading = false;
    private boolean isRefresh = false;
    private int currentPage = Constants.PAGE_START;
    public static final String ID = "id";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_list);
        ButterKnife.bind(this);

        getData();
        initListener();
//        setPostAdapter(posts_list);
        swipeRefreshLayout.setRefreshing(true);
        getPosts(Constants.PAGE_STARTING_INDEX);
    }

    private void getData() {

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            user_id = extras.getString(ID, "0");
        }
    }

    private void initListener() {
        swipeRefreshLayout.setOnRefreshListener(() -> {
            scrollListener.resetState();
            isRefresh = true;
            currentPage = Constants.PAGE_START;
            getPosts(Constants.PAGE_STARTING_INDEX);
        });
        btnBack.setOnClickListener(this);

        gridLayoutManager = new GridLayoutManager(this, 3, RecyclerView.VERTICAL, false);
        rvPosts.setLayoutManager(gridLayoutManager);
        ((SimpleItemAnimator) Objects.requireNonNull(rvPosts.getItemAnimator())).setSupportsChangeAnimations(false);

        scrollListener = new EndlessRecyclerViewScrollListener(gridLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {

                // Triggered only when new data needs to be appended to the list
                // Add whatever code is needed to append new items to the bottom of the list

                if (loading) {
                    isRefresh = false;
//                    postAdapter.setShowFooter(true);
                    getPosts(page);
                }
            }

            @Override
            public void showArrowButton(int count) {

            }
        };

        rvPosts.addOnScrollListener(scrollListener);
    }

    private void setPostAdapter(List<PostModel> postList) {
        posts_list.addAll(postList);
        if (postAdapter == null) {
            postAdapter = new PostListAdapter(PostListActivity.this, posts_list, this);
            postAdapter.setMargin(getResources().getDimensionPixelSize(R.dimen._1sdp));
            postAdapter.setBlockSize(getBlockSize(getResources().getDimensionPixelSize(R.dimen._1sdp)));
            postAdapter.setHasStableIds(true);
            rvPosts.setAdapter(postAdapter);
        } else {
            postAdapter.refreshList(posts_list);
        }

        if (posts_list.isEmpty()) {
            swipeRefreshLayout.setVisibility(View.GONE);
            noData.setVisibility(View.VISIBLE);
        } else {
            swipeRefreshLayout.setVisibility(View.VISIBLE);
            noData.setVisibility(View.GONE);
        }
    }

    private int getBlockSize(int margin) {

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int widthOfDevice = displayMetrics.widthPixels;
        int actualWidth = widthOfDevice - (margin * 2);
        int blockSize = actualWidth / 3;

        return blockSize;
    }

    private void getPosts(int Page) {

        String url;
        if (user_id.equals(Utils.getPrefDataString(PostListActivity.this, Constants.USER_ID))) {
            url = Constants.GET_MY_POSTS;
        } else {
            url = Constants.GET_AUTHOR_POSTS;
        }

        NetworkManager.getInstance().getAuthorsPosts(url, Page, user_id, new NetworkManager.DataObserver() {
            @SuppressLint("StaticFieldLeak")
            @Override
            public void onSuccess(JSONObject response) {
                new AsyncTask<JSONObject, Void, List<PostModel>>() {
                    @Override
                    protected List<PostModel> doInBackground(JSONObject... jsonObjects) {
                        return Utils.parseResponse(response);
                    }

                    @Override
                    protected void onPostExecute(List<PostModel> postModels) {

                        if (isRefresh) {
                            posts_list.clear();
                        }

                        setPostAdapter(postModels);

                        loading = postModels.size() == TOTAL_RECORD_PER_POST;
                        postAdapter.setShowFooter(loading);

                        if (swipeRefreshLayout.isRefreshing()) {
                            swipeRefreshLayout.setRefreshing(false);
                        }
                    }
                }.execute(response);
            }

            @Override
            public void onFailure(String error, int errorCode) {
                Debug.trace("api error", error);
                postAdapter.setShowFooter(false);
                if (swipeRefreshLayout.isRefreshing()) {
                    swipeRefreshLayout.setRefreshing(false);
                }
                CustomDialog.hideLoadDialog();
                showSnackBar(error, true, getString(R.string.str_ok));
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_back:
                finish();
                break;

            case R.id.ivPostImage:
                MediaModel mediaFileModel = (MediaModel) v.getTag(R.id.ivPostImage);

                if (Utils.isVideo(mediaFileModel.getUrl())) {
                    startActivity(new Intent(this, ViewMedia.class)
                            .putExtra(ViewMedia.KEY_MEDIA_URL, mediaFileModel.getUrl())
                            .putExtra(ViewMedia.KEY_MEDIA_TYPE, Constants.VIDEO));
                } else {
                    new PhotoFullPopupWindow(this, R.layout.popup_photo_full, v, mediaFileModel.getUrl(), null);
                }

                break;

            case R.id.cl_main:
                PostModel postModel = (PostModel) v.getTag();
                if (postModel != null) {
                    startActivityForResult(new Intent(this, ArticleDetailActivity.class)
                                    .putExtra(Constants.KEY_MODEL, postModel)
                            , Constants.REQUEST_CODE_ARTICLE_DETAIL);
                }
                break;
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK && data != null) {

            PostModel postModel = data.getParcelableExtra(EditPostActivity.KEY_MODEL);
            int itemIndex = postModel.getPosition();

            switch (requestCode) {

                case Constants.REQUEST_CODE_ARTICLE_DETAIL:

                    if (itemIndex != -1 && itemIndex < posts_list.size()) {

                        posts_list.set(itemIndex, postModel);

                        if (postAdapter != null)
                            postAdapter.notifyDataSetChanged();
                    }
                    break;
            }
        } else if (resultCode == Constants.RESULT_CODE_DELETE_POST && data != null) {
            switch (requestCode) {
                case Constants.REQUEST_CODE_ARTICLE_DETAIL:

                    PostModel postModel = data.getParcelableExtra(Constants.KEY_MODEL);
                    int deletePosition = postModel.getPosition();

                    if (deletePosition != -1 && deletePosition < posts_list.size()) {
                        posts_list.remove(deletePosition);
                        postAdapter.notifyItemRemoved(deletePosition);
                        postAdapter.notifyItemRangeChanged(deletePosition, posts_list.size());
                    }
                    if (posts_list.size() == 0)
                        setViewVisibility(View.VISIBLE, View.GONE);
                    break;
            }
        }
    }

    public void setViewVisibility(int textVisibility, int rvVisibility) {
        noData.setVisibility(textVisibility);
        swipeRefreshLayout.setVisibility(rvVisibility);
    }
}
