package osvin.com.VOAC.activities;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Html;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.snackbar.Snackbar;
import com.rbrooks.indefinitepagerindicator.IndefinitePagerIndicator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import osvin.com.VOAC.R;
import osvin.com.VOAC.adapter.AdpAddPost;
import osvin.com.VOAC.adapter.AdpPostMedia;
import osvin.com.VOAC.api.NetworkManager;
import osvin.com.VOAC.base.BaseActivity;
import osvin.com.VOAC.location.GPSTracker;
import osvin.com.VOAC.models.MediaFileModel;
import osvin.com.VOAC.models.PostModel;
import osvin.com.VOAC.util.Constants;
import osvin.com.VOAC.util.CustomDialog;
import osvin.com.VOAC.util.Debug;
import osvin.com.VOAC.util.PermissionClass;
import osvin.com.VOAC.util.Utils;
import osvin.com.VOAC.util.ValidationUtils;


public class EditPostActivity extends BaseActivity implements View.OnClickListener {


    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.btn_back)
    AppCompatImageButton btnBack;
    @BindView(R.id.appBar_layout)
    AppBarLayout appBarLayout;
    @BindView(R.id.rvMedia)
    RecyclerView rvMedia;
    @BindView(R.id.et_location)
    EditText etLocation;
    @BindView(R.id.ivGetCurrentLocation)
    AppCompatImageView ivGetCurrentLocation;
    @BindView(R.id.et_title)
    EditText etTitle;
    @BindView(R.id.et_content)
    EditText etContent;
    @BindView(R.id.btn_add_post)
    Button btnAddPost;
    @BindView(R.id.spinView)
    LinearLayout spinView;
    @BindView(R.id.ivPhotoLibrary)
    AppCompatImageView ivPhotoLibrary;
    @BindView(R.id.dotView)
    IndefinitePagerIndicator dotView;
    @BindView(R.id.cbNeedHelp)
    AppCompatCheckBox cbNeedHelp;


    //    private byte type = Constants.ADD_POST;
    private PostModel postModel;

    public static final String KEY_TYPE = "type";
    public static final String KEY_MODEL = "model";


    private Dialog dialog;
    private AlertDialog.Builder alertDialog;
    private GPSTracker gps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_post);
        ButterKnife.bind(this);

        init();
        implementListener();
        getData();
    }

    private void getData() {

        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.containsKey(KEY_TYPE) && extras.containsKey(KEY_MODEL)) {

            postModel = extras.getParcelable(KEY_MODEL);
            if (postModel != null) {
                setDataOnView(postModel);
            }
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        if (getIntent().getExtras() != null) {
//            getCurrentLocation();
            startLocationUpdatesService(EditPostActivity.this);
        }
    }

    private void init() {

        tvTitle = findViewById(R.id.tvTitle);
        etTitle = findViewById(R.id.et_title);
        etContent = findViewById(R.id.et_content);
        etLocation = findViewById(R.id.et_location);
        btnAddPost = findViewById(R.id.btn_add_post);
//        iv_post_image = findViewById(R.id.iv_post_image);
//        tv_heading_post_image = findViewById(R.id.tv_heading_post_image);
        spinView = findViewById(R.id.spinView);
        btnBack = findViewById(R.id.btn_back);
    }

    private void implementListener() {
        btnAddPost.setOnClickListener(this);
//        iv_post_image.setOnClickListener(this);
//        tv_heading_post_image.setOnClickListener(this);
        btnBack.setOnClickListener(this);
        ivGetCurrentLocation.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btn_add_post:
                checkInternet();
                break;
            case R.id.btn_back:
                onBackPressed();
                break;

            case R.id.ivGetCurrentLocation:
                startLocationUpdatesService(EditPostActivity.this);
                break;
        }
    }

    public void validate_Data() {

        if (!ValidationUtils.ValidateEditText(etTitle)) {
            showSnackBar(getString(R.string.error_title), false, "");

        } else if (!ValidationUtils.ValidateEditText(etContent)) {
            showSnackBar(getString(R.string.error_content), false, "");

        } else if (!ValidationUtils.ValidateEditText(etLocation)) {
            showSnackBar(getString(R.string.str_please_enter_location_of_post), true, getString(R.string.str_ok));
        } else {

            hideKeyboard(this);


            // getNonceApiInterface.get_nonce("posts", "update_post", mContext, this, call, api);

            String cookie = Utils.getPrefDataString(this, Constants.USER_COOKIE);
            CustomDialog.showLoadDialog(this);
            NetworkManager.getInstance().getNonce("posts", "update_post", cookie, new NetworkManager.DataObserver() {
                @Override
                public void onSuccess(JSONObject response) {
                    try {
                        String status = response.getString("status");
                        if (status.toLowerCase().equals("ok")) {
                            String nonce = response.getString("nonce");

                            String titleREQ = "" + etTitle.getText().toString();
                            String contentREQ = "" + etContent.getText().toString();
                            String locationREQ = "" + etLocation.getText().toString();
                            String useridREQ = "" + Utils.getPrefDataString(EditPostActivity.this, Constants.USER_ID);
                            String cookieREQ = "" + Utils.getPrefDataString(EditPostActivity.this, Constants.USER_COOKIE);
//                            File post_image_file = postImageFile;

//                            JSONObject jsonObject = new JSONObject();
//                            jsonObject.put("location", "" + locationREQ);
                            NetworkManager.getInstance().UpdatePost(postModel.getId(), titleREQ, contentREQ, locationREQ, nonce, useridREQ, cookieREQ, new NetworkManager.DataObserver() {
                                @Override
                                public void onSuccess(JSONObject response) {

                                    Debug.trace("Post", response.toString());
                                    try {
                                        String status = response.getString("status");
                                        if (status.toLowerCase().equals("ok")) {
//                                            PostModel postModel = new PostModel();

                                            JSONObject parent_object = null;

                                            if (response.has("parent_post"))
                                                parent_object = response.getJSONObject("parent_post");

                                            JSONObject object = null;
                                            if (parent_object != null && parent_object.has("post"))
                                                object = parent_object.getJSONObject("post");

                                            if (object != null) {
                                                postModel.setLocation(etLocation.getText().toString().trim());
                                                postModel.setId(object.getString("ID"));
                                                postModel.setType(object.getString("post_type"));
                                                postModel.setTitle(object.getString("post_title"));
                                                postModel.setDate(object.getString("post_modified"));
                                                postModel.setStatus(object.getString("post_status"));
                                                postModel.setContent(object.getString("post_content"));
                                            }

                                            if (parent_object != null && parent_object.has("categories")) {
                                                JSONArray categoryArray = parent_object.getJSONArray("categories");
                                                for (int j = 0; j < categoryArray.length(); j++) {
                                                    JSONObject cat_object = categoryArray.getJSONObject(j);
                                                    postModel.setCategories(cat_object.getString("name"));
                                                }
                                            }

                                            if (parent_object != null && parent_object.has("author")) {
                                                JSONObject authorObject = parent_object.getJSONObject("author");
                                                postModel.setAuthor_id(authorObject.getString("ID"));
                                                postModel.setAuthor_name(authorObject.getString("display_name"));
                                            }

                                            JSONObject attachment_object;

                                            if (response.has("attachment_post") && !response.isNull("attachment_post")) {
                                                attachment_object = response.getJSONObject("attachment_post");

                                                if (attachment_object != null) {
                                                    postModel.setImage(attachment_object.getString("guid"));
                                                }
                                            }

                                            CustomDialog.hideLoadDialog();
                                            setResult(RESULT_OK, new Intent().putExtra(KEY_MODEL, postModel));
                                            finish();
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        CustomDialog.hideLoadDialog();
                                    }
                                }

                                @Override
                                public void onFailure(String error, int errorCode) {
                                    CustomDialog.hideLoadDialog();
                                    Debug.trace("Post", error);
                                    CFAlertDialog.Builder builder = new CFAlertDialog.Builder(EditPostActivity.this)
                                            .setDialogStyle(CFAlertDialog.CFAlertStyle.BOTTOM_SHEET)
                                            .setMessage("Add Post Failed, Please try later.")
                                            .addButton("Cancel", -1, -1, CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.END, (dialog, which) -> dialog.dismiss());
                                    builder.show();
                                }
                            });
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        CustomDialog.hideLoadDialog();
                    }
                }

                @Override
                public void onFailure(String error, int errorCode) {

                    //  Debug.trace("Register", error);
                    CustomDialog.hideLoadDialog();
                    CFAlertDialog.Builder builder = new CFAlertDialog.Builder(EditPostActivity.this)
                            .setDialogStyle(CFAlertDialog.CFAlertStyle.BOTTOM_SHEET)
                            .setMessage("Add Post Failed, Please try later.")
                            .addButton("Cancel", -1, -1, CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.END, (dialog, which) -> dialog.dismiss());
                    builder.show();
                }
            });
        }
    }

    public void hideKeyboard(Context ctx) {
        InputMethodManager inputManager = (InputMethodManager) ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
        View focusedView = getCurrentFocus();

        if (focusedView != null) {
            assert inputManager != null;
            inputManager.hideSoftInputFromWindow(focusedView.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public void checkInternet() {
        if (Utils.checkInternetConnection(this)) {

            validate_Data();

        } else {
            Snackbar snackbar = Snackbar.make(spinView, getString(R.string.str_no_internet_connection), Snackbar.LENGTH_LONG)
                    .setAction("RETRY", (View view) -> checkInternet());
            snackbar.setActionTextColor(Color.WHITE);
            snackbar.show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case Constants.REQUEST_CODE_LOCATION:
                if (PermissionClass.checkPermission(permissions)) {

                    startLocationUpdatesService(EditPostActivity.this);
                } else {
                    if (!ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)
                            || !ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION)) {
                        alertDialogDeny(getString(R.string.location_permission));

                    } else {
                        alertDialog(Constants.CHECK_LOCATION_PERMISSION, false);
                    }
                }
                break;
        }
    }

    private void setDataOnView(PostModel postModel) {

        if (postModel.getMediaFileModelList() != null && !postModel.getMediaFileModelList().isEmpty()) {

            if (postModel.getMediaFileModelList().size() > 1) {
                ivPhotoLibrary.setVisibility(View.VISIBLE);
            } else {
                ivPhotoLibrary.setVisibility(View.GONE);
            }

            rvMedia.setLayoutManager(new LinearLayoutManager(EditPostActivity.this, RecyclerView.HORIZONTAL, false));
            rvMedia.setAdapter(new AdpPostMedia(postModel.getMediaFileModelList(), EditPostActivity.this, this));
        } else {
            ivPhotoLibrary.setVisibility(View.GONE);
            List<MediaFileModel> list = new ArrayList<>();
            MediaFileModel mediaFileModel = new MediaFileModel();
            mediaFileModel.setPath(postModel.getImage());
            list.add(mediaFileModel);
            rvMedia.setLayoutManager(new LinearLayoutManager(EditPostActivity.this, RecyclerView.VERTICAL, false));
            rvMedia.setAdapter(new AdpAddPost(list, EditPostActivity.this, this, R.layout.list_item_add_post_single_post));
        }

        etTitle.setText(Html.fromHtml(postModel.getTitle()));
        etContent.setText(Html.fromHtml(postModel.getContent()));
        etLocation.setText(postModel.getLocation());
        btnAddPost.setText(getString(R.string.btn_update_post));
        tvTitle.setText(getString(R.string.title_update_post));

        cbNeedHelp.setVisibility(View.GONE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public String GetAddress(String lat, String lon) {
        Geocoder geocoder = new Geocoder(this, Locale.ENGLISH);
        StringBuilder ret = new StringBuilder();
        try {
            List<Address> addresses = geocoder.getFromLocation(Double.parseDouble(lat), Double.parseDouble(lon), 1);
            if (addresses != null && addresses.size() > 0) {

                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
//                String postalCode = addresses.get(0).getPostalCode();
//                String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL

                if (!TextUtils.isEmpty(address)) {
                    ret.append(address).append(",");
                }

                if (!TextUtils.isEmpty(city)) {
                    ret.append(city).append(",");
                }
                if (!TextUtils.isEmpty(state)) {
                    ret.append(state).append(",");
                }
                if (!TextUtils.isEmpty(country)) {
                    ret.append(country);
                }

            } else {
                return ret.toString();
            }
        } catch (IOException e) {
            e.printStackTrace();
            return ret.toString();
        }
        return ret.toString();
    }


    /**
     * Start location update if gps is available
     */
    public void startLocationUpdatesService(Activity activity) {

        gps = new GPSTracker(EditPostActivity.this, location -> {

            etLocation.setText(GetAddress(Double.toString(location.getLatitude()), Double.toString(location.getLongitude())));
            CustomDialog.hideLoadDialog();
            gps.stopUsingGPS();
        });

        String[] permissions = new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
        if (PermissionClass.checkPermission(permissions)) {
            if (!gps.isGPSEnabled()) {
                showSettingsAlert(activity);
            } else {
                CustomDialog.showLoadDialog(this, getString(R.string.str_please_wait_while_we_are_getting_your_location));
                gps.startLocationRequest();
            }
        } else {
            requestPermissionLocation();
        }
    }

    /**
     * Function to show settings alert dialog On pressing Settings button will
     * launch Settings Options
     */
    public void showSettingsAlert(Context context) {

        if (context != null && alertDialog == null) {

            alertDialog = new AlertDialog.Builder(context);

            // Setting DialogHelp Title
            alertDialog.setTitle(context.getString(R.string.str_gps_settings));

            // Setting DialogHelp Message
            alertDialog.setMessage(context.getString(R.string.str_gps_is_not_enable));

            // On pressing Settings button
            alertDialog.setPositiveButton(context.getString(R.string.str_settings),
                    (dialog, which) -> {
                        dialog.dismiss();
                        alertDialog = null;
                        Intent intent = new Intent(
                                Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        context.startActivity(intent);
                    });

            alertDialog.setCancelable(false);

            dialog = alertDialog.create();
            // Showing Alert Message
            dialog.show();
        }
    }

    /**
     * Dismiss the GPS setting alert dialog
     */
    public void dismissSettingDialog() {

        try {
            if (dialog != null) {
                dialog.dismiss();
                alertDialog = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        dismissSettingDialog();
    }
}