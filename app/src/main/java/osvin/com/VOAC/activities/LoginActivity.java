package osvin.com.VOAC.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import osvin.com.VOAC.R;
import osvin.com.VOAC.api.NetworkManager;
import osvin.com.VOAC.models.ProfileModel;
import osvin.com.VOAC.util.Constants;
import osvin.com.VOAC.util.Utils;
import osvin.com.VOAC.util.ValidationUtils;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnCheck;
    private EditText emailField, passwordField;

//    private LinearLayout spinView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Button btnForgot = findViewById(R.id.btn_forgot);
        btnForgot.setOnClickListener(this);
        Button btnLogin = findViewById(R.id.btn_login);
        btnLogin.setOnClickListener(this);

        Button btnRegister = findViewById(R.id.btn_register);
        btnRegister.setOnClickListener(this);

        btnCheck = findViewById(R.id.btn_check);
        btnCheck.setBackgroundResource(R.drawable.uncheck);
        btnCheck.setOnClickListener(this);
        btnCheck.setTag(false);

        emailField = findViewById(R.id.email_field);
        passwordField = findViewById(R.id.password_field);
        setPersist();

//        spinView = findViewById(R.id.spinView);
//        setSpinner(false);
    }

//    private void setView() {
//
//        if (Utils.getPrefDataBoolean(this, Constants.PREF_REMEMBER_ME)) {
//            btnCheck.setTag(true);
//            emailField.setText(Utils.getPrefDataString(this, Constants.USER_NAME_PERSIST));
//            passwordField.setText(Utils.getPrefDataString(this, Constants.USER_PASSWORD_PERSIST));
//        }
//    }

    private void removePersist() {
        SharedPreferences sharedPreferences = getSharedPreferences(Constants.APP_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(Constants.USER_NAME_PERSIST);
        editor.remove(Constants.USER_PASSWORD_PERSIST);
        editor.apply();
    }

    private void setPersist() {
        if (Utils.getPrefDataBoolean(this, Constants.PREF_REMEMBER_ME)) {
            btnCheck.setTag(true);
            boolean stats = (boolean) btnCheck.getTag();
            if (!stats) {
                btnCheck.setBackgroundResource(R.drawable.uncheck);
            } else {
                btnCheck.setBackgroundResource(R.drawable.check);
            }
        }
        SharedPreferences sharedPreferences = getSharedPreferences(Constants.APP_PREF, Context.MODE_PRIVATE);
        if (sharedPreferences.contains(Constants.USER_PASSWORD_PERSIST)) {
            passwordField.setText(sharedPreferences.getString(Constants.USER_PASSWORD_PERSIST, ""));
        }
        if (sharedPreferences.contains(Constants.USER_NAME_PERSIST)) {
            emailField.setText(sharedPreferences.getString(Constants.USER_NAME_PERSIST, ""));
        }

        emailField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                boolean status = (boolean) btnCheck.getTag();
                if (status) {
                    SharedPreferences sharedPreferences = getSharedPreferences(Constants.APP_PREF, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString(Constants.USER_NAME_PERSIST, String.valueOf(s)).apply();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        passwordField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                boolean status = (boolean) btnCheck.getTag();
                if (status) {
                    SharedPreferences sharedPreferences = getSharedPreferences(Constants.APP_PREF, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString(Constants.USER_PASSWORD_PERSIST, String.valueOf(s)).apply();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

//    private void setSpinner(boolean stats) {
//        if (stats) {
//            spinView.setVisibility(View.VISIBLE);
//        } else {
//            spinView.setVisibility(View.GONE);
//        }
//    }

    private String getDeviceIdentifier() {
        return Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    private String getDeviceToken() {
        SharedPreferences preferences = getSharedPreferences(Constants.APP_PREF, Context.MODE_PRIVATE);
        return preferences.getString(Constants.DEVICE_TOKEN, "");
    }

    private void setUserLoginStatus(boolean s) {
        SharedPreferences sharedPreferences = getSharedPreferences(Constants.APP_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(Constants.USER_LOGIN_STATUS, s).apply();
    }

    private void storeUserInfo(String email, String name, String username, String userId) {

        SharedPreferences sharedPreferences = getSharedPreferences(Constants.APP_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(Constants.USER_EMAIL, email);
        editor.putString(Constants.USER_NAME, name);
        editor.putString(Constants.USER_USERNAME, username);
        editor.putString(Constants.USER_ID, userId);
        editor.apply();

    }

    private void storeUserCookie(String cookie) {
        SharedPreferences sharedPreferences = getSharedPreferences(Constants.APP_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(Constants.USER_COOKIE, cookie).apply();
    }

    private void handleLogin() {

        Utils.setPrefDataBoolean(this, Constants.PREF_REMEMBER_ME, (Boolean) btnCheck.getTag());
        if (!Utils.getPrefDataBoolean(this, Constants.PREF_REMEMBER_ME)) {
            removePersist();
        }


        String fcm_token = getDeviceToken();
        String device_id = getDeviceIdentifier();
        String username = emailField.getText().toString();
        String password = passwordField.getText().toString();

        if (username.isEmpty() || password.isEmpty()) {
            CFAlertDialog.Builder builder = new CFAlertDialog.Builder(this)
                    .setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT)
                    .setMessage("Please enter all information.")
                    .addButton("Ok", -1, -1, CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.END, (dialog, which) -> dialog.dismiss());
            builder.show();
            return;
        }

        if (username.contains("@")) {
            if (!ValidationUtils.isEmailValid(username)) {
                CFAlertDialog.Builder builder = new CFAlertDialog.Builder(this)
                        .setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT)
                        .setMessage("Please enter valid email address.")
                        .addButton("Ok", -1, -1, CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.END, (dialog, which) -> {
                            dialog.dismiss();
                            emailField.setText("");
                        });
                builder.show();
                return;
            }
        }

//        setSpinner(true);
        Map<String, String> requestBody = new HashMap<>();
        requestBody.put("username", username);
        requestBody.put("password", password);
        requestBody.put("device_type", String.valueOf(Constants.DEVICE_TYPE));
        requestBody.put("fcm_token", fcm_token);
        requestBody.put("unique_device_id", device_id);
        requestBody.put("insecure", Constants.INSECURE);

        NetworkManager.getInstance().post(LoginActivity.this, true, Constants.LOGIN, requestBody, new NetworkManager.DataObserver() {
            @Override
            public void onSuccess(JSONObject response) {

                try {
                    String status = response.getString("status");
                    if (status.toLowerCase().equals("ok")) {
                        String cookie = response.getString("cookie");
                        storeUserCookie(cookie);
                        String email = response.getString("email");
                        String name = response.getString("user_nicename");
                        String userName = response.getString("user_login");
                        int userId = response.getInt("user_id");
                        storeUserInfo(email, name, userName, String.valueOf(userId));

                        Map<String, String> requestBody = new HashMap<>();

                        requestBody.put("user_id", String.valueOf(userId));
                        requestBody.put("cookie", cookie);
                        requestBody.put("insecure", Constants.INSECURE);

                        NetworkManager.getInstance().post(LoginActivity.this, true, Constants.GET_PROFILE, requestBody, new NetworkManager.DataObserver() {
                            @Override
                            public void onSuccess(JSONObject response) {
//                                setSpinner(false);
                                try {
                                    ProfileModel profileModel = new Gson().fromJson(response.toString(), ProfileModel.class);
                                    if (profileModel != null) {
                                        if (profileModel.getStatus() != null && profileModel.getStatus().toLowerCase().equals(Constants.RESPONSE_SUCCESS)) {

                                            Utils.setPrefDataString(LoginActivity.this, Constants.USER_ID, String.valueOf(profileModel.getId()));
                                            Utils.setPrefDataString(LoginActivity.this, Constants.USER_NAME, profileModel.getDisplayname());
                                            Utils.setPrefDataString(LoginActivity.this, Constants.USER_EMAIL, profileModel.getEmail());
                                            Utils.setPrefDataString(LoginActivity.this, Constants.USER_USERNAME, profileModel.getUsername());
                                            if (profileModel.getAvatar() == null) {
                                                profileModel.setAvatar("");
                                            }
                                            Utils.setPrefDataString(LoginActivity.this, Constants.USER_PROFILE_IMAGE_URL, profileModel.getAvatar());

                                            setUserLoginStatus(true);

                                            Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                                            startActivity(intent);
                                            finish();
                                        }
                                    }

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onFailure(String error, int errorCode) {
//                                setSpinner(false);
                                CFAlertDialog.Builder builder = new CFAlertDialog.Builder(LoginActivity.this)
                                        .setDialogStyle(CFAlertDialog.CFAlertStyle.BOTTOM_SHEET)
                                        .setMessage(getString(R.string.ERROR_MSG))
                                        .addButton("Cancel", -1, -1, CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.END, (dialog, which) -> dialog.dismiss());
                                builder.show();
                            }
                        });

                    } else {
//                        setSpinner(false);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
//                    setSpinner(false);
                }
            }

            @Override
            public void onFailure(String error, int errorCode) {

//                setSpinner(false);
                CFAlertDialog.Builder builder = new CFAlertDialog.Builder(LoginActivity.this)
                        .setDialogStyle(CFAlertDialog.CFAlertStyle.BOTTOM_SHEET)
                        .setMessage(error)
                        .addButton("Cancel", -1, -1, CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.END, (dialog, which) -> dialog.dismiss());
                builder.show();
            }
        });
    }

    @Override
    public void onClick(View v) {

        int id = v.getId();

        if (id == R.id.btn_login) {
            handleLogin();
        }

        if (id == R.id.btn_forgot) {
            Intent intent = new Intent(this, ForgotActivity.class);
            startActivity(intent);
        }

        if (id == R.id.btn_register) {
            Intent intent = new Intent(this, RegisterActivity.class);
            startActivity(intent);
        }

        if (id == R.id.btn_check) {
            boolean stats = (boolean) btnCheck.getTag();
            if (stats) {
                btnCheck.setBackgroundResource(R.drawable.uncheck);
                removePersist();
            } else {
                btnCheck.setBackgroundResource(R.drawable.check);
            }

            btnCheck.setTag(!stats);
        }
    }
}
