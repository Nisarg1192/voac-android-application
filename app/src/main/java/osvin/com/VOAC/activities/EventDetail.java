package osvin.com.VOAC.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONObject;

import osvin.com.VOAC.R;
import osvin.com.VOAC.api.NetworkManager;
import osvin.com.VOAC.base.BaseActivity;
import osvin.com.VOAC.models.EventModel;
import osvin.com.VOAC.util.CustomDialog;
import osvin.com.VOAC.imageloader.ImageUtils;

public class EventDetail extends BaseActivity implements View.OnClickListener {

    private ImageView iv_event_banner;
    private Button btn_back;

    private EventModel eventDetail;


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        getData();

        checkInternet();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_detail);

        getData();

        init();

        implementListeners();

        //SetViews.set_title_to_actionbar(R.drawable.ic_arrow_left_white, getString(R.string.event_title), mContext, true);

        checkInternet();
    }

    private void getData() {
        Bundle extras = getIntent().getExtras();
        if (extras != null)
            eventDetail = (EventModel) extras.getSerializable("eventDetail");
        else
            onBackPressed();
    }


    private void init() {


        iv_event_banner = findViewById(R.id.iv_event_banner);
        btn_back = findViewById(R.id.btn_back);

        TextView tv_title = findViewById(R.id.tv_title);
        TextView tv_desc = findViewById(R.id.tv_desc);
        TextView tv_start_date = findViewById(R.id.tv_start_date);
        TextView tv_end_date = findViewById(R.id.tv_end_date);
        TextView tv_location = findViewById(R.id.tv_location);

        tv_title.setText(eventDetail.getTitle());
        tv_desc.setText(eventDetail.getDescription().length() > 0 ? eventDetail.getDescription() : "No description");
        tv_start_date.setText(eventDetail.getStart_date());
        tv_end_date.setText(eventDetail.getEnd_date());

        String location = eventDetail.getAddress() + ", " + eventDetail.getState();
        tv_location.setText(location);

    }

    private void implementListeners() {
        btn_back.setOnClickListener(this);
    }

    private void checkInternet() {
        if (checkInternetConnection()) {
            CustomDialog.showLoadDialog(this);
            NetworkManager.getInstance().getEventPost(eventDetail.getPost_id(), new NetworkManager.DataObserver() {
                @Override
                public void onSuccess(JSONObject response) {
                    CustomDialog.hideLoadDialog();
                    try {
                        String status = response.getString("status");
                        if (status.toLowerCase().equals("ok")) {
                            String picture = response.getString("featured_image");
                            ImageUtils.setImage(iv_event_banner, picture, EventDetail.this, R.color.whitePressed);
                        } else {
                            showSnackBar("Something went wrong.", true, getString(R.string.str_ok));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(String error, int errorCode) {
                    //Debug.trace("Login", error);
                    CustomDialog.hideLoadDialog();
                    showSnackBar(error, true, getString(R.string.str_ok));
                }
            });
        } else
            showSnackBar(getString(R.string.str_no_internet_connection), true, getString(R.string.str_ok));
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_back) {
            finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
