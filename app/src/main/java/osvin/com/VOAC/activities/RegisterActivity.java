package osvin.com.VOAC.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.crowdfire.cfalertdialog.CFAlertDialog;

import org.json.JSONException;
import org.json.JSONObject;

import osvin.com.VOAC.R;
import osvin.com.VOAC.api.NetworkManager;
import osvin.com.VOAC.base.BaseActivity;
import osvin.com.VOAC.util.Constants;
import osvin.com.VOAC.util.Utils;
import osvin.com.VOAC.util.ValidationUtils;

public class RegisterActivity extends BaseActivity implements View.OnClickListener {

    private EditText emailField, passwordField, confirmField, userNameField, nameField;
    private LinearLayout spinView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        Button btnBack = findViewById(R.id.btn_back);
        btnBack.setOnClickListener(this);

        Button btnLogin = findViewById(R.id.btn_login);
        btnLogin.setOnClickListener(this);

        Button btnRegister = findViewById(R.id.btn_register);
        btnRegister.setOnClickListener(this);

        emailField = findViewById(R.id.email_field);
        passwordField = findViewById(R.id.password_field);
        confirmField = findViewById(R.id.confirm_field);
        nameField = findViewById(R.id.name_field);
        userNameField = findViewById(R.id.username_field);

        spinView = findViewById(R.id.spinView);
        setSpinner(false);
    }

    private void setSpinner(boolean s) {
        if (s) {
            spinView.setVisibility(View.VISIBLE);
        } else {
            spinView.setVisibility(View.GONE);
        }
    }

    private String getDeviceIdentifier() {
        return Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    private String getDeviceToken() {
        SharedPreferences preferences = getSharedPreferences(Constants.APP_PREF, Context.MODE_PRIVATE);
        return preferences.getString(Constants.DEVICE_TOKEN, "");
    }

    private void handleRegister() {

        final String email = emailField.getText().toString().trim();
        final String userName = userNameField.getText().toString().trim();
        final String name = nameField.getText().toString().trim();
        final String password = passwordField.getText().toString().trim();
        final String confirmPassword = confirmField.getText().toString().trim();
        final String device = getDeviceIdentifier();
        final String token = getDeviceToken();

        if (email.isEmpty() || userName.isEmpty() || name.isEmpty() || password.isEmpty() || confirmPassword.isEmpty()) {

            CFAlertDialog.Builder builder = new CFAlertDialog.Builder(this)
                    .setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT)
                    .setMessage("Please enter all information.")
                    .addButton("Ok", -1, -1, CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.END, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            builder.show();
            return;
        }

        if (!ValidationUtils.isEmailValid(email)) {
            CFAlertDialog.Builder builder = new CFAlertDialog.Builder(this)
                    .setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT)
                    .setMessage("Please enter valid email address.")
                    .addButton("Ok", -1, -1, CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.END, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            emailField.setText("");
                        }
                    });
            builder.show();
            return;
        }

        if (!password.equals(confirmPassword)) {
            CFAlertDialog.Builder builder = new CFAlertDialog.Builder(this)
                    .setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT)
                    .setMessage("Password and confirm password should match")
                    .addButton("Ok", -1, -1, CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.END, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            passwordField.setText("");
                            confirmField.setText("");
                        }
                    });
            builder.show();
            return;
        }

        setSpinner(true);
        String cookie = Utils.getPrefDataString(this,Constants.USER_COOKIE);
        NetworkManager.getInstance().getNonce("user","register",cookie,new NetworkManager.DataObserver() {
            @Override
            public void onSuccess(JSONObject response) {
                try {
                    String status = response.getString("status");
                    if (status.toLowerCase().equals("ok")) {
                        String nonce = response.getString("nonce");
                        NetworkManager.getInstance().register(userName, email, password, name, nonce, token, device, new NetworkManager.DataObserver() {
                            @Override
                            public void onSuccess(JSONObject response) {
                                setSpinner(false);
                              //  Debug.trace("Register", response.toString());
                                finish();
                            }

                            @Override
                            public void onFailure(String error, int errorCode) {
                                setSpinner(false);
                               // Debug.trace("Register", error);
                                CFAlertDialog.Builder builder = new CFAlertDialog.Builder(RegisterActivity.this)
                                        .setDialogStyle(CFAlertDialog.CFAlertStyle.BOTTOM_SHEET)
                                        .setMessage(error)
                                        .addButton("Cancel", -1, -1, CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.END, new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                            }
                                        });
                                builder.show();
                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String error, int errorCode) {
                setSpinner(false);
//                Debug.trace("Register", error);
                CFAlertDialog.Builder builder = new CFAlertDialog.Builder(RegisterActivity.this)
                        .setDialogStyle(CFAlertDialog.CFAlertStyle.BOTTOM_SHEET)
                        .setMessage(error)
                        .addButton("Cancel", -1, -1, CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.END, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                builder.show();
            }
        });
    }

    @Override
    public void onClick(View v) {

        int id = v.getId();
        if (id == R.id.btn_back) {
            finish();
        }

        if (id == R.id.btn_login) {
            finish();
        }

        if (id == R.id.btn_register) {
            handleRegister();
        }
    }
}
