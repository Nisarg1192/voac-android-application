package osvin.com.VOAC.activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.rbrooks.indefinitepagerindicator.IndefinitePagerIndicator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import osvin.com.VOAC.R;
import osvin.com.VOAC.adapter.AdpPostMedia;
import osvin.com.VOAC.adapter.CommentAdapter;
import osvin.com.VOAC.api.NetworkManager;
import osvin.com.VOAC.base.BaseActivity;
import osvin.com.VOAC.models.CommentModel;
import osvin.com.VOAC.models.MediaModel;
import osvin.com.VOAC.models.PostModel;
import osvin.com.VOAC.util.Constants;
import osvin.com.VOAC.util.CustomDialog;
import osvin.com.VOAC.util.DateUtils;
import osvin.com.VOAC.util.Debug;
import osvin.com.VOAC.util.DialogUtils;
import osvin.com.VOAC.util.HideKeyboardEdittext;
import osvin.com.VOAC.util.PhotoFullPopupWindow;
import osvin.com.VOAC.util.SetViews;
import osvin.com.VOAC.util.Utils;
import osvin.com.VOAC.util.ValidationUtils;

public class ArticleDetailActivity extends BaseActivity implements View.OnClickListener {

    @BindView(R.id.btn_back)
    AppCompatImageView btnBack;
    @BindView(R.id.rvMedia)
    RecyclerView rvMedia;
    @BindView(R.id.tv_date)
    TextView tvDate;
    @BindView(R.id.ivPhotoLibrary)
    AppCompatImageView ivPhotoLibrary;
    @BindView(R.id.tv_author)
    TextView tvAuthor;
    @BindView(R.id.tv_comments)
    TextView tvComments;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_description)
    AppCompatTextView tvDescription;
    @BindView(R.id.nsw_main)
    NestedScrollView nswMain;
    @BindView(R.id.ibEdit)
    AppCompatImageButton ibEdit;
    @BindView(R.id.ibDelete)
    AppCompatImageButton ibDelete;
    @BindView(R.id.dotView)
    IndefinitePagerIndicator dotView;
    @BindView(R.id.ivShare)
    AppCompatImageView ivShare;
    @BindView(R.id.rvComments)
    RecyclerView rvComments;
    @BindView(R.id.clLoader)
    ConstraintLayout clLoader;
    @BindView(R.id.et_comment)
    HideKeyboardEdittext etComment;
    @BindView(R.id.iv_add_comment)
    AppCompatImageView ivAddComment;
    @BindView(R.id.llCommentView)
    LinearLayout llCommentView;
    @BindView(R.id.rootView)
    LinearLayout rootView;
    @BindView(R.id.ivSubscribe)
    AppCompatImageView ivSubscribe;

    private PostModel postModel;
    public static final String KEY_ID = "id";
    private Dialog dialog;
    private boolean isFromNotification = false;
    private List<CommentModel> commentList = new ArrayList<>();
    private int POST_PER_PAGE = 10;
    private CommentAdapter adpComments;
    private int totalCommentCount = 0;
    private LinearLayoutManager linearLayoutManager;
    private boolean loading = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article_detail);
        ButterKnife.bind(this);

        init();

        getData();

    }

    private void getData() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            postModel = extras.getParcelable(Constants.KEY_MODEL);
            if (postModel != null) {
                setDataToViews(postModel);
                getComments(postModel, true);
            } else if (extras.containsKey(Constants.FROM_PUSH_NOTIFICATION)) {
                isFromNotification = extras.getBoolean(Constants.FROM_PUSH_NOTIFICATION, false);
                rootView.setVisibility(View.GONE);
                callArticleDetailApi("id", extras.getString(Constants.POST_ID));
            } else if (extras.containsKey(Constants.FROM_DEEP_LINK)) {
                isFromNotification = extras.getBoolean(Constants.FROM_DEEP_LINK, false);
                rootView.setVisibility(View.GONE);
                callArticleDetailApi("slug", extras.getString(Constants.POST_ID));
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        Debug.trace("ArticleDetail", "onResume");
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Debug.trace("ArticleDetail", "new Intent");
        Bundle extras = intent.getExtras();
        if (extras != null && extras.containsKey(Constants.FROM_PUSH_NOTIFICATION)) {
            isFromNotification = extras.getBoolean(Constants.FROM_PUSH_NOTIFICATION, false);
            rootView.setVisibility(View.GONE);
            callArticleDetailApi("id", extras.getString(Constants.POST_ID));
        }
    }

    private void init() {

        btnBack.setOnClickListener(this);
//        tvAuthor.setOnClickListener(this);
//        tvComments.setOnClickListener(this);
        ibEdit.setOnClickListener(this);
        ibDelete.setOnClickListener(this);
        ivShare.setOnClickListener(this);
        ivAddComment.setOnClickListener(this::onClick);
        ivSubscribe.setOnClickListener(this::onClick);

        SnapHelper snapHelper = new PagerSnapHelper();
        snapHelper.attachToRecyclerView(rvMedia);

        adpComments = new CommentAdapter(ArticleDetailActivity.this, commentList, this, R.layout.custom_comment_list_item);
        linearLayoutManager = new LinearLayoutManager(ArticleDetailActivity.this, RecyclerView.VERTICAL, false) {
            @Override
            public boolean supportsPredictiveItemAnimations() {
                return false;
            }
        };
        rvComments.setLayoutManager(linearLayoutManager);
        rvComments.setAdapter(adpComments);

        nswMain.getViewTreeObserver().addOnScrollChangedListener(() -> {
            View view = nswMain.getChildAt(nswMain.getChildCount() - 1);

            int diff = (view.getBottom() - (nswMain.getHeight() + nswMain
                    .getScrollY()));

            if (diff == 0) {
                // your pagination code


                Debug.trace("nestedScrollView", "ON_LOAD");
                int visibleItemCount = linearLayoutManager.getChildCount();
                int totalItemCount = linearLayoutManager.getItemCount();
                int pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition();

                if (loading) {
                    if ((visibleItemCount + 1 + pastVisibleItems) > totalItemCount) {
//                            loading = false;
                        Debug.trace("Pagination", "Last Item Wow !");
                        //Do pagination.. i.e. fetch new data

                        rvComments.post(new Runnable() {
                            @Override
                            public void run() {
                                if (postModel != null) {
                                    clLoader.setVisibility(View.VISIBLE);
                                    loading = false;
                                    getComments(postModel, false);
                                }
                            }
                        });
                    }
                }
            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_back:
                onBackPressed();
                break;
            case R.id.ivPostImage:
                MediaModel mediaFileModel = (MediaModel) v.getTag(R.id.ivPostImage);

                if (Utils.isVideo(mediaFileModel.getUrl())) {
                    startActivity(new Intent(this, ViewMedia.class)
                            .putExtra(ViewMedia.KEY_MEDIA_URL, mediaFileModel.getUrl())
                            .putExtra(ViewMedia.KEY_MEDIA_TYPE, Constants.VIDEO));
                } else {
                    new PhotoFullPopupWindow(this, R.layout.popup_photo_full, v, mediaFileModel.getUrl(), null);
                }
                break;
            case R.id.tv_author:
//                if (!author_id.equalsIgnoreCase(Utils.getPrefDataString(this, Constants.USER_ID)) && !isFromPostList) {
//                    Intent intent = new Intent(this, PostListActivity.class);
//                    intent.putExtra(PostListActivity.ID, Integer.parseInt(author_id));
//                    startActivity(intent);
//                }
                break;

//            case R.id.tv_comments:
//                startActivityForResult(new Intent(this, CommentsActivity.class)
//                        .putExtra(CommentsActivity.KEY_ARTICLE_MODEL, this.postModel), Constants.REQUEST_CODE_COMMENTS);
//                break;

            case R.id.ibEdit:
                if (postModel != null) {
                    startActivityForResult(new Intent(this, EditPostActivity.class)
                            .putExtra(EditPostActivity.KEY_TYPE, Constants.EDIT_POST)
                            .putExtra(EditPostActivity.KEY_MODEL, postModel), Constants.REQUEST_CODE_UPDATE_POST);
                }
                break;

            case R.id.ibDelete:
                if (postModel != null) {
                    dialog = DialogUtils.twoButtonDialog(this, getString(R.string.confirm_delete_dialog), v1 -> {
                        dialog.dismiss();
                        deletePost();
                    });
                }
                break;

            case R.id.ivShare:
                if (postModel != null) {

                    Utils.shareContent(ArticleDetailActivity.this, "Share post", Constants.BASE_URL + postModel.getSlug());
                }
                break;

            case R.id.iv_add_comment:
                if (postModel != null) {
//                    EditText editText = view.findViewById(R.id.et_comment);
                    hideKeyboard();
                    if (ValidationUtils.ValidateEditText(etComment)) {
                        addComment(postModel, etComment.getText().toString().trim());
                        etComment.setText("");
                    } else {
                        showSnackBar(rvMedia, "Enter valid comment.", getString(R.string.str_ok), false, null);
                    }
                }
                break;

            case R.id.ivSubscribe:
                if (postModel != null) {
                    subscribeAndUnsubscribe(postModel);
                }
                break;

            case R.id.ivUser:
                CommentModel commentModel = (CommentModel) v.getTag(R.id.ivUser);
                if (commentModel != null) {
                    new PhotoFullPopupWindow(ArticleDetailActivity.this, R.layout.popup_photo_full, v, commentModel.getProfile().replace("-150x150", ""), null);
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (isFromNotification) {
            startActivitywithAnimation(new Intent(ArticleDetailActivity.this, HomeActivity.class)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK), true);
        } else {
            if (postModel != null) {
                setResult(RESULT_OK, new Intent().putExtra(EditPostActivity.KEY_MODEL, postModel));
            }
            finish();
        }
    }


    private void addComment(PostModel postModel, String comment) {

        Map<String, String> requestBody = new HashMap<>();
        requestBody.put("post_id", postModel.getId());
        requestBody.put("user_id", Utils.getPrefDataString(ArticleDetailActivity.this, Constants.USER_ID));
        requestBody.put("content", comment);
        requestBody.put("insecure", Constants.INSECURE);

        NetworkManager.getInstance().post(ArticleDetailActivity.this, true, Constants.SUBMIT_COMMENT, requestBody, new NetworkManager.DataObserver() {
            @Override
            public void onSuccess(JSONObject response) {
                CustomDialog.hideLoadDialog();
                try {

                    String status = response.getString("status");
                    if (status.toLowerCase().equals("ok")) {

                        CommentModel commentModel = new CommentModel();
                        commentModel.setId(response.getString("comment_ID"));
                        commentModel.setName(response.getString("comment_author"));
                        commentModel.setContent(response.getString("comment_content"));
                        commentModel.setDate(response.getString("comment_date"));
                        commentModel.setProfile("" + Utils.getPrefDataString(ArticleDetailActivity.this, Constants.USER_PROFILE_IMAGE_URL));

                        commentList.add(Constants.DEFAULT_INT, commentModel);
                        adpComments.notifyDataSetChanged();

                        postModel.getComment_list().add(Constants.DEFAULT_INT, commentModel);

                        if (postModel.getComment_list().size() > 2) {
                            postModel.getComment_list().remove(2);
                        }
//                        postModel.setComment_list(commentList);
                        postModel.setComment_count(commentList.size());
                        postModel.setFormattedCommentCount(Utils.getCommentCount(postModel));

                    } else {

                        CFAlertDialog.Builder builder = new CFAlertDialog.Builder(ArticleDetailActivity.this)
                                .setDialogStyle(CFAlertDialog.CFAlertStyle.BOTTOM_SHEET)
                                .setMessage(getString(R.string.ERROR_MSG))
                                .addButton("Cancel", -1, -1, CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.END, (dialog, which) -> dialog.dismiss());
                        builder.show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String error, int errorCode) {
                // Debug.trace("Login", error);
                CustomDialog.hideLoadDialog();
                CFAlertDialog.Builder builder = new CFAlertDialog.Builder(ArticleDetailActivity.this)
                        .setDialogStyle(CFAlertDialog.CFAlertStyle.BOTTOM_SHEET)
                        .setMessage(error)
                        .addButton("Cancel", -1, -1, CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.END, (dialog, which) -> dialog.dismiss());
                builder.show();
            }
        });
    }

    private void getComments(PostModel postModel, boolean isDialogRequired) {

        Map<String, String> requestBody = new HashMap<>();
        requestBody.put("post_id", postModel.getId());
        requestBody.put("number", String.valueOf(POST_PER_PAGE));
        requestBody.put("offset", String.valueOf(commentList.size()));

        NetworkManager.getInstance().post(ArticleDetailActivity.this, isDialogRequired, Constants.GET_ALL_COMMENTS, requestBody, new NetworkManager.DataObserver() {
            @Override
            public void onSuccess(JSONObject response) {

                clLoader.setVisibility(View.GONE);
                try {
                    addComments(response);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String error, int errorCode) {
                clLoader.setVisibility(View.GONE);
                loading = false;
            }
        });
    }

    private void addComments(JSONObject response) {

        if (response != null && response.optString("status").equals("ok")) {

            totalCommentCount = response.optInt("count_total", 0);


            if (response.has("posts") && !response.isNull("posts")) {

                JSONArray jsonArray = response.optJSONArray("posts");
                if (postModel != null) {
                    postModel.getComment_list().clear();
                }
                for (int j = 0; j < jsonArray.length(); j++) {
                    JSONObject comment_object = jsonArray.optJSONObject(j);
                    CommentModel commentModel = new CommentModel();
                    commentModel.setId(comment_object.optString("comment_ID"));
                    commentModel.setName(comment_object.optString("comment_author"));
                    commentModel.setContent(comment_object.optString("comment_content"));

//                    String date = String.valueOf(android.text.format.DateUtils.getRelativeTimeSpanString(DateUtils.getDateInMillis(comment_object.optString("comment_date")), Calendar.getInstance(Locale.getDefault()).getTimeInMillis(), android.text.format.DateUtils.HOUR_IN_MILLIS));
                    String date = DateUtils.convertDateTimeFromAPI(comment_object.optString("comment_date"));
                    commentModel.setDate(date);
                    commentModel.setProfile(comment_object.optString("user_profile"));
                    commentList.add(commentModel);

                    if (postModel != null) {

                        if (j < 2) {
                            postModel.getComment_list().add(j, commentModel);
                        }
                    }
                }

                if (totalCommentCount > commentList.size()) {
                    loading = true;
                } else {
                    loading = false;
                }

                adpComments.notifyItemRangeInserted(commentList.size() - 1, jsonArray.length());
            }
        }
    }

    private void callArticleDetailApi(String key, String value) {

        if (Utils.checkInternetConnection(this)) {
            CustomDialog.showLoadDialog(this);
            String apiPath;
//            if (postModel.getAuthor_id().equalsIgnoreCase(Utils.getPrefDataString(this, Constants.USER_ID))) {
            apiPath = Constants.GET_OWN_ARTICLE_DETAIL;
//                ibEdit.setVisibility(View.VISIBLE);
//                ibDelete.setVisibility(View.VISIBLE);
//            } else {
//                apiPath = Constants.GET_ARTICLE_DETAIL;
//                ibEdit.setVisibility(View.GONE);
//                ibDelete.setVisibility(View.GONE);
//            }

            Map<String, String> requestBody = new HashMap<>();
            requestBody.put(key, value);
            requestBody.put("insecure", Constants.INSECURE);

            NetworkManager.getInstance().post(ArticleDetailActivity.this, true, apiPath, requestBody, new NetworkManager.DataObserver() {
                @Override
                public void onSuccess(JSONObject response) {
                    CustomDialog.hideLoadDialog();
                    try {
                        Debug.trace("POST_DETAIL", "onSuccess: " + response.toString());
                        String status = response.getString("status");
                        if (status.toLowerCase().equals("ok")) {

                            JSONObject object = response.getJSONObject("post");

                            if (postModel != null) {
                                postModel = Utils.parseResponseInModel(object).setPosition(postModel.getPosition());
                            } else {
                                postModel = Utils.parseResponseInModel(object);
                            }

                            setDataToViews(postModel);
                            getComments(postModel, true);
                        } else {

                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(String error, int errorCode) {
                    CustomDialog.hideLoadDialog();
                    CFAlertDialog.Builder builder = new CFAlertDialog.Builder(ArticleDetailActivity.this)
                            .setDialogStyle(CFAlertDialog.CFAlertStyle.BOTTOM_SHEET)
                            .setMessage("Something went wrong.")
                            .addButton("Cancel", -1, -1, CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.END, (dialog, which) -> dialog.dismiss());
                    builder.show();
                }
            });
        }
    }


    void subscribeAndUnsubscribe(PostModel postModel) {

        Map<String, String> requestBody = new HashMap<>();
        requestBody.put("pid", postModel.getId());
        requestBody.put("uid", Utils.getPrefDataString(ArticleDetailActivity.this, Constants.USER_ID));

        if (postModel.getSubscriptionId() == Constants.SUBSCRIBE_ID) {
            requestBody.put("subcribe", String.valueOf(Constants.UNSUBSCRIBE_ID));
        } else {
            requestBody.put("subcribe", String.valueOf(Constants.SUBSCRIBE_ID));
        }

        requestBody.put("insecure", Constants.INSECURE);
        NetworkManager.getInstance().post(ArticleDetailActivity.this, true, Constants.USER_SUBSCRIBE, requestBody, new NetworkManager.DataObserver() {
            @Override
            public void onSuccess(JSONObject response) {

                if (response != null && !response.isNull("Responsecode") && response.optInt("Responsecode") == 200) {

                    if (postModel.getSubscriptionId() == Constants.SUBSCRIBE_ID) {
                        postModel.setSubscriptionId(Constants.UNSUBSCRIBE_ID);
                        postModel.setSubscriptionImage(R.drawable.ic_unsubscribe);
                    } else {
                        postModel.setSubscriptionId(Constants.SUBSCRIBE_ID);
                        postModel.setSubscriptionImage(R.drawable.ic_subscribe);
                    }

                    ivSubscribe.setImageResource(postModel.getSubscriptionImage());

                } else {
                    showSnackBar(ArticleDetailActivity.this.getString(R.string.ERROR_MSG), true, ArticleDetailActivity.this.getString(R.string.str_ok));
                }
            }

            @Override
            public void onFailure(String error, int errorCode) {
                showSnackBar(error, true, getString(R.string.str_ok));
            }
        });
    }

    private void setDataToViews(PostModel postModel) {

        rootView.setVisibility(View.VISIBLE);
        if (postModel.getAuthor_id().equalsIgnoreCase(Utils.getPrefDataString(this, Constants.USER_ID)) && !isFromNotification) {
            ibEdit.setVisibility(View.VISIBLE);
            ibDelete.setVisibility(View.VISIBLE);
        } else {
            ibEdit.setVisibility(View.GONE);
            ibDelete.setVisibility(View.GONE);
        }

        tvComments.setText(postModel.getFormattedCommentCount());
//        ivPhotoLibrary.setVisibility(postModel.getIsMultipleVisible());
        dotView.setVisibility(postModel.getIsMultipleVisible());
        rvMedia.setLayoutManager(new LinearLayoutManager(ArticleDetailActivity.this, RecyclerView.HORIZONTAL, false));
        rvMedia.setAdapter(new AdpPostMedia(postModel.getMediaFileModelList(), ArticleDetailActivity.this, ArticleDetailActivity.this));
        dotView.attachToRecyclerView(rvMedia);

        tvTitle.setText(Html.fromHtml(postModel.getTitle()));
        tvAuthor.setText(postModel.getAuthor_name());
        tvDate.setText(DateUtils.convertDateTimeFullMonthAPI(postModel.getDate()));
        tvDescription.setText(Html.fromHtml(postModel.getContent()));
        ivSubscribe.setVisibility(postModel.getSubscribeVisibility());
        ivSubscribe.setImageResource(postModel.getSubscriptionImage());
        rootView.setVisibility(View.VISIBLE);
    }

    private void setCommentCount() {
        long comment_count = this.postModel.getComment_count();
        String comment_str = comment_count == 1 ? comment_count + " comment" : SetViews.format(comment_count) + " comments";
        tvComments.setText(comment_str);
    }

    private void deletePost() {
        String cookie = Utils.getPrefDataString(this, Constants.USER_COOKIE);
        CustomDialog.showLoadDialog(this);
        NetworkManager.getInstance().getNonce("posts", "delete_post", cookie, new NetworkManager.DataObserver() {
            @Override
            public void onSuccess(JSONObject response) {
                try {
                    String status = response.getString("status");
                    if (status.toLowerCase().equals("ok")) {
                        String nonce = response.getString("nonce");
                        String useridREQ = "" + Utils.getPrefDataString(ArticleDetailActivity.this, Constants.USER_ID);
                        String cookieREQ = "" + Utils.getPrefDataString(ArticleDetailActivity.this, Constants.USER_COOKIE);
                        NetworkManager.getInstance().deletePost(postModel.getId(), useridREQ, cookieREQ, nonce, new NetworkManager.DataObserver() {
                            @Override
                            public void onSuccess(JSONObject response) {
                                CustomDialog.hideLoadDialog();
                                Debug.trace("Post", response.toString());
                                try {
                                    String status = response.getString("status");
                                    if (status.toLowerCase().equals("ok")) {

                                        setResult(Constants.RESULT_CODE_DELETE_POST, new Intent().putExtra(Constants.KEY_MODEL, postModel));
                                        finish();
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    CustomDialog.hideLoadDialog();
                                }
                            }

                            @Override
                            public void onFailure(String error, int errorCode) {
                                CustomDialog.hideLoadDialog();
                                Debug.trace("Post", error);
                                showSnackBar("Post Delete Failed, Please try later.", true, getString(R.string.str_ok));
                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    CustomDialog.hideLoadDialog();
                }
            }

            @Override
            public void onFailure(String error, int errorCode) {

                //Debug.trace("Register", error);
                CustomDialog.hideLoadDialog();
                showSnackBar("Post Delete Failed, Please try later.", true, getString(R.string.str_ok));
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
//                case Constants.REQUEST_CODE_PERSON_ARTICLE_DETAIL:
//                    if (data != null) {
//                        nsw_main.setVisibility(View.GONE);
//                        id = data.getStringExtra(KEY_ID);
//                        callArticleDetailApi();
//                    }
//                    break;

                case Constants.REQUEST_CODE_COMMENTS:
                    postModel = Objects.requireNonNull(data).getParcelableExtra(EditPostActivity.KEY_MODEL);
                    setCommentCount();
                    break;

                case Constants.REQUEST_CODE_UPDATE_POST:
//                    recreate();
                    rootView.setVisibility(View.GONE);
                    postModel = Objects.requireNonNull(data).getParcelableExtra(EditPostActivity.KEY_MODEL);
                    setDataToViews(postModel);
//                    callArticleDetailApi();
                    break;
            }
        }
    }
}
