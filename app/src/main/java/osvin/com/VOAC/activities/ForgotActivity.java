package osvin.com.VOAC.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import com.crowdfire.cfalertdialog.CFAlertDialog;
import org.json.JSONException;
import org.json.JSONObject;
import osvin.com.VOAC.R;
import osvin.com.VOAC.api.NetworkManager;
import osvin.com.VOAC.util.ValidationUtils;

public class ForgotActivity extends AppCompatActivity implements View.OnClickListener {

    EditText emailField;
    LinearLayout spinView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot);

        Button btnBack = findViewById(R.id.btn_back);
        btnBack.setOnClickListener(this);

        Button btnSend = findViewById(R.id.btn_send);
        btnSend.setOnClickListener(this);

        spinView = findViewById(R.id.spinView);
        emailField = findViewById(R.id.email_field);
        setSpinner(false);
    }

    private void setSpinner(boolean s) {
        if (s) {
            spinView.setVisibility(View.VISIBLE);
        } else {
            spinView.setVisibility(View.GONE);
        }
    }

    private void handleSend() {
        String email = emailField.getText().toString();
        if (email.isEmpty()) {
            CFAlertDialog.Builder builder = new CFAlertDialog.Builder(this)
                    .setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT)
                    .setMessage("Please enter email address.")
                    .addButton("Ok", -1, -1, CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.END, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            builder.show();
            return;
        }

        if (!ValidationUtils.isEmailValid(email)) {
            CFAlertDialog.Builder builder = new CFAlertDialog.Builder(this)
                    .setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT)
                    .setMessage("Please enter valid email address.")
                    .addButton("Ok", -1, -1, CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.END, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            builder.show();
            return;
        }

        setSpinner(true);
        NetworkManager.getInstance().resetPassword(email, new NetworkManager.DataObserver() {
            @Override
            public void onSuccess(JSONObject response) {
                setSpinner(false);
                try {
                    String status = response.getString("status");
                    if (status.toLowerCase().equals("ok")) {
                        CFAlertDialog.Builder builder = new CFAlertDialog.Builder(ForgotActivity.this)
                                .setDialogStyle(CFAlertDialog.CFAlertStyle.NOTIFICATION)
                                .setMessage("You request was sent.")
                                .addButton("OK", -1, -1, CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.JUSTIFIED, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        finish();
                                    }
                                });
                        builder.show();
                    } else {
                        CFAlertDialog.Builder builder = new CFAlertDialog.Builder(ForgotActivity.this)
                                .setDialogStyle(CFAlertDialog.CFAlertStyle.BOTTOM_SHEET)
                                .setMessage("Failed, Please try later.")
                                .addButton("Cancel", -1, -1, CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.END, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        builder.show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String error, int errorCode) {
                setSpinner(false);
                CFAlertDialog.Builder builder = new CFAlertDialog.Builder(ForgotActivity.this)
                        .setDialogStyle(CFAlertDialog.CFAlertStyle.BOTTOM_SHEET)
                        .setMessage("Failed, Please try later.")
                        .addButton("Cancel", -1, -1, CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.END, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                builder.show();
            }
        });
    }

    @Override
    public void onClick(View v) {

        int id = v.getId();
        if (id == R.id.btn_back) {
            finish();
        }

        if (id == R.id.btn_send) {
            handleSend();
        }
    }
}
