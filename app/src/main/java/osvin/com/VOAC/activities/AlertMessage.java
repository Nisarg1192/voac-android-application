package osvin.com.VOAC.activities;

import android.app.Activity;
import android.app.Dialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import osvin.com.VOAC.R;
import osvin.com.VOAC.util.DialogUtils;
import osvin.com.VOAC.util.Utils;

public class AlertMessage extends Activity implements View.OnClickListener {

    private Dialog dialogAlert;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alert_message);

        clearNotification();
        initialize();
    }

    /**
     * clear all notification
     */
    private void clearNotification() {
        NotificationManager nMgr = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (nMgr != null) {
            nMgr.cancelAll();
        }
    }

    /**
     * initialize all the views
     */
    private void initialize() {

        mContext = AlertMessage.this;

        dialogAlert = DialogUtils.oneButtonDialog(mContext, "Your session has expired! Please login again.",
                this);
        dialogAlert.setCancelable(false);
        Utils.Prefclear(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.tv_ok:
                dialogAlert.dismiss();
                Intent intent = new Intent(mContext, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);

                finishAffinity();
                break;

        }
    }

    @Override
    public void onBackPressed() {
    }
}
