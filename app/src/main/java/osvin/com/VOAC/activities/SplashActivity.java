package osvin.com.VOAC.activities;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.google.android.material.snackbar.Snackbar;

import butterknife.BindView;
import butterknife.ButterKnife;
import osvin.com.VOAC.R;
import osvin.com.VOAC.base.BaseActivity;
import osvin.com.VOAC.base.MyApplication;
import osvin.com.VOAC.util.Constants;
import osvin.com.VOAC.util.DialogUtils;
import osvin.com.VOAC.util.GetVersionCode;
import osvin.com.VOAC.util.Utils;

public class SplashActivity extends BaseActivity {

    @BindView(R.id.iv_splash_logo)
    AppCompatImageView ivSplashLogo;
    @BindView(R.id.tv_splash_text)
    TextView tvSplashText;
    @BindView(R.id.rootView)
    ConstraintLayout rootView;
    private Dialog dialog;
    private long splashHold = 1500;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
//        forceCrash();
        initViews();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        initViews();
    }

    private void initViews() {
        if (Utils.checkInternetConnection(SplashActivity.this)) {
            checkPlayStoreVersion();
        } else {

            Snackbar snackBar = Snackbar.make(findViewById(android.R.id.content), getString(R.string.str_no_internet_connection), Snackbar.LENGTH_INDEFINITE);

            snackBar.setAction(getString(R.string.str_retry), v -> {
                // Call your action method here
                if (Utils.checkInternetConnection(SplashActivity.this)) {
                    checkPlayStoreVersion();
                } else {
                    initViews();
                }
            });
            snackBar.show();
        }
    }

    private void scheduleSplashScreen() {
        long splashScreenDuration = getSplashScreenDuration();
        Handler handler = new Handler();
        handler.postDelayed(() -> {

            Intent intent;
            if (Utils.getPrefDataBoolean(SplashActivity.this, Constants.USER_LOGIN_STATUS)) {

                if (getIntent().getDataString() != null && getIntent().getDataString().contains(Constants.BASE_URL)) {
                    String[] titleArray = getIntent().getDataString().split("/");
                    if (titleArray.length >= 2) {
                        intent = new Intent(SplashActivity.this, ArticleDetailActivity.class);
                        intent.putExtra(Constants.FROM_DEEP_LINK, true);
                        intent.putExtra(Constants.POST_ID, titleArray[titleArray.length - 1]);
                    } else {
                        intent = new Intent(SplashActivity.this, HomeActivity.class);
                    }

                } else {
                    intent = new Intent(SplashActivity.this, HomeActivity.class);
                }

            } else {
                intent = new Intent(SplashActivity.this, LoginActivity.class);
            }
            startActivity(intent);
            finish();
        }, splashScreenDuration);


    }

    public void forceCrash() {
        throw new RuntimeException("This is a test crash .");
    }


    private long getSplashScreenDuration() {

        return splashHold;
    }

    private void checkPlayStoreVersion() {

        new GetVersionCode(this, new GetVersionCode.onAppVersionCodeCheckListener() {
            @Override
            public void onGetVersion(String onlineVersion) {

                String currentVersion;
                try {
                    currentVersion = MyApplication.getInstance().getPackageManager().getPackageInfo(MyApplication.getInstance().getPackageName(), 0).versionName;

                    if (onlineVersion != null && !onlineVersion.isEmpty()) {

                        if (!currentVersion.equalsIgnoreCase(onlineVersion)) {

                            showAppUpdateVersionDialog();
                        } else {
                            /*flow To NextScreen*/
                            scheduleSplashScreen();
                        }
                    } else {
                        /*flow To NextScreen*/
                        scheduleSplashScreen();
                    }

                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();

                    /*flow To NextScreen*/
                    scheduleSplashScreen();
                }
            }

            @Override
            public void onException(String errorMsg) {

                /*flow To NextScreen*/
                scheduleSplashScreen();
            }
        }).execute();
    }

    private void showAppUpdateVersionDialog() {

        dialog = DialogUtils.oneButtonDialogNonCancelable(SplashActivity.this, getString(R.string.str_app_update), v -> {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getPackageName()));
            startActivity(intent);
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            if (dialog != null && dialog.isShowing()) {
                dialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
