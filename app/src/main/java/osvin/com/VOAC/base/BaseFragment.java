package osvin.com.VOAC.base;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.google.android.material.snackbar.Snackbar;

import osvin.com.VOAC.R;
import osvin.com.VOAC.util.Constants;

public class BaseFragment extends Fragment {

    /*
     * Declare Common variables that will use in all child classes.
     * */
    public View view;
    public Activity mContext;
    public boolean settings = false;
    protected Snackbar snackbar;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeVariable();
    }

    private void initializeVariable() {
        if (mContext == null)
            mContext = getActivity();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        setRetainInstance(true);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    /**
     * Alert call_permission_dialog to show message, when user deny the permissions of camera and gallery.
     *
     * @param type                 enter type to show different messages for permissions
     * @param isShowNegativeButton
     */
    public void alertDialog(final byte type, boolean isShowNegativeButton) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
        String message = "";
        if (type == Constants.CHECK_CAMERA_PERMISSION)
            message = getString(R.string.camera_permission);
        else if (type == Constants.CHECK_GALLERY_PERMISSION)
            message = getString(R.string.gallery_permission);
        else if (type == Constants.CHECK_MICROPHONE_PERMISSION)
            message = getString(R.string.microphone_permission);
        else if (type == Constants.CHECK_LOCATION_PERMISSION)
            message = getString(R.string.location_permission);
        alertDialogBuilder.setMessage(message)
                .setPositiveButton(getString(R.string.btn_continue), (DialogInterface dialog, int id) -> {
                    if (type == Constants.CHECK_CAMERA_PERMISSION) {
                        requestPermissionCamera();
                    } else if (type == Constants.CHECK_GALLERY_PERMISSION) {
                        requestPermissionGallery();
                    } else if (type == Constants.CHECK_MICROPHONE_PERMISSION) {
                        requestPermissionMicrophone();
                    } else if (type == Constants.CHECK_LOCATION_PERMISSION) {
                        requestPermissionLocation();
                    }
                });
        if (isShowNegativeButton) {
            alertDialogBuilder.setNegativeButton(getString(R.string.btn_not_now), (DialogInterface dialog, int id) -> dialog.dismiss());
        }
        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setOnShowListener((DialogInterface arg0) -> {
            alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.red_light));
            alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.red_light));
        });
        alertDialog.show();
    }

    /**
     * Method to show message in case of permissions deny and sendMessage user to settings to give permission.
     *
     * @param msg Can pass custom user friendly message according to permissions.
     */
    public void alertDialogDeny(String msg) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setMessage(msg)
                .setNegativeButton(getString(R.string.ok), (DialogInterface dialog, int id) -> {
                    settings = true;
                    startActivity(new Intent()
                            .setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                            .addCategory(Intent.CATEGORY_DEFAULT)
                            .setData(Uri.parse("package:" + mContext.getPackageName()))
                            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS));

                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    /**
     * Request permissions for camera before get data from local memory.
     */
    @TargetApi(Build.VERSION_CODES.M)
    public void requestPermissionCamera() {
        requestPermissions(new String[]{Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE}, Constants.REQUEST_CODE_CAMERA);
    }

    /**
     * Request permissions for camera before get data from local memory.
     */
    @TargetApi(Build.VERSION_CODES.M)
    public void requestPermissionCameraAndMediaFiles() {
        requestPermissions(new String[]{Manifest.permission.CAMERA,
                Manifest.permission.RECORD_AUDIO,
                Manifest.permission.WRITE_EXTERNAL_STORAGE}, Constants.REQUEST_CODE_CAMERA);
    }

    /**
     * Request permissions for gallery before get data from local memory.
     */
    @TargetApi(Build.VERSION_CODES.M)
    public void requestPermissionGallery() {
        requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, Constants.REQUEST_CODE_GALLERY);
    }

    /**
     * Request permissions for camera before get data from local memory.
     */
    @TargetApi(Build.VERSION_CODES.M)
    public void requestPermissionMicrophone() {
        requestPermissions(new String[]{Manifest.permission.RECORD_AUDIO,
                Manifest.permission.WRITE_EXTERNAL_STORAGE}, Constants.REQUEST_CODE_MICROPHONE);
    }

    /**
     * Check Permissions for camera permissions
     *
     * @return if permissions are given then true otherwise it will return false.
     */
    public boolean checkPermissionForCamera() {
        return (isPermissionGranted(Manifest.permission.CAMERA)
                && isPermissionGranted(Manifest.permission.WRITE_EXTERNAL_STORAGE));
    }

    /**
     * Check Permissions for camera permissions
     *
     * @return if permissions are given then true otherwise it will return false.
     */
    public boolean checkPermissionForCameraAndMediaPicker() {
        return (isPermissionGranted(Manifest.permission.CAMERA)
                && isPermissionGranted(Manifest.permission.RECORD_AUDIO)
                && isPermissionGranted(Manifest.permission.WRITE_EXTERNAL_STORAGE));
    }

    /**
     * Check Permissions for gallery permissions
     *
     * @return if permissions are given then true otherwise it will return false.
     */
    public boolean checkPermissionForGallery() {
        return (isPermissionGranted(Manifest.permission.WRITE_EXTERNAL_STORAGE));
    }

    /**
     * Check Permissions for microphone permissions
     *
     * @return if permissions are given then true otherwise it will return false.
     */
    public boolean checkPermissionForMicrophone() {
        return (isPermissionGranted(Manifest.permission.RECORD_AUDIO)
                && isPermissionGranted(Manifest.permission.WRITE_EXTERNAL_STORAGE));
    }

    @TargetApi(Build.VERSION_CODES.M)
    public void requestPermissionLocation() {
        requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION}, Constants.REQUEST_CODE_LOCATION);
    }

    boolean isPermissionGranted(String permission) {
        return (ContextCompat.checkSelfPermission(mContext, permission) == PackageManager.PERMISSION_GRANTED);
    }


    /**
     * Method to hide keyboard.
     */
    public void hideKeyboard() {
        InputMethodManager inputManager = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        View focusedView = mContext.getCurrentFocus();

        if (focusedView != null) {
            assert inputManager != null;
            inputManager.hideSoftInputFromWindow(focusedView.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    /**
     * Check internet connection.
     *
     * @return If internet is working fine then return true else return false.
     */
    public boolean checkInternetConnection() {
        ConnectivityManager conMgr = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        assert conMgr != null;
        return conMgr.getActiveNetworkInfo() != null && conMgr.getActiveNetworkInfo().isAvailable() && conMgr.getActiveNetworkInfo().isConnected();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }


    protected void showSnackBar(String massage, boolean isCancelVisible, String cancelText) {
        CFAlertDialog.Builder builder = new CFAlertDialog.Builder(getActivity())
                .setDialogStyle(CFAlertDialog.CFAlertStyle.BOTTOM_SHEET)
                .setMessage(massage);

        if (isCancelVisible) {
            builder.addButton(cancelText, -1, -1, CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.END, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
        }
        builder.show();
    }

    /**
     * Show message to user in bottom of screen
     *
     * @param parentView   rootview of current screen
     * @param message      to display message to user
     * @param btnText      action button text
     * @param isIndefinite to show snackbar infinite or not default will be non-infinite
     * @param mListener    click listener of action button
     */
    public void showSnackBar(View parentView, String message, String btnText, boolean isIndefinite, final OnSnackListener mListener) {
        snackbar = Snackbar.make(parentView, message, (isIndefinite) ? Snackbar.LENGTH_INDEFINITE : Snackbar.LENGTH_LONG)
                .setAction(btnText, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        snackbar.dismiss();
                        if (mListener != null) mListener.selectOk();
                    }
                });

        snackbar.setActionTextColor(getResources().getColor(R.color.colorAccent));
        snackbar.show();
    }

    /**
     * To remove snack bar from botton of screen
     */
    public void dismissSnackBar() {
        if (snackbar != null && snackbar.isShown()) {
            snackbar.dismiss();
        }
    }

    /**
     * Snackbar action button click listener
     */
    public interface OnSnackListener {
        void selectOk();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }
}