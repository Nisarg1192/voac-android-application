package osvin.com.VOAC.base;

import android.app.Activity;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;

import com.adcolony.sdk.AdColony;
import com.adcolony.sdk.AdColonyAppOptions;
import com.adcolony.sdk.AdColonyInterstitial;
import com.adcolony.sdk.AdColonyInterstitialListener;
import com.adcolony.sdk.AdColonyZone;
import com.bumptech.glide.Glide;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.core.CrashlyticsCore;
import com.google.firebase.iid.FirebaseInstanceId;

import co.paystack.android.PaystackSdk;
import io.fabric.sdk.android.Fabric;
import osvin.com.VOAC.BuildConfig;
import osvin.com.VOAC.activities.AlertMessage;
import osvin.com.VOAC.util.Constants;
import osvin.com.VOAC.util.Debug;
import osvin.com.VOAC.util.Utils;

public class MyApplication extends Application implements Application.ActivityLifecycleCallbacks {

    public Bitmap Photo;
    public static String TAG = "VOAC_APP";
    private static MyApplication mInstance;

    private IntentFilter userAccountFilter;
    private BroadcastReceiver userAccountReceiver;

    /**
     * If you want a static function you can use to check if your application is
     * foreground/background, you can use the following:
     * Replace the four variables above with these four
     */
    private int resumed;
    private int paused;
    private int started;
    private int stopped;
    private boolean isAppRunning;
    private Activity currentActivity;
    //    private InterstitialAd mInterstitialAd;
    /*Add colony variables*/
    private String ADCOLONY_APP_ID = "app94a898b532ba46f58b";
    private String[] ADCOLONY_ZONE_IDS = new String[]{"vze4e832f3d2c0477a8e", "vz9afbe95b4af2477ea6"};
    private String ADSLOG = "ADSLOG";
    private int minimumSeconds = 1000 * 60 * 2;

    @Override
    public void onCreate() {
        super.onCreate();

        init();
    }

    private void init() {

        mInstance = this;

        Fabric.with(this, new Crashlytics.Builder().core(new CrashlyticsCore.Builder().disabled(BuildConfig.DEBUG).build()).build());
//        Fabric.with(this, new Crashlytics());
        Crashlytics.setUserIdentifier("UserId :" + Utils.getPrefDataString(this, Constants.USER_ID));

        registerActivityLifecycleCallbacks(this);
        retrieveTokenAndStore();
        PaystackSdk.initialize(getApplicationContext());
        logoutBroadcast();
        // Construct optional app options object to be sent with configure
        AdColonyAppOptions appOptions = new AdColonyAppOptions()
                .setKeepScreenOn(true);
        AdColony.configure(this, appOptions, ADCOLONY_APP_ID, ADCOLONY_ZONE_IDS);
        resetDisconnectTimer();
        clearGlideCache();
    }


    private void clearGlideCache() {
        new Thread(() -> {
            try {
                Glide.get(MyApplication.this).clearDiskCache();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).start();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);

    }

    public static synchronized MyApplication getInstance() {
        return mInstance;
    }

    private void retrieveTokenAndStore() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnSuccessListener(instanceIdResult -> {

                    String token = instanceIdResult.getToken();
                    Debug.trace("FirebaseToken", token);
                    storeToken(token);
                })
                .addOnFailureListener(e -> Debug.trace(TAG, "getInstanceId failed" + e.getLocalizedMessage()));
    }

    private void storeToken(String token) {
        SharedPreferences preferences = getSharedPreferences(Constants.APP_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(Constants.DEVICE_TOKEN, token).apply();
    }

    /* broadcast related to logout and account suspend */
    public void logoutBroadcast() {

        userAccountFilter = new IntentFilter();
        userAccountFilter.addAction("VOTACLogout");

        userAccountReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getStringExtra("action");
                startActivity(new Intent(getApplicationContext(), AlertMessage.class)
                        .putExtra("action", action).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            }
        };

        getApplicationContext().registerReceiver(userAccountReceiver, userAccountFilter);
    }

    private Handler disconnectHandler = new Handler(msg -> true);

    private Runnable disconnectCallback = this::initAds;

    public void resetDisconnectTimer() {

        disconnectHandler.removeCallbacks(disconnectCallback);
        disconnectHandler.postDelayed(disconnectCallback, minimumSeconds);
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
        currentActivity = activity;
    }

    @Override
    public void onActivityStarted(Activity activity) {
        currentActivity = activity;
        ++started;
    }

    @Override
    public void onActivityResumed(Activity activity) {
        ++resumed;
        isAppRunning = true;
    }

    @Override
    public void onActivityPaused(Activity activity) {
        ++paused;
    }

    @Override
    public void onActivityStopped(Activity activity) {
        ++stopped;
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        isAppRunning = resumed != paused || resumed != started || resumed != stopped;
    }

    /**
     * Return if the app is in foreground or not
     *
     * @return true if app in foreground
     */
    public boolean isApplicationInForeground() {
        return resumed > paused;
    }

    private void initAds() {

        AdColony.requestInterstitial(ADCOLONY_ZONE_IDS[1], listener);

//        if (mInterstitialAd == null) {
////            AdColony.configure(this, ADCOLONY_APP_ID, ADCOLONY_ZONE_IDS);
//            MobileAds.initialize(this);
//            mInterstitialAd = new InterstitialAd(this);
//            mInterstitialAd.setAdUnitId(Constants.ADS_UNIT_ID);
//            mInterstitialAd.loadAd(getAdsRequest());
//            mInterstitialAd.setAdListener(adListener);
//
//        } else {
//            mInterstitialAd.loadAd(getAdsRequest());
//            mInterstitialAd.setAdListener(adListener);
//        }
    }

    AdColonyInterstitialListener listener = new AdColonyInterstitialListener() {

        @Override
        public void onRequestNotFilled(AdColonyZone zone) {
            super.onRequestNotFilled(zone);
            Debug.trace(ADSLOG, "AdLoadFailed : " + zone.isValid());
            resetDisconnectTimer();
        }

        @Override
        public void onOpened(AdColonyInterstitial ad) {
            super.onOpened(ad);
            Debug.trace(ADSLOG, "AdOpened");
        }

        @Override
        public void onClosed(AdColonyInterstitial ad) {
            super.onClosed(ad);
            Debug.trace(ADSLOG, "AdClosed");
            // Code to be executed when the interstitial ad is closed.
            resetDisconnectTimer();
        }

        @Override
        public void onIAPEvent(AdColonyInterstitial ad, String product_id, int engagement_type) {
            super.onIAPEvent(ad, product_id, engagement_type);
            Debug.trace(ADSLOG, "AdIAPEvent" + ad.toString());
        }

        @Override
        public void onExpiring(AdColonyInterstitial ad) {
            super.onExpiring(ad);
            Debug.trace(ADSLOG, "AdExpired" + ad.toString());
            resetDisconnectTimer();
        }

        @Override
        public void onLeftApplication(AdColonyInterstitial ad) {
            super.onLeftApplication(ad);
            Debug.trace(ADSLOG, "AdLeftApp");
        }

        @Override
        public void onClicked(AdColonyInterstitial ad) {
            super.onClicked(ad);
            Debug.trace(ADSLOG, "AdClick");
        }

        @Override
        public void onRequestFilled(AdColonyInterstitial ad) {
            /* Store and use this ad object to show your ad when appropriate */
            Debug.trace(ADSLOG, "AdLoad");
            ad.show();
        }
    };

//    AdListener adListener = new AdListener() {
//        @Override
//        public void onAdLoaded() {
//            Debug.trace(ADSLOG, "AdLoad");
//            // Code to be executed when an ad finishes loading.
//            if (isApplicationInForeground()) {
//                mInterstitialAd.show();
//            } else {
//                resetDisconnectTimer();
//            }
//        }
//
//        @Override
//        public void onAdFailedToLoad(int errorCode) {
//            Debug.trace(ADSLOG, "AdLoadFailed : " + errorCode);
//            // Code to be executed when an ad request fails.
//            resetDisconnectTimer();
//        }
//
//        @Override
//        public void onAdOpened() {
//            Debug.trace(ADSLOG, "AdOpened");
//            // Code to be executed when the ad is displayed.
//        }
//
//        @Override
//        public void onAdClicked() {
//            Debug.trace(ADSLOG, "AdClick");
//            // Code to be executed when the user clicks on an ad.
//        }
//
//        @Override
//        public void onAdLeftApplication() {
//            Debug.trace(ADSLOG, "AdLeftApp");
//            // Code to be executed when the user has left the app.
//        }
//
//        @Override
//        public void onAdClosed() {
//            Debug.trace(ADSLOG, "AdClosed");
//            // Code to be executed when the interstitial ad is closed.
//            resetDisconnectTimer();
//        }
//    };

//    public static String md5(final String s) {
//        try {
//            // Create MD5 Hash
//            MessageDigest digest = java.security.MessageDigest
//                    .getInstance("MD5");
//            digest.update(s.getBytes());
//            byte messageDigest[] = digest.digest();
//
//            // Create Hex String
//            StringBuffer hexString = new StringBuffer();
//            for (int i = 0; i < messageDigest.length; i++) {
//                String h = Integer.toHexString(0xFF & messageDigest[i]);
//                while (h.length() < 2)
//                    h = "0" + h;
//                hexString.append(h);
//            }
//            return hexString.toString();
//
//        } catch (NoSuchAlgorithmException e) {
//            Debug.trace(TAG, e.getLocalizedMessage());
//        }
//        return "";
//    }

//    public AdRequest getAdsRequest() {
//
//        AdRequest.Builder adRequestBuilder = new AdRequest.Builder();
//
//        String android_id = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
//        String deviceId = md5(android_id).toUpperCase();
//        Debug.trace("TestDeviceId", deviceId);
//        adRequestBuilder.addTestDevice("5EB54DB477D39A5954BEDF0343094000");
//
//        AdRequest adRequest = adRequestBuilder.build();
//        return adRequest;
//    }
}
