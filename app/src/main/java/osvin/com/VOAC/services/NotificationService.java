package osvin.com.VOAC.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import osvin.com.VOAC.R;
import osvin.com.VOAC.activities.ArticleDetailActivity;
import osvin.com.VOAC.activities.EventDetail;
import osvin.com.VOAC.activities.HomeActivity;
import osvin.com.VOAC.models.EventModel;
import osvin.com.VOAC.util.BitmapUtils;
import osvin.com.VOAC.util.Constants;
import osvin.com.VOAC.util.Debug;
import osvin.com.VOAC.util.Utils;

public class NotificationService extends FirebaseMessagingService {

    private Intent notificationIntent;

    @Override
    public void onMessageReceived(@NotNull RemoteMessage remoteMessage) {
        if (remoteMessage != null && Utils.getPrefDataBoolean(this, Constants.USER_LOGIN_STATUS))
            setNotification(remoteMessage);
        else
            Debug.trace("notification exception", "remoteMessage null");
    }

    @Override
    public void onNewToken(@NotNull String s) {
        super.onNewToken(s);
        storeToken(s);
    }

    private void storeToken(String token) {
        SharedPreferences preferences = getSharedPreferences(Constants.APP_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(Constants.DEVICE_TOKEN, token).apply();
    }

    private void sendNotification(String title, String messageBody, String picture) {
        Bitmap bitmap;
        if (picture != null && !picture.isEmpty())
            bitmap = BitmapUtils.getCircleBitmap(BitmapUtils.getBitmapFromURL(picture));
        else
            bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);

        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent intent = PendingIntent.getActivity(this, 0, notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        int pushIcon = R.drawable.ic_notification;
        int notificationId = 1;
        String channelId = getApplicationContext().getString(R.string.notification_channel_id);
        String channelName = getApplicationContext().getString(R.string.notification_channel_name);

        NotificationManager mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(channelId, channelName, importance);
            if (mNotificationManager != null) {
                mNotificationManager.createNotificationChannel(mChannel);
            }
        }

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, channelId);
        builder.setContentIntent(intent)
                .setContentTitle(title)
                .setContentText(messageBody)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody))
                .setSmallIcon(pushIcon)
                .setLargeIcon(bitmap)
                .setAutoCancel(true)
                .setTicker(messageBody)
                .setWhen(System.currentTimeMillis())
                .setOngoing(false);

        Notification notification = builder.build();
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notification.defaults |= Notification.DEFAULT_SOUND;
        notification.defaults |= Notification.DEFAULT_VIBRATE;

        if (mNotificationManager != null) {
            mNotificationManager.notify(notificationId, notification);
        }

    }

    public void setNotification(RemoteMessage remoteMessage) {

        String userId = Utils.getPrefDataString(getApplicationContext(), Constants.USER_ID);
        String appName = getString(R.string.app_name);
        EventModel eventDetail = null;
        Debug.trace("pushReceived", remoteMessage.getData().toString());

        try {

            String action = remoteMessage.getData().get("action");

            if (!userId.equalsIgnoreCase("")) {
                if (action != null && !action.equalsIgnoreCase("")) {
                    if (action.equalsIgnoreCase("Post published")) { // for post approved by admin
                        notificationIntent = new Intent(this, HomeActivity.class);
                        String bodymessage = remoteMessage.getData().get("bodymessage");
                        String title = remoteMessage.getData().get("title");
                        sendNotification(title, bodymessage, "");
                    } else if (action.equalsIgnoreCase("Session Expired")) { // login session expire
                        sendBroadcast(new Intent("VOTACLogout").putExtra("action", action));
                    } else if (action.contains("Event created")) { // for event
                        JSONObject main = new JSONObject(remoteMessage.getData().toString());
                        String post_id = main.getString("post_id");
                        JSONObject event = new JSONObject(main.getString("event_data"));
                        String title = event.getString("event_name");
                        String desc = event.getString("post_content");
                        String start_date = event.getString("event_start_date");
                        String end_date = event.getString("event_end_date");

                        JSONObject location = new JSONObject(main.getString("location_data"));
                        String address = location.getString("location_address");
                        String state = location.getString("location_state");

                        eventDetail = new EventModel(post_id, title, desc, start_date, end_date, address, state);

                        notificationIntent = new Intent(this, EventDetail.class)
                                .putExtra("eventDetail", eventDetail);
                        sendNotification(appName, "New event", "");
                    } else if (action.contains("Comment published")) {
//                        JSONObject notificationJson = new JSONObject(remoteMessage.getData().toString());
                        String post_id = remoteMessage.getData().get("post_id");
                        notificationIntent = new Intent(this, ArticleDetailActivity.class);
                        notificationIntent.putExtra(Constants.FROM_PUSH_NOTIFICATION, true);
                        notificationIntent.putExtra(Constants.POST_ID, post_id);
                        String bodymessage = remoteMessage.getData().get("bodymessage");
                        String title = remoteMessage.getData().get("title");
                        sendNotification(title, bodymessage, "");
                    }
                }
            }

        } catch (JSONException ae) {
            ae.printStackTrace();
        }
    }
}
