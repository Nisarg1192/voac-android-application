package osvin.com.VOAC.fragments.volunteer;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.List;

import osvin.com.VOAC.R;
import osvin.com.VOAC.api.NetworkManager;
import osvin.com.VOAC.util.Constants;
import osvin.com.VOAC.util.Utils;

public class VolunteerApi implements VolunteerApiInterface {

    //private Call call;

    @Override
    public void cancelRequest() {

       /* if (call != null)
            call.cancel();*/
    }

    @Override
    public void submit_volunteer_form(String name, String email, String phone_number, String address,
                                      String city, String state, String country, String lat, String lng,
                                      File volunteer_image, Context context, OnSubmitVolunteerFormApiResponse listener) {


        NetworkManager.getInstance().SubmitVolunteer(Utils.getPrefDataString(context, Constants.USER_ID), name,
                email, phone_number, address, city, state, country, lat, lng, volunteer_image, new NetworkManager.DataObserver() {
                    @Override
                    public void onSuccess(JSONObject response) {

                        try {
                            if (response.getString("status").equalsIgnoreCase("ok")) {

                                if (listener != null)
                                    listener.onSubmitVolunteerFormApiSuccess("VolunteerFragment added successfully.");

                            } else {
                                if (listener != null)
                                    listener.onSubmitVolunteerFormApiFailure(context.getString(R.string.NO_RESPONSE_MSG));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (listener != null)
                                listener.onSubmitVolunteerFormApiFailure(context.getString(R.string.ERROR_MSG));
                        }

                    }

                    @Override
                    public void onFailure(String error, int errorCode) {
                        //Debug.trace("api error", error);
                        if (listener != null)
                            listener.onSubmitVolunteerFormApiFailure(error);
                    }
                });

    }

    @Override
    public void getAddressInfoApi(String address, Context context, onGetAddressInfoApiListener listener) {

        LatLng latLng = getLocationFromAddress(context, address);
        if (latLng != null) {
            if (listener != null) {
                listener.onGetAddressInfoApiSuccess(latLng.latitude, latLng.longitude);
            }
        } else {
            if (listener != null)
                listener.onGetAddressInfoApiFailure(context.getString(R.string.NO_RESPONSE_MSG));
        }


        /*NetworkManager.getInstance().getAddressInfoApi(address,context.getString(R.string.GOOGLE_API_KEY),new NetworkManager.DataObserver() {
            @Override
            public void onSuccess(JSONObject response) {

                try {

                    if (response.getString("status").equalsIgnoreCase("Ok")) {

                        JSONArray result = response.getJSONArray("results");
                        JSONObject obj = result.getJSONObject(0);
                        JSONObject objgeometry = obj.getJSONObject("geometry");
                        JSONObject objLocation = objgeometry.getJSONObject("location");
                        double latitude = objLocation.getDouble("lat");
                        double longitude = objLocation.getDouble("lng");

                        if (listener != null)
                            listener.onGetAddressInfoApiSuccess(latitude, longitude);

                    } else {
                        if (listener != null)
                            listener.onGetAddressInfoApiFailure(context.getString(R.string.NO_RESPONSE_MSG));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    if (listener != null)
                        listener.onGetAddressInfoApiFailure(context.getString(R.string.ERROR_MSG));
                }

            }

            @Override
            public void onFailure(String error) {
                Debug.trace("api error", error);
                if (listener != null)
                    listener.onGetAddressInfoApiFailure(context.getString(R.string.NO_RESPONSE_MSG));
            }
        });*/
    }

    public LatLng getLocationFromAddress(Context context, String strAddress) {

        Geocoder coder = new Geocoder(context);
        List<Address> address;
        LatLng p1 = null;

        try {
            // May throw an IOException
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null) {
                return null;
            }
            if (address.size() <= 0) {
                return null;
            }

            Address location = address.get(0);
            p1 = new LatLng(location.getLatitude() * 1E6, location.getLongitude() * 1E6);

        } catch (IOException ex) {

            ex.printStackTrace();
        }

        return p1;
    }
}