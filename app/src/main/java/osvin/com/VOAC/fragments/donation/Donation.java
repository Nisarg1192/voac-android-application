package osvin.com.VOAC.fragments.donation;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import org.json.JSONException;

import java.util.Calendar;

import co.paystack.android.Paystack;
import co.paystack.android.PaystackSdk;
import co.paystack.android.Transaction;
import co.paystack.android.exceptions.ExpiredAccessCodeException;
import co.paystack.android.model.Card;
import co.paystack.android.model.Charge;
import osvin.com.VOAC.R;
import osvin.com.VOAC.base.BaseFragment;
import osvin.com.VOAC.models.TransactionModel;
import osvin.com.VOAC.util.Constants;
import osvin.com.VOAC.util.CustomDialog;
import osvin.com.VOAC.util.Utils;
import osvin.com.VOAC.util.ValidationUtils;


public class Donation extends BaseFragment implements View.OnClickListener,
        DonationApiInterface.VerifyTransactionApiResponse,
        DonationApiInterface.UpdateTransactionApiResponse {

    private TextView tv_name, tv_email_address;
    private Button btn_donate, btn_donation_history;
    private EditText et_amount, et_card_number, et_cvc, et_expiry_month, et_expiry_year;

    private DonationApiInterface donationApiInterface;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_donation, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        init(view);

        setDataOnView();

        implementListeners();
    }

    private void init(View view) {

        donationApiInterface = new DonationApi();


        et_cvc = view.findViewById(R.id.et_cvc);
        tv_name = view.findViewById(R.id.tv_name);
        et_amount = view.findViewById(R.id.et_amount);
        btn_donate = view.findViewById(R.id.btn_donate);
        et_card_number = view.findViewById(R.id.et_card_number);
        et_expiry_year = view.findViewById(R.id.et_expiry_year);
        et_expiry_month = view.findViewById(R.id.et_expiry_month);
        tv_email_address = view.findViewById(R.id.tv_email_address);
        btn_donation_history = view.findViewById(R.id.btn_donation_history);

        //initialize sdk
        PaystackSdk.setPublicKey("pk_live_d016671240fa1d336ebf090e92e5e3e800acba70");//Live
        // PaystackSdk.setPublicKey("pk_test_2bd923811027eb993336c006cda0e8c235f2823c");//Test
    }

    private void setDataOnView() {
        tv_name.setText(Utils.getPrefDataString(mContext, Constants.USER_NAME));
        tv_email_address.setText(Utils.getPrefDataString(mContext, Constants.USER_EMAIL));
    }

    private void implementListeners() {
        btn_donate.setOnClickListener(this);
        btn_donation_history.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.btn_donate:
                validateFields();
                break;

            case R.id.btn_donation_history:
                startActivity(new Intent(mContext, TransactionsHistory.class));
                break;
        }
    }

    private void validateFields() {

        if (!ValidationUtils.ValidateEditText(et_amount)) {
            showSnackBar("Please enter donation amount.", true, getString(R.string.str_ok));
        } else if (!ValidationUtils.ValidateEditText(et_card_number)) {
            showSnackBar("Please enter card number.", true, getString(R.string.str_ok));
        } else if (!ValidationUtils.ValidateEditText(et_expiry_month)) {
            showSnackBar("Please enter expiry month.", true, getString(R.string.str_ok));
        } else if (!ValidationUtils.ValidateEditText(et_expiry_year)) {
            showSnackBar("Please enter expiry year.", true, getString(R.string.str_ok));
        } else if (!ValidationUtils.ValidateEditText(et_cvc)) {
            showSnackBar("Please enter cvv", true, getString(R.string.str_ok));
        } else {
            if (Utils.checkInternetConnection(mContext)) {
                validateCard();
            } else {
                showSnackBar(getString(R.string.str_no_internet_connection), true, getString(R.string.str_ok));
            }
        }
    }

    private void validateCard() {

        Card card;

        String cardNum = et_card_number.getText().toString().trim();

        //build card object with ONLY the number, update the other fields later
        card = new Card.Builder(cardNum, 0, 0, "").build();
        String cvc = et_cvc.getText().toString().trim();

        //update the cvc field of the card
        card.setCvc(cvc);

        //validate expiry month;
        String sMonth = et_expiry_month.getText().toString().trim();
        int month = 0;
        try {
            month = Integer.parseInt(sMonth);
        } catch (Exception ignored) {
        }

        card.setExpiryMonth(month);

        String sYear = et_expiry_year.getText().toString().trim();
        int year = 0;
        try {
            year = Integer.parseInt(sYear);
        } catch (Exception ignored) {
        }

        card.setExpiryYear(year);

        if (card.isValid()) {
            CustomDialog.showLoadDialog(mContext);
            startAFreshCharge(card);

        } else if (!card.validNumber()) {
            showSnackBar("The card number that you entered is invalid", true, getString(R.string.str_ok));
        } else if (!card.validExpiryDate()) {
            showSnackBar("The expiration date that you entered is invalid", true, getString(R.string.str_ok));
        } else if (!card.validCVC()) {
            showSnackBar("The CVC code that you entered is invalid", true, getString(R.string.str_ok));
        } else {
            showSnackBar("The card details that you entered are invalid", true, getString(R.string.str_ok));
        }
    }

    private void startAFreshCharge(Card card) {
        String referance = "ChargedFromAndroid_" + Calendar.getInstance().getTimeInMillis();

        // initialize the charge
        Charge charge = new Charge();
        charge.setCard(card);
        // Set transaction params directly in app (note that these params
        // are only used if an access_code is not set. In debug mode,
        // setting them after setting an access code would throw an exception
        charge.setEmail(tv_email_address.getText().toString().trim());
        charge.setCurrency("NGN");
        charge.setAmount(Integer.parseInt(et_amount.getText().toString()) * 100);
        charge.setReference(referance);

        try {
            charge.putCustomField("Charged From", "Voice of african child android application.");
        } catch (JSONException e) {
            e.printStackTrace();
        }


        /*String backend_url = "https://api.paystack.co/transaction/initialize";

        NetworkManager.getInstance().InitTransaction(backend_url, referance, Integer.parseInt(et_amount.getText().toString()), tv_email_address.getText().toString().trim(), AUTHORIZATION, new NetworkManager.DataObserver() {
            @Override
            public void onSuccess(JSONObject response) {

                try {
                    boolean status = response.getBoolean("status");
                    if (status) {
                        JSONObject data = response.getJSONObject("data");

                        String access_code = data.getString("access_code");
                        charge.setAccessCode(access_code);
                        chargeCard(charge);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String error) {
                // Debug.trace("api error", error);
                CustomDialog.hideLoadDialog();
            }
        });*/
        chargeCard(charge);

    }


    private void chargeCard(Charge charge) {


        PaystackSdk.chargeCard(mContext, charge, new Paystack.TransactionCallback() {
            // This is called only after transaction is successful
            @Override
            public void onSuccess(Transaction transaction) {

                if (donationApiInterface != null)
                    donationApiInterface.verify_transaction(transaction.getReference(), mContext, Donation.this);
            }

            // This is called only before requesting OTP
            // Save reference so you may send to server if
            // error occurs with OTP
            // No need to dismiss dialog
            @Override
            public void beforeValidate(Transaction transaction) {
            }

            @Override
            public void onError(Throwable error, Transaction transaction) {
                // If an access code has expired, simply ask your server for a new one
                // and restart the charge instead of displaying error
                if (error instanceof ExpiredAccessCodeException) {
                    CustomDialog.hideLoadDialog();
                    Donation.this.validateCard();
                    Donation.this.chargeCard(charge);
                    return;
                }

                if (transaction.getReference() != null) {

                    if (donationApiInterface != null)
                        donationApiInterface.verify_transaction(transaction.getReference(), mContext, Donation.this);

                } else {
                    CustomDialog.hideLoadDialog();
                    showSnackBar("" + error.getMessage(), true, getString(R.string.str_ok));
                }
            }
        });
    }

    void resetView() {
        et_cvc.setText(Constants.DEFAULT_STRING);
        et_amount.setText(Constants.DEFAULT_STRING);
        et_card_number.setText(Constants.DEFAULT_STRING);
        et_expiry_year.setText(Constants.DEFAULT_STRING);
        et_expiry_month.setText(Constants.DEFAULT_STRING);
    }

    @Override
    public void onVerifyTransactionApiSuccess(TransactionModel transactionModel) {
        if (donationApiInterface != null)
            donationApiInterface.update_transaction(transactionModel, mContext, this);
    }

    @Override
    public void onVerifyTransactionApiFailure(String message) {
        CustomDialog.hideLoadDialog();
        if (isAdded()) {
            showSnackBar("" + message, true, getString(R.string.str_ok));
        }
    }

    @Override
    public void onUpdateTransactionApiSuccess(String message) {
        CustomDialog.hideLoadDialog();
        resetView();
        showSnackBar("" + getString(R.string.donation_thank_you), true, getString(R.string.str_ok));
    }

    @Override
    public void onUpdateTransactionApiFailure(String message) {
        CustomDialog.hideLoadDialog();
        if (isAdded()) {
            showSnackBar("" + message, true, getString(R.string.str_ok));
        }
    }
}