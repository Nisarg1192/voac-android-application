package osvin.com.VOAC.fragments.settings;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import osvin.com.VOAC.R;
import osvin.com.VOAC.activities.HomeActivity;
import osvin.com.VOAC.base.BaseFragment;
import osvin.com.VOAC.util.Constants;
import osvin.com.VOAC.util.CustomDialog;
import osvin.com.VOAC.util.Utils;

public class TermsFragment extends BaseFragment {

    static TermsFragment instance;
    //    FragNavController _mNavigator;
    private HomeActivity homeActivity;
    WebView webView;

    String LoadUrl = "https://docs.google.com/gview?embedded=true&url=" + Constants.pdfURL;

    public static TermsFragment newInstance() {
        if (instance == null) {
            instance = new TermsFragment();
        }
        return instance;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        HomeActivity activity = (HomeActivity) getActivity();
        assert activity != null;
//        _mNavigator = activity.getNavigator();
        homeActivity = (HomeActivity) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_terms, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        Button btnBack = view.findViewById(R.id.btn_back);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homeActivity.popBackFragment();
            }
        });
        webView = view.findViewById(R.id.web_view);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
        CustomDialog.showLoadDialog(getActivity());
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                CustomDialog.hideLoadDialog();
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
                CustomDialog.hideLoadDialog();
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                super.onReceivedError(view, errorCode, description, failingUrl);
                CustomDialog.hideLoadDialog();
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (LoadUrl.equals(url)) {
                    return true;
                } else {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    startActivity(Intent.createChooser(intent, getString(R.string.str_select_application)));
                }
                return true;
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                return super.shouldOverrideUrlLoading(view, request);
            }
        });

        if (Utils.checkInternetConnection(getActivity())) {
            CustomDialog.showLoadDialog(getActivity());
            webView.loadUrl(LoadUrl);
        } else {
            showSnackBar(getString(R.string.str_no_internet_connection), true, getString(R.string.str_ok));
        }

    }
}
