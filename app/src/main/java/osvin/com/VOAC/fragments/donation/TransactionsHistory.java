package osvin.com.VOAC.fragments.donation;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

import osvin.com.VOAC.R;
import osvin.com.VOAC.adapter.TransactionsAdapter;
import osvin.com.VOAC.base.BaseActivity;
import osvin.com.VOAC.interfaces.EndlessRecyclerViewScrollListener;
import osvin.com.VOAC.models.TransactionModel;
import osvin.com.VOAC.util.Constants;
import osvin.com.VOAC.util.CustomDialog;
import osvin.com.VOAC.util.SetViews;
import osvin.com.VOAC.util.Utils;

public class TransactionsHistory extends BaseActivity implements View.OnClickListener,
        SwipeRefreshLayout.OnRefreshListener,
        DonationApiInterface.GetAllTransactionApiResponse {

    Context mContext;

    private TextView no_data;
    private Button btn_back;
    private RecyclerView rv_transactions;
    private FloatingActionButton fab_scroll;
    private ConstraintLayout load_more_progress;
    private SwipeRefreshLayout swipe_refresh_layout;

    private int totalItemCount = 0;
    private TransactionsAdapter transactionsAdapter;
    private DonationApiInterface donationApiInterface;
    private EndlessRecyclerViewScrollListener scrollListener;
    private ArrayList<TransactionModel> transactions_list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transactions_history);
        mContext = this;
        initialize();

        setEmptyAdapter();

        implementListener();

        SetViews.setSwipeRefreshLayoutGreen(swipe_refresh_layout, mContext);

        checkInternet(Constants.LOADER_NORMAL, Constants.PAGE_STARTING_INDEX);

    }

    private void initialize() {
        donationApiInterface = new DonationApi();

        btn_back = findViewById(R.id.btn_back);
        no_data = findViewById(R.id.no_data);
        fab_scroll = findViewById(R.id.fab_scroll);
        rv_transactions = findViewById(R.id.rv_transactions);
        load_more_progress = findViewById(R.id.load_more_progress);
        swipe_refresh_layout = findViewById(R.id.swipe_refresh_layout);
    }


    private void implementListener() {
        setCustomScrollListener();
        btn_back.setOnClickListener(this);
        fab_scroll.setOnClickListener(this);
        swipe_refresh_layout.setOnRefreshListener(this);
        rv_transactions.addOnScrollListener(scrollListener);
    }

    /**
     * Set custom scroll listener in recycler view for load more functionality.
     */
    void setCustomScrollListener() {

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        rv_transactions.setLayoutManager(linearLayoutManager);

        // Retain an instance so that you can call `resetState()` for fresh searches
        scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {

                // Triggered only when new data needs to be appended to the list
                // Add whatever code is needed to append new items to the bottom of the list

                if (totalItemCount == 50) {
                    checkInternet(Constants.LOADER_LOAD_MORE, page);
                }
            }

            @Override
            public void showArrowButton(int count) {
                if (count >= 1)
                    fab_scroll.show();
                else
                    fab_scroll.hide();
            }
        };
    }

    void setEmptyAdapter() {
        transactionsAdapter = new TransactionsAdapter(mContext, transactions_list);
        rv_transactions.setAdapter(transactionsAdapter);
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.btn_back:
                onBackPressed();
                break;
            case R.id.fab_scroll:
                rv_transactions.smoothScrollToPosition(0);
                break;
        }
    }

    @Override
    public void onRefresh() {
        checkInternet(Constants.LOADER_SWIPE, Constants.PAGE_STARTING_INDEX);
    }

    private void checkInternet(byte loaderType, int page) {
        if (Utils.checkInternetConnection(mContext)) {

            if (donationApiInterface != null) {
                showLoader(loaderType);
                donationApiInterface.get_transaction_Api(page, loaderType, mContext, this);
            }

        } else {
            showSnackBar(getString(R.string.str_no_internet_connection), true, "" + getString(R.string.str_ok));
        }
    }

    /**
     * Set Visibility of views if list is empty or in any error case.
     *
     * @param textVisibility visibility of no data text view.
     * @param rvVisibility   visibility of no data recycler view.
     */
    public void setViewVisibility(int textVisibility, int rvVisibility) {
        no_data.setVisibility(textVisibility);
        rv_transactions.setVisibility(rvVisibility);
    }

    /**
     * Method to show loader according to loader type.
     *
     * @param loaderType 0 for normal loader.
     *                   1 for swipe to refresh loader.
     *                   2 for load more list bottom loader.
     */
    public void showLoader(byte loaderType) {
        if (loaderType == Constants.LOADER_NORMAL) {
            //loader.setVisibility(View.VISIBLE);
            CustomDialog.showLoadDialog(this);
        } else if (loaderType == Constants.LOADER_SWIPE)
            swipe_refresh_layout.setRefreshing(true);
        else if (loaderType == Constants.LOADER_LOAD_MORE)
            load_more_progress.setVisibility(View.VISIBLE);
    }

    /**
     * Method to hide loader according to loader type.
     *
     * @param loaderType 0 for normal loader.
     *                   1 for swipe to refresh loader.
     *                   2 for load more list bottom loader.
     */
    public void hideLoader(byte loaderType) {
        if (loaderType == Constants.LOADER_NORMAL) {
            //loader.setVisibility(View.GONE);
            CustomDialog.hideLoadDialog();
        } else if (loaderType == Constants.LOADER_SWIPE)
            swipe_refresh_layout.setRefreshing(false);
        else if (loaderType == Constants.LOADER_LOAD_MORE) {
            new Handler().postDelayed(() -> load_more_progress.setVisibility(View.GONE), 2000);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onGetAllTransactionApiSuccess(int totalItemCount, byte loaderType, ArrayList<TransactionModel> transactions_list) {

        boolean loaderCheck = (loaderType == Constants.LOADER_NORMAL || loaderType == Constants.LOADER_SWIPE);

        if (loaderCheck && this.transactions_list.size() > 0) {
            this.transactions_list.clear();
            totalItemCount = 0;
            scrollListener.resetState();
        }

        this.transactions_list.addAll(transactions_list);
        this.totalItemCount = totalItemCount;

        if (this.transactions_list.size() > 0) {
            setViewVisibility(View.GONE, View.VISIBLE);
            transactionsAdapter.notifyDataSetChanged();

        } else {
            setViewVisibility(View.VISIBLE, View.GONE);
        }

        hideLoader(loaderType);
    }

    @Override
    public void onGetAllTransactionApiFailure(String message) {
        hideLoader(Constants.LOADER_NORMAL);
        hideLoader(Constants.LOADER_LOAD_MORE);
        hideLoader(Constants.LOADER_SWIPE);
        showSnackBar(message, true, "" + getString(R.string.str_ok));
    }
}