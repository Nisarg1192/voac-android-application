package osvin.com.VOAC.fragments.home;

import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.tabs.TabLayout;

import osvin.com.VOAC.R;
import osvin.com.VOAC.activities.HomeActivity;
import osvin.com.VOAC.base.BaseFragment;
import osvin.com.VOAC.fragments.donation.Donation;
import osvin.com.VOAC.fragments.volunteer.VolunteerFragment;
import osvin.com.VOAC.util.Debug;

public class HomeFragment extends BaseFragment implements View.OnClickListener {

    private TabLayout tab_layout;
    private ImageView ivHome;

    private int TAB_ARTICLES = 0;
    private int TAB_VOLUNTEER = 1;
    private int TAB_DONATE = 2;
    private int TAB_PROJECT = 3;

    private int selectedTab;
    private static HomeFragment instance;
    private HomeActivity homeActivity;

    private FragmentTransaction fragmentTransaction;
    private FragmentManager fragmentManager;
    public Fragment currentFragment;

    public static HomeFragment newInstance() {
        if (instance == null) {
            instance = new HomeFragment();
        }
        return instance;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Debug.trace("TAG_HomeFragment", "onCreate");
        homeActivity = (HomeActivity) getActivity();
        fragmentManager = getChildFragmentManager();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Debug.trace("TAG_HomeFragment", "onCreateView");
//        if (getView() == null) {
//
//            return view;
//        } else {
//            return getView();
//        }
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        initViews(view);
        return view;
    }

    private void initViews(View view) {

        ivHome = view.findViewById(R.id.ivHome);

        tab_layout = view.findViewById(R.id.tab_layout);

        View articleTabView = getLayoutInflater().inflate(R.layout.custom_tab, null);
        AppCompatImageView articleImageView = articleTabView.findViewById(R.id.icon);
        int tabIconColor = ContextCompat.getColor(homeActivity, R.color.colorAccent);
        articleImageView.setImageResource(R.drawable.ic_article_black);
        articleImageView.setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
        TabLayout.Tab tabArticles = tab_layout.newTab();
        tabArticles.setCustomView(articleTabView);
        tab_layout.addTab(tabArticles);


        View volunteerTabView = getLayoutInflater().inflate(R.layout.custom_tab, null);
        AppCompatImageView volunteerImageView = volunteerTabView.findViewById(R.id.icon);
        volunteerImageView.setImageResource(R.drawable.ic_volunteer_black);
        TabLayout.Tab tabVolunteer = tab_layout.newTab();
        tabVolunteer.setCustomView(volunteerTabView);
        tab_layout.addTab(tabVolunteer);


        View donateTabView = getLayoutInflater().inflate(R.layout.custom_tab, null);
        AppCompatImageView donateImageView = donateTabView.findViewById(R.id.icon);
        donateImageView.setImageResource(R.drawable.ic_donate_black);
        TabLayout.Tab tabDonation = tab_layout.newTab();
        tabDonation.setCustomView(donateTabView);
        tab_layout.addTab(tabDonation);


        View projectTabView = getLayoutInflater().inflate(R.layout.custom_tab, null);
        AppCompatImageView projectImageView = projectTabView.findViewById(R.id.icon);
        projectImageView.setImageResource(R.drawable.ic_project);
        TabLayout.Tab tabProject = tab_layout.newTab();
        tabProject.setCustomView(projectTabView);
        tab_layout.addTab(tabProject);

        initListener();

        selectedTab = TAB_ARTICLES;
        selectTab(selectedTab);
    }


    public void initListener() {
        ivHome.setOnClickListener(this);

        tab_layout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                int tabIconColor = ContextCompat.getColor(homeActivity, R.color.colorAccent);
                ((AppCompatImageView) tab.getCustomView().findViewById(R.id.icon)).setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);

                selectedTab = tab.getPosition();
                selectTab(selectedTab);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                int tabIconColor = ContextCompat.getColor(homeActivity, R.color.colorPrimaryDark);
                ((AppCompatImageView) tab.getCustomView().findViewById(R.id.icon)).setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivHome:
                homeActivity.pushFragment(Home.newInstance(), true, false, null);
                break;
        }
    }

    private void selectTab(int tabPosition) {
        Fragment fragment = null;
        boolean addToBackStack = true;
        if (tabPosition == TAB_ARTICLES) {
            fragment = new ArticleFragment();
            addToBackStack = true;
        } else if (tabPosition == TAB_VOLUNTEER) {
            fragment = new VolunteerFragment();
            addToBackStack = false;
        } else if (tabPosition == TAB_DONATE) {
            fragment = new Donation();
            addToBackStack = false;
        } else if (tabPosition == TAB_PROJECT) {

            fragment = new ProjectFragment();
            addToBackStack = false;
        }

        pushFragment(fragment, addToBackStack, false, null);
    }


    @Override
    public void onResume() {
        super.onResume();
        Debug.trace("TAG_HomeFragment", "onResume");
    }

    /**
     * Push the fragment in the view provided in activity's xml layout
     * It uses FragmentManager to add fragment in current view
     *
     * @param fragment       object of fragment to be added in view
     * @param addToBackStack to define if fragment wants to keep in stack of fragments
     * @param shouldAnimate  to determine if the fragment add with animation or not
     * @param bundle         extras values or data pass to fragment from current view
     */
    public void pushFragment(Fragment fragment, boolean addToBackStack, boolean shouldAnimate, Bundle bundle) {

        try {


            Fragment fragmentFromBackstack = fragmentManager.findFragmentByTag(fragment.getClass().getCanonicalName());

            fragmentTransaction = fragmentManager.beginTransaction();

            if (fragmentFromBackstack == null) {

                if (bundle != null) {
                    fragment.setArguments(bundle);
                }

                if (addToBackStack) {
                    if (currentFragment != null) {
                        Fragment currentFragmentInBackStack = fragmentManager.findFragmentByTag(currentFragment.getClass().getCanonicalName());
                        if (currentFragmentInBackStack == null) {
                            fragmentTransaction.remove(currentFragment);
                        }
                    }
//                fragmentTransaction.addToBackStack(fragment.getClass().getCanonicalName());
                    fragmentTransaction.add(R.id.child_fragment_container, fragment, fragment.getClass().getCanonicalName());
                } else {
                    if (fragment.isAdded()) {
                        fragmentTransaction.remove(fragment);
                        // Commit the transaction
                        fragmentTransaction.commitAllowingStateLoss();
                        fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.add(R.id.child_fragment_container, fragment);
                    } else {
                        if (currentFragment != null) {
                            Fragment currentFragmentInBackStack = fragmentManager.findFragmentByTag(currentFragment.getClass().getCanonicalName());
                            if (currentFragmentInBackStack == null) {
                                fragmentTransaction.remove(currentFragment);
                            }
                        }
                        fragmentTransaction.add(R.id.child_fragment_container, fragment);
                    }
                }


                if (currentFragment != null) {
                    fragmentTransaction.hide(currentFragment);
                }

                currentFragment = fragment;

                fragmentTransaction.show(fragment);

                // Commit the transaction
                fragmentTransaction.commitAllowingStateLoss();

            } else {

                if (currentFragment != null) {
                    Fragment currentFragmentInBackStack = fragmentManager.findFragmentByTag(currentFragment.getClass().getCanonicalName());
                    if (currentFragmentInBackStack == null) {
                        fragmentTransaction.remove(currentFragment);
                    } else {
                        fragmentTransaction.hide(currentFragment);
                    }
                }

                currentFragment = fragmentFromBackstack;
                if (bundle != null) {
                    currentFragment.setArguments(bundle);
                }
                fragmentTransaction.show(currentFragment);

                // Commit the transaction
                fragmentTransaction.commitAllowingStateLoss();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
