package osvin.com.VOAC.fragments.settings;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.github.ybq.android.spinkit.SpinKitView;

import org.json.JSONObject;

import osvin.com.VOAC.R;
import osvin.com.VOAC.activities.HomeActivity;
import osvin.com.VOAC.api.NetworkManager;

public class ContactFormFragment extends Fragment implements View.OnClickListener {

    static ContactFormFragment instance;
//    FragNavController _mNavigator;

    Button btnBack, btnSend;
    EditText emailField, subjectField, nameField, messageField;
    SpinKitView spinner;
    private HomeActivity homeActivity;

    public static ContactFormFragment newInstance() {
        if (instance == null) {
            instance = new ContactFormFragment();
        }
        return instance;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        HomeActivity activity = (HomeActivity) getActivity();
//        _mNavigator = activity.getNavigator();
        homeActivity = (HomeActivity) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_contact_form, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        btnBack = view.findViewById(R.id.btn_back);
        btnBack.setOnClickListener(this);
        btnSend = view.findViewById(R.id.btn_send);
        btnSend.setOnClickListener(this);

        emailField = view.findViewById(R.id.email);
        nameField = view.findViewById(R.id.name);
        subjectField = view.findViewById(R.id.subject);
        messageField = view.findViewById(R.id.message);

        spinner = view.findViewById(R.id.spinner);
        spinner.setVisibility(View.GONE);
    }

    private void submit() {

        String email = emailField.getText().toString();
        String name = nameField.getText().toString();
        String subject = subjectField.getText().toString();
        String message = messageField.getText().toString();

        if (email.isEmpty() || name.isEmpty() || subject.isEmpty() || message.isEmpty()) {
            openWarningDialog();
        } else {
            spinner.setVisibility(View.VISIBLE);
            NetworkManager.getInstance().submitContact(name, email, message, subject, new NetworkManager.DataObserver() {
                @Override
                public void onSuccess(JSONObject response) {
                    spinner.setVisibility(View.GONE);
                    homeActivity.popBackFragment();
                }

                @Override
                public void onFailure(String error, int errorCode) {
                    spinner.setVisibility(View.GONE);
                    CFAlertDialog.Builder builder = new CFAlertDialog.Builder(getActivity())
                            .setDialogStyle(CFAlertDialog.CFAlertStyle.BOTTOM_SHEET)
                            .setMessage("Submission Failed, Please try later.")
                            .addButton("Cancel", -1, -1, CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.END, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    builder.show();
                }
            });
        }
    }

    private void openWarningDialog() {

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.btn_back) {
            homeActivity.popBackFragment();
        }

        if (id == R.id.btn_send) {
            submit();
        }
    }
}
