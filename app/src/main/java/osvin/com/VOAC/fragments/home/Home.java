package osvin.com.VOAC.fragments.home;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import osvin.com.VOAC.R;
import osvin.com.VOAC.activities.HomeActivity;
import osvin.com.VOAC.adapter.AreaOfInterestAdapter;
import osvin.com.VOAC.adapter.HomeSliderAdapter;
import osvin.com.VOAC.api.NetworkManager;
import osvin.com.VOAC.base.BaseFragment;
import osvin.com.VOAC.customviews.GridSpacingItemDecoration;
import osvin.com.VOAC.models.AreaOfInterestModel;
import osvin.com.VOAC.models.HomeModel;
import osvin.com.VOAC.util.CustomDialog;

public class Home extends BaseFragment {

    static Home instance;
//    FragNavController _mNavigator;

    private Button btn_back;
    private NestedScrollView nsw_main;
    private ViewPager header_viewpager;
    private LinearLayout ll_indicators;
    private RecyclerView rv_area_of_interest;
    private TextView tv_title, tv_main_description, tv_title_two, tv_bottom_description, tv_footer_text;

    private Handler handler;
    private HomeModel homeModel;
    private int totalItems = 0, currentPosition = 0;
    private HomeActivity homeActivity;

    public static Home newInstance() {
        if (instance == null) {
            instance = new Home();
        }
        return instance;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        HomeActivity activity = (HomeActivity) getActivity();
        assert activity != null;
//        _mNavigator = activity.getNavigator();
        homeActivity = (HomeActivity) getActivity();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home_, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        init(view);
        setEmptyRecyclerView();

        checkInternet();
    }

    private void init(View view) {
        homeModel = new HomeModel();
        nsw_main = view.findViewById(R.id.nsw_main);
        btn_back = view.findViewById(R.id.btn_back);
        tv_title = view.findViewById(R.id.tv_title);
        tv_title_two = view.findViewById(R.id.tv_title_two);
        ll_indicators = view.findViewById(R.id.ll_indicators);
        tv_footer_text = view.findViewById(R.id.tv_footer_text);
        header_viewpager = view.findViewById(R.id.header_viewpager);
        rv_area_of_interest = view.findViewById(R.id.rv_area_of_interest);
        tv_main_description = view.findViewById(R.id.tv_main_description);
        tv_bottom_description = view.findViewById(R.id.tv_bottom_description);

        header_viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                currentPosition = position;
                setIndicator(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        btn_back.setOnClickListener(v -> {
            homeActivity.popBackFragment();
        });
    }

    /**
     * set viewpager items selected bullet
     */
    private void setIndicator(int index) {
        if (index < totalItems) {
            for (int i = 0; i < totalItems; i++) {
                ImageView circle = (ImageView) ll_indicators.getChildAt(i);
                circle.setImageResource(i == index ? R.drawable.pink_indicator : R.drawable.white_indicator);
            }
        }
    }

    /**
     * set viewpager items count circle
     */
    private void buildCircles() {
        ll_indicators.removeAllViews();
        for (int i = 0; i < totalItems; i++) {
            ImageView circle = new ImageView(mContext);
            circle.setImageResource(R.drawable.white_indicator);
            int size = (int) (getResources().getDimensionPixelSize(R.dimen._10sdp) / getResources().getDisplayMetrics().density);
            circle.setLayoutParams(new ViewGroup.LayoutParams(size, size));
            circle.setAdjustViewBounds(true);
            circle.setPadding(7, 0, 7, 0);
            ll_indicators.addView(circle);
        }

        setIndicator(currentPosition);
    }

    private void setEmptyRecyclerView() {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(mContext, 3);
        rv_area_of_interest.setLayoutManager(gridLayoutManager);
        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen._3sdp);
        rv_area_of_interest.addItemDecoration(new GridSpacingItemDecoration(3, spacingInPixels, true, 0));
        rv_area_of_interest.setNestedScrollingEnabled(false);
    }

    private void checkInternet() {
        if (checkInternetConnection()) {

            CustomDialog.showLoadDialog(getActivity());

            getDataFromServer();

        } else {

            showSnackBar(getString(R.string.str_no_internet_connection), true, getString(R.string.str_ok));
        }
    }

    private void getDataFromServer() {

        NetworkManager.getInstance().getHome(new NetworkManager.DataObserver() {
            @Override
            public void onSuccess(JSONObject response) {
                ArrayList<String> pager_list = new ArrayList<>();
                ArrayList<AreaOfInterestModel> interest_list = new ArrayList<>();


                try {
                    if (response.getBoolean("Response")) {

                        JSONObject responseOBJ = response.getJSONObject("ResponseMsg");

                        homeModel.setMain_title(responseOBJ.getString("home_title"));
                        homeModel.setFooter_text(responseOBJ.getString("home_footer_content"));
                        homeModel.setMain_description(responseOBJ.getString("home_first_section"));
                        homeModel.setBottom_description(responseOBJ.getString("home_second_section"));

                        JSONArray silderArr = responseOBJ.getJSONArray("home_slider_images");
                        for (int i = 0; i < silderArr.length(); i++) {
                            pager_list.add(silderArr.getJSONObject(i).getString("home_slider_image"));
                        }
                        homeModel.setPager_list(pager_list);

                        homeModel.setSecond_title(responseOBJ.getString("home_interest_title"));

                        JSONArray interestArr = responseOBJ.getJSONArray("area_of_interest");
                        for (int i = 0; i < interestArr.length(); i++) {
                            JSONObject interestOBJ = interestArr.getJSONObject(i);
                            AreaOfInterestModel areaOfInterestModel = new AreaOfInterestModel();
                            areaOfInterestModel.setTitle(interestOBJ.getString("title"));
                            areaOfInterestModel.setImage(interestOBJ.getString("image"));
                            interest_list.add(areaOfInterestModel);
                        }

                        homeModel.setArea_of_interest_list(interest_list);

                        setDataOnView();

                        CustomDialog.hideLoadDialog();

                    } else {
                        CustomDialog.hideLoadDialog();
                        if (isAdded())
                            showSnackBar(response.getString("message"), true, getString(R.string.str_ok));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    CustomDialog.hideLoadDialog();
                }
            }

            @Override
            public void onFailure(String error, int errorCode) {
                CustomDialog.hideLoadDialog();
                if (isAdded())
                    showSnackBar(getString(R.string.NO_RESPONSE_MSG), true, getString(R.string.str_ok));
            }
        });

    }

    private void setDataOnView() {

        totalItems = homeModel.getPager_list().size();
        buildCircles();

        tv_title.setText(homeModel.getMain_title());
        tv_title_two.setText(homeModel.getSecond_title());
        tv_footer_text.setText(homeModel.getFooter_text());
        tv_main_description.setText(homeModel.getMain_description());
        tv_bottom_description.setText(homeModel.getBottom_description());

        PagerAdapter pagerAdapter = new HomeSliderAdapter(mContext, homeModel.getPager_list());
        header_viewpager.setAdapter(pagerAdapter);

        AreaOfInterestAdapter areaOfInterestAdapter = new AreaOfInterestAdapter(mContext, homeModel.getArea_of_interest_list());
        rv_area_of_interest.setAdapter(areaOfInterestAdapter);
        areaOfInterestAdapter.notifyDataSetChanged();


        handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (currentPosition == totalItems) {
                    currentPosition = 0;
                } else
                    header_viewpager.setCurrentItem(currentPosition++, true);

                handler.postDelayed(this, 3000);

            }
        }, 0);

        nsw_main.setVisibility(View.VISIBLE);
    }
}