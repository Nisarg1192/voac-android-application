package osvin.com.VOAC.fragments.settings;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import osvin.com.VOAC.R;
import osvin.com.VOAC.activities.HomeActivity;
import osvin.com.VOAC.api.NetworkManager;
import osvin.com.VOAC.base.BaseFragment;
import osvin.com.VOAC.util.CustomDialog;

public class AboutFragment extends BaseFragment {

    static AboutFragment instance;
    //    FragNavController _mNavigator;
    private HomeActivity homeActivity;
    ImageView titleImageView;
    TextView titleTextView, bottomContentTextView, causesTextView, valunteerTextView, childrenTextView, countryTextView;
    RecyclerView recyclerView;
    FrameLayout bottomLayout;

    ItemAdapter itemAdapter;

    public static AboutFragment newInstance() {
        if (instance == null) {
            instance = new AboutFragment();
        }
        return instance;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        HomeActivity activity = (HomeActivity) getActivity();
        assert activity != null;
//        _mNavigator = activity.getNavigator();
        homeActivity = (HomeActivity) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_about, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        Button btnBack = view.findViewById(R.id.btn_back);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homeActivity.popBackFragment();
            }
        });

        titleImageView = view.findViewById(R.id.title_image_view);
        titleTextView = view.findViewById(R.id.title_text_view);
        recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        itemAdapter = new ItemAdapter(new JSONArray());
        recyclerView.setAdapter(itemAdapter);

        bottomContentTextView = view.findViewById(R.id.bottom_content);
        bottomLayout = view.findViewById(R.id.bottom_layout);

        causesTextView = view.findViewById(R.id.causes);
        valunteerTextView = view.findViewById(R.id.valunteers);
        childrenTextView = view.findViewById(R.id.childrens);
        countryTextView = view.findViewById(R.id.countries);

        getAboutUsData();
    }


    private void getAboutUsData() {

        bottomLayout.setVisibility(View.GONE);
        CustomDialog.showLoadDialog(getActivity());
        NetworkManager.getInstance().getAboutData(new NetworkManager.DataObserver() {
            @Override
            public void onSuccess(JSONObject response) {

                bottomLayout.setVisibility(View.VISIBLE);

                try {
                    JSONObject responseObj = response.getJSONObject("ResponseMsg");
                    JSONArray titleImageArrayJSON = responseObj.getJSONArray("about_us_title_img");
                    if (titleImageArrayJSON.length() > 0) {
                        JSONObject object = titleImageArrayJSON.getJSONObject(0);
                        String titleImageUrl = object.getString("content");
                        Glide.with(getActivity()).load(titleImageUrl).into(titleImageView);
                    }
                    JSONArray titleTextArrayJSON = responseObj.getJSONArray("about_us_first_section");
                    if (titleTextArrayJSON.length() > 0) {
                        JSONObject object = titleTextArrayJSON.getJSONObject(0);
                        String titleText = object.getString("content");
                        titleTextView.setText(titleText);
                    }

                    JSONArray contentArrayJSON = responseObj.getJSONArray("about_us_content");
                    itemAdapter.setData(contentArrayJSON);

                    JSONArray titleLineArrayJSON = responseObj.getJSONArray("agency_title_line");
                    if (titleLineArrayJSON.length() > 0) {
                        bottomContentTextView.setText(titleLineArrayJSON.getJSONObject(0).getString("content"));
                    }

                    JSONArray bottomContentArrayJSON = responseObj.getJSONArray("about_us_img");
                    if (bottomContentArrayJSON.length() > 0) {
                        for (int k = 0; k < bottomContentArrayJSON.length(); k++) {
                            JSONObject object = bottomContentArrayJSON.getJSONObject(k);
                            if (object.getString("name").equals("Causes")) {
                                causesTextView.setText(object.getString("count") + " Causes");
                            } else if (object.getString("name").equals("Valunteer")) {
                                valunteerTextView.setText(object.getString("count") + " Valunteers");
                            } else if (object.getString("name").equals("Childrens")) {
                                childrenTextView.setText(object.getString("count") + " Childrens");
                            } else {
                                countryTextView.setText(object.getString("count") + " Countries");
                            }
                        }
                    }
                    CustomDialog.hideLoadDialog();
                } catch (JSONException e) {
                    e.printStackTrace();
                    CustomDialog.hideLoadDialog();
                }
            }

            @Override
            public void onFailure(String error, int errorCode) {
                // Debug.trace("api error", error);
                CustomDialog.hideLoadDialog();
                showSnackBar(error,true,getString(R.string.str_ok));
            }
        });
    }

    class ItemAdapter extends RecyclerView.Adapter<ItemViewHolder> {

        JSONArray itemsArray;

        ItemAdapter(JSONArray array) {
            this.itemsArray = array;
        }

        @NonNull
        @Override
        public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_about_us, parent, false);
            ItemViewHolder viewHolder = new ItemViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
            try {
                JSONObject object = itemsArray.getJSONObject(position);
                String title = object.getString("name");
                String content = object.getString("content");
                holder.contentView.setText(content);
                holder.titleView.setText(title);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return itemsArray.length();
        }

        public void setData(JSONArray array) {
            this.itemsArray = array;
            this.notifyDataSetChanged();
        }
    }


    class ItemViewHolder extends RecyclerView.ViewHolder {

        TextView titleView, contentView;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);

            titleView = itemView.findViewById(R.id.item_title);
            contentView = itemView.findViewById(R.id.item_content);

        }
    }
}
