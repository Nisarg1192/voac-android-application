package osvin.com.VOAC.fragments.donation;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import osvin.com.VOAC.R;
import osvin.com.VOAC.api.NetworkManager;
import osvin.com.VOAC.models.TransactionModel;
import osvin.com.VOAC.util.Constants;
import osvin.com.VOAC.util.Debug;
import osvin.com.VOAC.util.Utils;


public class DonationApi implements DonationApiInterface {


      // public static final String AUTHORIZATION = "Bearer sk_test_fe5b29cdc1a3b45f52a7872475a65e7362ed8484";
    public static final String AUTHORIZATION = "Bearer sk_live_ef3734d432c213e56ba99899b75eca4a973cb0da";


    @Override
    public void cancelRequest() {

    }

    @Override
    public void verify_transaction(String reference, Context context, VerifyTransactionApiResponse listener) {


        String backend_url = "https://api.paystack.co/transaction/verify/" + reference;

        NetworkManager.getInstance().VerifyTransaction(backend_url, AUTHORIZATION, new NetworkManager.DataObserver() {
            @Override
            public void onSuccess(JSONObject response) {

                try {

                    JSONObject data = response.getJSONObject("data");

                    String status = data.getString("status");

                    if (status.equalsIgnoreCase("success")) {

                        TransactionModel transactionModel = new TransactionModel();
                        transactionModel.setTransaction_id(data.getString("id"));
                        transactionModel.setAmount(data.getString("amount"));
                        transactionModel.setCurrency(data.getString("currency"));
                        transactionModel.setDate(data.getString("created_at"));

                        if (listener != null)
                            listener.onVerifyTransactionApiSuccess(transactionModel);

                    } else {
                        if (listener != null)
                            listener.onVerifyTransactionApiFailure(data.getString("gateway_response"));
                    }

                } catch (Exception e) {

                    e.printStackTrace();

                    if (listener != null)
                        listener.onVerifyTransactionApiFailure(context.getString(R.string.ERROR_MSG));
                }

            }

            @Override
            public void onFailure(String error, int errorCode) {
               // Debug.trace("api error", error);
                if (listener != null)
                    listener.onVerifyTransactionApiFailure(error);
            }
        });








      /*  String backend_url = "https://api.paystack.co/transaction/verify/" + reference;

        this.call = call;
        this.call = api.verify_transaction("Bearer sk_test_fe5b29cdc1a3b45f52a7872475a65e7362ed8484", backend_url);
        this.call.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {

                try {

                    if (response.code() == 200) {
                        JSONObject obj = new JSONObject(Objects.requireNonNull(response.body()).string());

                        JSONObject data = obj.getJSONObject("data");

                        String status = data.getString("status");

                        if (status.equalsIgnoreCase("success")) {

                            TransactionModel transactionModel = new TransactionModel();
                            transactionModel.setTransaction_id(data.getString("id"));
                            transactionModel.setAmount(data.getString("amount"));
                            transactionModel.setCurrency(data.getString("currency"));
                            transactionModel.setDate(data.getString("created_at"));

                            if (listener != null)
                                listener.onVerifyTransactionApiSuccess(transactionModel);

                        } else {
                            if (listener != null)
                                listener.onVerifyTransactionApiFailure(data.getString("gateway_response"));
                        }

                    } else {

                        if (listener != null)
                            listener.onVerifyTransactionApiFailure(context.getString(R.string.NO_RESPONSE_MSG));

                    }

                } catch (Exception e) {

                    e.printStackTrace();

                    if (listener != null)
                        listener.onVerifyTransactionApiFailure(context.getString(R.string.ERROR_MSG));
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {

                if (listener != null)
                    listener.onVerifyTransactionApiFailure(context.getString(R.string.NO_RESPONSE_MSG));
            }
        });*/

    }

    @Override
    public void update_transaction(TransactionModel transactionModel, Context context, UpdateTransactionApiResponse listener) {

        NetworkManager.getInstance().SaveTransaction(Utils.getPrefDataString(context, Constants.USER_ID), transactionModel.getTransaction_id(), AUTHORIZATION, new NetworkManager.DataObserver() {
            @Override
            public void onSuccess(JSONObject response) {

                try {

                    if (response.getBoolean("status")) {

                        if (listener != null)
                            listener.onUpdateTransactionApiSuccess(context.getString(R.string.donation_thank_you));

                    } else {

                        if (listener != null)
                            listener.onUpdateTransactionApiFailure(response.getString("message"));
                    }

                } catch (Exception e) {

                    e.printStackTrace();

                    if (listener != null)
                        listener.onUpdateTransactionApiFailure(context.getString(R.string.NO_RESPONSE_MSG));
                }

            }

            @Override
            public void onFailure(String error, int errorCode) {
                //Debug.trace("api error", error);
                if (listener != null)
                    listener.onUpdateTransactionApiFailure(error);
            }
        });

        /*this.call = call;
        this.call = api.save_transaction("Bearer sk_test_fe5b29cdc1a3b45f52a7872475a65e7362ed8484", transactionModel.getTransaction_id(), SharedPreference.getString(Constants.PREF_USER_ID, context));
        this.call.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {

                try {

                    if (response.code() == 200) {

                        JSONObject obj = new JSONObject(Objects.requireNonNull(response.body()).string());

                        if (obj.getBoolean("status")) {

                            if (listener != null)
                                listener.onUpdateTransactionApiSuccess(context.getString(R.string.donation_thank_you));

                        } else {

                            if (listener != null)
                                listener.onUpdateTransactionApiFailure(obj.getString("message"));
                        }

                    } else {

                        if (listener != null)
                            listener.onUpdateTransactionApiFailure(context.getString(R.string.NO_RESPONSE_MSG));

                    }

                } catch (Exception e) {

                    e.printStackTrace();

                    if (listener != null)
                        listener.onUpdateTransactionApiFailure(context.getString(R.string.ERROR_MSG));
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {

                if (listener != null)
                    listener.onUpdateTransactionApiFailure(context.getString(R.string.NO_RESPONSE_MSG));
            }
        });*/
    }

    @Override
    public void get_transaction_Api(int page, byte loaderType, Context context, GetAllTransactionApiResponse listener) {

        NetworkManager.getInstance().GetTransactionList(Utils.getPrefDataString(context, Constants.USER_ID), page, new NetworkManager.DataObserver() {
            @Override
            public void onSuccess(JSONObject json) {

                try {
                    Debug.trace("TRANSCATION", "onSuccess: " + json.toString());
                    if (json.getBoolean("status")) {

                        JSONArray responseArray = json.getJSONArray("data");

                        ArrayList<TransactionModel> transactions_list = new ArrayList<>();

                        for (int i = 0; i < responseArray.length(); i++) {

                            JSONObject object = responseArray.getJSONObject(i);
                            TransactionModel transactionModel = new TransactionModel();
                            transactionModel.setTransaction_id(object.getString("id"));
                            transactionModel.setAmount(object.getString("amount"));
                            transactionModel.setCurrency(object.getString("currency"));
                            transactionModel.setStatus(object.getString("status"));
                            transactionModel.setDate(object.getString("created_at"));
                            transactions_list.add(transactionModel);
                        }

                        int totalItemCount = responseArray.length();

                        if (listener != null)
                            listener.onGetAllTransactionApiSuccess(totalItemCount, loaderType, transactions_list);

                    } else {
                        if (listener != null)
                            listener.onGetAllTransactionApiFailure(json.getString("message"));
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    if (listener != null)
                        listener.onGetAllTransactionApiFailure(context.getString(R.string.ERROR_MSG));
                }

            }

            @Override
            public void onFailure(String error, int errorCode) {
                //Debug.trace("api error", error);
                if (listener != null)
                    listener.onGetAllTransactionApiFailure(error);
            }
        });


        /*this.call = call;
        this.call = api.get_my_txn_listing(SharedPreference.getString(Constants.PREF_USER_ID, context), page);
        this.call.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {

                try {

                    if (response.code() == 200) {

                        JSONObject json = new JSONObject(Objects.requireNonNull(response.body()).string());

                        if (json.getBoolean("status")) {

                            JSONArray responseArray = json.getJSONArray("data");

                            ArrayList<TransactionModel> transactions_list = new ArrayList<>();

                            for (int i = 0; i < responseArray.length(); i++) {

                                JSONObject object = responseArray.getJSONObject(i);
                                TransactionModel transactionModel = new TransactionModel();
                                transactionModel.setTransaction_id(object.getString("id"));
                                transactionModel.setAmount(object.getString("amount"));
                                transactionModel.setCurrency(object.getString("currency"));
                                transactionModel.setStatus(object.getString("status"));
                                transactionModel.setDate(object.getString("created_at"));
                                transactions_list.add(transactionModel);
                            }

                            int totalItemCount = responseArray.length();

                            if (listener != null)
                                listener.onGetAllTransactionApiSuccess(totalItemCount, loaderType, transactions_list);

                        } else {
                            if (listener != null)
                                listener.onGetAllTransactionApiFailure(json.getString("message"));
                        }
                    } else {

                        JSONObject json = new JSONObject(response.errorBody().string());

                        if (listener != null)
                            listener.onGetAllTransactionApiFailure((json.has("error"))
                                    ? json.getString("error")
                                    : context.getString(R.string.ERROR_MSG));

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    if (listener != null)
                        listener.onGetAllTransactionApiFailure(context.getString(R.string.ERROR_MSG));
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {

                if (listener != null)
                    listener.onGetAllTransactionApiFailure(context.getString(R.string.NO_RESPONSE_MSG));
            }
        });*/
    }
}
