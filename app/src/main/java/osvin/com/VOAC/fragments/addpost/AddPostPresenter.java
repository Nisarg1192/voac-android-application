package osvin.com.VOAC.fragments.addpost;

import android.content.Context;

import osvin.com.VOAC.models.AddPostModel;

public interface AddPostPresenter {

    void addPost(Context context, boolean isDialogRequired, AddPostModel addPostModel);

    void addTopic(Context context, boolean isDialogRequired, AddPostModel addPostModel);
}
