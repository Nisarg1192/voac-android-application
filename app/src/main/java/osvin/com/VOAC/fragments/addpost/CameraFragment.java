package osvin.com.VOAC.fragments.addpost;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.PointF;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.app.ActivityCompat;

import com.google.android.material.tabs.TabLayout;
import com.library.activity.FileDirsActivity;
import com.library.activity.FilePickerActivity;
import com.library.config.Configurations;
import com.library.model.MediaFile;
import com.otaliastudios.cameraview.CameraException;
import com.otaliastudios.cameraview.CameraListener;
import com.otaliastudios.cameraview.CameraOptions;
import com.otaliastudios.cameraview.CameraView;
import com.otaliastudios.cameraview.PictureResult;
import com.otaliastudios.cameraview.VideoResult;
import com.otaliastudios.cameraview.controls.Facing;
import com.otaliastudios.cameraview.controls.Mode;
import com.otaliastudios.cameraview.size.AspectRatio;
import com.otaliastudios.cameraview.size.SizeSelector;
import com.otaliastudios.cameraview.size.SizeSelectors;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import osvin.com.VOAC.R;
import osvin.com.VOAC.activities.HomeActivity;
import osvin.com.VOAC.base.BaseFragment;
import osvin.com.VOAC.models.MediaFileModel;
import osvin.com.VOAC.util.Constants;
import osvin.com.VOAC.util.Debug;
import osvin.com.VOAC.util.PermissionClass;

public class CameraFragment extends BaseFragment implements View.OnClickListener {


    //    @BindView(R.id.cameraView)
    CameraView cameraView;
    @BindView(R.id.tab_Layout)
    TabLayout tabLayout;
    @BindView(R.id.pgVideoView)
    ProgressBar pgVideoView;
    @BindView(R.id.ivTakePhotoVideo)
    AppCompatImageView ivTakePhotoVideo;
    @BindView(R.id.ivSyncVideo)
    AppCompatImageView ivSyncVideo;
    @BindView(R.id.tvSkip)
    AppCompatTextView tvSkip;
    @BindView(R.id.tvTime)
    TextView tvTime;
    @BindView(R.id.framLayout)
    FrameLayout framLayout;
    private View rootView;
    Unbinder unbinder;

    private static CameraFragment instance;
    private int tabPosition = 0;
    private final int TAB_PHOTO = 0, TAB_VIDEO = 1, TAB_GALLERY = 2;
    private final int IMAGE_REQUEST_CODE = 111;
    private HomeActivity homeActivity;
    private CountDownTimer countDownTimer;
    private int futureMilliSeconds = 60 * 1000;
    private TabLayout.Tab tabPhoto, tabVideo, tabGallery;
    private static final AspectRatio RATIO_3_4 = AspectRatio.of(1, 1);
    private int postType = 1;

    public static CameraFragment newInstance() {
        if (instance == null) {
            instance = new CameraFragment();
        }
        return instance;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Debug.trace("LifeCycle", "oncreate");

        homeActivity = (HomeActivity) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_camera, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        postType = getArguments().getInt(Constants.POST_TYPE);
        Debug.trace("LifeCycle", "onCreateView");
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Debug.trace("LifeCycle", "onViewCreated");

        initViews();
    }

    @Override
    public void onResume() {
        super.onResume();
        Debug.trace("LifeCycle", "onResume");
        if (checkPermissionForCameraAndMediaPicker()) {
            if (cameraView != null) {
                cameraView.open();
            }
            tabLayout.selectTab(tabPhoto);
        }
//        initViews();
    }

    @Override
    public void onPause() {
        super.onPause();
        Debug.trace("LifeCycle", "onPause");
        tabLayout.setVisibility(View.VISIBLE);
        if (cameraView != null) {
            cameraView.close();
        }
    }

//    @Override
//    public void onDestroy() {
//        super.onDestroy();
//        Debug.trace("LifeCycle", "onDestroy");
//
//    }

    private void initViews() {

        cameraView = new CameraView(homeActivity);
        cameraView.setUseDeviceOrientation(true);
        framLayout.addView(cameraView, 0);

        if (!checkPermissionForCameraAndMediaPicker()) {
            requestPermissionCameraAndMediaFiles();
            return;
        }

//        switch (postType) {
//
//            case Constants.POST_TYPE_POST:
//                tvSkip.setVisibility(View.GONE);
//                break;
//
//            case Constants.POST_TYPE_TOPIC:
//                tvSkip.setVisibility(View.VISIBLE);
//                break;
//        }

        tabPhoto = tabLayout.newTab().setText("Photo");
        tabVideo = tabLayout.newTab().setText("Video");
        tabGallery = tabLayout.newTab().setText("Gallery");

        tabLayout.addTab(tabPhoto);
        tabLayout.addTab(tabVideo);
        tabLayout.addTab(tabGallery);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                tabPosition = tab.getPosition();
//                operateTab();
                if (tabPosition == TAB_PHOTO) {
                    ivTakePhotoVideo.setImageResource(R.drawable.ic_photo_camera);
                }

                if (tabPosition == TAB_VIDEO) {
                    ivTakePhotoVideo.setImageResource(R.drawable.ic_video);
                }

                if (tabPosition == TAB_GALLERY) {
                    openMediaPicker();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                tabPosition = tab.getPosition();
//                operateTab();
            }
        });

        cameraView.setLifecycleOwner(getViewLifecycleOwner());
        cameraView.addCameraListener(new CameraListener() {
            @Override
            public void onCameraOpened(@NonNull CameraOptions options) {
                super.onCameraOpened(options);
            }

            @Override
            public void onCameraClosed() {
                super.onCameraClosed();
            }

            @Override
            public void onCameraError(@NonNull CameraException exception) {
                super.onCameraError(exception);
            }

            @Override
            public void onPictureTaken(@NonNull PictureResult result) {
                super.onPictureTaken(result);
                File fileDirectory = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpeg");
                try {
                    fileDirectory.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                result.toFile(fileDirectory, file -> {
                    if (file != null) {
                        List<MediaFileModel> list = new ArrayList<>();
                        MediaFileModel mediaFileModel = new MediaFileModel();
                        mediaFileModel.setUrl(file.getPath());
                        list.add(mediaFileModel);
                        homeActivity.pushFragment(AddPostFragment.newInstance(list, postType), false, false, null);
                    }
                });
            }

            @Override
            public void onVideoTaken(@NonNull VideoResult result) {
                super.onVideoTaken(result);
//                tabLayout.setVisibility(View.VISIBLE);
                List<MediaFileModel> list = new ArrayList<>();
                MediaFileModel mediaFileModel = new MediaFileModel();
                mediaFileModel.setUrl(result.getFile().getPath());
                list.add(mediaFileModel);
                homeActivity.pushFragment(AddPostFragment.newInstance(list, postType), false, false, null);
            }

            @Override
            public void onOrientationChanged(int orientation) {
                super.onOrientationChanged(orientation);
            }

            @Override
            public void onAutoFocusStart(@NonNull PointF point) {
                super.onAutoFocusStart(point);
            }

            @Override
            public void onAutoFocusEnd(boolean successful, @NonNull PointF point) {
                super.onAutoFocusEnd(successful, point);
            }

            @Override
            public void onZoomChanged(float newValue, @NonNull float[] bounds, @Nullable PointF[] fingers) {
                super.onZoomChanged(newValue, bounds, fingers);
            }

            @Override
            public void onExposureCorrectionChanged(float newValue, @NonNull float[] bounds, @Nullable PointF[] fingers) {
                super.onExposureCorrectionChanged(newValue, bounds, fingers);
            }

            @Override
            public void onVideoRecordingStart() {
                super.onVideoRecordingStart();
                startTimer();
            }

            @Override
            public void onVideoRecordingEnd() {
                super.onVideoRecordingEnd();
                //tvTime.setVisibility(View.GONE);
                cancelTimer();
            }
        });

        ivTakePhotoVideo.setOnClickListener(this);
        ivSyncVideo.setOnClickListener(this);
        tvSkip.setOnClickListener(this);
        SizeSelector matchRatioImage = SizeSelectors.and(
                SizeSelectors.and(SizeSelectors.aspectRatio(RATIO_3_4, 0)));
        cameraView.setPictureSize(matchRatioImage);
        cameraView.setPreviewStreamSize(matchRatioImage);
        cameraView.setVideoSize(matchRatioImage);
        cameraView.open();
    }


    private void operateTab() {

        if (tabPosition == TAB_PHOTO) {
            cameraView.setMode(Mode.PICTURE);
            cameraView.takePictureSnapshot();
        }

        if (tabPosition == TAB_VIDEO) {
            if (cameraView.isTakingVideo()) {
                cameraView.stopVideo();
            } else {
                File fileDirectory = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".mp4");
                try {
                    fileDirectory.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                cameraView.setMode(Mode.VIDEO);
                cameraView.takeVideo(fileDirectory, futureMilliSeconds);
                ivTakePhotoVideo.setImageResource(R.drawable.ic_video_player_stop_button);
                tabLayout.setVisibility(View.GONE);
            }
        }
    }

    private void openMediaPicker() {

        Intent intent = new Intent(getActivity(), FileDirsActivity.class);
        intent.putExtra(FilePickerActivity.CONFIGS, new Configurations.Builder()
                .setCheckPermission(true)
                .enableImageCapture(false)
                .enableVideoCapture(false)
                .setSkipZeroSizeFiles(true)
                .setSingleChoiceMode(false)
                .setMaxSelection(5)
                .setIgnoreHiddenFile(true)
                .setIgnorePaths()
                .setIsCropEnabled(false)
                .setChooseMediaType(Configurations.TYPE_MEDIA)
                .setMaxCropDuration(30)
                .build());
        startActivityForResult(intent, IMAGE_REQUEST_CODE);
    }


    private void startTimer() {
        pgVideoView.setVisibility(View.VISIBLE);
        tvTime.setVisibility(View.VISIBLE);
        pgVideoView.setProgress(0);
        countDownTimer = new CountDownTimer(futureMilliSeconds, 1000) {
            public void onTick(long millisUntilFinished) {
                long seconds = millisUntilFinished / 1000;
                pgVideoView.setSecondaryProgress((int) seconds);
                pgVideoView.setProgress((int) seconds);
                tvTime.setText("00 : "+seconds);
            }

            public void onFinish() {

            }
        };
        countDownTimer.start();
    }

    private void cancelTimer() {
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
        pgVideoView.setVisibility(View.GONE);
        tvTime.setVisibility(View.GONE);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        cancelTimer();
        cameraView.close();
        cameraView.destroy();
        unbinder.unbind();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case Constants.REQUEST_CODE_CAMERA:
                if (PermissionClass.checkPermission(permissions)) {

                    initViews();

                } else {

                    if (!ActivityCompat.shouldShowRequestPermissionRationale(homeActivity, Manifest.permission.CAMERA)
                            || !ActivityCompat.shouldShowRequestPermissionRationale(homeActivity, Manifest.permission.RECORD_AUDIO)
                            || !ActivityCompat.shouldShowRequestPermissionRationale(homeActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        alertDialogDeny(getString(R.string.camera_permission));
                    } else {
                        alertDialog(Constants.CHECK_CAMERA_PERMISSION, false);
                    }
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == IMAGE_REQUEST_CODE
                && resultCode == Activity.RESULT_OK
                && data != null) {
            List<MediaFileModel> fileList = new ArrayList<>();
            List<MediaFile> mediaFiles = data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES);
            for (int i = 0; i < mediaFiles.size(); i++) {
                MediaFileModel mediaFileModel = new MediaFileModel();
                mediaFileModel.setUrl(mediaFiles.get(i).getPath());
                fileList.add(mediaFileModel);
            }
            homeActivity.pushFragment(AddPostFragment.newInstance(fileList, postType), false, false, null);
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.ivTakePhotoVideo:
                operateTab();
                break;
            case R.id.ivSyncVideo:
               if(cameraView.getFacing()== Facing.BACK)
                   cameraView.setFacing(Facing.FRONT);
               else
                   cameraView.setFacing(Facing.BACK);

                break;

            case R.id.tvSkip:
                homeActivity.pushFragment(AddPostFragment.newInstance(null, postType), false, false, null);
                break;
        }
    }
}
