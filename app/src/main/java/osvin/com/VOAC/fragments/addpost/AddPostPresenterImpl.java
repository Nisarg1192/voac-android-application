package osvin.com.VOAC.fragments.addpost;

import android.content.Context;

import osvin.com.VOAC.models.AddPostModel;

public class AddPostPresenterImpl implements AddPostPresenter, AddPostListener {

    private AddPostView addPostView;
    private AddPostInteractor addPostInteractor;

    public AddPostPresenterImpl(AddPostView addPostView) {
        this.addPostView = addPostView;
        this.addPostInteractor = new AddPostInteractorImpl();
    }

    @Override
    public void addPost(Context context, boolean isDialogRequired, AddPostModel addPostModel) {
        addPostInteractor.addPost(context, isDialogRequired, addPostModel, this);
    }

    @Override
    public void addTopic(Context context, boolean isDialogRequired, AddPostModel addPostModel) {
        addPostInteractor.addTopic(context, isDialogRequired, addPostModel, this);
    }

    @Override
    public void onSuccessOfAddPost(String data) {
        addPostView.onSuccessOfAddPost(data);
    }

    @Override
    public void onFailureOfAddPost(String errorMsg, int errorCode) {
        addPostView.onFailureOfAddPost(errorMsg, errorCode);
    }

    @Override
    public void onSuccessOfAddTopic(String data) {
        addPostView.onSuccessOfAddTopic(data);
    }

    @Override
    public void onFailureOfAddTopic(String errorMsg, int errorCode) {
        addPostView.onFailureOfAddTopic(errorMsg, errorCode);
    }
}
