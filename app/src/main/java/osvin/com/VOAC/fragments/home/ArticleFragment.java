package osvin.com.VOAC.fragments.home;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import osvin.com.VOAC.R;
import osvin.com.VOAC.activities.ArticleDetailActivity;
import osvin.com.VOAC.activities.EditPostActivity;
import osvin.com.VOAC.activities.HomeActivity;
import osvin.com.VOAC.activities.PostListActivity;
import osvin.com.VOAC.activities.ReactionListActivity;
import osvin.com.VOAC.activities.ViewMedia;
import osvin.com.VOAC.adapter.CommentAdapter;
import osvin.com.VOAC.adapter.PostAdapter;
import osvin.com.VOAC.api.NetworkManager;
import osvin.com.VOAC.base.BaseFragment;
import osvin.com.VOAC.models.ArticleModel;
import osvin.com.VOAC.models.CommentModel;
import osvin.com.VOAC.models.MediaModel;
import osvin.com.VOAC.models.PostModel;
import osvin.com.VOAC.util.Constants;
import osvin.com.VOAC.util.CustomDialog;
import osvin.com.VOAC.util.Debug;
import osvin.com.VOAC.util.PhotoFullPopupWindow;
import osvin.com.VOAC.util.Utils;
import osvin.com.VOAC.util.ValidationUtils;

public class ArticleFragment extends BaseFragment implements View.OnClickListener, PostAdapter.onReactionClick {

    @BindView(R.id.rvPosts)
    RecyclerView rvPosts;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
//    private Context context;


    private PostAdapter postAdapter;
    private LinearLayoutManager linearLayoutManager;
    private static ArticleFragment instance;

    private int currentPage = Constants.PAGE_START;

    private HomeActivity homeActivity;
    private int TOTAL_RECORD_PER_POST = 10;


    public static ArticleFragment newInstance() {
        if (instance == null) {
            instance = new ArticleFragment();
        }
        return instance;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        HomeActivity activity = (HomeActivity) getActivity();
        assert activity != null;
        homeActivity = (HomeActivity) getActivity();
//        context = getActivity();
        Debug.trace("TAG_ArticleFragment", "onCreate");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Debug.trace("TAG_ArticleFragment", "onCreateView");
        if (getView() == null) {
            View view = inflater.inflate(R.layout.fragment_article, container, false);
            ButterKnife.bind(this, view);
            initViews();
            return view;
        } else {
            ButterKnife.bind(this, getView());
            return getView();
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
    }

    private void initViews() {
        Debug.trace("TAG_ArticleFragment", "onViewCreated");

        linearLayoutManager = new LinearLayoutManager(homeActivity, RecyclerView.VERTICAL, false) {
            @Override
            public boolean supportsPredictiveItemAnimations() {
                return false;
            }
        };
        rvPosts.setLayoutManager(linearLayoutManager);

        ((SimpleItemAnimator) Objects.requireNonNull(rvPosts.getItemAnimator())).setSupportsChangeAnimations(false);

        initListener();

        setPostAdapter(homeActivity.posts_list);

        if (homeActivity.article_list.size() == 0) {
            getArticles();
        } else {
            postAdapter.setArticleList(homeActivity.article_list);
            postAdapter.notifyItemRangeInserted(0, 1);
        }
    }

    private void initListener() {

        rvPosts.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {

                if (dy > 0) {

//                    int visibleItemCount = linearLayoutManager.getChildCount();
//                    int totalItemCount = linearLayoutManager.getItemCount();
//                    int pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition();
//
//                    if (homeActivity.loading) {
//                        if ((visibleItemCount + 1 + pastVisibleItems) > totalItemCount) {
////                            loading = false;
//                            Debug.trace("Pagination", "Last Item Wow !");
//                            //Do pagination.. i.e. fetch new data
//
//                            rvPosts.post(() -> {
//
//                                homeActivity.isRefresh = false;
//                                currentPage++;
//                                Debug.trace("currentPage", String.valueOf(currentPage));
//                                getPosts(Constants.LOADER_LOAD_MORE, currentPage);
//                            });
//                        }
//                    }

                    int totalItemCount = linearLayoutManager.getItemCount();
                    int lastItemPosition = linearLayoutManager.findLastCompletelyVisibleItemPosition();

                    if ((totalItemCount == lastItemPosition + 1) && homeActivity.loading) {
                        rvPosts.post(() -> {

                            homeActivity.isRefresh = false;
                            currentPage++;
                            Debug.trace("currentPage", String.valueOf(currentPage));
                            getPosts(Constants.LOADER_LOAD_MORE, currentPage);
                        });
                    }

                }
            }
        });


//        Pagination.getInstance().with(rvPosts, linearLayoutManager)
//                .setLoading(true)
//                .setVisibleThreshold(5)
//                .setWebservicePlayform(Pagination.WebservicePlayform.PHP)
//                .setSkip(homeActivity.posts_list.size())
//                .setLimit(10)
//                .setPaginationListener(new PaginationListener() {
//                    @Override
//                    public void onLoadMorePage(int currentPage, int totalItemCount, RecyclerView recyclerView) {
//
//                        rvPosts.post(() -> {
//
//                            homeActivity.isRefresh = false;
////                            currentPage++;
//                            Debug.trace("currentPage", String.valueOf(currentPage));
//                            getPosts(Constants.LOADER_LOAD_MORE, currentPage);
//                        });
//                    }
//
//                    @Override
//                    public void onLoadMoreData(int skip, int limit, RecyclerView recyclerView) {
////                        getFeedsList(MainActivity.this, 1, skip, limit);
//                    }
//                })
//                .startListener();

        swipeRefreshLayout.setOnRefreshListener(() -> {
//            Pagination.getInstance().resetState();
            homeActivity.isRefresh = true;

            homeActivity.article_list.clear();
            homeActivity.posts_list.clear();
            postAdapter.removeFooter();
            postAdapter.notifyDataSetChanged();

            currentPage = Constants.PAGE_START;
            getArticles();
        });
    }

    private void setPostAdapter(List<PostModel> postList) {

//        homeActivity.posts_list.addAll(postList);
        if (postAdapter == null) {
            postAdapter = new PostAdapter(homeActivity, homeActivity.posts_list, ArticleFragment.this);
            postAdapter.setHasStableIds(true);
            postAdapter.setOnReactionClick(this);
            rvPosts.setAdapter(postAdapter);

        } else {
            postAdapter.notifyItemRangeInserted(homeActivity.posts_list.size() - 1  /*(1 + postList.size())*/, postList.size());
        }
    }


    private void getPosts(byte loaderType, int page) {

        currentPage = page;
        showLoader(loaderType);

        NetworkManager.getInstance().getPosts(page, Utils.getPrefDataString(homeActivity, Constants.USER_ID), new NetworkManager.DataObserver() {
            @SuppressLint("StaticFieldLeak")
            @Override
            public void onSuccess(JSONObject response) {

                new AsyncTask<JSONObject, Void, List<PostModel>>() {
                    @Override
                    protected List<PostModel> doInBackground(JSONObject... jsonObjects) {
                        return Utils.parseResponse(response);
                    }

                    @Override
                    protected void onPostExecute(List<PostModel> postModels) {

                        postAdapter.removeFooter();

                        if (homeActivity.isRefresh) {
                            homeActivity.posts_list.clear();
                        }

                        homeActivity.posts_list.addAll(postModels);
                        setPostAdapter(postModels);

                        homeActivity.loading = postModels.size() == TOTAL_RECORD_PER_POST;

                        Debug.trace("currentPage", String.valueOf(homeActivity.loading));
                        Debug.trace("currentPage", "listSize " + postModels.size());

                        if (homeActivity.loading) {
                            postAdapter.addFooter();
                        }

                        hideLoader(loaderType);
                    }
                }.execute(response);

            }

            @Override
            public void onFailure(String error, int errorCode) {
                hideLoader(loaderType);
                homeActivity.loading = false;
                homeActivity.isRefresh = false;
                if (loaderType == Constants.LOADER_LOAD_MORE) {
                    postAdapter.removeFooter();
                }
                showSnackBar(error, true, getString(R.string.str_ok));
            }
        });
    }


    private void getArticles() {

        showLoader(Constants.LOADER_SWIPE);
//        setArticleVisibility();

        NetworkManager.getInstance().getArticles(new NetworkManager.DataObserver() {
            @Override
            public void onSuccess(JSONObject response) {

                List<ArticleModel> articleList = new ArrayList<>();

                if (response != null && !TextUtils.isEmpty(response.toString()) && response.optInt("Responsecode", 0) == 200) {

                    JSONArray jsonArray = response.optJSONArray("posts");

                    Gson gson = new Gson();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.optJSONObject(i);
                        if (!jsonObject.optString("post_author", "0").equals(Utils.getPrefDataString(homeActivity, Constants.USER_ID))) {

                            ArticleModel articleModel = gson.fromJson(jsonObject.toString(), ArticleModel.class);
                            articleList.add(articleModel);
                        } else {

                        }
                    }
                }

                ArticleModel articleModel = new ArticleModel();
                articleModel.setUser_display_name("You");
                articleModel.setPost_author(Utils.getPrefDataString(homeActivity, Constants.USER_ID));
                articleModel.setUser_profile(Utils.getUserProfileImage(homeActivity));
                articleList.add(0, articleModel);
                homeActivity.article_list.clear();
                homeActivity.article_list.addAll(articleList);
                postAdapter.setArticleList(homeActivity.article_list);
                postAdapter.notifyItemRangeInserted(0, 1);
//                setArticleAdapter();

                getPosts(Constants.LOADER_SWIPE, Constants.PAGE_STARTING_INDEX);
            }

            @Override
            public void onFailure(String error, int errorCode) {
                CustomDialog.hideLoadDialog();
                getPosts(Constants.LOADER_SWIPE, Constants.PAGE_STARTING_INDEX);
            }
        });
    }

    /**
     * Method to show loader according to loader type.
     *
     * @param loaderType 2 for swipe to refresh loader.
     */
    private void showLoader(byte loaderType) {

        if (loaderType == Constants.LOADER_SWIPE) {
            swipeRefreshLayout.setRefreshing(true);
        }
    }

    /**
     * Method to hide loader according to loader type.
     *
     * @param loaderType 2 for swipe to refresh loader.
     */
    private void hideLoader(byte loaderType) {

        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void onClick(View v) {
        PostModel postModel;
        switch (v.getId()) {

            case R.id.ivPostImage:
                MediaModel mediaModel = (MediaModel) v.getTag(R.id.ivPostImage);

                if (Utils.isVideo(mediaModel.getUrl())) {
                    startActivity(new Intent(homeActivity, ViewMedia.class)
                            .putExtra(ViewMedia.KEY_MEDIA_URL, mediaModel.getUrl())
                            .putExtra(ViewMedia.KEY_MEDIA_TYPE, Constants.VIDEO));
                } else {
                    new PhotoFullPopupWindow(homeActivity, R.layout.popup_photo_full, v, mediaModel.getUrl(), null);
                }
                break;
            case R.id.ivUser:
                postModel = (PostModel) v.getTag(R.id.ivUser);
                if (postModel != null) {
                    new PhotoFullPopupWindow(homeActivity, R.layout.popup_photo_full, v, postModel.getAuthor_image().replace("-150x150", ""), null);
                }
                break;
//            case R.id.tv_comments:
//
//                postModel = (PostModel) v.getTag();
//                if (postModel != null) {
//                    startActivityForResult(new Intent(homeActivity, CommentsActivity.class)
//                            .putExtra(CommentsActivity.KEY_ARTICLE_MODEL, postModel), Constants.REQUEST_CODE_COMMENTS);
//                }
//                break;

            case R.id.cv:
                postModel = (PostModel) v.getTag();
                if (postModel != null) {
                    startActivityForResult(new Intent(homeActivity, ArticleDetailActivity.class)
                                    .putExtra(Constants.KEY_MODEL, postModel)
                            , Constants.REQUEST_CODE_ARTICLE_DETAIL);
                }
                break;

            case R.id.tvReactionCount:
                postModel = (PostModel) v.getTag();
                if (postModel != null) {
                    startActivity(new Intent(homeActivity, ReactionListActivity.class)
                            .putExtra(Constants.POST_ID, postModel.getId()));
                }
                break;

            case R.id.itemUserView:
                ArticleModel articleModel = (ArticleModel) v.getTag();
                if (articleModel != null) {
                    Intent intent = new Intent(getActivity(), PostListActivity.class);
                    intent.putExtra(PostListActivity.ID, articleModel.getPost_author());
                    startActivity(intent);
                }
                break;
            case R.id.ivShare:
                postModel = (PostModel) v.getTag();
                if (postModel != null) {

                    Utils.shareContent(homeActivity, "Share post", Constants.BASE_URL + postModel.getSlug());
                }
                break;

            case R.id.ivSubscribe:
                postModel = (PostModel) v.getTag();
                if (postModel != null) {
                    subscribeAndUnsubscribe(postModel, (AppCompatImageView) v);
                }
                break;

            case R.id.iv_add_comment:
                postModel = (PostModel) v.getTag();
                if (postModel != null) {

                    hideKeyboard();

                    View view = linearLayoutManager.findViewByPosition(postModel.getPosition() + 1);
                    EditText editText = view.findViewById(R.id.et_comment);
                    RecyclerView comment = view.findViewById(R.id.rvComments);
                    if (ValidationUtils.ValidateEditText(editText)) {
                        addComment(postModel, editText.getText().toString().trim(), comment);
                        editText.setText("");
                    } else {
                        showSnackBar(rvPosts, "Enter valid comment.", getString(R.string.str_ok), false, null);
                    }
                }
                break;
        }
    }

    private void addComment(PostModel postModel, String comment, RecyclerView rvComments) {

        Map<String, String> requestBody = new HashMap<>();
        requestBody.put("post_id", postModel.getId());
        requestBody.put("user_id", Utils.getPrefDataString(homeActivity, Constants.USER_ID));
        requestBody.put("content", comment);
        requestBody.put("insecure", Constants.INSECURE);

        NetworkManager.getInstance().post(homeActivity, true, Constants.SUBMIT_COMMENT, requestBody, new NetworkManager.DataObserver() {
            @Override
            public void onSuccess(JSONObject response) {
                CustomDialog.hideLoadDialog();
                try {

                    String status = response.getString("status");
                    if (status.toLowerCase().equals("ok")) {

                        CommentModel commentModel = new CommentModel();
                        commentModel.setId(response.getString("comment_ID"));
                        commentModel.setName(response.getString("comment_author"));
                        commentModel.setContent(response.getString("comment_content"));
                        commentModel.setDate(response.getString("comment_date"));
                        commentModel.setProfile("" + Utils.getPrefDataString(homeActivity, Constants.USER_PROFILE_IMAGE_URL));

                        postModel.getComment_list().add(Constants.DEFAULT_INT, commentModel);

                        if (postModel.getComment_list().size() > 2) {
                            postModel.getComment_list().remove(2);
                        }

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {

//                        postModel.setComment_list(commentList);
                                postModel.setComment_count(postModel.getComment_count() + 1);
                                postModel.setFormattedCommentCount(Utils.getCommentCount(postModel));

                                homeActivity.posts_list.set(postModel.getPosition() + 1, postModel);

//                                postAdapter.refreshSingleItem(homeActivity.posts_list, postModel.getPosition() + 1);

                                if (rvComments.getAdapter() != null) {
                                    CommentAdapter commentAdapter = (CommentAdapter) rvComments.getAdapter();
                                    commentAdapter.refreshList(postModel.getComment_list());
                                } else {
                                    postAdapter.refreshSingleItem(homeActivity.posts_list, postModel.getPosition() + 1);
                                }

                            }
                        }, 500);

                    } else {

                        CFAlertDialog.Builder builder = new CFAlertDialog.Builder(homeActivity)
                                .setDialogStyle(CFAlertDialog.CFAlertStyle.BOTTOM_SHEET)
                                .setMessage(getString(R.string.ERROR_MSG))
                                .addButton("Cancel", -1, -1, CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.END, (dialog, which) -> dialog.dismiss());
                        builder.show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String error, int errorCode) {
                // Debug.trace("Login", error);
                CustomDialog.hideLoadDialog();
                CFAlertDialog.Builder builder = new CFAlertDialog.Builder(homeActivity)
                        .setDialogStyle(CFAlertDialog.CFAlertStyle.BOTTOM_SHEET)
                        .setMessage(error)
                        .addButton("Cancel", -1, -1, CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.END, (dialog, which) -> dialog.dismiss());
                builder.show();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_OK && data != null) {
            int itemIndex = -1;
            PostModel postModel;
            switch (requestCode) {

                case Constants.REQUEST_CODE_ARTICLE_DETAIL:

                    postModel = data.getParcelableExtra(EditPostActivity.KEY_MODEL);
                    itemIndex = postModel.getPosition();

                    if (itemIndex != -1 && itemIndex < homeActivity.posts_list.size()) {

                        homeActivity.posts_list.set(itemIndex, postModel);

                        if (postAdapter != null)
                            postAdapter.refreshSingleItem(homeActivity.posts_list, itemIndex + 1);
                    }
                    break;

                case Constants.REQUEST_CODE_COMMENTS:
                    postModel = data.getParcelableExtra(EditPostActivity.KEY_MODEL);
                    itemIndex = postModel.getPosition();
                    if (itemIndex != -1 && itemIndex < homeActivity.posts_list.size()) {

                        homeActivity.posts_list.set(itemIndex, postModel);

                        if (postAdapter != null)
                            postAdapter.refreshSingleItem(homeActivity.posts_list, itemIndex + 1);
                    }
                    break;
            }
        } else if (resultCode == Constants.RESULT_CODE_DELETE_POST && data != null) {
            switch (requestCode) {
                case Constants.REQUEST_CODE_ARTICLE_DETAIL:

                    PostModel postModel = data.getParcelableExtra(Constants.KEY_MODEL);
                    int deletePosition = postModel.getPosition();

                    if (deletePosition != -1 && deletePosition < homeActivity.posts_list.size()) {
                        homeActivity.posts_list.remove(deletePosition);
                        postAdapter.notifyItemRemoved(deletePosition + 1);
                        postAdapter.notifyItemRangeChanged(deletePosition + 1, homeActivity.posts_list.size());
                    }
                    break;
            }
        }
    }

    @Override
    public void onReactionSelection(PostModel postModel, int reactionId, AppCompatImageView ivReaction) {
        sendReactionApi(postModel, reactionId, ivReaction);
    }

    private void sendReactionApi(PostModel postModel, int reactionId, AppCompatImageView ivReaction) {

        Debug.trace("ReactionId", String.valueOf(reactionId));

        Map<String, String> requestBody = new HashMap<>();
        requestBody.put("insecure", Constants.INSECURE);
        requestBody.put("pid", postModel.getId());
        requestBody.put("uid", Utils.getPrefDataString(homeActivity, Constants.USER_ID));
        requestBody.put("rid", String.valueOf(reactionId + 1));

        NetworkManager.getInstance().post(homeActivity, true, Constants.SAVE_POST_REACTION, requestBody, new NetworkManager.DataObserver() {
            @Override
            public void onSuccess(JSONObject response) {

                if (response != null && !response.isNull("Responsecode") && response.optInt("Responsecode") == 200) {

                    if (postModel.getReactionId().equals("0")) {
                        postModel.setReactionCounter(String.valueOf(Long.parseLong(postModel.getReactionCounter()) + 1));
                    }

                    postModel.setReactionId(String.valueOf(reactionId + 1));
                    postModel.setReactionImage(Utils.getReactionImage(reactionId + 1));
                    homeActivity.posts_list.set(postModel.getPosition(), postModel);

                    if (postAdapter != null) {
//                        postAdapter.refreshSingleItem(homeActivity.posts_list, postModel.getPosition() + 1);
                        ivReaction.setImageResource(postModel.getReactionImage());
                        postAdapter.refreshList(homeActivity.posts_list);
                    }
                } else {
                    showSnackBar(homeActivity.getString(R.string.str_failed_to_add_your_reaction_in_post), true, homeActivity.getString(R.string.str_ok));
                }
            }

            @Override
            public void onFailure(String error, int errorCode) {
                showSnackBar(error, true, homeActivity.getString(R.string.str_ok));
            }
        });
    }

    void subscribeAndUnsubscribe(PostModel postModel, AppCompatImageView imageView) {

        Map<String, String> requestBody = new HashMap<>();
        requestBody.put("pid", postModel.getId());
        requestBody.put("uid", Utils.getPrefDataString(homeActivity, Constants.USER_ID));

        if (postModel.getSubscriptionId() == Constants.SUBSCRIBE_ID) {
            requestBody.put("subcribe", String.valueOf(Constants.UNSUBSCRIBE_ID));
        } else {
            requestBody.put("subcribe", String.valueOf(Constants.SUBSCRIBE_ID));
        }

        requestBody.put("insecure", Constants.INSECURE);
        NetworkManager.getInstance().post(homeActivity, true, Constants.USER_SUBSCRIBE, requestBody, new NetworkManager.DataObserver() {
            @Override
            public void onSuccess(JSONObject response) {

                if (response != null && !response.isNull("Responsecode") && response.optInt("Responsecode") == 200) {

                    if (postModel.getSubscriptionId() == Constants.SUBSCRIBE_ID) {
                        postModel.setSubscriptionId(Constants.UNSUBSCRIBE_ID);
                        postModel.setSubscriptionImage(R.drawable.ic_unsubscribe);
                    } else {
                        postModel.setSubscriptionId(Constants.SUBSCRIBE_ID);
                        postModel.setSubscriptionImage(R.drawable.ic_subscribe);
                    }
                    homeActivity.posts_list.set(postModel.getPosition(), postModel);
                    imageView.setImageResource(postModel.getSubscriptionImage());
                    if (postAdapter != null) {
                        postAdapter.refreshList(homeActivity.posts_list);
//                        postAdapter.refreshSingleItem(homeActivity.posts_list, postModel.getPosition() + 1);
                    }
                } else {
                    showSnackBar(homeActivity.getString(R.string.ERROR_MSG), true, homeActivity.getString(R.string.str_ok));
                }
            }

            @Override
            public void onFailure(String error, int errorCode) {
                showSnackBar(error, true, getString(R.string.str_ok));
            }
        });
    }
}
