package osvin.com.VOAC.fragments.volunteer;

import android.content.Context;

import java.io.File;

public interface VolunteerApiInterface {

    interface OnSubmitVolunteerFormApiResponse {
        void onSubmitVolunteerFormApiSuccess(String message);

        void onSubmitVolunteerFormApiFailure(String message);
    }

    void cancelRequest();

    void submit_volunteer_form(String name, String email, String phone_number, String address, String city,
                               String state, String country, String lat, String lng, File volunteer_image,
                               Context context, VolunteerApiInterface.OnSubmitVolunteerFormApiResponse listener);


    interface onGetAddressInfoApiListener {
        void onGetAddressInfoApiSuccess(double lat, double lng);

        void onGetAddressInfoApiFailure(String message);
    }

    void getAddressInfoApi(String address, Context context, VolunteerApiInterface.onGetAddressInfoApiListener listener);

}