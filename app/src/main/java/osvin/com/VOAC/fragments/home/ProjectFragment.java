package osvin.com.VOAC.fragments.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import osvin.com.VOAC.R;
import osvin.com.VOAC.activities.HomeActivity;
import osvin.com.VOAC.base.BaseFragment;
import osvin.com.VOAC.fragments.addpost.CameraFragment;
import osvin.com.VOAC.imageloader.ImageUtils;
import osvin.com.VOAC.util.Constants;

/**
 * A fragment representing a list of Items.
 * <p/>
 */
public class ProjectFragment extends BaseFragment implements View.OnClickListener {


    @BindView(R.id.btnAddHelp)
    AppCompatButton btnAddHelp;
    @BindView(R.id.ivHelp)
    AppCompatImageView ivHelp;
    private HomeActivity context;


    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ProjectFragment() {

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = (HomeActivity) getActivity();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frgment_project, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initViews();
    }

    private void initViews() {

        btnAddHelp.setOnClickListener(this::onClick);
        ImageUtils.load(R.drawable.howcanwehelpyou, context, R.color.whitePressed, ivHelp, false);
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.btnAddHelp:
                Bundle bundle = new Bundle();
                bundle.putInt(Constants.POST_TYPE, Constants.POST_TYPE_TOPIC);
                context.pushFragment(CameraFragment.newInstance(), false, true, bundle);
                break;
        }
    }
}
