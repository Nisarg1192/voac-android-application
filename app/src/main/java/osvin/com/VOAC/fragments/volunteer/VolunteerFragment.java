package osvin.com.VOAC.fragments.volunteer;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

import java.io.File;
import java.util.List;

import osvin.com.VOAC.R;
import osvin.com.VOAC.base.BaseFragment;
import osvin.com.VOAC.customviews.CropView;
import osvin.com.VOAC.util.BitmapUtils;
import osvin.com.VOAC.util.Constants;
import osvin.com.VOAC.util.CustomDialog;
import osvin.com.VOAC.util.DialogUtils;
import osvin.com.VOAC.base.MyApplication;
import osvin.com.VOAC.util.PermissionClass;
import osvin.com.VOAC.util.Utils;
import osvin.com.VOAC.util.ValidationUtils;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;

public class VolunteerFragment extends BaseFragment implements View.OnClickListener,
        VolunteerApiInterface.OnSubmitVolunteerFormApiResponse,
        VolunteerApiInterface.onGetAddressInfoApiListener {

    private Button btn_send;
    private ImageView iv_profile_pic;
    private TextView tv_add_profile_pic;
    private EditText et_name, et_email_address, et_phone_number,
            et_address, et_city, et_state, et_country;

    private File volunteerImageFile;
    private VolunteerApiInterface volunteerApiInterface;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_volunteer, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        init(view);

        implementListener();
    }

    private void init(View view) {

        volunteerApiInterface = new VolunteerApi();

        et_name = view.findViewById(R.id.et_name);
        et_city = view.findViewById(R.id.et_city);
        btn_send = view.findViewById(R.id.btn_send);
        et_state = view.findViewById(R.id.et_state);
        et_address = view.findViewById(R.id.et_address);
        et_country = view.findViewById(R.id.et_country);
        iv_profile_pic = view.findViewById(R.id.iv_profile_pic);
        et_phone_number = view.findViewById(R.id.et_phone_number);
        et_email_address = view.findViewById(R.id.et_email_address);
        tv_add_profile_pic = view.findViewById(R.id.tv_add_profile_pic);
    }

    private void implementListener() {
        btn_send.setOnClickListener(this);
        iv_profile_pic.setOnClickListener(this);
        tv_add_profile_pic.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.btn_send:
                checkInternet();
                break;

            case R.id.iv_profile_pic:
            case R.id.tv_add_profile_pic:
                selectImage();
                break;
        }
    }

    private void validate_Data() {
        if (!ValidationUtils.ValidateEditText(et_name)) {
            showSnackBar(getString(R.string.error_name), true, getString(R.string.str_ok));
        } else if (!ValidationUtils.ValidateEditText(et_email_address)) {
            showSnackBar(getString(R.string.error_email), true, getString(R.string.str_ok));
        } else if (!ValidationUtils.isValidEmail(et_email_address)) {
            showSnackBar(getString(R.string.error_invalid_email), true, getString(R.string.str_ok));
        } else if (!ValidationUtils.ValidateEditText(et_phone_number)) {
            showSnackBar(getString(R.string.error_phone), true, getString(R.string.str_ok));
        } else if (!ValidationUtils.ValidateEditText(et_address)) {
            showSnackBar(getString(R.string.error_enter_address), true, getString(R.string.str_ok));
        } else if (!ValidationUtils.ValidateEditText(et_city)) {
            showSnackBar(getString(R.string.error_enter_city), true, getString(R.string.str_ok));
        } else if (!ValidationUtils.ValidateEditText(et_state)) {
            showSnackBar(getString(R.string.error_enter_state), true, getString(R.string.str_ok));
        } else if (!ValidationUtils.ValidateEditText(et_country)) {
            showSnackBar(getString(R.string.error_enter_country), true, getString(R.string.str_ok));
        } else {

            hideKeyboard();


            // Register the listener with the Location Manager to receive location updates
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (mContext.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    Activity#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for Activity#requestPermissions for more details.
                    requestPermissionLocation();
                    return;
                }
            }
            CustomDialog.showLoadDialog(mContext);
            String address = et_address.getText().toString() + ", " + et_city.getText().toString()
                    + ", " + et_state.getText().toString() + ", " + et_country.getText().toString();
            volunteerApiInterface.getAddressInfoApi(address, mContext, this);
        }
    }

    private void checkInternet() {
        if (Utils.checkInternetConnection(mContext)) {

            if (volunteerApiInterface != null) {

                validate_Data();
            }

        } else {
            showSnackBar(getString(R.string.str_no_internet_connection), true, getString(R.string.str_ok));
        }
    }

    private void selectImage() {
        final Dialog dialog = DialogUtils.getCustomDialog(mContext, R.layout.cl_video_media_option, true);
        dialog.findViewById(R.id.iv_cross_media_selection).setOnClickListener((View v) -> dialog.dismiss());

        dialog.findViewById(R.id.tv_camera).setOnClickListener((View v) -> {
            dialog.dismiss();
            checkCameraPermission();
        });

        dialog.findViewById(R.id.tv_gallery).setOnClickListener((View v) -> {
            dialog.dismiss();
            checkGalleryPermission();
        });
    }

    private void checkCameraPermission() {

        if (checkPermissionForCamera()) {
            EasyImage.openCamera(VolunteerFragment.this, 1);
        } else {
            requestPermissionCamera();
        }
    }

    private void checkGalleryPermission() {

        if (checkPermissionForGallery()) {
            EasyImage.openGallery(VolunteerFragment.this, 0);
        } else {
            requestPermissionGallery();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case Constants.REQUEST_CODE_CAMERA:

                if (PermissionClass.checkPermission(permissions)) {

                    checkCameraPermission();

                } else {

                    if (!ActivityCompat.shouldShowRequestPermissionRationale(mContext, Manifest.permission.CAMERA)
                            || !ActivityCompat.shouldShowRequestPermissionRationale(mContext, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        alertDialogDeny(getString(R.string.camera_permission));
                    } else {
                        alertDialog(Constants.CHECK_CAMERA_PERMISSION, true);
                    }

                }
                break;

            case Constants.REQUEST_CODE_GALLERY:
                if (PermissionClass.checkPermission(permissions)) {

                    checkGalleryPermission();

                } else {
                    if ( !ActivityCompat.shouldShowRequestPermissionRationale(mContext, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        alertDialogDeny(getString(R.string.gallery_permission));

                    } else {
                        alertDialog(Constants.CHECK_GALLERY_PERMISSION, true);
                    }
                }
                break;
            case Constants.REQUEST_CODE_LOCATION:
                if (PermissionClass.checkPermission(permissions)) {

                    CustomDialog.showLoadDialog(mContext);
                    String address = et_address.getText().toString() + ", " + et_city.getText().toString()
                            + ", " + et_state.getText().toString() + ", " + et_country.getText().toString();
                    volunteerApiInterface.getAddressInfoApi(address, mContext, this);

                } else {
                    if (!ActivityCompat.shouldShowRequestPermissionRationale(mContext, Manifest.permission.ACCESS_FINE_LOCATION)
                            || !ActivityCompat.shouldShowRequestPermissionRationale(mContext, Manifest.permission.ACCESS_COARSE_LOCATION)) {
                        alertDialogDeny(getString(R.string.location_permission));

                    } else {
                        alertDialog(Constants.CHECK_LOCATION_PERMISSION, true);
                    }
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_OK) {

            if (requestCode == Constants.REQUEST_CODE_CROP_IMAGE) {

                iv_profile_pic.setImageBitmap(BitmapUtils.getCircleBitmap(MyApplication.getInstance().Photo));
                volunteerImageFile = BitmapUtils.getFileFromBitmap(mContext, MyApplication.getInstance().Photo);

            } else {
                EasyImage.handleActivityResult(requestCode, resultCode, data, mContext, new DefaultCallback() {
                    @Override
                    public void onImagesPicked(@NonNull List<File> imageFiles, EasyImage.ImageSource source, int type) {

                        Uri imageUri = Uri.fromFile(imageFiles.get(0));

                        startActivityForResult(new Intent(mContext, CropView.class)
                                .putExtra(CropView.IMAGE_URI, imageUri.toString())
                                .putExtra(CropView.RATIO_TYPE, CropView.SQUARE), Constants.REQUEST_CODE_CROP_IMAGE);
                    }
                });
            }
        }
    }

    @Override
    public void onSubmitVolunteerFormApiSuccess(String message) {

        CustomDialog.hideLoadDialog();
        resetView();
        showSnackBar(message, true, mContext.getString(R.string.str_ok));
    }

    @Override
    public void onSubmitVolunteerFormApiFailure(String message) {
        CustomDialog.hideLoadDialog();
        if (isAdded()) {
            showSnackBar(message, true, mContext.getString(R.string.str_ok));
        }
    }

    @Override
    public void onGetAddressInfoApiSuccess(double lat, double lng) {
        //CustomDialog.hideLoadDialog();
        volunteerApiInterface.submit_volunteer_form(et_name.getText().toString()
                , et_email_address.getText().toString()
                , et_phone_number.getText().toString()
                , et_address.getText().toString()
                , et_city.getText().toString()
                , et_state.getText().toString()
                , et_country.getText().toString()
                , lat + ""
                , lng + ""
                , volunteerImageFile
                , mContext, this);

    }

    @Override
    public void onGetAddressInfoApiFailure(String message) {
        CustomDialog.hideLoadDialog();
        if (isAdded()) {
            showSnackBar(message, true, mContext.getString(R.string.str_ok));
        }

    }

    private void resetView() {

        volunteerImageFile = null;
        iv_profile_pic.setImageResource(R.drawable.ic_add_profile_pic);
        et_name.setText(Constants.DEFAULT_STRING);
        et_email_address.setText(Constants.DEFAULT_STRING);
        et_phone_number.setText(Constants.DEFAULT_STRING);
        et_address.setText(Constants.DEFAULT_STRING);
        et_city.setText(Constants.DEFAULT_STRING);
        et_state.setText(Constants.DEFAULT_STRING);
        et_country.setText(Constants.DEFAULT_STRING);
    }
}