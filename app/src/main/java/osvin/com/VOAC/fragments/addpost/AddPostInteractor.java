package osvin.com.VOAC.fragments.addpost;

import android.content.Context;

import osvin.com.VOAC.models.AddPostModel;

public interface AddPostInteractor {

    void addPost(Context context, boolean isDialogRequired, AddPostModel addPostModel, AddPostListener addPostListener);

    void addTopic(Context context, boolean isDialogRequired, AddPostModel addPostModel, AddPostListener addPostListener);
}
