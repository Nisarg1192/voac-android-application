package osvin.com.VOAC.fragments.addpost;

import android.app.Activity;
import android.content.Context;

import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import osvin.com.VOAC.R;
import osvin.com.VOAC.api.NetworkManager;
import osvin.com.VOAC.models.AddPostModel;
import osvin.com.VOAC.models.MediaFileModel;
import osvin.com.VOAC.util.Constants;
import osvin.com.VOAC.util.CustomDialog;
import osvin.com.VOAC.util.Debug;
import osvin.com.VOAC.util.Utils;

public class AddPostInteractorImpl implements AddPostInteractor {

    @Override
    public void addPost(Context context, boolean isDialogRequired, AddPostModel addPostModel, AddPostListener addPostListener) {

        String cookie = Utils.getPrefDataString(context, Constants.USER_COOKIE);
        if (isDialogRequired) {
            CustomDialog.showLoadDialog((Activity) context);
        }

        NetworkManager.getInstance().getNonce("posts", "create_post", cookie, new NetworkManager.DataObserver() {
            @Override
            public void onSuccess(JSONObject response) {
                try {
                    String status = response.getString("status");
                    if (status.toLowerCase().equals("ok")) {
                        String nonce = response.getString("nonce");

                        String titleREQ = addPostModel.getTitle();
                        String contentREQ = addPostModel.getDescription();
                        String locationREQ = addPostModel.getLocation();
                        String useridREQ = "" + Utils.getPrefDataString(context, Constants.USER_ID);
                        String cookieREQ = "" + Utils.getPrefDataString(context, Constants.USER_COOKIE);

                        if (addPostModel.getMediaFileModelList() != null && !addPostModel.getMediaFileModelList().isEmpty()) {

                            uploadMedia(addPostModel.getMediaFileModelList(), context, locationREQ, titleREQ, contentREQ, nonce, useridREQ, cookieREQ, addPostListener);
                        } else {
                            addPost(context, locationREQ, titleREQ, contentREQ, nonce, useridREQ, cookieREQ, new JSONArray(), addPostListener);
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    CustomDialog.hideLoadDialog();
                    addPostListener.onFailureOfAddPost(context.getString(R.string.str_add_post_failed_try_again_later), 100);
                    sendErrorMessageToServer(context, e.getLocalizedMessage());
                }
            }

            @Override
            public void onFailure(String error, int errorCode) {
                CustomDialog.hideLoadDialog();
                addPostListener.onFailureOfAddPost(context.getString(R.string.str_add_post_failed_try_again_later), 100);
                sendErrorMessageToServer(context, error);
            }
        });
    }

    @Override
    public void addTopic(Context context, boolean isDialogRequired, AddPostModel addPostModel, AddPostListener addPostListener) {

        String cookie = Utils.getPrefDataString(context, Constants.USER_COOKIE);

        if (isDialogRequired) {
            CustomDialog.showLoadDialog((Activity) context);
        }

        NetworkManager.getInstance().getNonce("posts", "create_post", cookie, new NetworkManager.DataObserver() {
            @Override
            public void onSuccess(JSONObject response) {
                try {
                    String status = response.getString("status");
                    if (status.toLowerCase().equals("ok")) {
                        String nonce = response.getString("nonce");

                        uploadAndAddTopic(context, nonce, addPostModel, addPostListener);

                    } else {
                        addPostListener.onFailureOfAddTopic(context.getString(R.string.str_add_help_failed_try_again_later), 100);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    CustomDialog.hideLoadDialog();
                    addPostListener.onFailureOfAddTopic(context.getString(R.string.str_add_help_failed_try_again_later), 100);
                }
            }

            @Override
            public void onFailure(String error, int errorCode) {
                CustomDialog.hideLoadDialog();
                addPostListener.onFailureOfAddTopic(context.getString(R.string.str_add_help_failed_try_again_later), 100);
            }
        });
    }

    private void uploadAndAddTopic(Context context, String nonce, AddPostModel addPostModel, AddPostListener addPostListener) {

        if (addPostModel.getMediaFileModelList() != null && !addPostModel.getMediaFileModelList().isEmpty()) {

            NetworkManager.getInstance().uploadMedia(addPostModel.getMediaFileModelList(), new JSONObjectRequestListener() {
                @Override
                public void onResponse(JSONObject response) {
                    Debug.trace("Response", response.toString());
//                    Toast.makeText(MyApplication.getInstance(), response.toString(), Toast.LENGTH_LONG).show();

                    String status = response.optString("status");
                    if (status.toLowerCase().equals("ok")) {
                        JSONArray postMediaArray = response.optJSONArray("post_media");
                        if (postMediaArray != null && postMediaArray.length() > 0) {

                            JSONObject jsonObject = new JSONObject();
                            try {
                                jsonObject.put("location", addPostModel.getLocation());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            String useridREQ = "" + Utils.getPrefDataString(context, Constants.USER_ID);
                            String cookieREQ = "" + Utils.getPrefDataString(context, Constants.USER_COOKIE);

                            Map<String, String> requestBody = new HashMap<>();

                            for (int i = 0; i < postMediaArray.length(); i++) {
                                Debug.trace("MEDIA", "media" + (i + 1));
                                requestBody.put("media" + (i + 1), postMediaArray.optString(i));
                            }

                            requestBody.put("title", addPostModel.getTitle());
                            requestBody.put("content", addPostModel.getDescription());
                            requestBody.put("nonce", nonce);
                            requestBody.put("author", useridREQ);
                            requestBody.put("insecure", Constants.INSECURE);
                            requestBody.put("cookie", cookieREQ);
                            requestBody.put("custom_fields", jsonObject.toString());
                            requestBody.put("categories", "Projects");

                            NetworkManager.getInstance().post(context, false, Constants.CREATE_POST, requestBody, new NetworkManager.DataObserver() {
                                @Override
                                public void onSuccess(JSONObject response) {

                                    CustomDialog.hideLoadDialog();
                                    try {
                                        String status = response.getString("status");
                                        if (status.toLowerCase().equals("ok")) {
                                            addPostListener.onSuccessOfAddTopic(response.toString());
                                        } else {
                                            sendErrorMessageToServer(context, status);
                                            addPostListener.onFailureOfAddTopic(context.getString(R.string.str_add_help_failed_try_again_later), 100);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }

                                @Override
                                public void onFailure(String error, int errorCode) {
                                    sendErrorMessageToServer(context, error);
                                    addPostListener.onFailureOfAddTopic(context.getString(R.string.str_add_help_failed_try_again_later), 100);
                                }
                            });

                        } else {
                            sendErrorMessageToServer(context, "post media upload response array is null or array length is 0");
                            CustomDialog.hideLoadDialog();
                            addPostListener.onFailureOfAddTopic(context.getString(R.string.str_add_help_failed_try_again_later), 100);
                        }

                    } else {
                        CustomDialog.hideLoadDialog();
                        sendErrorMessageToServer(context, status);
                        addPostListener.onFailureOfAddTopic(context.getString(R.string.str_add_help_failed_try_again_later), 100);
                    }
                }

                @Override
                public void onError(ANError anError) {
//                    Toast.makeText(MyApplication.getInstance(), anError.getErrorBody(), Toast.LENGTH_LONG).show();
                    CustomDialog.hideLoadDialog();
                    sendErrorMessageToServer(context, anError.getErrorBody());
                    addPostListener.onFailureOfAddTopic(context.getString(R.string.str_add_help_failed_try_again_later), 100);
                }
            });
        } else {

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("location", addPostModel.getLocation());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            String useridREQ = "" + Utils.getPrefDataString(context, Constants.USER_ID);
            String cookieREQ = "" + Utils.getPrefDataString(context, Constants.USER_COOKIE);

            Map<String, String> requestBody = new HashMap<>();

            requestBody.put("title", addPostModel.getTitle());
            requestBody.put("content", addPostModel.getDescription());
            requestBody.put("nonce", nonce);
            requestBody.put("author", useridREQ);
            requestBody.put("insecure", Constants.INSECURE);
            requestBody.put("cookie", cookieREQ);
            requestBody.put("custom_fields", jsonObject.toString());
            requestBody.put("categories", "Projects");

            NetworkManager.getInstance().post(context, false, Constants.CREATE_POST, requestBody, new NetworkManager.DataObserver() {
                @Override
                public void onSuccess(JSONObject response) {

                    CustomDialog.hideLoadDialog();

                    try {
                        String status = response.getString("status");
                        if (status.toLowerCase().equals("ok")) {
                            addPostListener.onSuccessOfAddTopic(response.toString());
                        } else {
                            sendErrorMessageToServer(context, status);
                            addPostListener.onFailureOfAddTopic(context.getString(R.string.str_add_help_failed_try_again_later), 100);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        sendErrorMessageToServer(context, e.getLocalizedMessage());
                    }
                }

                @Override
                public void onFailure(String error, int errorCode) {
                    sendErrorMessageToServer(context, error);
                    addPostListener.onFailureOfAddTopic(context.getString(R.string.str_add_help_failed_try_again_later), 100);
                }
            });
        }
    }

    private void uploadMedia(List<MediaFileModel> mediaFileModelList, Context context, String locationREQ, String titleREQ, String contentREQ, String nonce, String useridREQ, String cookieREQ, AddPostListener addPostListener) {

        NetworkManager.getInstance().uploadMedia(mediaFileModelList, new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {
                Debug.trace("Response", response.toString());
//                CustomDialog.hideLoadDialog();
                String status = response.optString("status");
                if (status.toLowerCase().equals("ok")) {
                    JSONArray postMediaArray = response.optJSONArray("post_media");
                    if (postMediaArray != null && postMediaArray.length() > 0) {
                        addPost(context, locationREQ, titleREQ, contentREQ, nonce, useridREQ, cookieREQ, postMediaArray, addPostListener);
                    } else {
                        CustomDialog.hideLoadDialog();
                        addPostListener.onFailureOfAddPost(context.getString(R.string.str_add_post_failed_try_again_later), 100);
                        sendErrorMessageToServer(context, "post media upload response array is null or array length is 0");
                    }

                } else {
                    CustomDialog.hideLoadDialog();
                    addPostListener.onFailureOfAddPost(context.getString(R.string.str_add_post_failed_try_again_later), 100);
                    sendErrorMessageToServer(context, status);
                }
            }

            @Override
            public void onError(ANError anError) {
                Debug.trace("ANError", "" + anError.getErrorBody());
                CustomDialog.hideLoadDialog();
                addPostListener.onFailureOfAddPost(context.getString(R.string.str_add_post_failed_try_again_later), 100);
                sendErrorMessageToServer(context, anError.getErrorBody());
            }
        });
    }

    private void addPost(Context context, String locationREQ, String titleREQ, String contentREQ, String nonce, String useridREQ, String cookieREQ, JSONArray postImageArray, AddPostListener addPostListener) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("location", "" + locationREQ);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        NetworkManager.getInstance().AddPost(titleREQ, contentREQ, jsonObject.toString(), nonce, useridREQ, cookieREQ, postImageArray, new NetworkManager.DataObserver() {
            @Override
            public void onSuccess(JSONObject response) {

                Debug.trace("Post", response.toString());
                CustomDialog.hideLoadDialog();
                try {
                    String status = response.getString("status");
                    if (status.toLowerCase().equals("ok")) {
                        addPostListener.onSuccessOfAddPost(response.toString());
                    } else {
                        addPostListener.onFailureOfAddPost(context.getString(R.string.str_add_post_failed_try_again_later), 100);
                        sendErrorMessageToServer(context, status);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    CustomDialog.hideLoadDialog();
                    sendErrorMessageToServer(context, e.getLocalizedMessage());
                }
            }

            @Override
            public void onFailure(String error, int errorCode) {
                CustomDialog.hideLoadDialog();
                addPostListener.onFailureOfAddPost(context.getString(R.string.str_add_post_failed_try_again_later), 100);
                sendErrorMessageToServer(context, error);
            }
        });
    }

    private void sendErrorMessageToServer(Context context, String error) {
        Map<String, String> requestBody = new HashMap<>();
        requestBody.put("data", error);
        NetworkManager.getInstance().post(context, false, Constants.CREATE_POST_FAIL, requestBody, new NetworkManager.DataObserver() {
            @Override
            public void onSuccess(JSONObject response) {

            }

            @Override
            public void onFailure(String error, int errorCode) {

            }
        });

    }
}
