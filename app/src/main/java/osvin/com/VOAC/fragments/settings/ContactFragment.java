package osvin.com.VOAC.fragments.settings;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.thefinestartist.finestwebview.FinestWebView;

import osvin.com.VOAC.R;
import osvin.com.VOAC.activities.HomeActivity;
import osvin.com.VOAC.util.Constants;

public class ContactFragment extends Fragment implements View.OnClickListener {

    Button btnFacebook, btnTwitter, btnInstagram, btnYoutube, btnForm;

    static ContactFragment instance;
    //    FragNavController _mNavigator;
    private HomeActivity homeActivity;

    public static ContactFragment newInstance() {
        if (instance == null) {
            instance = new ContactFragment();
        }
        return instance;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        HomeActivity activity = (HomeActivity) getActivity();
        assert activity != null;
//        _mNavigator = activity.getNavigator();
        homeActivity = (HomeActivity) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_contact, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        Button btnBack = view.findViewById(R.id.btn_back);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homeActivity.popBackFragment();
            }
        });

        btnFacebook = view.findViewById(R.id.btn_facebook);
        btnTwitter = view.findViewById(R.id.btn_twitter);
        btnInstagram = view.findViewById(R.id.btn_instagram);
        btnYoutube = view.findViewById(R.id.btn_youtube);
        btnFacebook.setOnClickListener(this);
        btnTwitter.setOnClickListener(this);
        btnInstagram.setOnClickListener(this);
        btnYoutube.setOnClickListener(this);

        btnForm = view.findViewById(R.id.btn_form);
        btnForm.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.btn_facebook) {
            new FinestWebView.Builder(homeActivity).show(Constants.FACEBOOK_CONTACT);
        }

        if (id == R.id.btn_youtube) {
            new FinestWebView.Builder(homeActivity).show(Constants.YOUTUBE_CONTACT);
        }

        if (id == R.id.btn_instagram) {
            new FinestWebView.Builder(homeActivity).show(Constants.INSTAGRAM_CONTACT);
        }

        if (id == R.id.btn_twitter) {
            new FinestWebView.Builder(homeActivity).show(Constants.TWITTER_CONTACT);
        }

        if (id == R.id.btn_form) {
            homeActivity.pushFragment(ContactFormFragment.newInstance(), true, true, null);
        }
    }
}
