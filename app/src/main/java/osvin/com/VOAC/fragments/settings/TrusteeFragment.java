package osvin.com.VOAC.fragments.settings;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import osvin.com.VOAC.R;
import osvin.com.VOAC.activities.HomeActivity;
import osvin.com.VOAC.activities.TrustDetailActivity;
import osvin.com.VOAC.api.NetworkManager;
import osvin.com.VOAC.base.BaseFragment;
import osvin.com.VOAC.models.TrusteeModel;
import osvin.com.VOAC.util.CustomDialog;

public class TrusteeFragment extends BaseFragment {

    static TrusteeFragment instance;
//    FragNavController _mNavigator;

    GridView gridView;

    List<TrusteeModel> trusteeModels;
    TrusteeAdapter adapter;
    private HomeActivity homeActivity;

    public static TrusteeFragment newInstance() {
        if (instance == null) {
            instance = new TrusteeFragment();
        }
        return instance;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        HomeActivity activity = (HomeActivity)getActivity();
//        assert activity != null;
//        _mNavigator = activity.getNavigator();
        homeActivity = (HomeActivity) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_trustee, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        Button btnBack = view.findViewById(R.id.btn_back);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homeActivity.popBackFragment();
            }
        });

        gridView = view.findViewById(R.id.grid_view);
        trusteeModels = new ArrayList<>();
        adapter = new TrusteeAdapter(getActivity(), trusteeModels);
        gridView.setAdapter(adapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                TrusteeModel trusteeModel = trusteeModels.get(position);
                Intent intent = new Intent(getActivity(), TrustDetailActivity.class);
                intent.putExtra("trusteeModel", trusteeModel);
                startActivity(intent);

            }
        });

        getTrustees();
    }

    private void getTrustees() {
        CustomDialog.showLoadDialog(getActivity());
        NetworkManager.getInstance().getTrustees(new NetworkManager.DataObserver() {
            @Override
            public void onSuccess(JSONObject response) {
                try {
                    boolean status = response.getBoolean("Response");
                    if (status) {
                        JSONArray jsonArray = response.getJSONArray("ResponseMsg");
                        trusteeModels = parseJSON(jsonArray);
                        adapter.setData(trusteeModels);
                    }
                    CustomDialog.hideLoadDialog();
                } catch (JSONException e) {
                    e.printStackTrace();
                    CustomDialog.hideLoadDialog();
                }
            }

            @Override
            public void onFailure(String error, int errorCode) {
                //Debug.trace("api error", error);
                CustomDialog.hideLoadDialog();
                showSnackBar(error,true,getString(R.string.str_ok));
            }
        });
    }

    private List<TrusteeModel> parseJSON(JSONArray jsonArray) {
        List<TrusteeModel> trusteeModels = new ArrayList<>();
        if (jsonArray.length() > 0) {
            for (int k = 0; k < jsonArray.length(); k++) {
                try {
                    JSONObject jsonObject = jsonArray.getJSONObject(k);
                    TrusteeModel trusteeModel = new TrusteeModel();
                    trusteeModel.name = jsonObject.getString("name");
                    trusteeModel.title = jsonObject.getString("title");
                    trusteeModel.photo = jsonObject.getString("image");
                    trusteeModel.description = jsonObject.getString("description");
                    trusteeModels.add(trusteeModel);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }

        return trusteeModels;
    }


    class TrusteeAdapter extends BaseAdapter {

        List<TrusteeModel> trusteeList;
        LayoutInflater layoutInflater;

        public TrusteeAdapter(Context context, List<TrusteeModel> trusteeModels) {
            this.trusteeList = trusteeModels;
            layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return trusteeList.size();
        }

        @Override
        public Object getItem(int position) {
            return trusteeList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public void setData(List<TrusteeModel> trusteeModels) {
            this.trusteeList = trusteeModels;
            notifyDataSetChanged();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            TrusteeViewHolder viewHolder;
            if (convertView == null) {
                viewHolder = new TrusteeViewHolder();
                convertView = layoutInflater.inflate(R.layout.item_trustee, null);
                viewHolder.imageView = (ImageView) convertView.findViewById(R.id.photo_view);
                viewHolder.nameView = (TextView) convertView.findViewById(R.id.name_view);
                viewHolder.positionView = (TextView) convertView.findViewById(R.id.position_view);
                convertView.setTag(viewHolder);

            } else {
                viewHolder = (TrusteeViewHolder) convertView.getTag();
            }

            if (trusteeList.get(position) != null) {
                viewHolder.nameView.setText(trusteeList.get(position).name);
                viewHolder.positionView.setText(trusteeList.get(position).title);
                Glide.with(getActivity()).load(trusteeList.get(position).photo).into(viewHolder.imageView);
            }
            return convertView;
        }
    }

    class TrusteeViewHolder {
        ImageView imageView;
        TextView nameView, positionView;
    }

}
