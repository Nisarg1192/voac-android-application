package osvin.com.VOAC.fragments.settings;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.Switch;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import org.json.JSONObject;

import osvin.com.VOAC.R;
import osvin.com.VOAC.activities.HomeActivity;
import osvin.com.VOAC.api.NetworkManager;
import osvin.com.VOAC.base.BaseFragment;
import osvin.com.VOAC.util.Constants;
import osvin.com.VOAC.util.CustomDialog;
import osvin.com.VOAC.util.Utils;

public class SettingFragment extends BaseFragment implements CompoundButton.OnCheckedChangeListener {

    private HomeActivity homeActivity;
    private Switch cb_notification;
    private int status = 1;

    static SettingFragment instance;

    public static SettingFragment newInstance() {
        if (instance == null) {
            instance = new SettingFragment();
        }
        return instance;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        homeActivity = (HomeActivity) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_setting, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        RelativeLayout boardTrustees = view.findViewById(R.id.board_of_trustees);
        boardTrustees.setOnClickListener(v -> homeActivity.pushFragment(TrusteeFragment.newInstance(), true, false, null));

        RelativeLayout aboutUs = view.findViewById(R.id.about_us);
        aboutUs.setOnClickListener(v -> homeActivity.pushFragment(AboutFragment.newInstance(), true, false, null));

        RelativeLayout contactUs = view.findViewById(R.id.contact_us);
        contactUs.setOnClickListener(v -> homeActivity.pushFragment(ContactFragment.newInstance(), true, false, null));

        RelativeLayout terms = view.findViewById(R.id.terms_and_conditions);
        terms.setOnClickListener(v -> homeActivity.pushFragment(TermsFragment.newInstance(), true, false, null));


        cb_notification = view.findViewById(R.id.cb_notification);

        String status = Utils.getPrefDataString(homeActivity, Constants.PREF_NOTIFICATION);
        cb_notification.setChecked(status.equalsIgnoreCase("2"));

        cb_notification.setOnCheckedChangeListener(this);
        checkInternet(Constants.GET_NOTIFICATION);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        status = isChecked ? 2 : 1;
        checkInternet(Constants.SET_NOTIFICATION);
    }

    private void checkInternet(byte apiType) {
        if (Utils.checkInternetConnection(getActivity())) {
            CustomDialog.showLoadDialog(getActivity());
            if (apiType == Constants.GET_NOTIFICATION) {
                NetworkManager.getInstance().getUserNotificationStatus(Utils.getPrefDataString(getActivity(), Constants.USER_ID), new NetworkManager.DataObserver() {
                    @Override
                    public void onSuccess(JSONObject response) {
                        CustomDialog.hideLoadDialog();
                        try {
                            String status = response.getString("status");
                            if (status.toLowerCase().equals("ok")) {
                                Utils.setPrefDataString(getActivity(), Constants.PREF_NOTIFICATION, response.getString("notification_status"));
                                String status1 = Utils.getPrefDataString(getActivity(), Constants.PREF_NOTIFICATION);
                                cb_notification.setChecked(status1.equalsIgnoreCase("2"));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(String error, int errorCode) {
                        //  Debug.trace("Login", error);
                        CustomDialog.hideLoadDialog();
                        showSnackBar(error, true, getString(R.string.str_ok));
                    }
                });


            } else {
                NetworkManager.getInstance().updateUserNotificationStatus(Utils.getPrefDataString(getActivity(), Constants.USER_ID), "" + status, new NetworkManager.DataObserver() {
                    @Override
                    public void onSuccess(JSONObject response) {
                        CustomDialog.hideLoadDialog();
                        try {
                            String status1 = response.getString("status");
                            if (status1.toLowerCase().equals("ok")) {
                                Utils.setPrefDataString(getActivity(), Constants.PREF_NOTIFICATION, status + "");
//                                showSnackBar(response.getString("response_message"), true, getString(R.string.str_ok));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(String error, int errorCode) {
                        //  Debug.trace("Login", error);
                        CustomDialog.hideLoadDialog();
                        showSnackBar(error, true, getString(R.string.str_ok));
                    }
                });
            }
        } else {
            if (apiType == Constants.GET_NOTIFICATION) {
                showSnackBar(getString(R.string.str_no_internet_connection), true, getString(R.string.str_ok));
            } else {
                showSnackBar(getString(R.string.str_no_internet_connection), true, getString(R.string.str_ok));
            }
        }
    }
}
