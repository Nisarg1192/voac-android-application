package osvin.com.VOAC.fragments.home;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.Fragment;

import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.github.ybq.android.spinkit.SpinKitView;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import osvin.com.VOAC.R;
import osvin.com.VOAC.activities.ChangePasswordActivity;
import osvin.com.VOAC.activities.EditProfileActivity;
import osvin.com.VOAC.activities.HomeActivity;
import osvin.com.VOAC.activities.LoginActivity;
import osvin.com.VOAC.api.NetworkManager;
import osvin.com.VOAC.models.ProfileModel;
import osvin.com.VOAC.util.Constants;
import osvin.com.VOAC.util.DialogUtils;
import osvin.com.VOAC.imageloader.ImageUtils;
import osvin.com.VOAC.util.Utils;

public class ProfileFragment extends Fragment implements View.OnClickListener {

    Context context;
    static ProfileFragment instance;
    //    FragNavController _mNavigator;
    private HomeActivity homeActivity;
    AppCompatImageView iv_profile_pic;
    private Dialog dialog;

    SpinKitView spinView;

    private EditText tv_name, tv_username, tv_email_address;
    private Button btn_change_password, btn_logout, btn_edit_profile;

    public static ProfileFragment newInstance() {
        if (instance == null) {
            instance = new ProfileFragment();
        }
        return instance;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        HomeActivity activity = (HomeActivity) getActivity();
//        _mNavigator = activity.getNavigator();
        homeActivity = (HomeActivity) getActivity();
        context = getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        spinView = view.findViewById(R.id.spinView);

        iv_profile_pic = view.findViewById(R.id.iv_profile_pic);

        tv_name = view.findViewById(R.id.name);
        btn_logout = view.findViewById(R.id.logout_btn);
        tv_username = view.findViewById(R.id.username);
        btn_edit_profile = view.findViewById(R.id.edit_btn);
        tv_email_address = view.findViewById(R.id.email);
        btn_change_password = view.findViewById(R.id.btn_change_password);


        implementListeners();
        setSpinner(false);
        callProfileApi();
    }

    @Override
    public void onResume() {
        super.onResume();
        setView();
    }

    private void implementListeners() {

        btn_logout.setOnClickListener(this);
        btn_edit_profile.setOnClickListener(this);

        btn_change_password.setOnClickListener(this);
    }

    private void setSpinner(boolean s) {
        if (s) {
            spinView.setVisibility(View.VISIBLE);
        } else {
            spinView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_OK) {

            switch (requestCode) {
                case Constants.REQUEST_CODE_UPDATE_PROFILE:
                    setView();
                    callProfileApi();
                    break;
            }
        }
    }

    private void callProfileApi() {

//        setSpinner(true);
        final HomeActivity activity = (HomeActivity) context;
        Map<String, String> requestBody = new HashMap<>();

        requestBody.put("user_id", Utils.getPrefDataString(activity, Constants.USER_ID));
        requestBody.put("cookie", Utils.getPrefDataString(activity, Constants.USER_COOKIE));
        requestBody.put("insecure", Constants.INSECURE);

        NetworkManager.getInstance().post(activity, true, Constants.GET_PROFILE, requestBody, new NetworkManager.DataObserver() {
            @Override
            public void onSuccess(JSONObject response) {
//                setSpinner(false);
                try {
                    ProfileModel profileModel = new Gson().fromJson(response.toString(), ProfileModel.class);
                    if (profileModel != null) {
                        if (profileModel.getStatus() != null && profileModel.getStatus().toLowerCase().equals(Constants.RESPONSE_SUCCESS)) {

                            Utils.setPrefDataString(activity, Constants.USER_ID, String.valueOf(profileModel.getId()));
                            Utils.setPrefDataString(activity, Constants.USER_NAME, profileModel.getDisplayname());
                            Utils.setPrefDataString(activity, Constants.USER_EMAIL, profileModel.getEmail());
                            Utils.setPrefDataString(activity, Constants.USER_USERNAME, profileModel.getUsername());
                            if (profileModel.getAvatar() == null) {
                                profileModel.setAvatar("");
                            }
                            Utils.setPrefDataString(activity, Constants.USER_PROFILE_IMAGE_URL, profileModel.getAvatar());
                            setView();

                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String error, int errorCode) {
//                setSpinner(false);
                CFAlertDialog.Builder builder = new CFAlertDialog.Builder(activity)
                        .setDialogStyle(CFAlertDialog.CFAlertStyle.BOTTOM_SHEET)
                        .setMessage("Something went wrong.")
                        .addButton("Cancel", -1, -1, CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.END, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                builder.show();
            }
        });

//        NetworkManager.getInstance().getUserInfo(Utils.getPrefDataString(activity, Constants.USER_ID), Utils.getPrefDataString(activity, Constants.USER_COOKIE), new NetworkManager.DataObserver() {
//            @Override
//            public void onSuccess(JSONObject response) {
//                setSpinner(false);
//                try {
//                    ProfileModel profileModel = new Gson().fromJson(response.toString(), ProfileModel.class);
//                    if (profileModel != null) {
//                        if (profileModel.getStatus() != null && profileModel.getStatus().toLowerCase().equals(Constants.RESPONSE_SUCCESS)) {
//
//                            Utils.setPrefDataString(activity, Constants.USER_ID, String.valueOf(profileModel.getId()));
//                            Utils.setPrefDataString(activity, Constants.USER_NAME, profileModel.getDisplayname());
//                            Utils.setPrefDataString(activity, Constants.USER_EMAIL, profileModel.getEmail());
//                            Utils.setPrefDataString(activity, Constants.USER_USERNAME, profileModel.getUsername());
//                            if (profileModel.getAvatar() == null) {
//                                profileModel.setAvatar("");
//                            }
//                            Utils.setPrefDataString(activity, Constants.USER_PROFILE_IMAGE_URL, profileModel.getAvatar());
//                            setView();
//
//                        }
//                    }
//
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//
//            @Override
//            public void onFailure(String error, int errorCode) {
//                // Debug.trace("Login", error);
//                setSpinner(false);
//                CFAlertDialog.Builder builder = new CFAlertDialog.Builder(activity)
//                        .setDialogStyle(CFAlertDialog.CFAlertStyle.BOTTOM_SHEET)
//                        .setMessage("Something want to wrong.")
//                        .addButton("Cancel", -1, -1, CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.END, new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                dialog.dismiss();
//                            }
//                        });
//                builder.show();
//            }
//        });
    }

    private void setView() {
        HomeActivity activity = (HomeActivity) context;
        tv_name.setText(Utils.getPrefDataString(activity, Constants.USER_NAME));
        tv_username.setText(Utils.getPrefDataString(activity, Constants.USER_USERNAME));
        tv_email_address.setText(Utils.getPrefDataString(activity, Constants.USER_EMAIL));

        ImageUtils.loadCircle("" + Utils.getUserProfileImage(context), context, R.drawable.ic_profile_pic, iv_profile_pic);

        if (Utils.getPrefDataBoolean(context, Constants.USER_LOGIN_STATUS)) {

            btn_edit_profile.setVisibility(View.VISIBLE);
            btn_change_password.setVisibility(View.VISIBLE);
            btn_logout.setVisibility(View.VISIBLE);
        } else {

            btn_logout.setVisibility(View.GONE);
            btn_edit_profile.setVisibility(View.GONE);
            btn_change_password.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_change_password:
                startActivity(new Intent(context, ChangePasswordActivity.class));
                break;
            case R.id.edit_btn:
                startActivityForResult(new Intent(context, EditProfileActivity.class), Constants.REQUEST_CODE_UPDATE_PROFILE);
                break;
            case R.id.logout_btn:
                dialog = DialogUtils.twoButtonDialog(context, getString(R.string.confirm_sign_out), this);

                break;
            case R.id.tv_yes:
                if (dialog != null && dialog.isShowing())
                    dialog.dismiss();

                logOut();
                break;
        }
    }

    private void logOut() {
        if (Utils.checkInternetConnection(context)) {
            setSpinner(true);
            final HomeActivity activity = (HomeActivity) context;
            NetworkManager.getInstance().Logout(Utils.getPrefDataString(activity, Constants.USER_ID), Utils.getPrefDataString(activity, Constants.USER_COOKIE), new NetworkManager.DataObserver() {
                @Override
                public void onSuccess(JSONObject response) {
                    setSpinner(false);
                    try {

                        String status = response.getString("status");
                        if (status.toLowerCase().equals("ok")) {

                            Utils.Prefclear(context);
                            setView();
                            btn_logout.setVisibility(View.GONE);
                            btn_edit_profile.setVisibility(View.GONE);
                            btn_change_password.setVisibility(View.GONE);

                            Intent intent = new Intent(context, LoginActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            if (getActivity() != null) {
                                getActivity().finish();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(String error, int errorCode) {
                    //Debug.trace("Login", error);
                    setSpinner(false);
                    CFAlertDialog.Builder builder = new CFAlertDialog.Builder(activity)
                            .setDialogStyle(CFAlertDialog.CFAlertStyle.BOTTOM_SHEET)
                            .setMessage("Something went wrong.")
                            .addButton("Cancel", -1, -1, CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.END, (dialog, which) -> dialog.dismiss());
                    builder.show();
                }
            });
        } else {
            CFAlertDialog.Builder builder = new CFAlertDialog.Builder(context)
                    .setDialogStyle(CFAlertDialog.CFAlertStyle.BOTTOM_SHEET)
                    .setMessage("No Internet Connection.")
                    .addButton("Ok", -1, -1, CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.END, (dialog, which) -> dialog.dismiss());
            builder.show();
        }
    }

}
