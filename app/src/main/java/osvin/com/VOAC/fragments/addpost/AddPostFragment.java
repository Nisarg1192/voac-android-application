package osvin.com.VOAC.fragments.addpost;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import com.google.android.material.appbar.AppBarLayout;
import com.rbrooks.indefinitepagerindicator.IndefinitePagerIndicator;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import osvin.com.VOAC.R;
import osvin.com.VOAC.activities.HomeActivity;
import osvin.com.VOAC.adapter.AdpAddPost;
import osvin.com.VOAC.base.BaseFragment;
import osvin.com.VOAC.fragments.home.HomeFragment;
import osvin.com.VOAC.location.GPSTracker;
import osvin.com.VOAC.models.AddPostModel;
import osvin.com.VOAC.models.MediaFileModel;
import osvin.com.VOAC.util.Constants;
import osvin.com.VOAC.util.CustomDialog;
import osvin.com.VOAC.util.DialogUtils;
import osvin.com.VOAC.util.PermissionClass;
import osvin.com.VOAC.util.ValidationUtils;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AddPostFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddPostFragment extends BaseFragment implements View.OnClickListener, AddPostView {

    private static final String MEDIA_LIST = "mediaList";
    private static final String POST_TYPE = "postType";

    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.btn_back)
    AppCompatImageButton btnBack;
    @BindView(R.id.appBar_layout)
    AppBarLayout appBarLayout;
    @BindView(R.id.rvMedia)
    RecyclerView rvMedia;
    @BindView(R.id.et_location)
    EditText etLocation;
    @BindView(R.id.ivGetCurrentLocation)
    AppCompatImageView ivGetCurrentLocation;
    @BindView(R.id.et_title)
    EditText etTitle;
    @BindView(R.id.et_content)
    EditText etContent;
    @BindView(R.id.btn_add_post)
    Button btnAddPost;
    @BindView(R.id.spinView)
    LinearLayout spinView;
    @BindView(R.id.ivPhotoLibrary)
    AppCompatImageView ivPhotoLibrary;
    @BindView(R.id.dotView)
    IndefinitePagerIndicator dotView;
    @BindView(R.id.cbNeedHelp)
    AppCompatCheckBox cbNeedHelp;

    private View rootView;
    private Unbinder unbinder;

    private List<MediaFileModel> mediaList;
    private HomeActivity homeActivity;
    private GPSTracker gps;
    private Dialog dialog;
    private AlertDialog.Builder alertDialog;
    private AddPostPresenter addPostPresenter;
    private int postType = 1;
    Dialog locationDialog;

    public AddPostFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param mediaList Parameter 1.
     * @return A new instance of fragment AddPostFragment.
     */
    public static AddPostFragment newInstance(List<MediaFileModel> mediaList, int postType) {
        AddPostFragment fragment = new AddPostFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(MEDIA_LIST, (ArrayList<? extends Parcelable>) mediaList);
        args.putInt(POST_TYPE, postType);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        homeActivity = (HomeActivity) getActivity();
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.activity_add_post, container, false);
        unbinder = ButterKnife.bind(this, rootView);

        if (getArguments() != null) {
            mediaList = getArguments().getParcelableArrayList(MEDIA_LIST);
            postType = getArguments().getInt(POST_TYPE);
        }

        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        switch (postType) {

            case Constants.POST_TYPE_POST:
                tvTitle.setText(getString(R.string.str_add_post));
                cbNeedHelp.setChecked(false);
                cbNeedHelp.setEnabled(true);
                etLocation.setEnabled(true);
                ivGetCurrentLocation.setEnabled(true);
                break;

            case Constants.POST_TYPE_TOPIC:
                tvTitle.setText(getString(R.string.str_add_help));
                cbNeedHelp.setChecked(true);
                cbNeedHelp.setEnabled(false);
                etLocation.setEnabled(false);
                ivGetCurrentLocation.setEnabled(false);
                startLocationUpdatesService(homeActivity);
                break;
        }

        if (mediaList != null && !mediaList.isEmpty()) {
            int orientation;
            if (mediaList.size() > 1) {
                orientation = RecyclerView.HORIZONTAL;
                ivPhotoLibrary.setVisibility(View.VISIBLE);
                dotView.setVisibility(View.VISIBLE);
                dotView.attachToRecyclerView(rvMedia);
            } else {
                orientation = RecyclerView.VERTICAL;
                ivPhotoLibrary.setVisibility(View.GONE);
                dotView.setVisibility(View.GONE);
            }

            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(homeActivity, orientation, false);
            rvMedia.setLayoutManager(linearLayoutManager);
            AdpAddPost adpAddPost = new AdpAddPost(mediaList, homeActivity, this, R.layout.list_item_add_post_single_post);
            rvMedia.setAdapter(adpAddPost);
        } else {
            rvMedia.setVisibility(View.GONE);
            ivPhotoLibrary.setVisibility(View.GONE);
            dotView.setVisibility(View.GONE);
        }
        SnapHelper snapHelper = new PagerSnapHelper();
        snapHelper.attachToRecyclerView(rvMedia);

        ivGetCurrentLocation.setOnClickListener(this);
        btnBack.setOnClickListener(this);
        btnAddPost.setOnClickListener(this);
        addPostPresenter = new AddPostPresenterImpl(this);

        cbNeedHelp.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    locationDialog = DialogUtils.oneButtonDialogNonCancelable(homeActivity, "For posting help it requires your current location.", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            locationDialog.dismiss();
                            etLocation.setEnabled(false);
                            ivGetCurrentLocation.setEnabled(false);
                            startLocationUpdatesService(homeActivity);
                        }
                    });
                } else {
                    etLocation.setEnabled(true);
                    ivGetCurrentLocation.setEnabled(true);
                }
            }
        });
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.ivGetCurrentLocation:
                startLocationUpdatesService(homeActivity);
                break;

            case R.id.btn_back:
                homeActivity.popBackFragment();
                break;

            case R.id.btn_add_post:
                validateForm();
                break;
        }
    }

    private void validateForm() {
        if (!ValidationUtils.ValidateEditText(etLocation)) {
            showSnackBar(getString(R.string.str_please_enter_location_of_post), true, getString(R.string.str_ok));
        } else if (!ValidationUtils.ValidateEditText(etTitle)) {
            showSnackBar(getString(R.string.error_title), false, "");

        } else if (!ValidationUtils.ValidateEditText(etContent)) {
            showSnackBar(getString(R.string.error_content), false, "");

        } else {
            AddPostModel addPostModel = new AddPostModel();
            addPostModel.setTitle(etTitle.getText().toString().trim());
            addPostModel.setDescription(etContent.getText().toString().trim());
            addPostModel.setLocation(etLocation.getText().toString().trim());
            addPostModel.setMediaFileModelList(mediaList);

            if (cbNeedHelp.isChecked()) {
                addPostPresenter.addTopic(homeActivity, true, addPostModel);
            } else {
                addPostPresenter.addPost(homeActivity, true, addPostModel);
            }

//            if (postType == Constants.POST_TYPE_POST) {
//                addPostPresenter.addPost(homeActivity, true, addPostModel);
//            } else if (postType == Constants.POST_TYPE_TOPIC) {
//                addPostPresenter.addTopic(homeActivity, true, addPostModel);
//            }
        }
    }

    /**
     * Start location update if gps is available
     */
    private void startLocationUpdatesService(Activity activity) {

        gps = new GPSTracker(activity, location -> {

            etLocation.setText(GetAddress(Double.toString(location.getLatitude()), Double.toString(location.getLongitude())));
            CustomDialog.hideLoadDialog();
            gps.stopUsingGPS();
        });

        String[] permissions = new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
        if (PermissionClass.checkPermission(permissions)) {
            if (!gps.isGPSEnabled()) {
                showSettingsAlert(activity);
            } else {
                CustomDialog.showLoadDialog(activity, getString(R.string.str_please_wait_while_we_are_getting_your_location));
                gps.startLocationRequest();
            }
        } else {
            requestPermissionLocation();
        }
    }

    private String GetAddress(String lat, String lon) {
        Geocoder geocoder = new Geocoder(homeActivity, Locale.ENGLISH);
        StringBuilder ret = new StringBuilder();
        try {
            List<Address> addresses = geocoder.getFromLocation(Double.parseDouble(lat), Double.parseDouble(lon), 1);
            if (addresses != null && addresses.size() > 0) {


                String address = ""; // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                try {
                    address = addresses.get(0).getAddressLine(0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
//                String postalCode = addresses.get(0).getPostalCode();
//                String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL

                if (!TextUtils.isEmpty(address)) {
                    ret.append(address);
                } else {
                    if (!TextUtils.isEmpty(city)) {
                        ret.append(city).append(",");
                    }
                    if (!TextUtils.isEmpty(state)) {
                        ret.append(state).append(",");
                    }
                    if (!TextUtils.isEmpty(country)) {
                        ret.append(country);
                    }
                }
//                ret = addresses.get(0).getAdminArea() + ", " + addresses.get(0).getCountryName();
            } else {
                return ret.toString();
            }
        } catch (IOException e) {
            e.printStackTrace();
            return ret.toString();
        }
        return ret.toString();
    }

    /**
     * Function to show settings alert dialog On pressing Settings button will
     * launch Settings Options
     */
    private void showSettingsAlert(Context context) {

        if (context != null && alertDialog == null) {

            alertDialog = new AlertDialog.Builder(context);

            // Setting DialogHelp Title
            alertDialog.setTitle(context.getString(R.string.str_gps_settings));

            // Setting DialogHelp Message
            alertDialog.setMessage(context.getString(R.string.str_gps_is_not_enable));

            // On pressing Settings button
            alertDialog.setPositiveButton(context.getString(R.string.str_settings),
                    (dialog, which) -> {
                        dialog.dismiss();
                        alertDialog = null;
                        Intent intent = new Intent(
                                Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        context.startActivity(intent);
                    });

            alertDialog.setCancelable(false);

            dialog = alertDialog.create();
            // Showing Alert Message
            dialog.show();
        }
    }

    /**
     * Dismiss the GPS setting alert dialog
     */
    private void dismissSettingDialog() {

        try {
            if (dialog != null) {
                dialog.dismiss();
                alertDialog = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        dismissSettingDialog();
    }

//    @Override
//    public void onDestroyView() {
//        super.onDestroyView();
//
//    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case Constants.REQUEST_CODE_LOCATION:
                if (PermissionClass.checkPermission(permissions)) {

                    startLocationUpdatesService(homeActivity);
                } else {
                    if (!ActivityCompat.shouldShowRequestPermissionRationale(homeActivity, Manifest.permission.ACCESS_FINE_LOCATION)
                            || !ActivityCompat.shouldShowRequestPermissionRationale(homeActivity, Manifest.permission.ACCESS_COARSE_LOCATION)) {
                        alertDialogDeny(getString(R.string.location_permission));

                    } else {
                        alertDialog(Constants.CHECK_LOCATION_PERMISSION, false);
                    }
                }
                break;
        }
    }

    @Override
    public void onSuccessOfAddPost(String data) {
        dialog = DialogUtils.oneButtonDialog(homeActivity, getString(R.string.str_post_is_being_reviewed_notify_when_get_published), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
//                homeActivity.getNavigator().popFragment();
                homeActivity.removeFragmentUntil(HomeFragment.class);
            }
        });
        dialog.setCancelable(false);
    }

    @Override
    public void onFailureOfAddPost(String errorMsg, int errorCode) {
        showSnackBar(rootView, errorMsg, getString(R.string.str_ok), false, null);
    }

    @Override
    public void onSuccessOfAddTopic(String data) {

        dialog = DialogUtils.oneButtonDialog(homeActivity, getString(R.string.str_post_is_being_reviewed_notify_when_get_published), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
//                homeActivity.getNavigator().popFragment();
                homeActivity.removeFragmentUntil(HomeFragment.class);
            }
        });
        dialog.setCancelable(false);
    }

    @Override
    public void onFailureOfAddTopic(String errorMsg, int errorCode) {
        showSnackBar(rootView, errorMsg, getString(R.string.str_ok), false, null);
    }
}
