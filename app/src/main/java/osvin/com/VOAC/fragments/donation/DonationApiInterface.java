package osvin.com.VOAC.fragments.donation;

import android.content.Context;

import java.util.ArrayList;

import osvin.com.VOAC.models.TransactionModel;

public interface DonationApiInterface {

    interface VerifyTransactionApiResponse {
        void onVerifyTransactionApiSuccess(TransactionModel transactionModel);

        void onVerifyTransactionApiFailure(String message);
    }

    interface UpdateTransactionApiResponse {
        void onUpdateTransactionApiSuccess(String message);

        void onUpdateTransactionApiFailure(String message);
    }

    interface GetAllTransactionApiResponse {
        void onGetAllTransactionApiSuccess(int totalItemCount, byte loaderType, ArrayList<TransactionModel> transactions_list);

        void onGetAllTransactionApiFailure(String message);
    }

    void cancelRequest();

    void verify_transaction(String reference, Context context, VerifyTransactionApiResponse
            listener);

    void update_transaction(TransactionModel transactionModel, Context context, UpdateTransactionApiResponse
            listener);

    void get_transaction_Api(int page, final byte loaderType, Context context,
                             GetAllTransactionApiResponse listener);
}