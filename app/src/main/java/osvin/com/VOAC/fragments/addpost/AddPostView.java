package osvin.com.VOAC.fragments.addpost;

public interface AddPostView {

    void onSuccessOfAddPost(String data);

    void onFailureOfAddPost(String errorMsg, int errorCode);

    void onSuccessOfAddTopic(String data);

    void onFailureOfAddTopic(String errorMsg, int errorCode);
}
