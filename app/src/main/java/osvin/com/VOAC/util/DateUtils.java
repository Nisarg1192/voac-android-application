package osvin.com.VOAC.util;

import android.annotation.SuppressLint;
import android.text.format.DateFormat;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

@SuppressLint("SimpleDateFormat")
public class DateUtils {

    public static String current_timeToUTC() {
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = null;
        try {
            sdf1.setTimeZone(TimeZone.getTimeZone("UTC"));
            date = sdf1.format(new Date());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return date;
    }

    public static String deviceTimeToUTC(String time, int type) {
        SimpleDateFormat sdf1;
        if (type == 0)
            sdf1 = new SimpleDateFormat("HH:mm:ss");
        else if (type == 1)
            sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        else
            sdf1 = new SimpleDateFormat("yyyy-MM-dd");

        String date = null;
        try {
            Date date1 = sdf1.parse(time);

            sdf1.setTimeZone(TimeZone.getTimeZone("UTC"));
            date = sdf1.format(date1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return date;
    }

    public static String UTC_ToDeviceTime(String dateOne) {
        String dd = "";
        try {
            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            sdf1.setTimeZone(TimeZone.getTimeZone("UTC"));

            Date date = sdf1.parse(dateOne);

            sdf1.setTimeZone(TimeZone.getDefault());

            Date crdate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(sdf1.format(date));
            dd = sdf1.format(crdate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dd;
    }

    public static String UTC_ToDeviceTimeTransaction(String dateOne) {
        String dd = "";
        try {
            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            sdf1.setTimeZone(TimeZone.getTimeZone("UTC"));

            Date date = sdf1.parse(dateOne);

            sdf1.setTimeZone(TimeZone.getDefault());

            Date crdate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(sdf1.format(date));
            dd = sdf1.format(crdate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dd;
    }

    public static String returnTime(String DateTime) {
        Calendar calendar = Calendar.getInstance();
        try {
            Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(DateTime);
            calendar.setTime(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String hour = calendar.get(Calendar.HOUR_OF_DAY) + "";
        String minute = calendar.get(Calendar.MINUTE) + "";
        String second = calendar.get(Calendar.SECOND) + "";
        String am_pm;
        int h = Integer.parseInt(hour);
        if (h >= 12) {
            am_pm = "PM";
            if (h > 12) {
                h = h - 12;
                hour = h + "";
            }
        } else {
            am_pm = "AM";
        }
        if (hour.length() == 1) {
            hour = "0" + hour;
        }
        if (minute.length() == 1) {
            minute = "0" + minute;
        }
        return hour + ":" + minute + " " + am_pm;
    }


    public static String convertDateFromAPI(String strDob) {
        String date = null;
        SimpleDateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat df = new SimpleDateFormat("dd MMM, yyyy");
        try {
            date = df.format(df1.parse(strDob));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return date;
    }

    public static String convertDateTimeFromAPI(String strDob) {
        String date = null;
        SimpleDateFormat df1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat df = new SimpleDateFormat("MMM dd,yyyy 'at' h:mm a");
        try {
            date = df.format(df1.parse(strDob));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return date;
    }

    public static String convertDateTimeFullMonthAPI(String strDob) {
        String date = null;
        SimpleDateFormat df1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat df = new SimpleDateFormat("MMMM dd, yyyy");
        try {
            date = df.format(df1.parse(strDob));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return date;
    }

    public static String convertDate(String strDob) {
        String date = null;
        SimpleDateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat df = new SimpleDateFormat("dd MMM");
        try {
            date = df.format(df1.parse(strDob));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return date;
    }

    public static String convertAppointmentListTime(String strDob) {
        String date = null;
        SimpleDateFormat df1 = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat df = new SimpleDateFormat("h:mm a");
        try {
            date = df.format(df1.parse(strDob));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return date;
    }

    public static Date getFormatDate(String localDate) {

        Date convertedDate = null;
        try {
            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            convertedDate = sdf.parse(sdf.format(sdf1.parse(localDate)));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertedDate;
    }

    public static String getFormatTransactionDate(String localDate) {

        String convertedDate = null;
        try {
            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            SimpleDateFormat sdf = new SimpleDateFormat("MMM dd,yyyy 'at' h:mm a");
            convertedDate = sdf.format(sdf1.parse(localDate));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertedDate;
    }

    public static String getConvertedTime(String localDate) {

        String convertedDate = null;
        try {
            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            convertedDate = sdf.format(sdf1.parse(localDate));

        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertedDate;
    }

    public static String getCallDate(String dateSTR) {

        String convertedDate = null;
        try {
            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM, hh:mm:ss");
            convertedDate = sdf.format(sdf1.parse(dateSTR));

        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertedDate;
    }

    public static String getFormatTime(String localDate, int type) {

        String convertedDate = null;
        SimpleDateFormat sdf1 = null, sdf = null;
        try {
            sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            if (type == 0)
                sdf = new SimpleDateFormat("yyyy-MM-dd");
            else
                sdf = new SimpleDateFormat("HH:mm:ss");

            convertedDate = sdf.format(sdf1.parse(localDate));

        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertedDate;
    }

    public static String convertTimePickerTime(String strDob) {
        String date = null;
        SimpleDateFormat df1 = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat df = new SimpleDateFormat("h:mm a");
        try {
            date = df.format(df1.parse(strDob));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return date;
    }

    public static String CompareAndReturnDateToNotification(String DateTime) {
        String dateString = DateTime;
        Calendar calendar = Calendar.getInstance();
        try {
            SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd");
            Date current_date2 = formatter2.parse(formatter2.format(Calendar.getInstance().getTime()));
            Date selected_date2 = formatter2.parse(DateTime.split(" ")[0]);

            if (selected_date2.equals(current_date2)) {
                dateString = "Today";
            } else {
                long timeOne = selected_date2.getTime();
                long timeTwo = current_date2.getTime();
                long diff = timeTwo - timeOne;
                long diffDays = diff / (24 * 60 * 60 * 1000);

                if (diffDays == 0 || diffDays == 1) {
                    dateString = "Yesterday";
                } else if (diffDays > 0) {
                    calendar.setTime(selected_date2);
                    dateString = calendar.get(Calendar.DAY_OF_MONTH) + "/"
                            + (calendar.get(Calendar.MONTH) + 1) + "/"
                            + calendar.get(Calendar.YEAR);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dateString;
    }

    public static long getDays(String date) {
        SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");
        String currentDate = myFormat.format(new Date());
        long days = 0;
        try {
            Date date1 = myFormat.parse(date);
            Date date2 = myFormat.parse(currentDate);
            long diff = date1.getTime() - date2.getTime();
            days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return days;
    }


    public static String convert24hourFormatTime(String strDob) {
        String date = null;
        SimpleDateFormat df1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat df = new SimpleDateFormat("hh:mm a");
        try {
            date = df.format(df1.parse(strDob));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return date;
    }

    public static String getFormattedDate(String date) {

        Calendar calendar = Calendar.getInstance();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date mDate = null;

        try {
            mDate = sdf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        long timeInMilliseconds = mDate.getTime();

        calendar.setTimeInMillis(timeInMilliseconds);

        Calendar now = Calendar.getInstance();

        final String dateTimeFormatString = "EEEE, MMMM d";

        if (now.get(Calendar.DATE) == calendar.get(Calendar.DATE)) {
            return "Today";
        } else if (now.get(Calendar.DATE) - calendar.get(Calendar.DATE) == 1) {
            return "Yesterday";
        } else if (now.get(Calendar.YEAR) == calendar.get(Calendar.YEAR)) {
            return DateFormat.format(dateTimeFormatString, calendar).toString();
        } else {
            return DateFormat.format("MMMM dd yyyy", calendar).toString();
        }
    }

    public static long getDateInMillis(String srcDate) {

        String myDate = srcDate;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date;
        try {
            date = sdf.parse(myDate);
        } catch (ParseException e) {
            e.printStackTrace();
            return 0;
        }
        long millis = date.getTime();

        return millis;
    }
}