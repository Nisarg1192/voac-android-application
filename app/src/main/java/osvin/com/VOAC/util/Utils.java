package osvin.com.VOAC.util;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.net.ConnectivityManager;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;

import androidx.core.app.ShareCompat;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import osvin.com.VOAC.R;
import osvin.com.VOAC.base.MyApplication;
import osvin.com.VOAC.models.CommentModel;
import osvin.com.VOAC.models.MediaModel;
import osvin.com.VOAC.models.PostModel;

public class Utils {


    public static String getPrefDataString(Context context, String PrefKey) {
        SharedPreferences preferences = context.getSharedPreferences(Constants.APP_PREF, Context.MODE_PRIVATE);
        return preferences.getString(PrefKey, "");
    }

    public static boolean getPrefDataBoolean(Context context, String PrefKey) {
        SharedPreferences preferences = context.getSharedPreferences(Constants.APP_PREF, Context.MODE_PRIVATE);
        return preferences.getBoolean(PrefKey, false);
    }

    public static void setPrefDataString(Context context, String PrefKey, String strData) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(Constants.APP_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PrefKey, strData).apply();
    }

    public static void setPrefDataBoolean(Context context, String PrefKey, boolean boolData) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(Constants.APP_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(PrefKey, boolData).apply();
    }

    //====== Clear all saved preferences on logout =========//
    public static void Prefclear(Context context) {

        String username = getPrefDataString(context, Constants.USER_NAME_PERSIST);
        String password = getPrefDataString(context, Constants.USER_PASSWORD_PERSIST);
        String fcmToken = getPrefDataString(context, Constants.DEVICE_TOKEN);
        boolean remember_me = getPrefDataBoolean(context, Constants.PREF_REMEMBER_ME);

        SharedPreferences sharedPreferences = context.getSharedPreferences(Constants.APP_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor sharePrefEditor = sharedPreferences.edit();
        sharePrefEditor.clear();
        sharePrefEditor.apply();

        setPrefDataString(context, Constants.DEVICE_TOKEN, fcmToken);
        if (remember_me) {
            setPrefDataString(context, Constants.USER_NAME_PERSIST, username);
            setPrefDataString(context, Constants.USER_PASSWORD_PERSIST, password);
            setPrefDataBoolean(context, Constants.PREF_REMEMBER_ME, true);
        }
    }

    public static String getUserProfileImage(Context context) {
        if (!getPrefDataString(context, Constants.USER_PROFILE_IMAGE_URL).isEmpty()) {
            return getPrefDataString(context, Constants.USER_PROFILE_IMAGE_URL);
        } else {
            //return "https://www.gravatar.com/avatar/" + MD5Util.md5Hex(getPrefDataString(context, Constants.USER_EMAIL)) + ".jpg";
            return "";
        }


    }

    public static boolean checkInternetConnection(Context context) {
        ConnectivityManager conMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        assert conMgr != null;
        return conMgr.getActiveNetworkInfo() != null && conMgr.getActiveNetworkInfo().isAvailable()
                && conMgr.getActiveNetworkInfo().isConnected();
    }

    public static boolean isVideo(String url) {
        return url != null && url.endsWith("mp4");
    }

    public static Bitmap getImageFromUrl(String url) {
        MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();

        mediaMetadataRetriever.setDataSource(url, new HashMap<String, String>());
        Bitmap bmFrame = mediaMetadataRetriever.getFrameAtTime(1000); //unit in microsecond

        return bmFrame;
    }

    public static String saveImage(String url) {

        String fileName = url.substring(url.lastIndexOf("/") + 1).split("\\.")[0];

//        Bitmap bitmap = getImageFromUrl(url);

        return CacheStore.getInstance().saveCacheFile(fileName, url, fileName);
    }


    // Function to insert string
    public static String insertString(String originalString, String stringToBeInserted, int index) {

        // Create a new string
        String newString = "";

        for (int i = 0; i < originalString.length(); i++) {

            // Insert the original string character
            // into the new string
            newString += originalString.charAt(i);

            if (i == index) {

                // Insert the string to be inserted
                // into the new string
                newString += stringToBeInserted;
            }
        }

        // return the modified String
        return newString;
    }

    public static int getScreenHeight(Context context) {

        DisplayMetrics displaymetrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        windowManager.getDefaultDisplay()
                .getMetrics(displaymetrics);

        int height = displaymetrics.heightPixels;
        int width = displaymetrics.widthPixels;

        return height;
    }

    public static int getScreenWidth(Context context) {

        DisplayMetrics displaymetrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        windowManager.getDefaultDisplay()
                .getMetrics(displaymetrics);

        int height = displaymetrics.heightPixels;
        int width = displaymetrics.widthPixels;

        return width;
    }

    public static int getReactionImage(int reactionId) {

        switch (reactionId) {

            case Constants.REACTION_LIKE:

                return R.drawable.ic_fb_like;

            case Constants.REACTION_LOVE:

                return R.drawable.ic_fb_love;

            case Constants.REACTION_HAHA:

                return R.drawable.ic_fb_haha;

            case Constants.REACTION_SOCKED:

                return R.drawable.ic_fb_shocked;

            case Constants.REACTION_SAD:

                return R.drawable.ic_fb_sad;

            case Constants.REACTION_ANGRY:

                return R.drawable.ic_fb_angry;

            default:
                return R.drawable.ic_like_grey;
        }
    }

    /**
     * reduces the size of the image
     *
     * @param image
     * @param maxSize
     * @return Bitmap
     */
    public static Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    public static void shareContent(Activity activity, String intentTitle, String content) {
        ShareCompat.IntentBuilder.from(activity)
                .setType("text/plain")
                .setChooserTitle(intentTitle)
                .setText(content)
                .startChooser();
    }

    public static String getReactionsFormat(long reactionCount) {

        if (reactionCount == 1) {
            return reactionCount + " reaction";
        } else {
            return SetViews.format(reactionCount) + " reactions";
        }
    }


    public static List<PostModel> parseResponse(JSONObject response) {

        List<PostModel> postslist = new ArrayList<>();
        try {

            Debug.trace("POST_", "" + response.toString());
            String status = response.getString("status");
            if (status.toLowerCase().equals("ok")) {

                JSONArray responseArray = response.getJSONArray("posts");

                for (int i = 0; i < responseArray.length(); i++) {

                    JSONObject object = responseArray.getJSONObject(i);

                    postslist.add(parseResponseInModel(object));
                }

            } else {

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return postslist;
    }

    public static PostModel parseResponseInModel(JSONObject object) {

        PostModel postModel = new PostModel();

        try {
            postModel.setId(object.optString("id"));
            postModel.setType(object.optString("type"));
            postModel.setSlug(object.optString("slug"));
            postModel.setContent(object.optString("content"));
            postModel.setDate(object.optString("modified"));
            postModel.setPostDate(DateUtils.convertDateTimeFromAPI(object.optString("modified")));
            postModel.setStatus(object.optString("status"));
            postModel.setTitle(object.optString("title"));

            JSONArray categoryArray = object.optJSONArray("categories");
            for (int j = 0; j < categoryArray.length(); j++) {
                JSONObject cat_object = categoryArray.optJSONObject(j);
                postModel.setCategories(cat_object.optString("title"));

                if (postModel.getCategories().equals(Constants.CAT_I_NEED_HELP)) {
                    postModel.setCategoryVisibility(View.VISIBLE);
                } else {
                    postModel.setCategoryVisibility(View.GONE);
                }
            }

            if (object.has("comment_count")) {
                postModel.setComment_count(object.optLong("comment_count", 0));
                postModel.setFormattedCommentCount(getCommentCount(postModel));

                if (postModel.getComment_count() > 2) {
                    postModel.setCommentVisibility(View.VISIBLE);
                }
            }
            if (object.has("custom_fields")) {
                JSONObject object1 = object.optJSONObject("custom_fields");
                if (object1.has("location")) {
                    JSONArray jsonArray = object1.optJSONArray("location");
                    if (jsonArray.length() > 0) {
                        postModel.setLocation(jsonArray.optString(0));
                        postModel.setLocationVisibility(View.VISIBLE);
                    }
                }

                try {
                    if (object1.has("media_images") && !object1.isNull("media_images")) {
                        JSONArray jsonArray = object1.optJSONArray("media_images");
                        List<MediaModel> mediaFileModelList = new ArrayList<>();
                        for (int j = 0; j < jsonArray.length(); j++) {
                            MediaModel mediaModel = new MediaModel();
                            Object path = jsonArray.opt(j);

                            if (path instanceof String && Utils.isVideo(path.toString())) {
                                mediaModel.setMediumUrl(Utils.saveImage(path.toString()));
                                mediaModel.setUrl(path.toString());

                                mediaFileModelList.add(mediaModel);

                            } else if (path instanceof JSONObject) {
                                JSONObject jsonObject = (JSONObject) path;

                                if (jsonObject.has("vp_sm")) {
                                    String url = jsonObject.optJSONObject("vp_sm").optString("url");
                                    mediaModel.setMediumUrl(url);
                                    mediaModel.setHeight(jsonObject.optJSONObject("vp_sm").optInt("height"));
                                    mediaModel.setWidth(jsonObject.optJSONObject("vp_sm").optInt("width"));

                                    mediaModel.setUrl(jsonObject.optJSONObject("vp_md").optString("url"));

                                    mediaFileModelList.add(mediaModel);
                                } else if (jsonObject.has("url")) {
                                    mediaModel.setMediumUrl(jsonObject.optString("preview"));
                                    mediaModel.setUrl(jsonObject.optString("url"));
                                    mediaFileModelList.add(mediaModel);
                                }
                            }
                        }

                        postModel.setMediaFileModelList(mediaFileModelList);

                    } else {


                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (object1.has("reaction_id") && !object1.isNull("reaction_id")) {
                    postModel.setReactionId(object1.optString("reaction_id"));
                    postModel.setReactionImage(getReactionImage(Integer.parseInt(postModel.getReactionId())));
                }

                if (object1.has("subscribe") && !object1.isNull("subscribe")) {
                    postModel.setSubscriptionId(Integer.parseInt(object1.optString("subscribe", "2")));

                    if (postModel.getSubscriptionId() == Constants.SUBSCRIBE_ID) {
                        postModel.setSubscriptionImage(R.drawable.ic_subscribe);
                    }
                }

                if (object1.has("reaction_count") && !object1.isNull("reaction_count")) {
                    postModel.setReactionCounter(object1.optString("reaction_count", "0"));
                    postModel.setFormattedReactionCounter(getReactionsFormat(Long.parseLong(postModel.getReactionCounter())));

                    if (!object1.optString("reaction_count", "0").equals("0")) {
                        postModel.setReactionCountVisibility(View.VISIBLE);
                    }
                }
            }

            try {
                if (postModel.getMediaFileModelList().isEmpty()) {

                    JSONArray attachmentsArray = object.getJSONArray("attachments");
                    if (attachmentsArray.length() > 0) {

                        JSONObject attachmentsObject = attachmentsArray.getJSONObject(0);

                        if (attachmentsObject.has("images")) {

                            JSONObject imagesObject = attachmentsObject.getJSONObject("images");
                            JSONObject vp_lgObject = imagesObject.getJSONObject("vp_md");
                            JSONObject mediumObj = imagesObject.optJSONObject("vp_sm");

                            String url = vp_lgObject.getString("url");

                            MediaModel mediaModel = new MediaModel();

                            String mediumUrl = mediumObj.getString("url");
                            mediaModel.setMediumUrl(mediumUrl);
                            mediaModel.setHeight(mediumObj.optInt("height"));
                            mediaModel.setWidth(mediumObj.optInt("width"));

                            mediaModel.setUrl(url);

                            List<MediaModel> mediaFileModelList = new ArrayList<>();
                            mediaFileModelList.add(mediaModel);
                            postModel.setMediaFileModelList(mediaFileModelList);

                        } else {

                            String url = attachmentsObject.optString("url");
                            MediaModel mediaModel = new MediaModel();
                            if (Utils.isVideo(url) && object.has("custom_fields")) {
                                JSONObject customFieldObj = object.optJSONObject("custom_fields");
                                if (customFieldObj.has("attached_gif") && !customFieldObj.isNull("attached_gif")) {
                                    mediaModel.setMediumUrl(customFieldObj.optString("attached_gif", ""));
                                }

                            } else {
                                try {
                                    mediaModel.setMediumUrl(Utils.saveImage(url));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            mediaModel.setUrl(url);

                            List<MediaModel> mediaFileModelList = new ArrayList<>();
                            mediaFileModelList.add(mediaModel);
                            postModel.setMediaFileModelList(mediaFileModelList);

                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (object.has("comments")) {

                ArrayList<CommentModel> comment_list = new ArrayList<>();

                JSONArray commentsArray = object.optJSONArray("comments");

                int commentCount;
                if (commentsArray != null && commentsArray.length() > 2) {
                    commentCount = 2;
                } else {
                    commentCount = commentsArray != null ? commentsArray.length() : 0;
                }

                for (int j = 0; j < commentCount; j++) {
                    JSONObject comment_object = commentsArray.optJSONObject(j);
                    CommentModel commentModel = new CommentModel();
                    commentModel.setId(comment_object.optString("id"));
                    commentModel.setName(comment_object.optString("name"));
                    commentModel.setContent(comment_object.optString("content"));
                    commentModel.setDate(comment_object.optString("date"));

                    if (comment_object.has("author")) {
                        JSONObject authorObject = comment_object.optJSONObject("author");
                        if (authorObject.has("email")) {
                            commentModel.setEmail(authorObject.optString("email"));
                        }
                        if (authorObject.has("profile")) {
                            commentModel.setProfile(authorObject.optString("profile"));
                        }
                    }
                    comment_list.add(commentModel);
                }

                postModel.setComment_list(comment_list);
            }

            JSONObject authorObject = object.optJSONObject("author");
            postModel.setAuthor_id(authorObject.optString("id"));

            if (!postModel.getAuthor_id().equals(Utils.getPrefDataString(MyApplication.getInstance(), Constants.USER_ID))) {
                postModel.setSubscribeVisibility(View.VISIBLE);
            }

            if (authorObject.has("profile")) {
                postModel.setAuthor_image(authorObject.optString("profile"));
            }
            postModel.setAuthor_name(authorObject.optString("name"));
            if (authorObject.has("email")) {
                postModel.setAuthor_email(authorObject.optString("email"));
            }

            if (postModel.getMediaFileModelList() != null && !postModel.getMediaFileModelList().isEmpty()) {

                if (postModel.getMediaFileModelList().size() > 1) {
                    postModel.setIsMultipleVisible(View.VISIBLE);
                } else {
                    postModel.setIsMultipleVisible(View.GONE);
                }

            } else {
                postModel.setIsMultipleVisible(View.GONE);
                List<MediaModel> list = new ArrayList<>();
                MediaModel mediaFileModel = new MediaModel();
                list.add(mediaFileModel);
                postModel.setMediaFileModelList(list);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return postModel;
    }

    public static String getCommentCount(PostModel postModel) {
        long comment_count = postModel.getComment_count();
        String comment_str = comment_count == 1 ? comment_count + " comment" : SetViews.format(comment_count) + " comments";
        return comment_str;
    }

    @SuppressLint("HardwareIds")
    public static String DeviceId(Context ctx) {
        return Settings.Secure.getString(ctx.getContentResolver(), Settings.Secure.ANDROID_ID);
    }
}


