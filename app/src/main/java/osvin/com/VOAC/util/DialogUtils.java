package osvin.com.VOAC.util;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import java.util.Objects;

import osvin.com.VOAC.R;


public class DialogUtils {

    public static Dialog twoButtonDialog(final Context ctx, String message, View.OnClickListener onClickListener) {

        final Dialog MessagePopup = getCustomDialog(ctx, R.layout.dialog_logout, true);

        ((TextView) MessagePopup.findViewById(R.id.message)).setText(message);
        (MessagePopup.findViewById(R.id.tv_yes)).setOnClickListener(onClickListener);
        (MessagePopup.findViewById(R.id.tv_no)).setOnClickListener((View v) -> MessagePopup.dismiss());

        return MessagePopup;
    }

    public static Dialog oneButtonDialog(final Context ctx, String message, View.OnClickListener onClickListener) {

        final Dialog MessagePopup = getCustomDialog(ctx, R.layout.dialog_onebtn, true);
        ((TextView) MessagePopup.findViewById(R.id.txt_message)).setText(Html.fromHtml(message));
        MessagePopup.findViewById(R.id.tv_ok).setOnClickListener(onClickListener);

        return MessagePopup;
    }

    public static Dialog oneButtonDialogNonCancelable(final Context ctx, String message, View.OnClickListener onClickListener) {

        final Dialog MessagePopup = getCustomDialog(ctx, R.layout.dialog_onebtn, false);
        ((TextView) MessagePopup.findViewById(R.id.txt_message)).setText(Html.fromHtml(message));
        MessagePopup.findViewById(R.id.tv_ok).setOnClickListener(onClickListener);

        return MessagePopup;
    }

    public static Dialog getCustomDialog(Context mContext, int layoutID, boolean cancelable) {

        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(layoutID);
        dialog.setCancelable(cancelable);
        dialog.setCanceledOnTouchOutside(cancelable);
        dialog.show();

        return dialog;
    }

    public static Dialog twoButtonDialog(final Context ctx, String message, View.OnClickListener yesClickListener, View.OnClickListener noClickListener) {

        final Dialog MessagePopup = getCustomDialog(ctx, R.layout.dialog_logout, true);

        ((TextView) MessagePopup.findViewById(R.id.message)).setText(message);
        (MessagePopup.findViewById(R.id.tv_yes)).setOnClickListener(yesClickListener);
        (MessagePopup.findViewById(R.id.tv_no)).setOnClickListener(yesClickListener);

        return MessagePopup;
    }
}