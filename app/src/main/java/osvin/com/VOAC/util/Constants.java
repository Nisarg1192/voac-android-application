package osvin.com.VOAC.util;

public interface Constants {

    String ADS_UNIT_ID = "ca-app-pub-1724001189929126/1407165453"; // ca-app-pub-1724001189929126/1407165453, ca-app-pub-1724001189929126/3665799182
    String ADS_APP_ID = "ca-app-pub-1724001189929126~4612528271";

    String APP_PREF = "app_pref";
    String DEVICE_TOKEN = "device-token";
    String USER_COOKIE = "user_cookie";
    String USER_EMAIL = "user_email";
    String USER_NAME = "user_name";
    String USER_USERNAME = "user_username";
    String USER_PROFILE_IMAGE_URL = "user_profile_image";
    String USER_ID = "user_id";
    String USER_LOGIN_STATUS = "user_login_status";
    String PREF_NOTIFICATION = "notification";
    String USER_NAME_PERSIST = "user_name_persist";
    String USER_PASSWORD_PERSIST = "user_password_persist";
    String PREF_REMEMBER_ME = "remember_me";

    String FACEBOOK_CONTACT = "http://facebook.com/The-Voice-Of-The-African-Child-Foundation-366286940603093/?ref=m_notif&amp;notif_t=page_admin";
    String TWITTER_CONTACT = "http://twitter.com/VoiceOfTheAfri1?s=08";
    String INSTAGRAM_CONTACT = "https://www.instagram.com/votacf/";
    String YOUTUBE_CONTACT = "https://www.youtube.com/channel/UCQd2vKEIuRgQ_aW3o0tLrig";

    String FACEBOOK_FEED = "https://www.voiceoftheafricanchildfoundation.com/facebook-feed-2/";
    String TWITTER_FEED = "https://www.voiceoftheafricanchildfoundation.com/twitter-feed-2/";
    String INSTAGRAM_FEED = "https://www.voiceoftheafricanchildfoundation.com/instagram-feed-2/";
    String pdfURL = "https://www.voiceoftheafricanchildfoundation.com/childPolicy.pdf";

    String BASE_URL = "https://www.voiceoftheafricanchildfoundation.com/";//Live
//    String BASE_URL = "http://voiceofafricanchild.sufalam.live/";//Test
//    String BASE_URL = "http://dev.voiceoftheafricanchildfoundation.com/";//Test
//    String BASE_URL = "http://192.168.1.47/voac/";//Local


//    String BASE_URL_APP_TEST = "http://voiceapp.sufalam.live/";//Test
//    String ABOUT_US = "api/Admin/get_about_us_data";
//    String BOARD_OF_TRUSTEES = "api/Admin/board_of_trustees";
//    String GET_HOME_DATA = "api/Admin/get_home_data";


    String ABOUT_US = "app/api/Admin/get_about_us_data";
    String BOARD_OF_TRUSTEES = "app/api/Admin/board_of_trustees";
    String GET_HOME_DATA = "app/api/Admin/get_home_data";

    String CONTACT_SUBMIT = "api/user/contact_us_submit";

    String LOGIN = "api/user/login";
    String GET_NONCE = "api/get_nonce";
    String REGISTER = "api/user/register";
    String RESET_PASSWORD = "api/user/retrieve_password";
    String GET_PROFILE = "api/user/get_userinfo";
    String GET_ARICLES = "api/get_posts";
    String GET_MY_POSTS = "api/get_my_posts";
    String UPLOAD_POST_MEDIA = "api/user/upload_post_media";
    String CREATE_POST = "api/posts/create_post";
    String UPDATE_PROFILE = "/api/user/update_profile";
    String UPLOAD_PROFILE_IMAGE = "api/user/upload_profile";
    String LOGOUT = "api/user/logout";
    String CHANGE_PASSWORD = "api/user/change_password";
    String GET_ARTICLE_DETAIL = "api/get_post";
    String GET_OWN_ARTICLE_DETAIL = "api/get_my_single_post";
    String GET_AUTHOR_POSTS = "api/get_author_posts";
    String SUBMIT_COMMENT = "api/respond/submit_comment";
    String SUBMIT_VOLUNTEER = "api/user/volunteer_submit";
    String SAVE_TRANSACTION = "/api/user/save_transaction";
    String GET_TRANSACTION_LIST = "api/user/get_my_txn_listing";
    String UPDATE_POST = "api/posts/update_post";
    String DELETE_POST = "api/posts/delete_post";
    String GET_NOTIFICATION_STATUS = "api/user/getnotification";
    String UPDATE_NOTIFICATION_STATUS = "api/user/setnotification";
    String GET_EVENT_POST = "api/get_event_post";
    String GET_POST_USER_LIST = "api/user/list_posts";
    String SAVE_POST_REACTION = "api/user/save_post_reaction/";
    String POST_REACTION_COUNT = "api/user/get_post_reaction/";
    String GET_REACTION_USER = "api/user/get_post_reaction_user/";
    String GET_PROJECT = "api/user/get_projects/";
    String GET_ALL_COMMENTS = "api/respond/get_all_comments/";
    String USER_SUBSCRIBE = "api/user/subcribe/";
    String CREATE_POST_FAIL = "api/user/create_post_fail/";

    String INSECURE = "cool";
    int DEVICE_TYPE = 1;

    String RESPONSE_SUCCESS = "ok";

    byte ADD_POST = 1;
    byte EDIT_POST = 2;

    byte DEFAULT_INT = 0;
    String DEFAULT_STRING = "";
    boolean DEFAULT_BOOLEAN = false;

    byte ANDROID = 1;

    String AUDIO = "audio";
    String VIDEO = "video";
    String IMAGE = "image";

    short BIG_IMAGE = 500;
    short SMALL_IMAGE = 200;
    int PAGINATION_LIMIT = 10;
    int PAGE_STARTING_INDEX = 1;

    short REQUEST_CODE_CAMERA = 207;
    short REQUEST_CODE_GALLERY = 208;
    short REQUEST_CODE_MICROPHONE = 209;
    short REQUEST_CODE_LOGIN = 210;
    short REQUEST_CODE_CROP_IMAGE = 211;
    short REQUEST_CODE_PERSON_ARTICLE_DETAIL = 212;
    short REQUEST_CODE_REGISTER = 213;
    short REQUEST_CODE_ADD_NEW_POST = 214;
    short REQUEST_CODE_UPDATE_PROFILE = 215;
    short REQUEST_CODE_UPDATE_POST = 216;
    short REQUEST_CODE_COMMENTS = 217;
    short REQUEST_CODE_ARTICLE_DETAIL = 218;
    short REQUEST_CODE_LOCATION = 219;
    short RESULT_CODE_DELETE_POST = 220;

    byte CHECK_CAMERA_PERMISSION = 0;
    byte CHECK_GALLERY_PERMISSION = 1;
    byte CHECK_MICROPHONE_PERMISSION = 2;
    byte CHECK_LOCATION_PERMISSION = 3;

    byte GET_NOTIFICATION = 1;
    byte SET_NOTIFICATION = 2;

    byte LOADER_NORMAL = 1;
    byte LOADER_SWIPE = 2;
    byte LOADER_LOAD_MORE = 3;

    String KEY_MODEL = "keyModel";

    /**
     * like =1
     * heart=2
     * laugh=3
     * shocked=4
     * sad=5
     * angry=6
     */
    short REACTION_LIKE = 1;
    short REACTION_LOVE = 2;
    short REACTION_HAHA = 3;
    short REACTION_SOCKED = 4;
    short REACTION_SAD = 5;
    short REACTION_ANGRY = 6;

    String FROM_PUSH_NOTIFICATION = "isFromPushNotification";
    String POST_ID = "postId";

    String POST_TYPE = "postType";

    int POST_TYPE_POST = 1;
    int POST_TYPE_TOPIC = 2;
    String FROM_DEEP_LINK = "fromDeepLink";
    String CAT_I_NEED_HELP = "I Need Help";

    int SUBSCRIBE_ID = 1;
    int UNSUBSCRIBE_ID = 2;

    int PAGE_START = 1;
}
