package osvin.com.VOAC.util;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;

import androidx.appcompat.widget.AppCompatEditText;

public class HideKeyboardEdittext extends AppCompatEditText {

    private onBackPressEvent onBackPressEvent;

    public HideKeyboardEdittext.onBackPressEvent getOnBackPressEvent() {
        return onBackPressEvent;
    }

    public void setOnBackPressEvent(HideKeyboardEdittext.onBackPressEvent onBackPressEvent) {
        this.onBackPressEvent = onBackPressEvent;
    }

    public HideKeyboardEdittext(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public HideKeyboardEdittext(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public HideKeyboardEdittext(Context context) {
        super(context);
    }

    public boolean onKeyPreIme(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK &&
                event.getAction() == KeyEvent.ACTION_UP) {
            // do your stuff
            if (getOnBackPressEvent() != null) {
                getOnBackPressEvent().onBackPress(true);
            }
            return false;
        }
        return super.dispatchKeyEvent(event);
    }

    public interface onBackPressEvent {
        void onBackPress(boolean isBackPress);
    }
}
