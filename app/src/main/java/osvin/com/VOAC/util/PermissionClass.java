package osvin.com.VOAC.util;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Build;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import osvin.com.VOAC.base.MyApplication;

public class PermissionClass {

    public static final String CAMERA = Manifest.permission.CAMERA;
    public static final String WRITE_STORAGE = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    public static final String READ_CONTACTS = Manifest.permission.READ_CONTACTS;
    public static final String RECEIVE_SMS = Manifest.permission.RECEIVE_SMS;
    public static final String FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
    public static final String COARSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION;


//    public static final String CALL_PHONE = Manifest.permission.CALL_PHONE;

    public static final int FILE_CHOOSER_PERMISSION_REQUEST_CODE = 1000;
    public static final int LOCATION_PERMISSION_REQUEST_CODE = 1001;
    public static final int READ_PHONE_PERMISSION_REQUEST_CODE = 1002;

    private static PermissionListener permissionListener;
    private static int requestCodeForPermission = 0;

    public static String[] getLocationPermission() {
        return new String[]{FINE_LOCATION, COARSE_LOCATION};
    }

    /**
     * Return true if the all permission granted by user else false
     *
     * @param permissions array of permissions
     */
    public static boolean checkPermission(String[] permissions) {

        for (String permission1 : permissions) {

            int permission = ActivityCompat.checkSelfPermission(MyApplication.getInstance(), permission1);

            if (permission != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    public static boolean shouldGoToSettings(int[] grantResults, String[] permissions, Activity context) {

        for (int i = 0; i < grantResults.length; i++) {

            String permission = permissions[i];

            if (grantResults[i] == PackageManager.PERMISSION_DENIED) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    boolean showRationale = context.shouldShowRequestPermissionRationale(permission);
                    if (!showRationale) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static boolean shouldGoToSettings(int[] grantResults, String[] permissions, Fragment fragment) {

        for (int i = 0; i < grantResults.length; i++) {

            String permission = permissions[i];

            if (grantResults[i] == PackageManager.PERMISSION_DENIED) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    boolean showRationale = fragment.shouldShowRequestPermissionRationale(permission);
                    if (!showRationale) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static void requestPermission(Activity activity, String[] permissions, int requestCode) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestCodeForPermission = requestCode;
            activity.requestPermissions(permissions, requestCode);
        }
    }

    public static void requestPermission(Fragment fragment, String[] permissions, int requestCode) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestCodeForPermission = requestCode;
            fragment.requestPermissions(permissions, requestCode);
        }
    }

//    public static void onRequestPermissionsResult(@NonNull int requestCode, @NonNull final String[] permissions,
//                                                  @NonNull int[] grantResults, @NonNull final Activity activity) {
//
//        if (requestCodeForPermission == requestCode) {
//
//            if (!checkPermission(permissions)) {
//
//
//                if (PermissionClass.shouldGoToSettings(grantResults, permissions, activity)) {
//
//                    CustomDialog.getInstance().showAlertDialog(activity, activity.getString(R.string.str_please_allow_required_permission), activity.getString(R.string.str_settings), true
//                            , (dialog, which) -> {
//                                dialog.dismiss();
//                                Intent intent = new Intent();
//                                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
//                                Uri uri = Uri.fromParts("package", SadadPosApp.getInstance().getPackageName(), null);
//                                intent.setData(uri);
//                                activity.startActivity(intent);
//                            }, (dialog, which) -> {
//                                dialog.dismiss();
//                                if (activity != null) {
//                                    activity.finish();
//                                }
//
//                            });
//
//                } else {
//                    if (permissionListener != null) {
//                        permissionListener.onFailure();
//                    }
//                }
//            } else {
//                if (permissionListener != null) {
//                    permissionListener.onSuccess();
//                }
//            }
//        }
//    }
//
//    public static void onRequestPermissionsResult(@NonNull int requestCode, @NonNull final String[] permissions,
//                                                  @NonNull int[] grantResults, @NonNull final Fragment fragment) {
//
//        if (requestCodeForPermission == requestCode) {
//
//            if (!checkPermission(permissions)) {
//
//                if (PermissionClass.shouldGoToSettings(grantResults, permissions, fragment)) {
//
//                    CustomDialog.getInstance().showAlertDialog(fragment.getActivity(), fragment.getString(R.string.str_please_allow_required_permission), fragment.getString(R.string.str_settings), true, new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            dialog.dismiss();
//                            Intent intent = new Intent();
//                            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
//                            Uri uri = Uri.fromParts("package", SadadPosApp.getInstance().getPackageName(), null);
//                            intent.setData(uri);
//                            fragment.startActivity(intent);
//                        }
//                    }, (dialog, which) -> {
//                        dialog.dismiss();
//                        if (fragment.getActivity() != null) {
//                            fragment.getActivity().finish();
//                        }
//
//                    });
//
//
//                } else {
//
//                    if (permissionListener != null) {
//                        permissionListener.onFailure();
//                    }
//                }
//            } else {
//                if (permissionListener != null) {
//                    permissionListener.onSuccess();
//                }
//            }
//        }
//    }

    public static void setSuccessListener(PermissionListener successListener) {

        permissionListener = successListener;
    }


    public interface PermissionListener {

        void onSuccess();

        void onFailure();
    }
}
