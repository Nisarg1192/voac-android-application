package osvin.com.VOAC.util;

import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import osvin.com.VOAC.R;
import osvin.com.VOAC.base.MyApplication;

/**
 * Created by Hitesh sarsava on 17/1/18.
 */

public class Debug {

    private static final String TAG = "Debug";
    private static final String LOG_FILE_NAME = MyApplication.getInstance().getString(R.string.app_name) + ".txt";
    private static boolean isDebug = false;
    private static boolean isPersistant = false;

    /**
     * This method print message in LOG window
     *
     * @param msg(String) : message to print in log
     */
    public static void trace(final String msg) {
        try {
            if (isDebug) {
                if (!TextUtils.isEmpty(msg)) {
                    Log.d(TAG, msg);
                }
                if (isPersistant) {
                    appendLog(msg);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method print message in LOG window
     * with keyTag
     *
     * @param tag(String) : it shows the particular class name
     * @param msg(String) : to showProgressBar the appropriate result
     */
    public static void trace(final String tag, final String msg) {
        try {
            if (isDebug) {
                if (!TextUtils.isEmpty(tag) && !TextUtils.isEmpty(msg)) {
                    Log.d(tag, msg);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method create new text file and save all the trace in sadad.txt
     *
     * @param text(String) : it pass the appropriate result
     */
    private static void appendLog(final String text) {
        final File logFile = new File(Environment.getExternalStorageDirectory() + LOG_FILE_NAME);
        if (!logFile.exists()) {
            try {
                logFile.createNewFile();
            } catch (final IOException e) {
                e.printStackTrace();
            }
        }
        try {
            // BufferedWriter for performance, true to set append to file flag
            final BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
            buf.append(text);
            buf.newLine();
            buf.close();
        } catch (final IOException e) {
            e.printStackTrace();
        }
    }

}
