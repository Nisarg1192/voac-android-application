package osvin.com.VOAC.util;

import android.app.Activity;
import android.app.Dialog;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.widget.LinearLayout;

import androidx.appcompat.widget.AppCompatTextView;

import osvin.com.VOAC.R;

public class CustomDialog {

    public static Dialog loadDialog = null;
//    public static AlertDialog dialog = null;

    public static void showLoadDialog(Activity context, String message) {

        try {
            if (loadDialog != null) {
                if (loadDialog.isShowing())
                    loadDialog.dismiss();
            }
            loadDialog = new Dialog(context, R.style.TransparentDialogTheme);
            loadDialog.setContentView(R.layout.spinner);
            loadDialog.setCanceledOnTouchOutside(false);
            loadDialog.setOnKeyListener((dialog, keyCode, event) -> {
                if (keyCode == KeyEvent.KEYCODE_BACK)
                    return true;
                return false;
            });

            if (!TextUtils.isEmpty(message)) {
                AppCompatTextView tvMessage = loadDialog.findViewById(R.id.tvMessage);
                tvMessage.setText(message);
                LinearLayout linearLayout = loadDialog.findViewById(R.id.rootView);
                linearLayout.setBackground(context.getDrawable(R.drawable.drw_rectangle_curve_filled_white));
            }

            if (!context.isFinishing())
                loadDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showLoadDialog(Activity context) {

        showLoadDialog(context, "");
    }

    public static void hideLoadDialog() {
        try {
            if (loadDialog != null && loadDialog.isShowing())
                loadDialog.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
