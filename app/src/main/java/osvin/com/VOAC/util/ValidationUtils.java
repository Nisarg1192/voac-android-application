package osvin.com.VOAC.util;

import android.text.TextUtils;
import android.widget.EditText;
import android.widget.TextView;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidationUtils {



    public static boolean isEmailValid(String email) {
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        return email.trim().matches(emailPattern);
    }

    public static boolean isValidEmail(EditText editText) {
        return !TextUtils.isEmpty(editText.getText().toString())
                && android.util.Patterns.EMAIL_ADDRESS.matcher(editText.getText().toString().trim()).matches();
    }

    public static boolean ValidateEditText(EditText editText) {
        return !TextUtils.isEmpty(editText.getText().toString().trim()) && editText.getText().toString().trim().length() > 0;
    }

    public static boolean ValidateTextView(TextView textView) {
        return !TextUtils.isEmpty(textView.getText().toString().trim()) && textView.getText().toString().trim().length() > 0;
    }

    public static int editTextLength(EditText edittext) {
        return edittext.getText().toString().trim().length();
    }

    public static int textViewLength(TextView textview) {
        return textview.getText().toString().trim().length();
    }

    public static boolean editTextCompare(EditText et_one, EditText et_two) {
        return et_one.getText().toString().trim().equals(et_two.getText().toString().trim());
    }

    ////// check if password is valid //////
    public static boolean isValidPassword(final String password) {
        Pattern pattern;
        Matcher matcher;
        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{8,}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);
        return matcher.matches();
    }

    public static boolean ValidatePassword(String password) {
        return password.trim().length() < 6;
    }
}