package osvin.com.VOAC.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.jsoup.Jsoup;

/**
 * Created by Hitesh Sarsava on 13,November,2018
 * <p>
 * This class is used to check current version of app on play store,
 * but it needs compulsory internet connection. so check for internet connection before using this class.
 */
public class GetVersionCode extends AsyncTask<Void, String, String> {

    private onAppVersionCodeCheckListener onAppVersionCodeCheckListener;
    @SuppressLint("StaticFieldLeak")
    private Context context;
    private boolean isException = false;

    public GetVersionCode(Context context, onAppVersionCodeCheckListener onAppVersionCodeCheckListener) {
        this.context = context;
        this.onAppVersionCodeCheckListener = onAppVersionCodeCheckListener;
    }

    @Override
    protected String doInBackground(Void... voids) {

        try {

            return Jsoup.connect("https://play.google.com/store/apps/details?id=" + context.getPackageName() + "&hl=en")
                    .timeout(30000)
                    .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                    .referrer("http://www.google.com")
                    .get()
                    .select("div.hAyfc:nth-child(4) > span:nth-child(2) > div:nth-child(1) > span:nth-child(1)")
                    .first()
                    .ownText();

        } catch (Exception e) {

            isException = true;

            return e.getLocalizedMessage();
        }
    }

    @Override

    protected void onPostExecute(String onlineVersion) {
        super.onPostExecute(onlineVersion);

        Debug.trace("currentVersion", onlineVersion);

        if (isException) {
            onAppVersionCodeCheckListener.onException(onlineVersion);
        } else {
            onAppVersionCodeCheckListener.onGetVersion(onlineVersion);
        }
    }

    public interface onAppVersionCodeCheckListener {

        void onGetVersion(String onlineVersion);

        void onException(String errorMsg);
    }
}