package osvin.com.VOAC.interfaces;

public interface OnItemClickListener {
    void onItemClick(int position);
}