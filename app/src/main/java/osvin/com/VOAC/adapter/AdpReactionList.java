package osvin.com.VOAC.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import osvin.com.VOAC.R;
import osvin.com.VOAC.imageloader.ImageUtils;
import osvin.com.VOAC.models.ReactionUserModel;

public class AdpReactionList extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<ReactionUserModel> reactionUserList;
    private View.OnClickListener onClickListener;


    public AdpReactionList(Context context, List<ReactionUserModel> reactionUserList, View.OnClickListener onClickListener) {
        this.context = context;
        this.reactionUserList = reactionUserList;
        this.onClickListener = onClickListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ReactionUserView(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_reaction_list, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof ReactionUserView) {
            ReactionUserModel reactionUserModel = reactionUserList.get(position);
            ReactionUserView reactionUserView = (ReactionUserView) holder;

            reactionUserView.tvUserName.setText(reactionUserModel.getUserName());
            ImageUtils.loadCircle(reactionUserModel.getProfilePhoto(), context, R.drawable.ic_profile_pic, reactionUserView.ivUser);
        }
    }

    @Override
    public int getItemCount() {
        return reactionUserList.size();
    }

    public void refreshList(List<ReactionUserModel> reactionUserList) {
        this.reactionUserList = reactionUserList;
        notifyDataSetChanged();
    }

    class ReactionUserView extends RecyclerView.ViewHolder {

        @BindView(R.id.tvUserName)
        TextView tvUserName;
        @BindView(R.id.ivUser)
        AppCompatImageView ivUser;

        public ReactionUserView(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
