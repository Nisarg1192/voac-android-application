package osvin.com.VOAC.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import osvin.com.VOAC.R;
import osvin.com.VOAC.imageloader.ImageUtils;
import osvin.com.VOAC.models.PostModel;
import osvin.com.VOAC.util.Utils;

public class PostListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private List<PostModel> list;
    //    private String userId = "";
    private View.OnClickListener onClickListener;
    private int margin = 0;
    private int blockSize = 0;
    private static final int LOADING = 0;
    private static final int STATUS_SIMPLE_POST = 1;
    private boolean showFooter = false;

    public PostListAdapter(Context context, List<PostModel> data, View.OnClickListener onClickListener) {
        this.list = data;
        this.mContext = context;
        this.onClickListener = onClickListener;
//        userId = Utils.getPrefDataString(mContext, Constants.USER_ID);

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        if (viewType == STATUS_SIMPLE_POST) {

            View transactionHistoryView = LayoutInflater.from(mContext).inflate(R.layout.custom_post_list_item, parent, false);
            return new MyViewHolder(transactionHistoryView);

        } else if (viewType == LOADING) {

            View viewLoading = LayoutInflater.from(mContext).inflate(R.layout.list_item_footer_progress_bar, parent, false);
            return new DataLoading(viewLoading);
        }

        throw new RuntimeException("there is no type matches" + viewType + "\n make sure you are using the correct type");
//        return new MyViewHolder(LayoutInflater.from(parent.getContext())
//                .inflate(R.layout.custom_post_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {

//        try {

        if (viewHolder instanceof MyViewHolder) {

            MyViewHolder holder = (MyViewHolder) viewHolder;
            PostModel postModel = list.get(position);
            postModel.setPosition(position);
            holder.ll_main.setTag(postModel);

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(blockSize, blockSize);

            int tempPosition = position + 1;
            layoutParams.topMargin = 0;
            if (tempPosition == 1 || tempPosition % 3 == 1) {
                layoutParams.rightMargin = margin;
                layoutParams.leftMargin = 0;
            } else if (tempPosition % 3 == 0) {
                layoutParams.leftMargin = margin;
                layoutParams.rightMargin = 0;
            } else {
                layoutParams.leftMargin = 0;
                layoutParams.rightMargin = 0;
            }

            if (tempPosition == list.size() || tempPosition == list.size() - 1 || tempPosition == list.size() - 2) {
                layoutParams.bottomMargin = 0;
            } else {
                layoutParams.bottomMargin = margin * 2;
            }

            holder.flMain.setLayoutParams(layoutParams);

            if (postModel.getMediaFileModelList() != null && !postModel.getMediaFileModelList().isEmpty()) {

                if (postModel.getMediaFileModelList().size() > 1) {
                    holder.ivPhotoLibrary.setVisibility(View.VISIBLE);
                } else {
                    holder.ivPhotoLibrary.setVisibility(View.GONE);
                }

                String imagePath = postModel.getMediaFileModelList().get(0).getMediumUrl();
                String imageUrl = postModel.getMediaFileModelList().get(0).getUrl();

                if (Utils.isVideo(imageUrl)) {
                    holder.ivVideoPlay.setVisibility(View.VISIBLE);
                    if (TextUtils.isEmpty(imagePath)) {
                        imagePath = imageUrl;
                    }

                    ImageUtils.loadGIF(imagePath, mContext, R.color.whitePressed, true, holder.ivPostImage);
                } else {
                    holder.ivVideoPlay.setVisibility(View.GONE);
                    if (TextUtils.isEmpty(imagePath)) {
                        imagePath = imageUrl;
                    }

                    ImageUtils.load(imagePath, mContext, R.color.whitePressed, true, holder.ivPostImage);
                }


            } else {

                holder.ivPhotoLibrary.setVisibility(View.GONE);
                holder.ivVideoPlay.setVisibility(View.GONE);

                ImageUtils.load(R.color.whitePressed, mContext, R.color.whitePressed, holder.ivPostImage);
            }

        } else if (viewHolder instanceof DataLoading) {

//                ((DataLoading) viewHolder).llLoadingView.setLayoutParams(layoutParams);
            DataLoading dataLoading = (DataLoading) viewHolder;
            if (showFooter) {
                dataLoading.itemView.setVisibility(View.VISIBLE);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(blockSize, blockSize);
                dataLoading.itemView.setLayoutParams(layoutParams);
            } else {
                dataLoading.itemView.setVisibility(View.GONE);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, 0);
                dataLoading.itemView.setLayoutParams(layoutParams);
            }
        }

//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    @Override
    public int getItemViewType(int position) {

        if ((position + 1) == getItemCount()) {
            return LOADING;
        } else {
            return STATUS_SIMPLE_POST;
        }
    }

    @Override
    public int getItemCount() {
//        if (showFooter) {
        return list.size() + 1;
//        } else {
//            return list.size();
//        }
    }

    public void setShowFooter(boolean state) {
        this.showFooter = state;
//        notifyDataSetChanged();
//        notifyItemRangeChanged(getItemCount() - 1, getItemCount());
    }

    public void setMargin(int margin) {
        this.margin = margin;
    }

    public void setBlockSize(int blockSize) {
        this.blockSize = blockSize;
    }

    public void refreshList(List<PostModel> list) {
//        int position = getItemCount();
//        this.list.addAll(list);
//        notifyItemRangeChanged(position, list.size());
        this.list = list;
        notifyDataSetChanged();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout ll_main;
        private FrameLayout flMain;
        private AppCompatImageView ivPhotoLibrary;
        private AppCompatImageView ivVideoPlay;
        private AppCompatImageView ivPostImage;

        MyViewHolder(View itemView) {
            super(itemView);
            flMain = itemView.findViewById(R.id.flMain);
            ll_main = itemView.findViewById(R.id.cl_main);
            ll_main.setOnClickListener(onClickListener);
//            iv_video_icon_image = itemView.findViewById(R.id.iv_video_icon_image);
            ivPhotoLibrary = itemView.findViewById(R.id.ivPhotoLibrary);
            ivVideoPlay = itemView.findViewById(R.id.ivVideoPlay);
            ivPostImage = itemView.findViewById(R.id.ivPostImage);
        }
    }

    public class DataLoading extends RecyclerView.ViewHolder {

        ProgressBar dataLoadingProgress;
        LinearLayout llLoadingView;

        DataLoading(View itemView) {
            super(itemView);

            dataLoadingProgress = itemView.findViewById(R.id.dataLoadingProgress);
            llLoadingView = itemView.findViewById(R.id.llLoadingView);
        }
    }
}