package osvin.com.VOAC.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import osvin.com.VOAC.R;
import osvin.com.VOAC.models.TransactionModel;
import osvin.com.VOAC.util.DateUtils;

public class TransactionsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private ArrayList<TransactionModel> list;

    public TransactionsAdapter(Context context, ArrayList<TransactionModel> data) {
        this.list = data;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_transaction_history_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position, @NonNull List<Object> payloads) {

        try {

            MyViewHolder holder = (MyViewHolder) viewHolder;
            TransactionModel transactionModel = list.get(position);
            holder.tv_transaction_id.setText("TransID: " + transactionModel.getTransaction_id());
            holder.tv_amount.setText(transactionModel.getCurrency() + " " + (Double.parseDouble(transactionModel.getAmount()) / 100));
            holder.tv_date.setText(DateUtils.getFormatTransactionDate(DateUtils.UTC_ToDeviceTimeTransaction(transactionModel.getDate())));

            String status = transactionModel.getStatus();
            holder.tv_status.setText(status);
            int status_color = ContextCompat.getColor(context, status.equalsIgnoreCase("success")
                    ? R.color.white : R.color.failure_color);
            holder.tv_status.setTextColor(status_color);
            holder.tv_status.setBackgroundResource(status.equalsIgnoreCase("success")
                    ? R.drawable.success_status_back : R.drawable.failed_status_back);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_transaction_id, tv_date, tv_amount, tv_status;

        MyViewHolder(View itemView) {
            super(itemView);
            tv_date = itemView.findViewById(R.id.tv_date);
            tv_status = itemView.findViewById(R.id.tv_status);
            tv_amount = itemView.findViewById(R.id.tv_amount);
            tv_transaction_id = itemView.findViewById(R.id.tv_transaction_id);
        }
    }
}