package osvin.com.VOAC.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import osvin.com.VOAC.R;
import osvin.com.VOAC.models.ArticleModel;
import osvin.com.VOAC.imageloader.ImageUtils;

public class ArticleAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private List<ArticleModel> list;
    private View.OnClickListener onClickListener;

    public ArticleAdapter(Context context, List<ArticleModel> data, View.OnClickListener onClickListener) {
        this.list = data;
        this.mContext = context;
        this.onClickListener = onClickListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_home_article, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {

        if (viewHolder instanceof MyViewHolder) {

            MyViewHolder holder = (MyViewHolder) viewHolder;

            ArticleModel articleModel = list.get(position);
            holder.itemUserView.setTag(articleModel);

            holder.name_view.setText(articleModel.getUser_display_name());

            ImageUtils.loadCircle(articleModel.getUser_profile(), mContext, R.drawable.ic_profile_pic, holder.photo_view);
        }
    }

    public void refreshList(List<ArticleModel> list) {
        this.list = list;
//        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView photo_view;
        private TextView name_view;
        private LinearLayout itemUserView;

        MyViewHolder(View itemView) {
            super(itemView);
            name_view = itemView.findViewById(R.id.name_view);
            photo_view = itemView.findViewById(R.id.photo_view);
            itemUserView = itemView.findViewById(R.id.itemUserView);
            itemUserView.setOnClickListener(onClickListener);
        }
    }

//    class MyProfileHolder extends RecyclerView.ViewHolder {
//
//        private ImageView photo_view;
//        private TextView name_view, tvDate;
//        private LinearLayout cv;
//        private RelativeLayout cardView;
//
//        MyProfileHolder(View itemView) {
//            super(itemView);
//            name_view = itemView.findViewById(R.id.name_view);
//            photo_view = itemView.findViewById(R.id.photo_view);
//            cv = itemView.findViewById(R.id.cv);
//            cardView = itemView.findViewById(R.id.cardView);
//        }
//    }
//
//    public interface OnItemArticleClickListener {
//        void onArticleItemClick(int position);
//    }
}