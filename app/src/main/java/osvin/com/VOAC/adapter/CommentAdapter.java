package osvin.com.VOAC.adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import osvin.com.VOAC.R;
import osvin.com.VOAC.imageloader.ImageUtils;
import osvin.com.VOAC.models.CommentModel;

public class CommentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<CommentModel> list;
    private Context mContext;
    private View.OnClickListener onClickListener;
    private int resource;


    public CommentAdapter(Context context, List<CommentModel> data, View.OnClickListener onClickListener, int resource) {
        this.list = data;
        this.mContext = context;
        this.onClickListener = onClickListener;
        this.resource = resource;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(resource, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position, @NonNull List<Object> payloads) {

        try {

            MyViewHolder holder = (MyViewHolder) viewHolder;

            CommentModel commentModel = list.get(position);
            holder.ivUser.setTag(R.id.ivUser, commentModel);

            holder.tv_name.setText(commentModel.getName());
            holder.tv_content.setText(Html.fromHtml(commentModel.getContent()));
            holder.tv_date.setText(commentModel.getDate());

            if (commentModel.getProfile() != null && !commentModel.getProfile().isEmpty()) {
                ImageUtils.loadCircle("" + commentModel.getProfile(), mContext, R.drawable.ic_profile_pic, holder.ivUser);
            } else {
                holder.ivUser.setImageResource(R.drawable.ic_profile_pic);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void refreshList(List<CommentModel> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_name, tv_content, tv_date;
        private AppCompatImageView ivUser;
        private LinearLayout llComment;

        MyViewHolder(View itemView) {
            super(itemView);
            tv_date = itemView.findViewById(R.id.tv_date);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_content = itemView.findViewById(R.id.tv_content);
            ivUser = itemView.findViewById(R.id.ivUser);
//            llComment = itemView.findViewById(R.id.llComment);
            ivUser.setOnClickListener(onClickListener);
        }
    }
}