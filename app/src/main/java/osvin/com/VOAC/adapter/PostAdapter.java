package osvin.com.VOAC.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import com.rbrooks.indefinitepagerindicator.IndefinitePagerIndicator;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.zla.reactions.ReactionPopup;
import io.zla.reactions.ReactionsConfigBuilder;
import osvin.com.VOAC.R;
import osvin.com.VOAC.imageloader.ImageUtils;
import osvin.com.VOAC.models.ArticleModel;
import osvin.com.VOAC.models.PostModel;
import osvin.com.VOAC.util.Debug;
import osvin.com.VOAC.util.HideKeyboardEdittext;

public class PostAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private Context mContext;
    private List<ArticleModel> articleList = new ArrayList<>();
    private List<PostModel> list;
    private static final int LOADING_VIEW = 0;
    private static final int POST_VIEW = 1;
    private static final int ARTICLE_VIEW = 2;
    private boolean showFooter = false;
    private View.OnClickListener onClickListener;
    private onReactionClick onReactionClick;
    private String[] strings = {"Like", "Love", "Haha", "Shocked", "Sad", "Angry"};//Please do not change the order of array
    //    private AdpPostMedia adpPostMedia;
    private ArticleAdapter articleAdapter;

    public PostAdapter(Context context, List<PostModel> data, View.OnClickListener onClickListener) {
        this.list = data;
        this.mContext = context;
        this.onClickListener = onClickListener;
        articleAdapter = new ArticleAdapter(mContext, articleList, onClickListener);
//        adpPostMedia = new AdpPostMedia(mContext, onClickListener);
    }

    public void setOnReactionClick(onReactionClick onReactionClick) {
        this.onReactionClick = onReactionClick;
    }

    public void setArticleList(List<ArticleModel> articleList) {
        this.articleList = articleList;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        if (viewType == POST_VIEW) {

            View transactionHistoryView = LayoutInflater.from(mContext).inflate(R.layout.list_item_home_post, parent, false);
            return new MyViewHolder(transactionHistoryView);

        } else if (viewType == LOADING_VIEW) {

            View viewLoading = LayoutInflater.from(mContext).inflate(R.layout.list_item_footer_progress_bar, parent, false);
            return new DataLoading(viewLoading);

        } else if (viewType == ARTICLE_VIEW) {

            View viewLoading = LayoutInflater.from(mContext).inflate(R.layout.list_item_article_view, parent, false);
            return new ArticleView(viewLoading);
        }

        throw new RuntimeException("there is no type matches" + viewType + "\n make sure you are using the correct type");
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {

        switch (getItemViewType(position)) {

            case ARTICLE_VIEW:
                bindArticleData((ArticleView) viewHolder);
                break;

            case POST_VIEW:
                bindPostData((MyViewHolder) viewHolder, position);
                break;

            case LOADING_VIEW:
                bindLoaderViewData((DataLoading) viewHolder);
                break;
        }
    }

    private void bindLoaderViewData(DataLoading dataLoading) {

        Debug.trace("Post", "LoaderHolder");
        if (showFooter) {
            dataLoading.itemView.setVisibility(View.VISIBLE);
            dataLoading.itemView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        } else {
            dataLoading.itemView.setLayoutParams(new LinearLayout.LayoutParams(0, 0));
            dataLoading.itemView.setVisibility(View.GONE);
        }
    }

    private void bindArticleData(ArticleView articleView) {
        articleAdapter.refreshList(articleList);
        articleView.rvArticles.setAdapter(articleAdapter);
    }

    @SuppressLint("ClickableViewAccessibility")
    private void bindPostData(MyViewHolder myViewHolder, int position) {

        Debug.trace("Post", "PostHolder");
        MyViewHolder holder = myViewHolder;

        PostModel postModel = list.get(position - 1);
        postModel.setPosition(position - 1);
        holder.ivUser.setTag(R.id.ivUser, postModel);
        holder.tvComments.setTag(postModel);
        holder.cv.setTag(postModel);
        holder.ivReaction.setTag(postModel);
        holder.tvReactionCount.setTag(postModel);
        holder.ivShare.setTag(postModel);
        holder.ivSend.setTag(postModel);
        holder.ivSubscribe.setTag(postModel);

        holder.nameView.setText(Html.fromHtml(postModel.getTitle()));
        holder.tvUserName.setText(postModel.getAuthor_name());
        holder.tvDate.setText(postModel.getPostDate());
        ImageUtils.loadCircle(postModel.getAuthor_image(), mContext, R.drawable.ic_profile_pic, holder.ivUser);
        holder.ivReaction.setImageResource(postModel.getReactionImage());
//        holder.ivPhotoLibrary.setVisibility(postModel.getIsMultipleVisible());
        holder.dotView.setVisibility(postModel.getIsMultipleVisible());

        AdpPostMedia adpPostMedia = new AdpPostMedia(postModel.getMediaFileModelList(), mContext, onClickListener);
//        adpPostMedia.refreshList(postModel.getMediaFileModelList());
        adpPostMedia.setHasStableIds(true);
        holder.rvMedia.setAdapter(adpPostMedia);
//        holder.rvMedia.setFocusable(false);
        holder.llLocation.setVisibility(postModel.getLocationVisibility());
//        holder.llCommentView.setFocusable(false);
        holder.tvLocation.setText(postModel.getLocation());
        holder.tvComments.setVisibility(postModel.getCommentVisibility());
        holder.tvComments.setText(String.format("View all %s", postModel.getFormattedCommentCount()));
        holder.tvReactionCount.setVisibility(postModel.getReactionCountVisibility());
        holder.tvReactionCount.setText(postModel.getFormattedReactionCounter());
        holder.ivReaction.setOnTouchListener(getReactionPop(postModel, holder.ivReaction));

        CommentAdapter commentAdapter = new CommentAdapter(mContext, postModel.getComment_list(), onClickListener, R.layout.list_item_comment_home);
        commentAdapter.setHasStableIds(true);
        holder.rvComments.setAdapter(commentAdapter);
//        holder.rvComments.setFocusable(false);
        holder.tvNeedHelp.setVisibility(postModel.getCategoryVisibility());
        holder.ivSubscribe.setVisibility(postModel.getSubscribeVisibility());
        holder.ivSubscribe.setImageResource(postModel.getSubscriptionImage());

        if (postModel.getAuthor_name() != null && postModel.getContent() != null)
            holder.tv_content.setText(Html.fromHtml("<b>" + postModel.getAuthor_name() + "</b> " + postModel.getContent()));
    }

    @Override
    public int getItemCount() {

        return list.size() + 2;
    }

    @Override
    public int getItemViewType(int position) {

        if (position == 0) {
            return ARTICLE_VIEW;
        } else if ((position + 1) == getItemCount()) {
            return LOADING_VIEW;
        } else {
            return POST_VIEW;
        }
    }

    public class DataLoading extends RecyclerView.ViewHolder {

        ProgressBar dataLoadingProgress;
        LinearLayout llLoader;

        DataLoading(View itemView) {
            super(itemView);

            dataLoadingProgress = itemView.findViewById(R.id.dataLoadingProgress);
            llLoader = itemView.findViewById(R.id.llLoadingView);
        }
    }

    public void addFooter() {
        this.showFooter = true;
    }

    public void removeFooter() {
        this.showFooter = false;
        notifyItemChanged(getItemCount() - 1);
    }

    public void refreshList(List<PostModel> list) {
        this.list = list;
//        notifyDataSetChanged();
    }

    public void refreshSingleItem(List<PostModel> list, int currentPosition) {
//        this.list = list;
        notifyItemChanged(currentPosition);
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.ivUser)
        AppCompatImageView ivUser;
        @BindView(R.id.tvUserName)
        TextView tvUserName;
        @BindView(R.id.tvDate)
        TextView tvDate;
        @BindView(R.id.tvLocation)
        TextView tvLocation;
        @BindView(R.id.llLocation)
        LinearLayout llLocation;
        @BindView(R.id.rvMedia)
        RecyclerView rvMedia;
        @BindView(R.id.ivPhotoLibrary)
        AppCompatImageView ivPhotoLibrary;
        @BindView(R.id.dotView)
        IndefinitePagerIndicator dotView;
        @BindView(R.id.name_view)
        TextView nameView;
        @BindView(R.id.ivReaction)
        AppCompatImageView ivReaction;
        @BindView(R.id.tvReactionCount)
        TextView tvReactionCount;
        @BindView(R.id.tv_comments)
        TextView tvComments;
        @BindView(R.id.ivShare)
        AppCompatImageView ivShare;
        @BindView(R.id.cv)
        LinearLayout cv;
        @BindView(R.id.rvComments)
        RecyclerView rvComments;
        @BindView(R.id.llCommentView)
        LinearLayout llCommentView;
        @BindView(R.id.et_comment)
        HideKeyboardEdittext etComment;
        @BindView(R.id.iv_add_comment)
        AppCompatImageView ivSend;
        @BindView(R.id.tvNeedHelp)
        AppCompatTextView tvNeedHelp;
        @BindView(R.id.ivSubscribe)
        AppCompatImageView ivSubscribe;
        @BindView(R.id.tv_content)
        TextView tv_content;

        MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            rvMedia.setLayoutManager(new LinearLayoutManager(mContext, RecyclerView.HORIZONTAL, false));
            rvMedia.setNestedScrollingEnabled(false);
            rvMedia.startNestedScroll(ViewCompat.SCROLL_AXIS_HORIZONTAL);

            SnapHelper snapHelper = new PagerSnapHelper();
            snapHelper.attachToRecyclerView(rvMedia);
            ivUser.setOnClickListener(onClickListener);
//            tvComments.setOnClickListener(onClickListener);
            cv.setOnClickListener(onClickListener);
            tvReactionCount.setOnClickListener(onClickListener);
            dotView.attachToRecyclerView(rvMedia);
            ivShare.setOnClickListener(onClickListener);
            rvComments.setLayoutManager(new LinearLayoutManager(mContext, RecyclerView.VERTICAL, false));
            rvComments.setNestedScrollingEnabled(false);
            ivSend.setOnClickListener(onClickListener);
            ivSubscribe.setOnClickListener(onClickListener);
            etComment.setOnBackPressEvent(new HideKeyboardEdittext.onBackPressEvent() {
                @Override
                public void onBackPress(boolean isBackPress) {
                    if (isBackPress) {
                        etComment.setText("");
                        etComment.clearFocus();
                    }
                }
            });
        }
    }

    public class ArticleView extends RecyclerView.ViewHolder {

        @BindView(R.id.rvArticles)
        RecyclerView rvArticles;

        public ArticleView(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            rvArticles.setLayoutManager(new LinearLayoutManager(mContext, RecyclerView.HORIZONTAL, false));
            rvArticles.setNestedScrollingEnabled(false);
            rvArticles.startNestedScroll(ViewCompat.SCROLL_AXIS_HORIZONTAL);
        }
    }

    private ReactionPopup getReactionPop(PostModel postModel, AppCompatImageView ivReaction) {

        ReactionPopup popup = new ReactionPopup(
                mContext,
                new ReactionsConfigBuilder(mContext)
                        .withReactions(new int[]{
                                R.drawable.like,
                                R.drawable.love,
                                R.drawable.haha,
                                R.drawable.wow,
                                R.drawable.sad,
                                R.drawable.angry,
                        })
                        .withReactionTexts(position -> strings[position])
                        .withHorizontalMargin(mContext.getResources().getDimensionPixelSize(R.dimen._10sdp))
                        .withPopupColor(Color.WHITE)
                        .build());
        popup.setReactionSelectedListener(integer -> {
            if (integer >= 0) {
                onReactionClick.onReactionSelection(postModel, integer, ivReaction);
            }
            return null;
        });

        return popup;
    }

    public interface onReactionClick {
        void onReactionSelection(PostModel postModel, int reactionId, AppCompatImageView ivReaction);
    }
}