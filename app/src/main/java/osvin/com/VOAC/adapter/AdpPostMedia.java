package osvin.com.VOAC.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import osvin.com.VOAC.R;
import osvin.com.VOAC.imageloader.ImageUtils;
import osvin.com.VOAC.models.MediaModel;
import osvin.com.VOAC.util.Debug;
import osvin.com.VOAC.util.Utils;

public class AdpPostMedia extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<MediaModel> mediaFileModelList;
    private Context context;
    private View.OnClickListener onClickListener;


    public AdpPostMedia(List<MediaModel> mediaFileModelList, Context context, View.OnClickListener onClickListener) {
        this.mediaFileModelList = mediaFileModelList;
        this.context = context;
        this.onClickListener = onClickListener;
    }

    public AdpPostMedia(Context context, View.OnClickListener onClickListener) {
        this.context = context;
        this.onClickListener = onClickListener;
    }

    public void refreshList(List<MediaModel> list) {
        this.mediaFileModelList = list;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new PostView(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_post_media_view, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof PostView) {

            PostView postView = (PostView) holder;
            MediaModel mediaModel = mediaFileModelList.get(position);
            postView.ivPostImage.setTag(R.id.ivPostImage, mediaModel);

            String imageUrl;

            if (Utils.isVideo(mediaModel.getUrl())) {
                postView.ivVideoPlay.setVisibility(View.VISIBLE);
                if (TextUtils.isEmpty(mediaModel.getMediumUrl())) {
                    imageUrl = mediaModel.getUrl();
                } else {
                    imageUrl = mediaModel.getMediumUrl();
                }
                Debug.trace("PostUrl", imageUrl);
                ImageUtils.loadGIF(imageUrl, context, R.color.whitePressed, true, postView.ivPostImage, postView.prgLoading);
            } else {
                postView.ivVideoPlay.setVisibility(View.GONE);
                if (TextUtils.isEmpty(mediaModel.getMediumUrl())) {
                    imageUrl = mediaModel.getUrl();
                } else {
                    imageUrl = mediaModel.getMediumUrl();
                }

                Debug.trace("PostUrl", imageUrl);
                ImageUtils.load(imageUrl, context, R.color.whitePressed, true, postView.ivPostImage, postView.prgLoading);
            }
        }
    }

    @Override
    public int getItemCount() {
        return mediaFileModelList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    class PostView extends RecyclerView.ViewHolder {

        @BindView(R.id.ivPostImage)
        AppCompatImageView ivPostImage;
        @BindView(R.id.ivVideoPlay)
        AppCompatImageView ivVideoPlay;
        @BindView(R.id.prgLoading)
        ProgressBar prgLoading;

        public PostView(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            if (onClickListener != null) {
                ivPostImage.setOnClickListener(onClickListener);
            }
        }
    }
}
