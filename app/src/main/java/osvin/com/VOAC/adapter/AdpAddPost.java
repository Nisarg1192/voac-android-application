package osvin.com.VOAC.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import osvin.com.VOAC.R;
import osvin.com.VOAC.models.MediaFileModel;
import osvin.com.VOAC.imageloader.ImageUtils;
import osvin.com.VOAC.util.Utils;

public class AdpAddPost extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<MediaFileModel> mediaFileModelList;
    private Context context;
    private View.OnClickListener onClickListener;
    private int layoutResource;

    public AdpAddPost(List<MediaFileModel> mediaFileModelList, Context context, View.OnClickListener onClickListener, int layoutResource) {
        this.mediaFileModelList = mediaFileModelList;
        this.context = context;
        this.onClickListener = onClickListener;
        this.layoutResource = layoutResource;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new PostView(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_add_post_single_post, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof PostView) {

            PostView postView = (PostView) holder;
            MediaFileModel mediaFileModel = mediaFileModelList.get(position);

            String path;

            if (TextUtils.isEmpty(mediaFileModel.getPath())) {
                path = mediaFileModel.getUrl();
            } else {
                path = mediaFileModel.getPath();
            }

            if (Utils.isVideo(mediaFileModel.getUrl())) {
                postView.ivVideoPlay.setVisibility(View.VISIBLE);
            } else {
                postView.ivVideoPlay.setVisibility(View.GONE);
            }

            ImageUtils.load(path, context, R.color.whitePressed, true, postView.ivPostImage);
        }
    }

    @Override
    public int getItemCount() {
        return mediaFileModelList.size();
    }

    class PostView extends RecyclerView.ViewHolder {

        @BindView(R.id.ivPostImage)
        AppCompatImageView ivPostImage;
        @BindView(R.id.ivVideoPlay)
        AppCompatImageView ivVideoPlay;

        public PostView(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
