package osvin.com.VOAC.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import osvin.com.VOAC.R;
import osvin.com.VOAC.models.AreaOfInterestModel;
import osvin.com.VOAC.interfaces.OnItemClickListener;
import osvin.com.VOAC.imageloader.ImageUtils;


public class AreaOfInterestAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private ArrayList<AreaOfInterestModel> list;
    private OnItemClickListener onItemClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public AreaOfInterestAdapter(Context context, ArrayList<AreaOfInterestModel> data) {
        this.list = data;
        this.mContext = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder viewHolder = null;
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_area_of_interest_item, parent, false);
        viewHolder = new MyViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position, @NonNull List<Object> payloads) {

        try {

            AreaOfInterestModel areaOfInterestModel = list.get(position);

            MyViewHolder holder = (MyViewHolder) viewHolder;

            holder.tv_title.setText(areaOfInterestModel.getTitle());

            ImageUtils.setImage(holder.iv_image
                    , areaOfInterestModel.getImage()
                    , mContext
                    , R.color.whitePressed);

            holder.cl_main.setTag(position);
            holder.cl_main.setOnClickListener((View v) -> {
                int pos = (int) v.getTag();
                if (onItemClickListener != null)
                    onItemClickListener.onItemClick(pos);
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    private class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView iv_image;
        private TextView tv_title;
        private ConstraintLayout cl_main;

        private MyViewHolder(View itemView) {
            super(itemView);
            cl_main = itemView.findViewById(R.id.cl_main);
            tv_title = itemView.findViewById(R.id.tv_title);
            iv_image = itemView.findViewById(R.id.rvMedia);
        }
    }
}