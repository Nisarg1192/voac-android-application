package osvin.com.VOAC.models;

import android.os.Parcel;
import android.os.Parcelable;

public class MediaModel implements Parcelable {

    private String url = "";
    private String mediumUrl = "";
    private int width = 0;
    private int height = 0;

    public MediaModel() {

    }

    protected MediaModel(Parcel in) {
        url = in.readString();
        mediumUrl = in.readString();
        width = in.readInt();
        height = in.readInt();
    }

    public static final Creator<MediaModel> CREATOR = new Creator<MediaModel>() {
        @Override
        public MediaModel createFromParcel(Parcel in) {
            return new MediaModel(in);
        }

        @Override
        public MediaModel[] newArray(int size) {
            return new MediaModel[size];
        }
    };

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMediumUrl() {
        return mediumUrl;
    }

    public void setMediumUrl(String mediumUrl) {
        this.mediumUrl = mediumUrl;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(url);
        dest.writeString(mediumUrl);
        dest.writeInt(width);
        dest.writeInt(height);
    }
}
