package osvin.com.VOAC.models;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import osvin.com.VOAC.R;
import osvin.com.VOAC.util.Constants;


public class PostModel implements Parcelable {

    private long comment_count = Constants.DEFAULT_INT;

    private List<CommentModel> comment_list = new ArrayList<>();

    private String id = Constants.DEFAULT_STRING, type = Constants.DEFAULT_STRING, categories = Constants.DEFAULT_STRING,
            image = Constants.DEFAULT_STRING, status = Constants.DEFAULT_STRING, title = Constants.DEFAULT_STRING,
            content = Constants.DEFAULT_STRING, date = Constants.DEFAULT_STRING, author_id = Constants.DEFAULT_STRING,
            author_name = Constants.DEFAULT_STRING, author_image = Constants.DEFAULT_STRING, author_email = Constants.DEFAULT_STRING,
            location = Constants.DEFAULT_STRING;
    private List<MediaModel> mediaFileModelList = new ArrayList<>();
    private int position = 0;
    private Bitmap bitmap;
    private String reactionId = "0";
    private String reactionCounter = "0";
    private int reactionImage = R.drawable.ic_like_grey;
    private int isMultipleVisible = View.GONE;
    private int locationVisibility = View.GONE;
    private String postDate = "";
    private String formattedCommentCount = "0 Comments";
    private int reactionCountVisibility = View.GONE;
    private String formattedReactionCounter = "0";
    private String slug = "";
    private int categoryVisibility = View.GONE;
    private int commentVisibility = View.GONE;
    private int subscribeVisibility = View.GONE;
    private int subscriptionId = Constants.UNSUBSCRIBE_ID;
    private int subscriptionImage = R.drawable.ic_unsubscribe;

    public PostModel() {

    }

    protected PostModel(Parcel in) {
        comment_count = in.readLong();
        comment_list = in.createTypedArrayList(CommentModel.CREATOR);
        id = in.readString();
        type = in.readString();
        categories = in.readString();
        image = in.readString();
        status = in.readString();
        title = in.readString();
        content = in.readString();
        date = in.readString();
        author_id = in.readString();
        author_name = in.readString();
        author_image = in.readString();
        author_email = in.readString();
        location = in.readString();
        mediaFileModelList = in.createTypedArrayList(MediaModel.CREATOR);
        position = in.readInt();
        bitmap = in.readParcelable(Bitmap.class.getClassLoader());
        reactionId = in.readString();
        reactionCounter = in.readString();
        reactionImage = in.readInt();
        isMultipleVisible = in.readInt();
        locationVisibility = in.readInt();
        postDate = in.readString();
        formattedCommentCount = in.readString();
        reactionCountVisibility = in.readInt();
        formattedReactionCounter = in.readString();
        slug = in.readString();
        categoryVisibility = in.readInt();
        commentVisibility = in.readInt();
        subscribeVisibility = in.readInt();
        subscriptionId = in.readInt();
        subscriptionImage = in.readInt();
    }

    public static final Creator<PostModel> CREATOR = new Creator<PostModel>() {
        @Override
        public PostModel createFromParcel(Parcel in) {
            return new PostModel(in);
        }

        @Override
        public PostModel[] newArray(int size) {
            return new PostModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCategories() {
        return categories;
    }

    public void setCategories(String categories) {
        this.categories = categories;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAuthor_id() {
        return author_id;
    }

    public void setAuthor_id(String author_id) {
        this.author_id = author_id;
    }

    public String getAuthor_name() {
        return author_name;
    }

    public void setAuthor_name(String author_name) {
        this.author_name = author_name;
    }

    public String getAuthor_image() {
        return author_image;
    }

    public void setAuthor_image(String author_image) {
        this.author_image = author_image;
    }

    public List<CommentModel> getComment_list() {
        return comment_list;
    }

    public void setComment_list(List<CommentModel> comment_list) {
        this.comment_list = comment_list;
    }

    public long getComment_count() {
        return comment_count;
    }

    public void setComment_count(long comment_count) {
        this.comment_count = comment_count;
    }

    public String getAuthor_email() {
        return author_email;
    }

    public void setAuthor_email(String author_email) {
        this.author_email = author_email;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public List<MediaModel> getMediaFileModelList() {
        return mediaFileModelList;
    }

    public void setMediaFileModelList(List<MediaModel> mediaFileModelList) {
        this.mediaFileModelList = mediaFileModelList;
    }

    public int getPosition() {
        return position;
    }

    public PostModel setPosition(int position) {
        this.position = position;
        return this;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public String getReactionId() {
        return reactionId;
    }

    public void setReactionId(String reactionId) {
        this.reactionId = reactionId;
    }

    public String getReactionCounter() {
        return reactionCounter;
    }

    public void setReactionCounter(String reactionCounter) {
        this.reactionCounter = reactionCounter;
    }

    public int getReactionImage() {
        return reactionImage;
    }

    public void setReactionImage(int reactionImage) {
        this.reactionImage = reactionImage;
    }

    public int getIsMultipleVisible() {
        return isMultipleVisible;
    }

    public void setIsMultipleVisible(int isMultipleVisible) {
        this.isMultipleVisible = isMultipleVisible;
    }

    public int getLocationVisibility() {
        return locationVisibility;
    }

    public void setLocationVisibility(int locationVisibility) {
        this.locationVisibility = locationVisibility;
    }

    public String getPostDate() {
        return postDate;
    }

    public void setPostDate(String postDate) {
        this.postDate = postDate;
    }

    public String getFormattedCommentCount() {
        return formattedCommentCount;
    }

    public void setFormattedCommentCount(String formattedCommentCount) {
        this.formattedCommentCount = formattedCommentCount;
    }

    public int getReactionCountVisibility() {
        return reactionCountVisibility;
    }

    public void setReactionCountVisibility(int reactionCountVisibility) {
        this.reactionCountVisibility = reactionCountVisibility;
    }

    public String getFormattedReactionCounter() {
        return formattedReactionCounter;
    }

    public void setFormattedReactionCounter(String formattedReactionCounter) {
        this.formattedReactionCounter = formattedReactionCounter;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public int getCategoryVisibility() {
        return categoryVisibility;
    }

    public void setCategoryVisibility(int categoryVisibility) {
        this.categoryVisibility = categoryVisibility;
    }

    public int getCommentVisibility() {
        return commentVisibility;
    }

    public void setCommentVisibility(int commentVisibility) {
        this.commentVisibility = commentVisibility;
    }

    public int getSubscribeVisibility() {
        return subscribeVisibility;
    }

    public void setSubscribeVisibility(int subscribeVisibility) {
        this.subscribeVisibility = subscribeVisibility;
    }

    public int getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(int subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public int getSubscriptionImage() {
        return subscriptionImage;
    }

    public void setSubscriptionImage(int subscriptionImage) {
        this.subscriptionImage = subscriptionImage;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(comment_count);
        dest.writeTypedList(comment_list);
        dest.writeString(id);
        dest.writeString(type);
        dest.writeString(categories);
        dest.writeString(image);
        dest.writeString(status);
        dest.writeString(title);
        dest.writeString(content);
        dest.writeString(date);
        dest.writeString(author_id);
        dest.writeString(author_name);
        dest.writeString(author_image);
        dest.writeString(author_email);
        dest.writeString(location);
        dest.writeTypedList(mediaFileModelList);
        dest.writeInt(position);
        dest.writeValue(bitmap);
        dest.writeString(reactionId);
        dest.writeString(reactionCounter);
        dest.writeInt(reactionImage);
        dest.writeInt(isMultipleVisible);
        dest.writeInt(locationVisibility);
        dest.writeString(postDate);
        dest.writeString(formattedCommentCount);
        dest.writeInt(reactionCountVisibility);
        dest.writeString(formattedReactionCounter);
        dest.writeString(slug);
        dest.writeInt(categoryVisibility);
        dest.writeInt(commentVisibility);
        dest.writeInt(subscribeVisibility);
        dest.writeInt(subscriptionId);
        dest.writeInt(subscriptionImage);
    }
}
