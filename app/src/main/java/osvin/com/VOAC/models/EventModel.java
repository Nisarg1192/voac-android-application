package osvin.com.VOAC.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class EventModel implements Parcelable {

    private String post_id, title, description, start_date, end_date, address, state;

    protected EventModel(Parcel in) {
        post_id = in.readString();
        title = in.readString();
        description = in.readString();
        start_date = in.readString();
        end_date = in.readString();
        address = in.readString();
        state = in.readString();
    }

    public static final Creator<EventModel> CREATOR = new Creator<EventModel>() {
        @Override
        public EventModel createFromParcel(Parcel in) {
            return new EventModel(in);
        }

        @Override
        public EventModel[] newArray(int size) {
            return new EventModel[size];
        }
    };

    public String getPost_id() {
        return post_id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getStart_date() {
        return start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public String getAddress() {
        return address;
    }

    public String getState() {
        return state;
    }

    public EventModel(String post_id, String title, String description, String start_date,
                      String end_date, String address, String state) {
        this.post_id = post_id;
        this.title = title;
        this.description = description;
        this.start_date = start_date;
        this.end_date = end_date;
        this.address = address;
        this.state = state;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(post_id);
        dest.writeString(title);
        dest.writeString(description);
        dest.writeString(start_date);
        dest.writeString(end_date);
        dest.writeString(address);
        dest.writeString(state);
    }
}
