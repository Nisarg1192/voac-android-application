package osvin.com.VOAC.models;

import android.os.Parcel;
import android.os.Parcelable;

public class AreaOfInterestModel implements Parcelable {

    private String image, title;

    public AreaOfInterestModel() {
    }

    protected AreaOfInterestModel(Parcel in) {
        image = in.readString();
        title = in.readString();
    }

    public static final Creator<AreaOfInterestModel> CREATOR = new Creator<AreaOfInterestModel>() {
        @Override
        public AreaOfInterestModel createFromParcel(Parcel in) {
            return new AreaOfInterestModel(in);
        }

        @Override
        public AreaOfInterestModel[] newArray(int size) {
            return new AreaOfInterestModel[size];
        }
    };

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(image);
        dest.writeString(title);
    }
}