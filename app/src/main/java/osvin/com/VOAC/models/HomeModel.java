package osvin.com.VOAC.models;

import java.io.Serializable;
import java.util.ArrayList;

import osvin.com.VOAC.models.AreaOfInterestModel;

public class HomeModel implements Serializable {

    private ArrayList<String> pager_list = new ArrayList<>();
    private ArrayList<AreaOfInterestModel> area_of_interest_list = new ArrayList<>();
    private String main_title, second_title, main_description, bottom_description, footer_text;

    public ArrayList<String> getPager_list() {
        return pager_list;
    }

    public void setPager_list(ArrayList<String> pager_list) {
        this.pager_list = pager_list;
    }

    public ArrayList<AreaOfInterestModel> getArea_of_interest_list() {
        return area_of_interest_list;
    }

    public void setArea_of_interest_list(ArrayList<AreaOfInterestModel> area_of_interest_list) {
        this.area_of_interest_list = area_of_interest_list;
    }

    public String getMain_title() {
        return main_title;
    }

    public void setMain_title(String main_title) {
        this.main_title = main_title;
    }

    public String getSecond_title() {
        return second_title;
    }

    public void setSecond_title(String second_title) {
        this.second_title = second_title;
    }

    public String getMain_description() {
        return main_description;
    }

    public void setMain_description(String main_description) {
        this.main_description = main_description;
    }

    public String getBottom_description() {
        return bottom_description;
    }

    public void setBottom_description(String bottom_description) {
        this.bottom_description = bottom_description;
    }

    public String getFooter_text() {
        return footer_text;
    }

    public void setFooter_text(String footer_text) {
        this.footer_text = footer_text;
    }
}