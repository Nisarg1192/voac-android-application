package osvin.com.VOAC.models;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

public class MediaFileModel implements Parcelable {

    private String url = "";
    private String path = "";
//    private Bitmap bitmap;

    public MediaFileModel() {
    }

    protected MediaFileModel(Parcel in) {
        path = in.readString();
        url = in.readString();
//        bitmap = in.readParcelable(Bitmap.class.getClassLoader());
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

//    public Bitmap getBitmap() {
//        return bitmap;
//    }
//
//    public void setBitmap(Bitmap bitmap) {
//        this.bitmap = bitmap;
//    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public static final Creator<MediaFileModel> CREATOR = new Creator<MediaFileModel>() {
        @Override
        public MediaFileModel createFromParcel(Parcel in) {
            return new MediaFileModel(in);
        }

        @Override
        public MediaFileModel[] newArray(int size) {
            return new MediaFileModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(path);
//        dest.writeValue(bitmap);
        dest.writeString(url);
    }
}
