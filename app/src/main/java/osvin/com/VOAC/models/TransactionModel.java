package osvin.com.VOAC.models;

import android.os.Parcel;
import android.os.Parcelable;

public class TransactionModel implements Parcelable {

    private String transaction_id, currency, amount, date, status;

    public TransactionModel() {
    }

    protected TransactionModel(Parcel in) {
        transaction_id = in.readString();
        currency = in.readString();
        amount = in.readString();
        date = in.readString();
        status = in.readString();
    }

    public static final Creator<TransactionModel> CREATOR = new Creator<TransactionModel>() {
        @Override
        public TransactionModel createFromParcel(Parcel in) {
            return new TransactionModel(in);
        }

        @Override
        public TransactionModel[] newArray(int size) {
            return new TransactionModel[size];
        }
    };

    public String getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(String transaction_id) {
        this.transaction_id = transaction_id;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(transaction_id);
        dest.writeString(currency);
        dest.writeString(amount);
        dest.writeString(date);
        dest.writeString(status);
    }
}