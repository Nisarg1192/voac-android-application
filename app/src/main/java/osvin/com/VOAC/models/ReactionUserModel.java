package osvin.com.VOAC.models;

import android.os.Parcel;
import android.os.Parcelable;

public class ReactionUserModel implements Parcelable {

    private String userName = "";
    private String profilePhoto = "";

    public ReactionUserModel() {
    }

    public ReactionUserModel(Parcel in) {
        userName = in.readString();
        profilePhoto = in.readString();
    }

    public static final Creator<ReactionUserModel> CREATOR = new Creator<ReactionUserModel>() {
        @Override
        public ReactionUserModel createFromParcel(Parcel in) {
            return new ReactionUserModel(in);
        }

        @Override
        public ReactionUserModel[] newArray(int size) {
            return new ReactionUserModel[size];
        }
    };

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getProfilePhoto() {
        return profilePhoto;
    }

    public void setProfilePhoto(String profilePhoto) {
        this.profilePhoto = profilePhoto;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(userName);
        dest.writeString(profilePhoto);
    }
}
