package osvin.com.VOAC.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class AddPostModel implements Parcelable {

    private List<MediaFileModel> mediaFileModelList;
    private String location = "";
    private String title = "";
    private String description = "";

    public AddPostModel() {
    }

    public AddPostModel(Parcel in) {
        mediaFileModelList = in.readArrayList(ArrayList.class.getClassLoader());
        location = in.readString();
        title = in.readString();
        description = in.readString();
    }

    public static final Creator<AddPostModel> CREATOR = new Creator<AddPostModel>() {
        @Override
        public AddPostModel createFromParcel(Parcel in) {
            return new AddPostModel(in);
        }

        @Override
        public AddPostModel[] newArray(int size) {
            return new AddPostModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(mediaFileModelList);
        dest.writeString(location);
        dest.writeString(title);
        dest.writeString(description);
    }

    public List<MediaFileModel> getMediaFileModelList() {
        return mediaFileModelList;
    }

    public void setMediaFileModelList(List<MediaFileModel> mediaFileModelList) {
        this.mediaFileModelList = mediaFileModelList;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
