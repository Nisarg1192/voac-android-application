package osvin.com.VOAC.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ArticleModel implements Parcelable {

    @Expose
    @SerializedName("user_profile")
    private String user_profile;
    @Expose
    @SerializedName("user_display_name")
    private String user_display_name;
    @Expose
    @SerializedName("user_email")
    private String user_email;
    @Expose
    @SerializedName("filter")
    private String filter;
    @Expose
    @SerializedName("comment_count")
    private String comment_count;
    @Expose
    @SerializedName("post_mime_type")
    private String post_mime_type;
    @Expose
    @SerializedName("post_type")
    private String post_type;
    @Expose
    @SerializedName("menu_order")
    private int menu_order;
    @Expose
    @SerializedName("guid")
    private String guid;
    @Expose
    @SerializedName("post_parent")
    private int post_parent;
    @Expose
    @SerializedName("post_content_filtered")
    private String post_content_filtered;
    @Expose
    @SerializedName("post_modified_gmt")
    private String post_modified_gmt;
    @Expose
    @SerializedName("post_modified")
    private String post_modified;
    @Expose
    @SerializedName("pinged")
    private String pinged;
    @Expose
    @SerializedName("to_ping")
    private String to_ping;
    @Expose
    @SerializedName("post_name")
    private String post_name;
    @Expose
    @SerializedName("post_password")
    private String post_password;
    @Expose
    @SerializedName("ping_status")
    private String ping_status;
    @Expose
    @SerializedName("comment_status")
    private String comment_status;
    @Expose
    @SerializedName("post_status")
    private String post_status;
    @Expose
    @SerializedName("post_excerpt")
    private String post_excerpt;
    @Expose
    @SerializedName("post_title")
    private String post_title;
    @Expose
    @SerializedName("post_content")
    private String post_content;
    @Expose
    @SerializedName("post_date_gmt")
    private String post_date_gmt;
    @Expose
    @SerializedName("post_date")
    private String post_date;
    @Expose
    @SerializedName("post_author")
    private String post_author;
    @Expose
    @SerializedName("ID")
    private int ID;

    public ArticleModel() {
    }

    protected ArticleModel(Parcel in) {
        user_profile = in.readString();
        user_display_name = in.readString();
        user_email = in.readString();
        filter = in.readString();
        comment_count = in.readString();
        post_mime_type = in.readString();
        post_type = in.readString();
        menu_order = in.readInt();
        guid = in.readString();
        post_parent = in.readInt();
        post_content_filtered = in.readString();
        post_modified_gmt = in.readString();
        post_modified = in.readString();
        pinged = in.readString();
        to_ping = in.readString();
        post_name = in.readString();
        post_password = in.readString();
        ping_status = in.readString();
        comment_status = in.readString();
        post_status = in.readString();
        post_excerpt = in.readString();
        post_title = in.readString();
        post_content = in.readString();
        post_date_gmt = in.readString();
        post_date = in.readString();
        post_author = in.readString();
        ID = in.readInt();
    }

    public static final Creator<ArticleModel> CREATOR = new Creator<ArticleModel>() {
        @Override
        public ArticleModel createFromParcel(Parcel in) {
            return new ArticleModel(in);
        }

        @Override
        public ArticleModel[] newArray(int size) {
            return new ArticleModel[size];
        }
    };

    public String getUser_profile() {
        return user_profile;
    }

    public void setUser_profile(String user_profile) {
        this.user_profile = user_profile;
    }

    public String getUser_display_name() {
        return user_display_name;
    }

    public void setUser_display_name(String user_display_name) {
        this.user_display_name = user_display_name;
    }

    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public String getComment_count() {
        return comment_count;
    }

    public void setComment_count(String comment_count) {
        this.comment_count = comment_count;
    }

    public String getPost_mime_type() {
        return post_mime_type;
    }

    public void setPost_mime_type(String post_mime_type) {
        this.post_mime_type = post_mime_type;
    }

    public String getPost_type() {
        return post_type;
    }

    public void setPost_type(String post_type) {
        this.post_type = post_type;
    }

    public int getMenu_order() {
        return menu_order;
    }

    public void setMenu_order(int menu_order) {
        this.menu_order = menu_order;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public int getPost_parent() {
        return post_parent;
    }

    public void setPost_parent(int post_parent) {
        this.post_parent = post_parent;
    }

    public String getPost_content_filtered() {
        return post_content_filtered;
    }

    public void setPost_content_filtered(String post_content_filtered) {
        this.post_content_filtered = post_content_filtered;
    }

    public String getPost_modified_gmt() {
        return post_modified_gmt;
    }

    public void setPost_modified_gmt(String post_modified_gmt) {
        this.post_modified_gmt = post_modified_gmt;
    }

    public String getPost_modified() {
        return post_modified;
    }

    public void setPost_modified(String post_modified) {
        this.post_modified = post_modified;
    }

    public String getPinged() {
        return pinged;
    }

    public void setPinged(String pinged) {
        this.pinged = pinged;
    }

    public String getTo_ping() {
        return to_ping;
    }

    public void setTo_ping(String to_ping) {
        this.to_ping = to_ping;
    }

    public String getPost_name() {
        return post_name;
    }

    public void setPost_name(String post_name) {
        this.post_name = post_name;
    }

    public String getPost_password() {
        return post_password;
    }

    public void setPost_password(String post_password) {
        this.post_password = post_password;
    }

    public String getPing_status() {
        return ping_status;
    }

    public void setPing_status(String ping_status) {
        this.ping_status = ping_status;
    }

    public String getComment_status() {
        return comment_status;
    }

    public void setComment_status(String comment_status) {
        this.comment_status = comment_status;
    }

    public String getPost_status() {
        return post_status;
    }

    public void setPost_status(String post_status) {
        this.post_status = post_status;
    }

    public String getPost_excerpt() {
        return post_excerpt;
    }

    public void setPost_excerpt(String post_excerpt) {
        this.post_excerpt = post_excerpt;
    }

    public String getPost_title() {
        return post_title;
    }

    public void setPost_title(String post_title) {
        this.post_title = post_title;
    }

    public String getPost_content() {
        return post_content;
    }

    public void setPost_content(String post_content) {
        this.post_content = post_content;
    }

    public String getPost_date_gmt() {
        return post_date_gmt;
    }

    public void setPost_date_gmt(String post_date_gmt) {
        this.post_date_gmt = post_date_gmt;
    }

    public String getPost_date() {
        return post_date;
    }

    public void setPost_date(String post_date) {
        this.post_date = post_date;
    }

    public String getPost_author() {
        return post_author;
    }

    public void setPost_author(String post_author) {
        this.post_author = post_author;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(user_profile);
        dest.writeString(user_display_name);
        dest.writeString(user_email);
        dest.writeString(filter);
        dest.writeString(comment_count);
        dest.writeString(post_mime_type);
        dest.writeString(post_type);
        dest.writeInt(menu_order);
        dest.writeString(guid);
        dest.writeInt(post_parent);
        dest.writeString(post_content_filtered);
        dest.writeString(post_modified_gmt);
        dest.writeString(post_modified);
        dest.writeString(pinged);
        dest.writeString(to_ping);
        dest.writeString(post_name);
        dest.writeString(post_password);
        dest.writeString(ping_status);
        dest.writeString(comment_status);
        dest.writeString(post_status);
        dest.writeString(post_excerpt);
        dest.writeString(post_title);
        dest.writeString(post_content);
        dest.writeString(post_date_gmt);
        dest.writeString(post_date);
        dest.writeString(post_author);
        dest.writeInt(ID);
    }
}
