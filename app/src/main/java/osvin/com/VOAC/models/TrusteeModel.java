package osvin.com.VOAC.models;

import android.os.Parcel;
import android.os.Parcelable;

public class TrusteeModel implements Parcelable {

    public String name;
    public String title;
    public String photo;
    public String description;

    public TrusteeModel() {
        name = "";
        title = "";
        photo = "";
        description = "";
    }

    public TrusteeModel(String name, String title, String photo, String description) {
        this.name = name;
        this.title = title;
        this.photo = photo;
        this.description = description;
    }

    protected TrusteeModel(Parcel in) {
        name = in.readString();
        title = in.readString();
        photo = in.readString();
        description = in.readString();
    }

    public static final Creator<TrusteeModel> CREATOR = new Creator<TrusteeModel>() {
        @Override
        public TrusteeModel createFromParcel(Parcel in) {
            return new TrusteeModel(in);
        }

        @Override
        public TrusteeModel[] newArray(int size) {
            return new TrusteeModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(title);
        dest.writeString(photo);
        dest.writeString(description);
    }
}
