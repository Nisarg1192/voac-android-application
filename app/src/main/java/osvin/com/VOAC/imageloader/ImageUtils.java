package osvin.com.VOAC.imageloader;

import android.annotation.SuppressLint;
import android.content.Context;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;

import osvin.com.VOAC.util.Constants;

public class ImageUtils {

    public static void setImage(ImageView imageView, String image_path, Context context, int placeholder) {
        try {
//            if (image_path == null || image_path.trim().length() <= 0 || image_path.isEmpty())
//                image_path = "NoImage";

            if (!image_path.contains("http"))
                image_path = Constants.BASE_URL + image_path;

            //Picasso.get().load(image_path).placeholder(placeholder).error(placeholder).into(imageView);
            Glide.with(context.getApplicationContext()).load(image_path)
//                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .placeholder(placeholder).error(placeholder)
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .into(imageView);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("CheckResult")
    public static void load(String imagePath, Context context, int placeholder, boolean isCenterCrop, ImageView imageView) {
        try {

            RequestBuilder requestManager = Glide.with(context.getApplicationContext()).load(imagePath);

            requestManager.placeholder(placeholder);
            requestManager.error(placeholder);
//            requestManager.diskCacheStrategy(DiskCacheStrategy.NONE);
            if (isCenterCrop) {
                requestManager.centerCrop();
            }
            //.centerCrop()
            requestManager.override(400);
            requestManager.transition(DrawableTransitionOptions.withCrossFade());
            requestManager.listener(new RequestListener() {

                @Override
                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target target, boolean isFirstResource) {
//                    Debug.trace("LoadFailed", e.getLocalizedMessage());
                    return false;
                }

                @Override
                public boolean onResourceReady(Object resource, Object model, Target target, DataSource dataSource, boolean isFirstResource) {
//                    Toast.makeText(context, "Loaded", Toast.LENGTH_LONG).show();
                    return false;
                }
            });
            requestManager.into(imageView);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("CheckResult")
    public static void loadGIF(String imagePath, Context context, int placeholder, boolean isCenterCrop, ImageView imageView) {
        try {

            RequestBuilder requestManager = Glide.with(context.getApplicationContext()).asGif().load(imagePath);

            requestManager.placeholder(placeholder);
            requestManager.error(placeholder);
//            requestManager.diskCacheStrategy(DiskCacheStrategy.NONE);
            if (isCenterCrop) {
                requestManager.centerCrop();
            }
            //.centerCrop()
            requestManager.override(400);
            requestManager.transition(DrawableTransitionOptions.withCrossFade());
            requestManager.listener(new RequestListener() {

                @Override
                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target target, boolean isFirstResource) {
//                    Debug.trace("LoadFailed", e.getLocalizedMessage());
                    return false;
                }

                @Override
                public boolean onResourceReady(Object resource, Object model, Target target, DataSource dataSource, boolean isFirstResource) {
//                    Toast.makeText(context, "Loaded", Toast.LENGTH_LONG).show();
                    return false;
                }
            });
            requestManager.into(imageView);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void load(String imagePath, Context context, int placeholder, boolean isCenterCrop, ImageView imageView, ProgressBar progressBar) {
        try {

            RequestOptions options = new RequestOptions()
//                    .centerCrop()
                    .placeholder(placeholder)
                    .error(placeholder)
//                    .override(400)
                    .priority(Priority.HIGH);

            if (isCenterCrop) {
                options.centerCrop();
            }

            new GlideImageLoader(context, imageView,
                    progressBar).load(imagePath, options);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void loadGIF(String imagePath, Context context, int placeholder, boolean isCenterCrop, ImageView imageView, ProgressBar progressBar) {
        try {

            RequestOptions options = new RequestOptions()
//                    .centerCrop()
                    .placeholder(placeholder)
                    .error(placeholder)
//                    .override(400)
                    .priority(Priority.HIGH);

            if (isCenterCrop) {
                options.centerCrop();
            }

            new GlideImageLoader(context, imageView,
                    progressBar).loadGif(imagePath, options);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void loadCircle(String imagePath, Context context, int placeholder, ImageView imageView) {
        try {
//            if (imagePath == null || imagePath.trim().length() <= 0 || imagePath.isEmpty())
//                imagePath = "NoImage";

            //Picasso.get().load(image_path).resize(width, height).placeholder(placeholder).error(placeholder).centerCrop().into(imageView);
            Glide.with(context.getApplicationContext())
                    .load(imagePath)
//                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .placeholder(placeholder)
                    .error(placeholder)
                    .centerCrop()
                    .apply(new RequestOptions().circleCrop())
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .into(imageView);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void loadCircle(int imagePath, Context context, int placeholder, ImageView imageView) {
        try {
            //Picasso.get().load(image_path).resize(width, height).placeholder(placeholder).error(placeholder).centerCrop().into(imageView);
            Glide.with(context.getApplicationContext())
                    .load(imagePath)
//                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .placeholder(placeholder)
                    .error(placeholder)
                    .centerCrop()
                    .apply(new RequestOptions().circleCrop())
                    .into(imageView);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void load(int resourceId, Context context, int placeholder, ImageView imageView) {
        try {

            Glide.with(context.getApplicationContext())
                    .load(resourceId)
//                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .placeholder(placeholder)
                    .error(placeholder)
                    .centerCrop()
                    //.apply(new RequestOptions().override(400, 400))
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .into(imageView);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void load(int resourceId, Context context, int placeholder, ImageView imageView, boolean isCenterCrop) {
        try {

            Glide.with(context.getApplicationContext())
                    .load(resourceId)
//                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .placeholder(placeholder)
                    .error(placeholder)
                    //.apply(new RequestOptions().override(400, 400))
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .into(imageView);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}