package osvin.com.VOAC.location;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

/**
 * For getting location by GPS
 */

public class GPSTracker implements LocationListener {

    private Context mContext;

    // The minimum distance to change Updates in meters
    private static final float MIN_DISTANCE_CHANGE_FOR_UPDATES = 0.01f; // 10 meters
    // Declaring a Location Manager
    private LocationManager locationManager;
    private boolean isLocationUpdating = false;
    private static final long UPDATE_INTERVAL = /*5 * 60 * *//*5000*/0, FASTEST_INTERVAL = /*3 * 60 **/ /*5000*/0; // = 5 minutes
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationListener locationListener;


    public GPSTracker(Context context, LocationListener locationListener) {
        this.mContext = context;
        this.locationListener = locationListener;
        locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
    }


    /**
     * Start location request. It also verify it location is enable or not
     */
    @SuppressLint("MissingPermission")
    public void startLocationRequest() {
        try {

            if (isGooglePlayServicesAvailable(mContext)) {
                if (!isLocationUpdating) {
                    mFusedLocationClient = LocationServices.getFusedLocationProviderClient(mContext);
                    startLocationUpdates();
                    isLocationUpdating = true;
                }
            } else {

                if (!isLocationUpdating) {
                    // if GPS Enabled get lat/long using GPS Services
                    locationManager.requestLocationUpdates(
                            LocationManager.GPS_PROVIDER,
                            UPDATE_INTERVAL,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                    isLocationUpdating = true;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Stop using GPS listener Calling this function will stop using GPS in your
     * app.
     */
    public void stopUsingGPS() {

        if (locationManager != null && locationListener != null) {
            locationManager.removeUpdates(this);
        }

        if (mFusedLocationClient != null && locationCallback != null) {
            mFusedLocationClient.removeLocationUpdates(locationCallback);
        }
    }


    /**
     * Function to check GPS/wifi enabled
     *
     * @return boolean
     */
    public boolean isGPSEnabled() {
        // getting GPS status
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)/* || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)*/;
    }

    @SuppressLint("MissingPermission")
    private void startLocationUpdates() {

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(UPDATE_INTERVAL);
        locationRequest.setFastestInterval(FASTEST_INTERVAL);

        mFusedLocationClient.requestLocationUpdates(locationRequest,
                locationCallback,
                Looper.myLooper());
    }

    private boolean isGooglePlayServicesAvailable(Context activity) {
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int status = googleApiAvailability.isGooglePlayServicesAvailable(activity);
        return status == ConnectionResult.SUCCESS;
    }

    @Override
    public void onLocationChanged(Location location) {
        if (locationListener != null) {
            locationListener.onLocation(location);
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    public interface LocationListener {
        void onLocation(Location location);
    }

    private LocationCallback locationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            super.onLocationResult(locationResult);

            if (locationResult == null) {
                return;
            }

            if (locationListener != null) {
                locationListener.onLocation(locationResult.getLastLocation());
            }
        }
    };
}
