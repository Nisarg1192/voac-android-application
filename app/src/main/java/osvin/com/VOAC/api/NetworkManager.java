package osvin.com.VOAC.api;

import android.app.Activity;
import android.content.Context;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ANRequest;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import osvin.com.VOAC.R;
import osvin.com.VOAC.base.MyApplication;
import osvin.com.VOAC.models.MediaFileModel;
import osvin.com.VOAC.util.Constants;
import osvin.com.VOAC.util.CustomDialog;
import osvin.com.VOAC.util.Debug;

public class NetworkManager {

    private static NetworkManager instance;

    public static NetworkManager getInstance() {
        if (instance == null) {
            instance = new NetworkManager();
        }
        return instance;
    }

//    public void login(String username, String password, String token, String device, final DataObserver listener) {
//
//        String url = Constants.BASE_URL + Constants.LOGIN;
//        AndroidNetworking.post(url)
//                .addBodyParameter("username", username)
//                .addBodyParameter("password", password)
//                .addBodyParameter("device_type", String.valueOf(Constants.DEVICE_TYPE))
//                .addBodyParameter("fcm_token", token)
//                .addBodyParameter("unique_device_id", device)
//                .addBodyParameter("insecure", Constants.INSECURE)
//                .setPriority(Priority.HIGH)
//                .setContentType("application/x-www-form-urlencoded")
//                .build()
//                .getAsJSONObject(new JSONObjectRequestListener() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        Debug.trace("Response", "" + response.toString());
//                        listener.onSuccess(response);
//                    }
//
//                    @Override
//                    public void onError(ANError anError) {
//                        Debug.trace("ANError", "" + anError.getErrorBody());
//                        listener.onFailure(anError.getErrorBody(), 100);
//                    }
//                });
//    }

    public void getNonce(String controller, String method, String cookie, final DataObserver listener) {
        String url = Constants.BASE_URL + Constants.GET_NONCE;
        AndroidNetworking.post(url)
                .addBodyParameter("controller", "" + controller)
                .addBodyParameter("method", "" + method)
                .addBodyParameter("insecure", Constants.INSECURE)
                .addBodyParameter("cookie", "" + cookie)
                .setContentType("application/x-www-form-urlencoded")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Debug.trace("Response", "" + response.toString());
                        listener.onSuccess(response);
                    }

                    @Override
                    public void onError(ANError anError) {
                        Debug.trace("ANError", "" + anError.getErrorBody());
                        try {
                            listener.onFailure(new JSONObject(anError.getErrorBody()).optString("error"), anError.getErrorCode());
                        } catch (Exception e) {
                            e.printStackTrace();
                            listener.onFailure(MyApplication.getInstance().getString(R.string.ERROR_MSG), anError.getErrorCode());
                        }
                    }
                });
    }

    public void register(String username, String email, String password, String name, String nonce, String token, String device, final DataObserver listener) {

        String url = Constants.BASE_URL + Constants.REGISTER;
        AndroidNetworking.post(url)
                .addBodyParameter("username", username)
                .addBodyParameter("email", email)
                .addBodyParameter("user_pass", password)
                .addBodyParameter("nonce", nonce)
                .addBodyParameter("display_name", name)
                .addBodyParameter("insecure", Constants.INSECURE)
                .addBodyParameter("device_type", String.valueOf(Constants.DEVICE_TYPE))
                .addBodyParameter("fcm_token", token)
                .setContentType("application/x-www-form-urlencoded")
                .addBodyParameter("unique_device_id", device)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Debug.trace("Response", "" + response.toString());
                        listener.onSuccess(response);
                    }

                    @Override
                    public void onError(ANError anError) {
                        Debug.trace("ANError", "" + anError.getErrorBody());
                        try {
                            JSONObject jsonObject = new JSONObject(anError.getErrorBody());
                            listener.onFailure(jsonObject.optString("error"), 100);
                        } catch (Exception e) {
                            e.printStackTrace();
                            listener.onFailure(MyApplication.getInstance().getString(R.string.str_registration_failed_please_try_again), 100);
                        }
                    }
                });
    }

    public void updateProfile(String userId, String name, String email, String nonce, String cookie, String profileImage, final DataObserver listener) {

        String url = Constants.BASE_URL + Constants.UPDATE_PROFILE;
        /*if (profileImage != null) {
            AndroidNetworking.upload(url)
                    .addMultipartFile("avatar", profileImage)
                    .addMultipartParameter("user_id", userId, "text/plain")
                    .addMultipartParameter("user_nicename", name, "text/plain")
                    .addMultipartParameter("user_nickname", name, "text/plain")
                    .addMultipartParameter("user_displayname", name, "text/plain")
                    .addMultipartParameter("insecure", Constants.INSECURE)
                    .addMultipartParameter("user_email", email, "text/plain")
                    .addMultipartParameter("nonce", nonce, "text/plain")
                    .addMultipartParameter("cookie", cookie, "text/plain")
                    .setPriority(Priority.HIGH)
                    .build()
                    .setUploadProgressListener(new UploadProgressListener() {
                        @Override
                        public void onProgress(long bytesUploaded, long totalBytes) {
                            // do anything with progress
                            Debug.trace("onProgress", " :: " + (((float) bytesUploaded / totalBytes) * 100));
                        }
                    })
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Debug.trace("updateProfile", "" + response);
                            listener.onSuccess(response);
                        }

                        @Override
                        public void onError(ANError anError) {
                            Debug.trace("ANError", "" + anError.getErrorBody());
                            listener.onFailure(anError.getErrorBody());
                        }
                    });
        }else {*/
        AndroidNetworking.post(url)
                .addBodyParameter("avatar", "" + profileImage)
                .addBodyParameter("user_id", userId)
                .addBodyParameter("user_nicename", name)
                .addBodyParameter("user_nickname", name)
                .addBodyParameter("user_displayname", name)
                .addBodyParameter("user_email", email)
                .addBodyParameter("insecure", Constants.INSECURE)
                .addBodyParameter("nonce", nonce)
                .addBodyParameter("cookie", cookie)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Debug.trace("Response", "" + response.toString());
                        listener.onSuccess(response);
                    }

                    @Override
                    public void onError(ANError anError) {
                        Debug.trace("ANError", "" + anError.getErrorBody());
                        try {
                            listener.onFailure(new JSONObject(anError.getErrorBody()).optString("error"), anError.getErrorCode());
                        } catch (Exception e) {
                            e.printStackTrace();
                            listener.onFailure(MyApplication.getInstance().getString(R.string.ERROR_MSG), anError.getErrorCode());
                        }
                    }
                });
        //}
    }

    public void uploadProfileImage(File profileImage, final DataObserver listener) {

        String url = Constants.BASE_URL + Constants.UPLOAD_PROFILE_IMAGE;
        if (profileImage != null) {
            AndroidNetworking.upload(url)
                    .addMultipartFile("profile", profileImage)
                    .addMultipartParameter("insecure", Constants.INSECURE)
                    .setPriority(Priority.HIGH)
                    .build()
                    .setUploadProgressListener((bytesUploaded, totalBytes) -> {
                        // do anything with progress
                        Debug.trace("onProgress", " :: " + (((float) bytesUploaded / totalBytes) * 100));
                    })
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Debug.trace("Response", "" + response.toString());
                            listener.onSuccess(response);
                        }

                        @Override
                        public void onError(ANError anError) {
                            Debug.trace("ANError", "" + anError.getErrorBody());
                            try {
                                listener.onFailure(new JSONObject(anError.getErrorBody()).optString("error"), anError.getErrorCode());
                            } catch (Exception e) {
                                e.printStackTrace();
                                listener.onFailure(MyApplication.getInstance().getString(R.string.ERROR_MSG), anError.getErrorCode());
                            }
                        }
                    });
        }
    }

    public void changePassword(String userId, String new_pass, String confirm_pass, String nonce, final DataObserver listener) {

        String url = Constants.BASE_URL + Constants.CHANGE_PASSWORD;
        AndroidNetworking.post(url)
                .addBodyParameter("user_id", userId)
                .addBodyParameter("user_pass", new_pass)
                .addBodyParameter("confirmation_password", confirm_pass)
                .addBodyParameter("insecure", Constants.INSECURE)
                .addBodyParameter("nonce", nonce)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Debug.trace("Response", "" + response.toString());
                        listener.onSuccess(response);
                    }

                    @Override
                    public void onError(ANError anError) {
                        Debug.trace("ANError", "" + anError.getErrorBody());
                        try {
                            listener.onFailure(new JSONObject(anError.getErrorBody()).optString("error"), anError.getErrorCode());
                        } catch (Exception e) {
                            e.printStackTrace();
                            listener.onFailure(MyApplication.getInstance().getString(R.string.ERROR_MSG), anError.getErrorCode());
                        }
                    }
                });
    }

    public void uploadMedia(List<MediaFileModel> mediaFileModelList, JSONObjectRequestListener jsonObjectRequestListener) {

        String url = Constants.BASE_URL + Constants.UPLOAD_POST_MEDIA;
        ANRequest.MultiPartBuilder multiPartBuilder = AndroidNetworking.upload(url);

        for (int i = 0; i < mediaFileModelList.size(); i++) {
            Debug.trace("MEDIA", "media" + (i + 1));
            multiPartBuilder.addMultipartFile("media" + (i + 1), new File(mediaFileModelList.get(i).getUrl())/*,"multipart/form-data"*/);
        }

        multiPartBuilder.addMultipartParameter("insecure", Constants.INSECURE/*,"multipart/form-data"*/);
        multiPartBuilder.setPriority(Priority.HIGH)
                .build()
                .setUploadProgressListener((bytesUploaded, totalBytes) -> {
                    // do anything with progress
                    Debug.trace("onProgress", " :: " + (((float) bytesUploaded / totalBytes) * 100));
                })
                .getAsJSONObject(jsonObjectRequestListener);
    }

    public void AddPost(String titleREQ, String contentREQ, String locationREQ, String nonce, String useridREQ, String cookieREQ, JSONArray postMediaNameArray, final DataObserver listener) {

        String url = Constants.BASE_URL + Constants.CREATE_POST;
        ANRequest.PostRequestBuilder multiPartBuilder = AndroidNetworking.post(url);
        if (postMediaNameArray != null && postMediaNameArray.length() > 0) {
            for (int i = 0; i < postMediaNameArray.length(); i++) {
                Debug.trace("MEDIA", "media" + (i + 1));
                multiPartBuilder.addBodyParameter("media" + (i + 1), postMediaNameArray.optString(i));
            }
        }

        multiPartBuilder.addBodyParameter("title", titleREQ)
                .addBodyParameter("content", contentREQ)
                .addBodyParameter("nonce", nonce)
                .addBodyParameter("author", useridREQ)
                .addBodyParameter("insecure", Constants.INSECURE)
                .addBodyParameter("cookie", cookieREQ)
                .addBodyParameter("custom_fields", locationREQ)
                .setPriority(Priority.HIGH)
                .build()
                .setUploadProgressListener((bytesUploaded, totalBytes) -> {
                    // do anything with progress
                    Debug.trace("onProgress", " :: " + (((float) bytesUploaded / totalBytes) * 100));
                })
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Debug.trace("Response", "" + response.toString());
//                        Toast.makeText(MyApplication.getInstance(), response.toString(), Toast.LENGTH_LONG).show();
                        listener.onSuccess(response);
                    }

                    @Override
                    public void onError(ANError anError) {
//                        Toast.makeText(MyApplication.getInstance(), anError.getErrorBody(), Toast.LENGTH_LONG).show();
                        Debug.trace("ANError", "" + anError.getErrorBody());
                        try {
                            listener.onFailure(new JSONObject(anError.getErrorBody()).optString("error"), anError.getErrorCode());
                        } catch (Exception e) {
                            e.printStackTrace();
                            listener.onFailure(MyApplication.getInstance().getString(R.string.ERROR_MSG), anError.getErrorCode());
                        }
                    }
                });
    }

    public void UpdatePost(String idREQ, String titleREQ, String contentREQ, String locationREQ, String nonce, String useridREQ, String cookieREQ, final DataObserver listener) {

        String url = Constants.BASE_URL + Constants.UPDATE_POST;

        AndroidNetworking.post(url)
                .addBodyParameter("id", idREQ)
                .addBodyParameter("title", titleREQ)
                .addBodyParameter("content", contentREQ)
                .addBodyParameter("nonce", nonce)
                .addBodyParameter("author", useridREQ)
                .addBodyParameter("insecure", Constants.INSECURE)
                .addBodyParameter("cookie", cookieREQ)
                .addBodyParameter("location", locationREQ)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Debug.trace("Response", "" + response.toString());
                        listener.onSuccess(response);
                    }

                    @Override
                    public void onError(ANError anError) {
                        Debug.trace("ANError", "" + anError.getErrorBody());
                        try {
                            listener.onFailure(new JSONObject(anError.getErrorBody()).optString("error"), anError.getErrorCode());
                        } catch (Exception e) {
                            e.printStackTrace();
                            listener.onFailure(MyApplication.getInstance().getString(R.string.ERROR_MSG), anError.getErrorCode());
                        }
                    }
                });
    }

    public void deletePost(String id, String userId, String cookie, String nonce, final DataObserver listener) {

        String url = Constants.BASE_URL + Constants.DELETE_POST;
        AndroidNetworking.post(url)
                .addBodyParameter("id", id)
                .addBodyParameter("user_id", userId)
                .addBodyParameter("cookie", cookie)
                .addBodyParameter("insecure", Constants.INSECURE)
                .addBodyParameter("nonce", nonce)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Debug.trace("Response", "" + response.toString());
                        listener.onSuccess(response);
                    }

                    @Override
                    public void onError(ANError anError) {
                        Debug.trace("ANError", "" + anError.getErrorBody());
                        try {
                            listener.onFailure(new JSONObject(anError.getErrorBody()).optString("error"), anError.getErrorCode());
                        } catch (Exception e) {
                            e.printStackTrace();
                            listener.onFailure(MyApplication.getInstance().getString(R.string.ERROR_MSG), anError.getErrorCode());
                        }
                    }
                });
    }

    public void resetPassword(String email, final DataObserver listener) {
        String url = Constants.BASE_URL + Constants.RESET_PASSWORD;
        AndroidNetworking.post(url)
                .addBodyParameter("user_login", email)
                .addBodyParameter("insecure", Constants.INSECURE)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Debug.trace("Response", "" + response.toString());
                        listener.onSuccess(response);
                    }

                    @Override
                    public void onError(ANError anError) {
                        Debug.trace("ANError", "" + anError.getErrorBody());
                        try {
                            listener.onFailure(new JSONObject(anError.getErrorBody()).optString("error"), anError.getErrorCode());
                        } catch (Exception e) {
                            e.printStackTrace();
                            listener.onFailure(MyApplication.getInstance().getString(R.string.ERROR_MSG), anError.getErrorCode());
                        }
                    }
                });
    }

//    public void getUserInfo(String UserId, String cookie, final DataObserver listener) {
//
//        String url = Constants.BASE_URL + Constants.GET_PROFILE;
//        AndroidNetworking.post(url)
//                .addBodyParameter("user_id", UserId)
//                .addBodyParameter("cookie", cookie)
//                .addBodyParameter("insecure", Constants.INSECURE)
//                .setPriority(Priority.HIGH)
//                .build()
//                .getAsJSONObject(new JSONObjectRequestListener() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        Debug.trace("Response", "" + response.toString());
//                        listener.onSuccess(response);
//                    }
//
//                    @Override
//                    public void onError(ANError anError) {
//                        Debug.trace("ANError", "" + anError.getErrorBody());
//                        listener.onFailure(anError.getErrorBody(), 100);
//                    }
//                });
//    }

    public void getHome(final DataObserver listener) {

        String url = Constants.BASE_URL + Constants.GET_HOME_DATA;
        AndroidNetworking.get(url)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Debug.trace("Response", "" + response.toString());
                        listener.onSuccess(response);
                    }

                    @Override
                    public void onError(ANError anError) {
                        Debug.trace("ANError", "" + anError.getErrorBody());
                        try {
                            listener.onFailure(new JSONObject(anError.getErrorBody()).optString("error"), anError.getErrorCode());
                        } catch (Exception e) {
                            e.printStackTrace();
                            listener.onFailure(MyApplication.getInstance().getString(R.string.ERROR_MSG), anError.getErrorCode());
                        }
                    }
                });
    }

    public void Logout(String UserId, String token, final DataObserver listener) {

        String url = Constants.BASE_URL + Constants.LOGOUT;
        AndroidNetworking.post(url)
                .addBodyParameter("user_id", UserId)
                .addBodyParameter("cookie", token)
                .addBodyParameter("nonce", "")
                .addBodyParameter("insecure", Constants.INSECURE)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Debug.trace("Response", "" + response.toString());
                        listener.onSuccess(response);
                    }

                    @Override
                    public void onError(ANError anError) {
                        Debug.trace("ANError", "" + anError.getErrorBody());
                        try {
                            listener.onFailure(new JSONObject(anError.getErrorBody()).optString("error"), anError.getErrorCode());
                        } catch (Exception e) {
                            e.printStackTrace();
                            listener.onFailure(MyApplication.getInstance().getString(R.string.ERROR_MSG), anError.getErrorCode());
                        }
                    }
                });
    }

    public void getArticles(final DataObserver listener) {

        String url = Constants.BASE_URL + Constants.GET_POST_USER_LIST;
        AndroidNetworking.get(url)
                .addQueryParameter("insecure", Constants.INSECURE)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Debug.trace("Response", "" + response.toString());
                        listener.onSuccess(response);
                    }

                    @Override
                    public void onError(ANError anError) {
                        Debug.trace("ANError", "" + anError.getErrorBody());
                        try {
                            listener.onFailure(new JSONObject(anError.getErrorBody()).optString("error"), anError.getErrorCode());
                        } catch (Exception e) {
                            e.printStackTrace();
                            listener.onFailure(MyApplication.getInstance().getString(R.string.ERROR_MSG), anError.getErrorCode());
                        }
                    }
                });
    }

    public void getPosts(int PageNumber, String userId, final DataObserver listener) {

        String url = Constants.BASE_URL + Constants.GET_ARICLES;
        AndroidNetworking.get(url)
                .addQueryParameter("page", String.valueOf(PageNumber))
                .addQueryParameter("insecure", Constants.INSECURE)
                .addQueryParameter("uid", userId)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Debug.trace("Response", "" + response.toString());
                        listener.onSuccess(response);
                    }

                    @Override
                    public void onError(ANError anError) {
                        Debug.trace("ANError", "" + anError.getErrorBody());
                        try {
                            listener.onFailure(new JSONObject(anError.getErrorBody()).optString("error"), anError.getErrorCode());
                        } catch (Exception e) {
                            e.printStackTrace();
                            listener.onFailure(MyApplication.getInstance().getString(R.string.ERROR_MSG), anError.getErrorCode());
                        }
                    }
                });
    }

    public void getMyPosts(int PageNumber, String userId, String cookie, final DataObserver listener) {

        String url = Constants.BASE_URL + Constants.GET_MY_POSTS;
        AndroidNetworking.post(url)
                .addBodyParameter("page", String.valueOf(PageNumber))
                .addBodyParameter("cookie", cookie)
                .addBodyParameter("id", userId)
                .addBodyParameter("insecure", Constants.INSECURE)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Debug.trace("Response", "" + response.toString());
                        listener.onSuccess(response);
                    }

                    @Override
                    public void onError(ANError anError) {
                        Debug.trace("ANError", "" + anError.getErrorBody());
                        try {
                            listener.onFailure(new JSONObject(anError.getErrorBody()).optString("error"), anError.getErrorCode());
                        } catch (Exception e) {
                            e.printStackTrace();
                            listener.onFailure(MyApplication.getInstance().getString(R.string.ERROR_MSG), anError.getErrorCode());
                        }
                    }
                });
    }

    public void getAuthorsPosts(String postUrl, int PageNumber, String userId, final DataObserver listener) {

        String url = Constants.BASE_URL + postUrl;
        AndroidNetworking.post(url)
                .addBodyParameter("page", String.valueOf(PageNumber))
                .addBodyParameter("id", userId)
                .addBodyParameter("insecure", Constants.INSECURE)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Debug.trace("Response", "" + response.toString());
                        listener.onSuccess(response);
                    }

                    @Override
                    public void onError(ANError anError) {
                        Debug.trace("ANError", "" + anError.getErrorBody());
                        try {
                            listener.onFailure(new JSONObject(anError.getErrorBody()).optString("error"), anError.getErrorCode());
                        } catch (Exception e) {
                            e.printStackTrace();
                            listener.onFailure(MyApplication.getInstance().getString(R.string.ERROR_MSG), anError.getErrorCode());
                        }
                    }
                });
    }

//    public void getArticleDetail(String postId, String apiPath, final DataObserver listener) {
//
//        String url = Constants.BASE_URL + apiPath;
//        AndroidNetworking.post(url)
//                .addBodyParameter("id", postId)
//                .addBodyParameter("insecure", Constants.INSECURE)
//                .setPriority(Priority.HIGH)
//                .build()
//                .getAsJSONObject(new JSONObjectRequestListener() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        Debug.trace("Response", "" + response.toString());
//                        listener.onSuccess(response);
//                    }
//
//                    @Override
//                    public void onError(ANError anError) {
//                        Debug.trace("ANError", "" + anError.getErrorBody());
//                        listener.onFailure(anError.getErrorBody(), 100);
//                    }
//                });
//    }

    public void SubmitComment(String postId, String user_id, String content, final DataObserver listener) {

        String url = Constants.BASE_URL + Constants.SUBMIT_COMMENT;
        AndroidNetworking.post(url)
                .addBodyParameter("post_id", postId)
                .addBodyParameter("user_id", user_id)
                .addBodyParameter("content", content)
                .addBodyParameter("insecure", Constants.INSECURE)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Debug.trace("Response", "" + response.toString());
                        listener.onSuccess(response);
                    }

                    @Override
                    public void onError(ANError anError) {
                        Debug.trace("ANError", "" + anError.getErrorBody());
                        try {
                            listener.onFailure(new JSONObject(anError.getErrorBody()).optString("error"), anError.getErrorCode());
                        } catch (Exception e) {
                            e.printStackTrace();
                            listener.onFailure(MyApplication.getInstance().getString(R.string.ERROR_MSG), anError.getErrorCode());
                        }
                    }
                });
    }

//    public void getAddressInfoApi(String address, String key, final DataObserver listener) {
//
//        String url = "https://maps.googleapis.com/maps/api/geocode/json";
//        AndroidNetworking.get(url)
//                .addQueryParameter("address", address)
//                .addQueryParameter("key", key)
//                .setPriority(Priority.HIGH)
//                .build()
//                .getAsJSONObject(new JSONObjectRequestListener() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        Debug.trace("Response", "" + response.toString());
//                        listener.onSuccess(response);
//                    }
//
//                    @Override
//                    public void onError(ANError anError) {
//                        Debug.trace("ANError", "" + anError.getErrorBody());
//                        try {
//                            listener.onFailure(new JSONObject(anError.getErrorBody()).optString("error"), anError.getErrorCode());
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                            listener.onFailure(MyApplication.getInstance().getString(R.string.ERROR_MSG), anError.getErrorCode());
//                        }
//                    }
//                });
//    }

    public void SubmitVolunteer(String user_idREQ, String nameREQ, String emailREQ,
                                String phone_numberREQ, String addressREQ, String cityREQ,
                                String stateREQ, String countryREQ, String latREQ, String lngREQ,
                                File post_image_file, final DataObserver listener) {

        String url = Constants.BASE_URL + Constants.SUBMIT_VOLUNTEER;

        Map<String, String> reqMap = new HashMap<>();
        reqMap.put("user_id", "" + user_idREQ);
        reqMap.put("your-name", "" + nameREQ);
        reqMap.put("your-email", "" + emailREQ);
        reqMap.put("phoneNumber", "" + phone_numberREQ);
        reqMap.put("ownPlaces", "" + addressREQ);
        reqMap.put("ownCity", "" + cityREQ);
        reqMap.put("ownState", "" + stateREQ);
        reqMap.put("ownCountry", "" + countryREQ);
        reqMap.put("lat", "" + latREQ);
        reqMap.put("lng", "" + lngREQ);
        reqMap.put("_wpcf7", "633");
        reqMap.put("_wpcf7_version", "5.1.1");
        reqMap.put("_wpcf7_locale", "en");
        reqMap.put("_wpcf7_unit_tag", "wpcf7-f633-p634-o1");
        reqMap.put("_wpcf7_container_post", "634");
        reqMap.put("_wpcf7cf_hidden_group_fields", "[]");
        reqMap.put("_wpcf7cf_hidden_groups", "[]");
        reqMap.put("_wpcf7cf_visible_groups", "[]");
        reqMap.put("_wpcf7cf_options", "{&quot;form_id&quot;:633,&quot;conditions&quot;:[{&quot;then_field&quot;:&quot;-1&quot;,&quot;and_rules&quot;:[{&quot;if_field&quot;:&quot;-1&quot;,&quot;operator&quot;:&quot;equals&quot;,&quot;if_value&quot;:&quot;India&quot;}]},{&quot;then_field&quot;:&quot;-1&quot;,&quot;and_rules&quot;:[{&quot;if_field&quot;:&quot;-1&quot;,&quot;operator&quot;:&quot;equals&quot;,&quot;if_value&quot;:&quot;United States of America&quot;}]},{&quot;then_field&quot;:&quot;-1&quot;,&quot;and_rules&quot;:[{&quot;if_field&quot;:&quot;-1&quot;,&quot;operator&quot;:&quot;equals&quot;,&quot;if_value&quot;:&quot;Afghanistan&quot;}]}],&quot;settings&quot;:false}");
        reqMap.put("g-recaptcha-response", "" + Constants.DEFAULT_STRING);
        reqMap.put("insecure", "" + Constants.INSECURE);

        if (post_image_file != null) {
            AndroidNetworking.upload(url)
                    .addMultipartFile("image", post_image_file)
                    .addMultipartParameter(reqMap)
                    .setPriority(Priority.HIGH)
                    .build()
                    .setUploadProgressListener((bytesUploaded, totalBytes) -> {
                        // do anything with progress
                        Debug.trace("onProgress", " :: " + (((float) bytesUploaded / totalBytes) * 100));
                    })
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Debug.trace("Response", "" + response.toString());
                            listener.onSuccess(response);
                        }

                        @Override
                        public void onError(ANError anError) {
                            Debug.trace("ANError", "" + anError.getErrorBody());
                            try {
                                listener.onFailure(new JSONObject(anError.getErrorBody()).optString("error"), anError.getErrorCode());
                            } catch (Exception e) {
                                e.printStackTrace();
                                listener.onFailure(MyApplication.getInstance().getString(R.string.ERROR_MSG), anError.getErrorCode());
                            }
                        }
                    });
        } else {
            AndroidNetworking.post(url)
                    .addBodyParameter(reqMap)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Debug.trace("Response", "" + response.toString());
                            listener.onSuccess(response);
                        }

                        @Override
                        public void onError(ANError anError) {
                            Debug.trace("ANError", "" + anError.getErrorBody());
                            try {
                                listener.onFailure(new JSONObject(anError.getErrorBody()).optString("error"), anError.getErrorCode());
                            } catch (Exception e) {
                                e.printStackTrace();
                                listener.onFailure(MyApplication.getInstance().getString(R.string.ERROR_MSG), anError.getErrorCode());
                            }
                        }
                    });
        }
    }

    public void VerifyTransaction(String url, String authorization, final DataObserver listener) {

        AndroidNetworking.get(url)
                .setPriority(Priority.HIGH)
                .addHeaders("Content-Type", "application/json")
                .addHeaders("Authorization", "" + authorization)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Debug.trace("Response", "" + response.toString());
                        listener.onSuccess(response);
                    }

                    @Override
                    public void onError(ANError anError) {
                        Debug.trace("ANError", "" + anError.getErrorBody());
                        try {
                            listener.onFailure(new JSONObject(anError.getErrorBody()).optString("error"), anError.getErrorCode());
                        } catch (Exception e) {
                            e.printStackTrace();
                            listener.onFailure(MyApplication.getInstance().getString(R.string.ERROR_MSG), anError.getErrorCode());
                        }
                    }
                });
    }

//    public void InitTransaction(String url, String reference, int amount, String email, String authorization, final DataObserver listener) {
//
//        AndroidNetworking.post(url)
//                .setPriority(Priority.HIGH)
//                .addHeaders("Authorization", "" + authorization)
//                .addBodyParameter("reference", "" + reference)
//                .addBodyParameter("amount", "" + amount)
//                .addBodyParameter("email", "" + email)
//                .build()
//                .getAsJSONObject(new JSONObjectRequestListener() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        Debug.trace("Response", "" + response.toString());
//                        listener.onSuccess(response);
//                    }
//
//                    @Override
//                    public void onError(ANError anError) {
//                        Debug.trace("ANError", "" + anError.getErrorBody());
//                        try {
//                            listener.onFailure(new JSONObject(anError.getErrorBody()).optString("error"), anError.getErrorCode());
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                            listener.onFailure(MyApplication.getInstance().getString(R.string.ERROR_MSG), anError.getErrorCode());
//                        }
//                    }
//                });
//    }

    public void SaveTransaction(String user_id, String id, String authorization, final DataObserver listener) {

        String url = Constants.BASE_URL + Constants.SAVE_TRANSACTION;
        AndroidNetworking.post(url)
                .setPriority(Priority.HIGH)
                .addHeaders("Authorization", "" + authorization)
                .addBodyParameter("txn_id", "" + id)
                .addBodyParameter("user_id", "" + user_id)
                .addBodyParameter("insecure", Constants.INSECURE)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Debug.trace("Response", "" + response.toString());
                        listener.onSuccess(response);
                    }

                    @Override
                    public void onError(ANError anError) {
                        Debug.trace("ANError", "" + anError.getErrorBody());
                        try {
                            listener.onFailure(new JSONObject(anError.getErrorBody()).optString("error"), anError.getErrorCode());
                        } catch (Exception e) {
                            e.printStackTrace();
                            listener.onFailure(MyApplication.getInstance().getString(R.string.ERROR_MSG), anError.getErrorCode());
                        }
                    }
                });
    }

    public void getUserNotificationStatus(String UserId, final DataObserver listener) {

        String url = Constants.BASE_URL + Constants.GET_NOTIFICATION_STATUS;
        AndroidNetworking.post(url)
                .addBodyParameter("user_id", UserId)
                .addBodyParameter("insecure", Constants.INSECURE)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Debug.trace("Response", "" + response.toString());
                        listener.onSuccess(response);
                    }

                    @Override
                    public void onError(ANError anError) {
                        Debug.trace("ANError", "" + anError.getErrorBody());
                        try {
                            listener.onFailure(new JSONObject(anError.getErrorBody()).optString("error"), anError.getErrorCode());
                        } catch (Exception e) {
                            e.printStackTrace();
                            listener.onFailure(MyApplication.getInstance().getString(R.string.ERROR_MSG), anError.getErrorCode());
                        }
                    }
                });
    }

    public void updateUserNotificationStatus(String UserId, String status, final DataObserver listener) {

        String url = Constants.BASE_URL + Constants.UPDATE_NOTIFICATION_STATUS;
        AndroidNetworking.post(url)
                .addBodyParameter("user_id", UserId)
                .addBodyParameter("notification_status", status)
                .addBodyParameter("insecure", Constants.INSECURE)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Debug.trace("Response", "" + response.toString());
                        listener.onSuccess(response);
                    }

                    @Override
                    public void onError(ANError anError) {
                        Debug.trace("ANError", "" + anError.getErrorBody());
                        try {
                            listener.onFailure(new JSONObject(anError.getErrorBody()).optString("error"), anError.getErrorCode());
                        } catch (Exception e) {
                            e.printStackTrace();
                            listener.onFailure(MyApplication.getInstance().getString(R.string.ERROR_MSG), anError.getErrorCode());
                        }
                    }
                });
    }

    public void getEventPost(String UserId, final DataObserver listener) {

        String url = Constants.BASE_URL + Constants.GET_EVENT_POST;
        AndroidNetworking.post(url)
                .addBodyParameter("post_id", UserId)
                .addBodyParameter("insecure", Constants.INSECURE)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Debug.trace("Response", "" + response.toString());
                        listener.onSuccess(response);
                    }

                    @Override
                    public void onError(ANError anError) {
                        Debug.trace("ANError", "" + anError.getErrorBody());
                        try {
                            listener.onFailure(new JSONObject(anError.getErrorBody()).optString("error"), anError.getErrorCode());
                        } catch (Exception e) {
                            e.printStackTrace();
                            listener.onFailure(MyApplication.getInstance().getString(R.string.ERROR_MSG), anError.getErrorCode());
                        }
                    }
                });
    }

    public void GetTransactionList(String user_id, int page, final DataObserver listener) {

        String url = Constants.BASE_URL + Constants.GET_TRANSACTION_LIST;
        AndroidNetworking.post(url)
                .setPriority(Priority.HIGH)
                .addBodyParameter("page", String.valueOf(page))
                .addBodyParameter("user_id", "" + user_id)
                .addBodyParameter("insecure", Constants.INSECURE)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Debug.trace("Response", "" + response.toString());
                        listener.onSuccess(response);
                    }

                    @Override
                    public void onError(ANError anError) {
                        Debug.trace("ANError", "" + anError.getErrorBody());
                        try {
                            listener.onFailure(new JSONObject(anError.getErrorBody()).optString("error"), anError.getErrorCode());
                        } catch (Exception e) {
                            e.printStackTrace();
                            listener.onFailure(MyApplication.getInstance().getString(R.string.ERROR_MSG), anError.getErrorCode());
                        }
                    }
                });
    }

    public void getAboutData(final DataObserver listener) {

        String url = Constants.BASE_URL + Constants.ABOUT_US;
        AndroidNetworking.get(url)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Debug.trace("Response", "" + response.toString());
                        listener.onSuccess(response);
                    }

                    @Override
                    public void onError(ANError anError) {
                        Debug.trace("ANError", "" + anError.getErrorBody());
                        try {
                            listener.onFailure(new JSONObject(anError.getErrorBody()).optString("error"), anError.getErrorCode());
                        } catch (Exception e) {
                            e.printStackTrace();
                            listener.onFailure(MyApplication.getInstance().getString(R.string.ERROR_MSG), anError.getErrorCode());
                        }
                    }
                });
    }

    public void getTrustees(final DataObserver listener) {

        String url = Constants.BASE_URL + Constants.BOARD_OF_TRUSTEES;
        AndroidNetworking.post(url)
                .addBodyParameter("page", "1")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Debug.trace("Response", "" + response.toString());
                        listener.onSuccess(response);
                    }

                    @Override
                    public void onError(ANError anError) {
                        Debug.trace("ANError", "" + anError.getErrorBody());
                        try {
                            listener.onFailure(new JSONObject(anError.getErrorBody()).optString("error"), anError.getErrorCode());
                        } catch (Exception e) {
                            e.printStackTrace();
                            listener.onFailure(MyApplication.getInstance().getString(R.string.ERROR_MSG), anError.getErrorCode());
                        }
                    }
                });
    }

    public void submitContact(String userName, String email, String message, String subject, final DataObserver listener) {

        String url = Constants.BASE_URL + Constants.CONTACT_SUBMIT;
        AndroidNetworking.post(url)
                .addBodyParameter("_wpcf7", "629")
                .addBodyParameter("_wpcf7_version", "5.1.1")
                .addBodyParameter("_wpcf7_locale", "en")
                .addBodyParameter("_wpcf7_unit_tag", "wpcf7-f629-p19-o1")
                .addBodyParameter("_wpcf7_container_post", "19")
                .addBodyParameter("_wpcf7cf_hidden_group_fields", "[]")
                .addBodyParameter("_wpcf7cf_hidden_groups", "[]")
                .addBodyParameter("_wpcf7cf_visible_groups", "[]")
                .addBodyParameter("_wpcf7cf_options", "{&quot;form_id&quot;:629,&quot;conditions&quot;:[],&quot;settings&quot;:false}")
                .addBodyParameter("g-recaptcha-response", "")
                .addBodyParameter("your-name", userName)
                .addBodyParameter("your-email", email)
                .addBodyParameter("your-message", message)
                .addBodyParameter("your-subject", subject)
                .addBodyParameter("insecure", "cool")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Debug.trace("Response", "" + response.toString());
                        listener.onSuccess(response);
                    }

                    @Override
                    public void onError(ANError anError) {
                        Debug.trace("ANError", "" + anError.getErrorBody());
                        try {
                            listener.onFailure(new JSONObject(anError.getErrorBody()).optString("error"), anError.getErrorCode());
                        } catch (Exception e) {
                            e.printStackTrace();
                            listener.onFailure(MyApplication.getInstance().getString(R.string.ERROR_MSG), anError.getErrorCode());
                        }
                    }
                });

    }

    public void get(Context context, boolean dialogRequired, String url, Map<String, String> queryParams, DataObserver dataObserver) {

        if (dialogRequired) {
            CustomDialog.showLoadDialog((Activity) context);
        }
        String apiUrl = getApiUrl(url);
        Debug.trace("ApiUrl", apiUrl);
        AndroidNetworking.get(apiUrl)
                .addQueryParameter(queryParams)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Debug.trace("Response", "" + response.toString());
                        if (dialogRequired) {
                            CustomDialog.hideLoadDialog();
                        }
                        dataObserver.onSuccess(response);
                    }

                    @Override
                    public void onError(ANError anError) {
                        Debug.trace("ResponseError", anError.getErrorBody());

                        if (dialogRequired) {
                            CustomDialog.hideLoadDialog();
                        }

                        try {
                            dataObserver.onFailure(new JSONObject(anError.getErrorBody()).optString("error"), anError.getErrorCode());
                        } catch (Exception e) {
                            e.printStackTrace();
                            dataObserver.onFailure(MyApplication.getInstance().getString(R.string.ERROR_MSG), anError.getErrorCode());
                        }
                    }
                });
    }

    public void post(Context context, boolean dialogRequired, String url, Map<String, String> queryParams, DataObserver dataObserver) {

        if (dialogRequired) {
            CustomDialog.showLoadDialog((Activity) context);
        }

        String apiUrl = getApiUrl(url);
        Debug.trace("ApiUrl", apiUrl);
        AndroidNetworking.post(apiUrl)
                .addQueryParameter(queryParams)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Debug.trace("Response", "" + response.toString());
//                        Toast.makeText(MyApplication.getInstance(), response.toString(), Toast.LENGTH_LONG).show();
                        if (dialogRequired) {
                            CustomDialog.hideLoadDialog();
                        }
                        dataObserver.onSuccess(response);
                    }

                    @Override
                    public void onError(ANError anError) {
                        Debug.trace("ResponseError", anError.getErrorBody());
//                        Toast.makeText(MyApplication.getInstance(), anError.getErrorBody(), Toast.LENGTH_LONG).show();
                        if (dialogRequired) {
                            CustomDialog.hideLoadDialog();
                        }

                        try {
                            dataObserver.onFailure(new JSONObject(anError.getErrorBody()).optString("error"), anError.getErrorCode());
                        } catch (Exception e) {
                            e.printStackTrace();
                            dataObserver.onFailure(MyApplication.getInstance().getString(R.string.ERROR_MSG), anError.getErrorCode());
                        }
                    }
                });
    }

    public void upload(Context context, String url, Map<String, File> fileMap, Map<Object, Object> queryParams, DataObserver dataObserver) {

        String apiUrl = getApiUrl(url);
        Debug.trace("ApiUrl", apiUrl);
        AndroidNetworking.upload(apiUrl)
                .addMultipartFile(fileMap)
                .addQueryParameter(queryParams)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Debug.trace("Response", "" + response.toString());
                        dataObserver.onSuccess(response);
                    }

                    @Override
                    public void onError(ANError anError) {
                        Debug.trace("ResponseError", anError.getErrorBody());
                        try {
                            dataObserver.onFailure(new JSONObject(anError.getErrorBody()).optString("error"), anError.getErrorCode());
                        } catch (Exception e) {
                            e.printStackTrace();
                            dataObserver.onFailure(e.getLocalizedMessage(), anError.getErrorCode());
                        }
                    }
                });
    }

    private String getApiUrl(String urlPart) {
        if (!urlPart.startsWith("http")) {
            return Constants.BASE_URL + urlPart;
        } else {
            return urlPart;
        }
    }

    public interface DataObserver {

        void onSuccess(JSONObject response);

        void onFailure(String error, int errorCode);
    }
}
