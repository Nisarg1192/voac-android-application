package com.library.config;

import com.library.model.MediaFile;

import java.util.ArrayList;

public class PickerManager {

    public static PickerManager instance;

    private ArrayList<MediaFile> selectedItems = new ArrayList<>();

    private PickerManager() {
    }

    public static PickerManager getInstance() {
        //instantiate a new CustomerLab if we didn't instantiate one yet
        if (instance == null) {
            instance = new PickerManager();
        }
        return instance;
    }

    public ArrayList<MediaFile> getSelectedItems() {
        if (selectedItems == null) {
            selectedItems = new ArrayList<>();
        }
        return selectedItems;
    }

    public void setSelectedItems(ArrayList<MediaFile> selectedItems) {
        if (instance == null) {
            instance = new PickerManager();
        }
        if (selectedItems == null) {
            selectedItems = new ArrayList<>();
        }
        this.selectedItems = selectedItems;
    }

    public int getSelectedItemCount() {
        if (this.selectedItems == null) {
            this.selectedItems = new ArrayList<>();
        }
        return this.selectedItems.size();
    }

    public void add(MediaFile file) {
        this.selectedItems.add(file);
    }

    public void addAll(ArrayList<MediaFile> files) {
        this.selectedItems.addAll(files);
    }

    public boolean remove(MediaFile file) {
        return this.selectedItems.remove(file);
    }

    public void clear() {
        this.selectedItems.clear();
    }
}
