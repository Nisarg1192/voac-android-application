

package com.library.config;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.Nullable;

import com.library.model.MediaFile;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Configurations implements Parcelable {

    public static final int TYPE_FILE = 0;
    public static final int TYPE_IMAGE = 1;
    public static final int TYPE_AUDIO = 2;
    public static final int TYPE_VIDEO = 3;
    public static final int TYPE_MEDIA = 4;

    public static final Creator<Configurations> CREATOR = new Creator<Configurations>() {
        @Override
        public Configurations createFromParcel(Parcel in) {
            return new Configurations(in);
        }

        @Override
        public Configurations[] newArray(int size) {
            return new Configurations[size];
        }
    };
    private final boolean imageCaptureEnabled;
    private final boolean videoCaptureEnabled;
    /*private final boolean showVideos;
    private final boolean showImages;
    private final boolean showAudios;
    private final boolean showFiles;*/
    private final boolean singleClickSelection;
    private final boolean singleChoiceMode;
    private final boolean checkPermission, skipZeroSizeFiles;
    private final int imageSize, maxSelection;
    private final int landscapeSpanCount;
    private final int portraitSpanCount;
    private final String rootPath;
    private final String[] suffixes;
    private final ArrayList<MediaFile> selectedMediaFiles;
    private Matcher[] ignorePathMatchers;
    private final boolean ignoreNoMedia;
    private final boolean ignoreHiddenFile;
    private final boolean isCropEnabled;
    private final int mediaType;
    private final int maxCropDuration;

    private Configurations(Builder builder) {
        this.imageCaptureEnabled = builder.imageCapture;
        this.videoCaptureEnabled = builder.videoCapture;
        /*this.showVideos = builder.showVideos;
        this.showImages = builder.showImages;
        this.showAudios = builder.showAudios;
        this.showFiles = builder.showFiles;*/
        this.singleClickSelection = builder.singleClickSelection;
        this.singleChoiceMode = builder.singleChoiceMode;
        this.checkPermission = builder.checkPermission;
        this.skipZeroSizeFiles = builder.skipZeroSizeFiles;
        this.imageSize = builder.imageSize;
        this.maxSelection = builder.maxSelection;
        this.landscapeSpanCount = builder.landscapeSpanCount;
        this.portraitSpanCount = builder.portraitSpanCount;
        this.rootPath = builder.rootPath;
        this.suffixes = builder.suffixes;
        this.selectedMediaFiles = builder.selectedMediaFiles;
        setIgnorePathMatchers(builder.ignorePaths);
        this.ignoreNoMedia = builder.ignoreNoMedia;
        this.ignoreHiddenFile = builder.ignoreHiddenFile;
        this.isCropEnabled = builder.isCropEnabled;
        this.mediaType = builder.mediaType;
        this.maxCropDuration = builder.maxCropDuration;
    }

    protected Configurations(Parcel in) {
        imageCaptureEnabled = in.readByte() != 0;
        videoCaptureEnabled = in.readByte() != 0;
        /*showVideos = in.readByte() != 0;
        showImages = in.readByte() != 0;
        showAudios = in.readByte() != 0;
        showFiles = in.readByte() != 0;*/
        singleClickSelection = in.readByte() != 0;
        singleChoiceMode = in.readByte() != 0;
        checkPermission = in.readByte() != 0;
        skipZeroSizeFiles = in.readByte() != 0;
        imageSize = in.readInt();
        maxSelection = in.readInt();
        landscapeSpanCount = in.readInt();
        portraitSpanCount = in.readInt();
        rootPath = in.readString();
        suffixes = in.createStringArray();
        selectedMediaFiles = in.createTypedArrayList(MediaFile.CREATOR);
        setIgnorePathMatchers(in.createStringArray());
        ignoreNoMedia = in.readByte() != 0;
        ignoreHiddenFile = in.readByte() != 0;
        isCropEnabled = in.readByte() != 0;
        mediaType = in.readInt();
        maxCropDuration = in.readInt();
    }

    /*public boolean isShowVideos() {
        return showVideos;
    }

    public boolean isShowImages() {
        return showImages;
    }

    public boolean isShowAudios() {
        return showAudios;
    }

    public boolean isShowFiles() {
        return showFiles;
    }*/

    public boolean isSkipZeroSizeFiles() {
        return skipZeroSizeFiles;
    }

    public ArrayList<MediaFile> getSelectedMediaFiles() {
        return selectedMediaFiles;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (imageCaptureEnabled ? 1 : 0));
        dest.writeByte((byte) (videoCaptureEnabled ? 1 : 0));
        /*dest.writeByte((byte) (showVideos ? 1 : 0));
        dest.writeByte((byte) (showImages ? 1 : 0));
        dest.writeByte((byte) (showAudios ? 1 : 0));
        dest.writeByte((byte) (showFiles ? 1 : 0));*/
        dest.writeByte((byte) (singleClickSelection ? 1 : 0));
        dest.writeByte((byte) (singleChoiceMode ? 1 : 0));
        dest.writeByte((byte) (checkPermission ? 1 : 0));
        dest.writeByte((byte) (skipZeroSizeFiles ? 1 : 0));
        dest.writeInt(imageSize);
        dest.writeInt(maxSelection);
        dest.writeInt(landscapeSpanCount);
        dest.writeInt(portraitSpanCount);
        dest.writeString(rootPath);
        dest.writeStringArray(suffixes);
        dest.writeTypedList(selectedMediaFiles);
        dest.writeStringArray(getIgnorePaths());
        dest.writeByte((byte) (ignoreNoMedia ? 1 : 0));
        dest.writeByte((byte) (ignoreHiddenFile ? 1 : 0));
        dest.writeByte((byte) (isCropEnabled ? 1 : 0));
        dest.writeInt(mediaType);
        dest.writeInt(maxCropDuration);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public boolean isSingleClickSelection() {
        return singleClickSelection;
    }

    public boolean isSingleChoiceMode() {
        return singleChoiceMode;
    }

    public int getMaxSelection() {
        return maxSelection;
    }

    public int getLandscapeSpanCount() {
        return landscapeSpanCount;
    }

    public int getPortraitSpanCount() {
        return portraitSpanCount;
    }

    public boolean isCheckPermission() {
        return checkPermission;
    }

    public boolean isImageCaptureEnabled() {
        return imageCaptureEnabled;
    }

    public boolean isVideoCaptureEnabled() {
        return videoCaptureEnabled;
    }

    public int getImageSize() {
        return imageSize;
    }

    public String getRootPath() {
        return rootPath;
    }

    public String[] getSuffixes() {
        return suffixes;
    }

    public Matcher[] getIgnorePathMatchers() {
        return ignorePathMatchers;
    }

    public boolean isIgnoreNoMediaDir() {
        return ignoreNoMedia;
    }

    public boolean isIgnoreHiddenFile() {
        return ignoreHiddenFile;
    }

    public boolean isCropEnabled() {
        return isCropEnabled;
    }

    public int getMediaType() {
        return mediaType;
    }

    public int getMaxCropDuration() {
        return maxCropDuration;
    }

    private void setIgnorePathMatchers(String ignorePaths[]) {
        if (ignorePaths != null && ignorePaths.length > 0) {
            ignorePathMatchers = new Matcher[ignorePaths.length];
            for (int i = 0; i < ignorePaths.length; i++) {
                ignorePathMatchers[i] = Pattern.compile(ignorePaths[i]).matcher("");
            }
        }
    }

    @Nullable
    private String[] getIgnorePaths() {
        if (ignorePathMatchers != null && ignorePathMatchers.length > 0) {
            String ignorePaths[] = new String[ignorePathMatchers.length];
            for (int i = 0; i < ignorePaths.length; i++) {
                ignorePaths[i] = ignorePathMatchers[i].pattern().pattern();
            }
            return ignorePaths;
        }
        return null;
    }

    public static class Builder {
        private boolean imageCapture = true, videoCapture = false,
                checkPermission = false, singleClickSelection = true,
                singleChoiceMode = false, skipZeroSizeFiles = true;
        private int imageSize = -1, maxSelection = -1;
        private int landscapeSpanCount = 5;
        private int portraitSpanCount = 3;
        private String rootPath;
        private String[] suffixes = new String[]{
                "txt", "pdf", "html", "rtf", "csv", "xml",
                "zip", "tar", "gz", "rar", "7z", "torrent",
                "doc", "docx", "odt", "ott",
                "ppt", "pptx", "pps",
                "xls", "xlsx", "ods", "ots"};
        private ArrayList<MediaFile> selectedMediaFiles = null;
        private String[] ignorePaths = null;
        private boolean ignoreNoMedia = true;
        private boolean ignoreHiddenFile = true;
        private boolean isCropEnabled = true;
        private int mediaType = TYPE_IMAGE;
        private int maxCropDuration = 0;

        public Builder setSingleClickSelection(boolean singleClickSelection) {
            this.singleClickSelection = singleClickSelection;
            return this;
        }

        public Builder setSingleChoiceMode(boolean singleChoiceMode) {
            this.singleChoiceMode = singleChoiceMode;
            this.maxSelection = 1;
            this.selectedMediaFiles = null;
            return this;
        }

        public Builder setSkipZeroSizeFiles(boolean skipZeroSizeFiles) {
            this.skipZeroSizeFiles = skipZeroSizeFiles;
            return this;
        }

        public Builder setLandscapeSpanCount(int landscapeSpanCount) {
            if (landscapeSpanCount > 5) {
                this.landscapeSpanCount = 5;
            } else {
                this.landscapeSpanCount = landscapeSpanCount;
            }
            return this;
        }

        public Builder setPortraitSpanCount(int portraitSpanCount) {
            if (portraitSpanCount > 3) {
                this.portraitSpanCount = 3;
            } else {
                this.portraitSpanCount = portraitSpanCount;
            }
            return this;
        }

        public Builder setMaxSelection(int maxSelection) {
            if (!singleChoiceMode)
                this.maxSelection = maxSelection;
            return this;
        }

        public Builder setCheckPermission(boolean checkPermission) {
            this.checkPermission = checkPermission;
            return this;
        }

        public Builder enableImageCapture(boolean imageCapture) {
            this.imageCapture = imageCapture;
            return this;
        }

        public Builder enableVideoCapture(boolean videoCapture) {
            this.videoCapture = videoCapture;
            return this;
        }

        public Builder setImageSize(int imageSize) {
            this.imageSize = imageSize;
            return this;
        }

        public Builder setRootPath(String rootPath) {
            this.rootPath = rootPath;
            return this;
        }

        public Builder setSuffixes(String... suffixes) {
            this.suffixes = suffixes;
            return this;
        }

        public Builder setSelectedMediaFiles(ArrayList<MediaFile> selectedMediaFiles) {
            if (!singleChoiceMode)
                this.selectedMediaFiles = selectedMediaFiles;
            return this;
        }

        public Builder setSelectedMediaFile(@Nullable MediaFile selectedMediaFile) {
            if (selectedMediaFile != null) {
                this.selectedMediaFiles = new ArrayList<>();
                this.selectedMediaFiles.add(selectedMediaFile);
            }
            return this;
        }

        public Builder setIgnorePaths(String... ignorePaths) {
            this.ignorePaths = ignorePaths;
            return this;
        }

        public Builder setIgnoreNoMedia(boolean ignoreNoMedia) {
            this.ignoreNoMedia = ignoreNoMedia;
            return this;
        }

        public Builder setIgnoreHiddenFile(boolean ignoreHiddenFile) {
            this.ignoreHiddenFile = ignoreHiddenFile;
            return this;
        }

        public Builder setIsCropEnabled(boolean isCropEnabled) {
            this.isCropEnabled = isCropEnabled;
            return this;
        }

        public Builder setChooseMediaType(int mediaType) {
            if (mediaType != TYPE_FILE && mediaType != TYPE_IMAGE && mediaType != TYPE_MEDIA && mediaType != TYPE_AUDIO && mediaType != TYPE_VIDEO) {
                this.mediaType = TYPE_IMAGE;
            } else {
                this.mediaType = mediaType;
            }
            return this;
        }

        public Builder setMaxCropDuration(int maxCropDuration) {
            if (maxCropDuration > 0) {
                this.maxCropDuration = maxCropDuration;
            } else {
                this.maxCropDuration = 0;
            }
            return this;
        }

        public Configurations build() {
            if (this.singleChoiceMode) {
                this.maxSelection = 1;
                this.selectedMediaFiles = null;
            }
            if (this.mediaType == Configurations.TYPE_IMAGE) {
                this.videoCapture = false;
            }

//            if (this.mediaType == Configurations.TYPE_MEDIA) {
//                this.videoCapture = true;
//                this.imageCapture = true;
//            }

            if (this.mediaType == Configurations.TYPE_VIDEO) {
                this.imageCapture = false;
            }
            if (this.mediaType == Configurations.TYPE_FILE) {
                this.imageCapture = false;
                this.videoCapture = false;
                if (this.portraitSpanCount > 2) this.portraitSpanCount = 2;
            }
            if (this.mediaType == Configurations.TYPE_AUDIO) {
                this.imageCapture = false;
                this.videoCapture = false;
            }
            return new Configurations(this);
        }
    }
}
