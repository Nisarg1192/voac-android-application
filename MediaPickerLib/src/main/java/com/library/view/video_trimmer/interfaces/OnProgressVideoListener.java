package com.library.view.video_trimmer.interfaces;

public interface OnProgressVideoListener {

    void updateProgress(int time, int max, float scale);
}
