package com.library.view.video_trimmer;

import android.app.ProgressDialog;
import android.content.CursorLoader;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;

import com.googlecode.mp4parser.h264.Debug;
import com.library.R;
import com.library.activity.VideoGalleryActivity;
import com.library.view.video_trimmer.interfaces.OnHgLVideoListener;
import com.library.view.video_trimmer.interfaces.OnTrimVideoListener;

import androidx.appcompat.app.AppCompatActivity;

public class TrimmerActivity extends AppCompatActivity implements OnTrimVideoListener, OnHgLVideoListener {

    private HgLVideoTrimmer mVideoTrimmer;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trimmer);

        Intent extraIntent = getIntent();
        String path = "";
        int maxDuration = 10;

        if (extraIntent != null) {
            path = extraIntent.getStringExtra(VideoGalleryActivity.EXTRA_VIDEO_PATH);
            maxDuration = extraIntent.getIntExtra(VideoGalleryActivity.VIDEO_TOTAL_DURATION, 10);
        }

        //setting progressbar
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(getString(R.string.trimming_progress));

        mVideoTrimmer = ((HgLVideoTrimmer) findViewById(R.id.timeLine));
        if (mVideoTrimmer != null) {


            /**
             * get total duration of video file
             */
            Debug.trace("tg", "maxDuration = " + maxDuration);
            //mVideoTrimmer.setMaxDuration(maxDuration);
            mVideoTrimmer.setMaxDuration(maxDuration);
            mVideoTrimmer.setOnTrimVideoListener(this);
            mVideoTrimmer.setOnHgLVideoListener(this);
            mVideoTrimmer.setVideoURI(Uri.parse(path));
            mVideoTrimmer.setVideoInformationVisibility(true);
        }
    }

    @Override
    public void onTrimStarted() {
        if (mProgressDialog != null) {
            mProgressDialog.show();
        }
    }

    @Override
    public void getResult(final Uri contentUri) {
        if (mProgressDialog != null) {
            mProgressDialog.cancel();
        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // Toast.makeText(TrimmerActivity.this, getString(R.string.video_saved_at, contentUri.getPath()), Toast.LENGTH_SHORT).show();

            }
        });

        try {


            String path = contentUri.getPath();

            Intent intent = new Intent();
            intent.putExtra(VideoGalleryActivity.EXTRA_VIDEO_PATH, "" + path);
            setResult(RESULT_OK, intent);
            finish();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        CursorLoader loader = new CursorLoader(TrimmerActivity.this, contentUri, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_index);
        cursor.close();
        return result;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Debug.trace("tg", "resultCode = " + resultCode + " data " + data);
    }

    @Override
    public void cancelAction() {
        if (mProgressDialog != null) {
            mProgressDialog.cancel();
        }
        mVideoTrimmer.destroy();
        finish();
    }

    @Override
    public void onError(final String message) {
        if (mProgressDialog != null) {
            mProgressDialog.cancel();
        }

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // Toast.makeText(TrimmerActivity.this, message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onVideoPrepared() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // Toast.makeText(TrimmerActivity.this, "onVideoPrepared", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }
}
