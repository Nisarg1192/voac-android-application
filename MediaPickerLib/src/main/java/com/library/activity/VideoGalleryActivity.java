package com.library.activity;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.library.R;
import com.library.adapter.ImageGalleryAdapter;
import com.library.adapter.OnSelectionListener;
import com.library.adapter.VideoGalleryAdapter;
import com.library.config.Configurations;
import com.library.model.MediaFile;
import com.library.utils.FileUtils;
import com.library.view.cropper.CropImage;
import com.library.view.cropper.CropImageActivity;
import com.library.view.video_trimmer.TrimmerActivity;

import java.io.File;
import java.util.ArrayList;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import static android.provider.BaseColumns._ID;
import static android.provider.MediaStore.Files.FileColumns.MIME_TYPE;
import static android.provider.MediaStore.Images.ImageColumns.BUCKET_DISPLAY_NAME;
import static android.provider.MediaStore.Images.ImageColumns.BUCKET_ID;
import static android.provider.MediaStore.MediaColumns.DATA;
import static android.provider.MediaStore.MediaColumns.DATE_ADDED;
import static android.provider.MediaStore.MediaColumns.HEIGHT;
import static android.provider.MediaStore.MediaColumns.SIZE;
import static android.provider.MediaStore.MediaColumns.TITLE;
import static android.provider.MediaStore.MediaColumns.WIDTH;
import static com.library.activity.FilePickerActivity.MEDIA_FILES;

public class VideoGalleryActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String EXTRA_VIDEO_PATH = "EXTRA_VIDEO_PATH";
    public static final String VIDEO_TOTAL_DURATION = "VIDEO_TOTAL_DURATION";
    public static final int TRIM_VIDEO_ACTIVITY_REQUEST_CODE = 111;

    VideoGalleryAdapter initaliseadapter;
    ArrayList<MediaFile> arrayList = new ArrayList<>();
    ArrayList<MediaFile> arrayTempCropList = new ArrayList<>();
    private RequestManager glide;
    private ImageView mIvBack;
    private ImageView mIvCrop;
    private ImageView btnPlay;
    private TextView mTvDone;
    private RelativeLayout mRlToolbar;
    private VideoView vidPreview;
    private RecyclerView mInstantRecyclerView;
    int selectedImagePos;
    Activity activity = VideoGalleryActivity.this;
    private Configurations configs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_gallery);
        initView();
    }

    /**
     * init views
     */
    private void initView() {
        mIvBack = (ImageView) findViewById(R.id.ivBack);
        mIvCrop = (ImageView) findViewById(R.id.ivCrop);
        btnPlay = (ImageView) findViewById(R.id.btnPlay);
        mTvDone = (TextView) findViewById(R.id.tvDone);
        mRlToolbar = (RelativeLayout) findViewById(R.id.rlToolbar);
        vidPreview = (VideoView) findViewById(R.id.vidPreview);
        mInstantRecyclerView = (RecyclerView) findViewById(R.id.instantRecyclerView);

        configs = getIntent().getParcelableExtra(FilePickerActivity.CONFIGS);
        if (configs == null) {
            configs = new Configurations.Builder().build();
        }
        arrayList = (ArrayList<MediaFile>) getIntent().getSerializableExtra(MEDIA_FILES);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mInstantRecyclerView.setLayoutManager(linearLayoutManager);
        initaliseadapter = new VideoGalleryAdapter(this);
        initaliseadapter.addOnSelectionListener(onSelectionListener);
        mInstantRecyclerView.setAdapter(initaliseadapter);

        initaliseadapter.addImageList(arrayList, true);

        /** get data from array list and Display and set adapter */

        if (arrayList != null && arrayList.size() == 1) {
            mInstantRecyclerView.setVisibility(View.GONE);
            selectedImagePos = 0;
        }

        glide = Glide.with(this);
        if (arrayList != null && arrayList.size() > 0) {
            vidPreview.setVideoURI(Uri.fromFile(new File(arrayList.get(0).getPath())));
            selectedImagePos = 0;
            arrayList.get(selectedImagePos).setSelected(1);
            initaliseadapter.notifyDataSetChanged();
            vidPreview.seekTo(1);
        }

        mTvDone.setOnClickListener(this);
        mIvBack.setOnClickListener(this);
        mIvCrop.setOnClickListener(this);
        vidPreview.setOnClickListener(this);
        vidPreview.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                btnPlay.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.tvDone) {
            pauseVideo();
            /** Return to FilePicker activity with images data */
            Intent intent = new Intent();
            intent.putExtra(MEDIA_FILES, arrayList);
            setResult(RESULT_OK, intent);
            finish();
        } else if (i == R.id.ivBack) {
            pauseVideo();
            onBackPressed();
        } else if (i == R.id.ivCrop) {
            pauseVideo();
            /** Open Crop Activty for crop Image */
            if (arrayList != null && arrayList.size() > selectedImagePos) {
                startTrimActivity(Uri.fromFile(new File(arrayList.get(selectedImagePos).getPath())));
            }
        } else if (i == R.id.vidPreview) {
            if (btnPlay.getVisibility() == View.VISIBLE) {
                vidPreview.start();
                btnPlay.setVisibility(View.GONE);
            } else {
                vidPreview.pause();
                btnPlay.setVisibility(View.VISIBLE);
            }
        }
    }

    public void pauseVideo(){
        if (vidPreview != null) {
            vidPreview.pause();
        }
        btnPlay.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onResume() {
        try {
            if (vidPreview != null) {
                vidPreview.seekTo((vidPreview.getCurrentPosition() == 0) ? 1 : vidPreview.getCurrentPosition());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onResume();
    }

    private void startTrimActivity(@NonNull Uri uri) {
        if (vidPreview != null) {
            vidPreview.pause();
        }
        btnPlay.setVisibility(View.VISIBLE);
        Intent intent = new Intent(this, TrimmerActivity.class);
        intent.putExtra(EXTRA_VIDEO_PATH, FileUtils.getPath(this, uri));
        intent.putExtra(VIDEO_TOTAL_DURATION, (configs.getMaxCropDuration() > 0) ? configs.getMaxCropDuration() : getMediaDuration(uri));
        startActivityForResult(intent, TRIM_VIDEO_ACTIVITY_REQUEST_CODE);
    }

    private int getMediaDuration(Uri uriOfFile) {
        MediaPlayer mp = MediaPlayer.create(this, uriOfFile);
        int duration = mp.getDuration();
        return duration;
    }

    /**
     * set Selected method for select image
     *
     * @param pos position of item of image
     */
    public void setSelectedItem(int pos) {
        for (int i = 0; i < arrayList.size(); i++) {
            arrayList.get(i).setSelected((i == pos) ? 1 : 0);
        }
        initaliseadapter.addImageList(arrayList, true);
    }

    /**
     * Set Selection Listener of Recylerview
     */

    private OnSelectionListener onSelectionListener = new OnSelectionListener() {
        @Override
        public void onClick(MediaFile Img, View view, int position) {

            if (arrayList != null && arrayList.size() > position) {
                vidPreview.stopPlayback();
                vidPreview.setVideoURI(Uri.fromFile(new File(arrayList.get(position).getPath())));
                vidPreview.seekTo(1);
                btnPlay.setVisibility(View.VISIBLE);
                selectedImagePos = position;
                setSelectedItem(position);
            }

        }

        @Override
        public void onLongClick(MediaFile img, View view, int position) {

        }
    };

    /**
     * Activity Result of Crop Image
     */

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case TRIM_VIDEO_ACTIVITY_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    if (data != null && data.hasExtra(EXTRA_VIDEO_PATH)) {
                        String path = data.getStringExtra(EXTRA_VIDEO_PATH);
                        MediaFile mediaFile = getFileData(FileUtils.getImageContentUri(this, new File(path)));
                        if (mediaFile != null) {
                            arrayList.set(selectedImagePos, mediaFile);
                            vidPreview.setVideoPath(arrayList.get(selectedImagePos).getPath());
                            setSelectedItem(selectedImagePos);
                            arrayTempCropList.add(mediaFile);
                            vidPreview.seekTo(1);
                        }
                    }


                }
                break;
        }
    }


    public MediaFile getFileData(Uri uri) {

        /** The query, since it only applies to a single document, will only return
         * one row. There's no need to filter, sort, or select fields, since we want
         * all fields for one document.*/
        MediaFile mediaFile = null;
        Cursor cursor = null;
        final String[] projection = {
                MediaStore.Files.FileColumns._ID,
                MediaStore.Files.FileColumns.TITLE,
                MediaStore.Files.FileColumns.DATA,
                MediaStore.Files.FileColumns.SIZE,
                MediaStore.Files.FileColumns.DATE_ADDED,
                MediaStore.Files.FileColumns.MIME_TYPE,
                MediaStore.Images.Media.BUCKET_ID,
                MediaStore.Images.Media.BUCKET_DISPLAY_NAME,
                MediaStore.Files.FileColumns.HEIGHT,
                MediaStore.Files.FileColumns.WIDTH
        };
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            cursor = getContentResolver().query(uri, projection, null, null, null);
        }

        try {
            /** moveToFirst() returns false if the cursor has 0 rows.  Very handy for*/
            /** "if there's anything to look at, look at it" conditionals.*/
            if (cursor != null) {
                if (cursor.moveToFirst()) {

                    /** Note it's called "Display Name".  This is
                     provider-specific, and might not necessarily be the file name.*/
                    long size = cursor.getLong(cursor.getColumnIndex(SIZE));
                    mediaFile = new MediaFile();
                    mediaFile.setSize(size);
                    mediaFile.setId(cursor.getLong(cursor.getColumnIndex(_ID)));
                    mediaFile.setName(cursor.getString(cursor.getColumnIndex(TITLE)));
                    mediaFile.setPath(cursor.getString(cursor.getColumnIndex(DATA)));
                    mediaFile.setDate(cursor.getLong(cursor.getColumnIndex(DATE_ADDED)));
                    mediaFile.setMimeType(cursor.getString(cursor.getColumnIndex(MIME_TYPE)));
//                    mediaFile.setMediaType(cursor.getInt(cursor.getColumnIndex(MEDIA_TYPE)));
                    if (mediaFile.getMediaType() == MediaFile.TYPE_FILE
                            && mediaFile.getMimeType() != null) {
                        //Double check correct MediaType
                        mediaFile.setMediaType(getMediaType(mediaFile.getMimeType()));
                    }
                    mediaFile.setBucketId(cursor.getString(cursor.getColumnIndex(BUCKET_ID)));
                    mediaFile.setBucketName(cursor.getString(cursor.getColumnIndex(BUCKET_DISPLAY_NAME)));
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        mediaFile.setHeight(cursor.getLong(cursor.getColumnIndex(HEIGHT)));
                        mediaFile.setWidth(cursor.getLong(cursor.getColumnIndex(WIDTH)));
                    }
                }
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return mediaFile;
    }


    private static @MediaFile.Type
    int getMediaType(String mime) {
        if (mime.startsWith("image/")) {
            return MediaFile.TYPE_IMAGE;
        } else if (mime.startsWith("video/")) {
            return MediaFile.TYPE_VIDEO;
        } else if (mime.startsWith("audio/")) {
            return MediaFile.TYPE_AUDIO;
        } else {
            return MediaFile.TYPE_FILE;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        try {
            FileUtils.deleteCache(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }


}
