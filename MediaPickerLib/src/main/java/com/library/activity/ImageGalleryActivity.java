package com.library.activity;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.googlecode.mp4parser.h264.Debug;
import com.library.R;
import com.library.adapter.ImageGalleryAdapter;
import com.library.adapter.OnSelectionListener;
import com.library.model.MediaFile;
import com.library.utils.FileUtils;
import com.library.view.cropper.CropImage;
import com.library.view.cropper.CropImageActivity;

import java.io.File;
import java.util.ArrayList;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import static android.provider.BaseColumns._ID;
import static android.provider.MediaStore.Files.FileColumns.MIME_TYPE;
import static android.provider.MediaStore.Images.ImageColumns.BUCKET_DISPLAY_NAME;
import static android.provider.MediaStore.Images.ImageColumns.BUCKET_ID;
import static android.provider.MediaStore.MediaColumns.DATA;
import static android.provider.MediaStore.MediaColumns.DATE_ADDED;
import static android.provider.MediaStore.MediaColumns.HEIGHT;
import static android.provider.MediaStore.MediaColumns.SIZE;
import static android.provider.MediaStore.MediaColumns.TITLE;
import static android.provider.MediaStore.MediaColumns.WIDTH;
import static com.library.activity.FilePickerActivity.MEDIA_FILES;

public class ImageGalleryActivity extends AppCompatActivity implements View.OnClickListener {

    ImageGalleryAdapter initaliseadapter;
    ArrayList<MediaFile> arrayList = new ArrayList<>();
    ArrayList<MediaFile> arrayTempCropList = new ArrayList<>();
    private RequestManager glide;
    private ImageView mIvBack;
    private ImageView mIvCrop;

    private TextView mTvDone;
    private RelativeLayout mRlToolbar;
    private ImageView mImgPreview;
    private RecyclerView mInstantRecyclerView;

    int selectedImagePos;
    Activity activity = ImageGalleryActivity.this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_gallery);
        initView();
    }

    /**
     * init views
     */
    private void initView() {
        mIvBack = (ImageView) findViewById(R.id.ivBack);
        mIvCrop = (ImageView) findViewById(R.id.ivCrop);
        mTvDone = (TextView) findViewById(R.id.tvDone);
        mRlToolbar = (RelativeLayout) findViewById(R.id.rlToolbar);
        mImgPreview = (ImageView) findViewById(R.id.imgPreview);
        mInstantRecyclerView = (RecyclerView) findViewById(R.id.instantRecyclerView);


        arrayList = (ArrayList<MediaFile>) getIntent().getSerializableExtra(MEDIA_FILES);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mInstantRecyclerView.setLayoutManager(linearLayoutManager);
        initaliseadapter = new ImageGalleryAdapter(this);
        initaliseadapter.addOnSelectionListener(onSelectionListener);
        mInstantRecyclerView.setAdapter(initaliseadapter);

        initaliseadapter.addImageList(arrayList, true);

        /** get data from array list and Display and set adapter */

        if (arrayList != null && arrayList.size() == 1) {
            mInstantRecyclerView.setVisibility(View.GONE);
            selectedImagePos = 0;
        }

        glide = Glide.with(this);
        if (arrayList != null && arrayList.size() > 0) {
            glide.load(arrayList.get(0).getPath()).into(mImgPreview);
            selectedImagePos = 0;
            arrayList.get(selectedImagePos).setSelected(1);
            initaliseadapter.notifyDataSetChanged();
        }

        mTvDone.setOnClickListener(this);
        mIvBack.setOnClickListener(this);
        mIvCrop.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.tvDone) {
            /** Return to FilePicker activity with images data */
            Intent intent = new Intent();
            intent.putExtra(MEDIA_FILES, arrayList);
            setResult(RESULT_OK, intent);
            finish();
        } else if (i == R.id.ivBack) {
            onBackPressed();
        } else if (i == R.id.ivCrop) {
            /** Open Crop Activty for crop Image */
            if (arrayList != null && arrayList.size() > selectedImagePos) {
                CropImage.activity(Uri.parse(new File(arrayList.get(selectedImagePos).getPath()).toURI().toString()))
                        .start(activity, CropImageActivity.class);
            }
        }
    }

    /**
     * set Selected method for select image
     *
     * @param pos position of item of image
     */
    public void setSelectedItem(int pos) {
        for (int i = 0; i < arrayList.size(); i++) {
            arrayList.get(i).setSelected((i == pos) ? 1 : 0);
        }
        initaliseadapter.addImageList(arrayList, true);
    }

    /**
     * Set Selection Listener of Recylerview
     */

    private OnSelectionListener onSelectionListener = new OnSelectionListener() {
        @Override
        public void onClick(MediaFile Img, View view, int position) {

            if (arrayList != null && arrayList.size() > position) {
                glide.load(arrayList.get(position).getPath()).into(mImgPreview);
                selectedImagePos = position;
                setSelectedItem(position);
            }

        }

        @Override
        public void onLongClick(MediaFile img, View view, int position) {

        }
    };

    /**
     * Activity Result of Crop Image
     */

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                if (resultCode == RESULT_OK) {

                    MediaFile mediaFile = getFileData(FileUtils.getImageContentUri(this, FileUtils.saveToInternalStorage(ImageGalleryActivity.this, result.getUri(), arrayList.get(selectedImagePos).getName())));
                    if (mediaFile != null) {
                        arrayList.set(selectedImagePos, mediaFile);
                        glide.load(arrayList.get(selectedImagePos).getPath()).into(mImgPreview);
                        setSelectedItem(selectedImagePos);
                        arrayTempCropList.add(mediaFile);
                    }

                } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                    Exception error = result.getError();
                    Debug.trace("error", "onActivityResult: " + error.getMessage());
                }
                break;
        }
    }


    public MediaFile getFileData(Uri uri) {

        /** The query, since it only applies to a single document, will only return
         * one row. There's no need to filter, sort, or select fields, since we want
         * all fields for one document.*/
        MediaFile mediaFile = null;
        Cursor cursor = null;
        final String[] projection = {
                MediaStore.Files.FileColumns._ID,
                MediaStore.Files.FileColumns.TITLE,
                MediaStore.Files.FileColumns.DATA,
                MediaStore.Files.FileColumns.SIZE,
                MediaStore.Files.FileColumns.DATE_ADDED,
                MediaStore.Files.FileColumns.MIME_TYPE,
                MediaStore.Images.Media.BUCKET_ID,
                MediaStore.Images.Media.BUCKET_DISPLAY_NAME,
                MediaStore.Files.FileColumns.HEIGHT,
                MediaStore.Files.FileColumns.WIDTH
        };
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
            cursor = getContentResolver().query(uri, projection, null, null, null);
        }

        try {
            /** moveToFirst() returns false if the cursor has 0 rows.  Very handy for*/
            /** "if there's anything to look at, look at it" conditionals.*/
            if (cursor != null) {
                if (cursor.moveToFirst()) {

                    /** Note it's called "Display Name".  This is
                     provider-specific, and might not necessarily be the file name.*/
                    long size = cursor.getLong(cursor.getColumnIndex(SIZE));
                    mediaFile = new MediaFile();
                    mediaFile.setSize(size);
                    mediaFile.setId(cursor.getLong(cursor.getColumnIndex(_ID)));
                    mediaFile.setName(cursor.getString(cursor.getColumnIndex(TITLE)));
                    mediaFile.setPath(cursor.getString(cursor.getColumnIndex(DATA)));
                    mediaFile.setDate(cursor.getLong(cursor.getColumnIndex(DATE_ADDED)));
                    mediaFile.setMimeType(cursor.getString(cursor.getColumnIndex(MIME_TYPE)));
//                    mediaFile.setMediaType(cursor.getInt(cursor.getColumnIndex(MEDIA_TYPE)));
                    if (mediaFile.getMediaType() == MediaFile.TYPE_FILE
                            && mediaFile.getMimeType() != null) {
                        //Double check correct MediaType
                        mediaFile.setMediaType(getMediaType(mediaFile.getMimeType()));
                    }
                    mediaFile.setBucketId(cursor.getString(cursor.getColumnIndex(BUCKET_ID)));
                    mediaFile.setBucketName(cursor.getString(cursor.getColumnIndex(BUCKET_DISPLAY_NAME)));
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        mediaFile.setHeight(cursor.getLong(cursor.getColumnIndex(HEIGHT)));
                        mediaFile.setWidth(cursor.getLong(cursor.getColumnIndex(WIDTH)));
                    }
                }
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return mediaFile;
    }


    private static @MediaFile.Type
    int getMediaType(String mime) {
        if (mime.startsWith("image/")) {
            return MediaFile.TYPE_IMAGE;
        } else if (mime.startsWith("video/")) {
            return MediaFile.TYPE_VIDEO;
        } else if (mime.startsWith("audio/")) {
            return MediaFile.TYPE_AUDIO;
        } else {
            return MediaFile.TYPE_FILE;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        try {
            FileUtils.deleteCache(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }


}
