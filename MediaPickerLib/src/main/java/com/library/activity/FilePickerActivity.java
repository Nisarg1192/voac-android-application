

package com.library.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Point;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.library.R;
import com.library.adapter.FileGalleryAdapter;
import com.library.adapter.MultiSelectionAdapter;
import com.library.config.Configurations;
import com.library.config.PickerManager;
import com.library.loader.FileLoader;
import com.library.loader.FileResultCallback;
import com.library.model.MediaDirFile;
import com.library.model.MediaFile;
import com.library.utils.FileUtils;
import com.library.view.DividerItemDecoration;

import java.io.File;
import java.util.ArrayList;

public class FilePickerActivity extends AppCompatActivity
        implements MultiSelectionAdapter.OnSelectionListener<FileGalleryAdapter.ViewHolder>, FileGalleryAdapter.OnCameraClickListener {
    public static final String MEDIA_FILES = "MEDIA_FILES";
    public static final String MEDIA_DIR = "MEDIA_DIR";
    public static final String SELECTED_MEDIA_FILES = "SELECTED_MEDIA_FILES";
    public static final String CONFIGS = "CONFIGS";
    public static final String TAG = "FilePicker";
    private static final String PATH = "PATH";
    private static final String URI = "URI";
    private static final int CROP_REQUEST_CODE = 1123;
    private static final int REQUEST_WRITE_PERMISSION = 1;
    private static final int REQUEST_CAMERA_PERMISSION_FOR_CAMERA = 2;
    private static final int REQUEST_CAMERA_PERMISSION_FOR_VIDEO = 3;
    private Configurations configs;
    private ArrayList<MediaFile> mediaFiles = new ArrayList<>();
    private FileGalleryAdapter fileGalleryAdapter;
    private int maxCount;
    MenuItem menuItem;
    boolean isCheckedCropped = false;
    MediaDirFile dirFile;

    /**
     * In FilePicker Activity get configuration and set data according to that
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.filepicker_gallery);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        /**
         * get configuration using intent data
         */

        configs = getIntent().getParcelableExtra(CONFIGS);
        if (configs == null) {
            configs = new Configurations.Builder().build();
        }
        if (getIntent().hasExtra(MEDIA_DIR)) {
            dirFile = getIntent().getParcelableExtra(MEDIA_DIR);
        }
        if (dirFile == null) {
            dirFile = new MediaDirFile();
        }

        int spanCount;
        if (getResources().getConfiguration().orientation
                == Configuration.ORIENTATION_LANDSCAPE) {
            spanCount = configs.getLandscapeSpanCount();
        } else {
            spanCount = configs.getPortraitSpanCount();
        }

        int imageSize = configs.getImageSize();
        if (imageSize <= 0) {
            Point point = new Point();
            getWindowManager().getDefaultDisplay().getSize(point);
            imageSize = Math.min(point.x, point.y) / configs.getPortraitSpanCount();
        }

        boolean isSingleChoice = configs.isSingleChoiceMode();
        fileGalleryAdapter = new FileGalleryAdapter(this, configs.getMediaType(), mediaFiles, imageSize,
                false, false);
        fileGalleryAdapter.enableSelection(true);
        fileGalleryAdapter.enableSingleClickSelection(configs.isSingleClickSelection());
        fileGalleryAdapter.setOnSelectionListener(this);
        fileGalleryAdapter.setSingleChoiceMode(isSingleChoice);
        fileGalleryAdapter.setMaxSelection(isSingleChoice ? 1 : configs.getMaxSelection());
        fileGalleryAdapter.setSelectedItems(PickerManager.getInstance().getSelectedItems());
        fileGalleryAdapter.setOnCameraClickListener(this);
        RecyclerView recyclerView = findViewById(R.id.file_gallery);
        recyclerView.setLayoutManager(new GridLayoutManager(this, spanCount));
        recyclerView.setAdapter(fileGalleryAdapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(this));
        recyclerView.setItemAnimator(null);

        if (savedInstanceState == null) {
            if (requestPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, REQUEST_WRITE_PERMISSION)) {
                loadFiles(false);
            }
        } else {
            loadFiles(false);
        }

        maxCount = configs.getMaxSelection();
        if (maxCount > 0) {
            setTitle(getResources().getString(R.string.selection_count, fileGalleryAdapter.getSelectedItemCount(), maxCount));
        }
    }


    /**
     * Load Files From storage using loadFiles() method
     *
     * @param restart true : Refresh data when call method when after Capture image
     */
    private void loadFiles(boolean restart) {
        if (dirFile != null && dirFile.getMedias() != null && dirFile.getMedias().size() > 0) {

            if (!isCheckedCropped) {
                isCheckedCropped = true;
                ArrayList<MediaFile> mediaFiles1 = new ArrayList<>();
                for (int i = 0; i < dirFile.getMedias().size(); i++) {

                    int findpos = -1;
                    if (PickerManager.getInstance().getSelectedItems() != null && PickerManager.getInstance().getSelectedItems().size() > 0) {

                        for (int j = 0; j < PickerManager.getInstance().getSelectedItems().size(); j++) {
                            if (PickerManager.getInstance().getSelectedItems().get(j).getName().contains("_cropped_mp_")) {

                                String[] split = PickerManager.getInstance().getSelectedItems().get(j).getName().split("_cropped_mp_");
                                String firstSubString = split[0];
                                String ImageName = split[1];

                                if (dirFile.getMedias().get(i).getName().equals(ImageName)) {
                                    mediaFiles1.add(dirFile.getMedias().get(i));
                                    findpos = j;
                                    break;
                                }
                            }
                        }
                    }
                    if (findpos != -1) {
                        PickerManager.getInstance().getSelectedItems().remove(findpos);
                    }
                }
                if (PickerManager.getInstance().getSelectedItems() != null && PickerManager.getInstance().getSelectedItems().size() > 0) {
                    mediaFiles1.addAll(PickerManager.getInstance().getSelectedItems());
                }
                fileGalleryAdapter.setSelectedItems(mediaFiles1);


                mediaFiles.clear();
                mediaFiles.addAll(dirFile.getMedias());
                fileGalleryAdapter.notifyDataSetChanged();
            } else {
                mediaFiles.clear();
                mediaFiles.addAll(dirFile.getMedias());
                fileGalleryAdapter.notifyDataSetChanged();
            }
        } else if (!getIntent().hasExtra(MEDIA_DIR)) {
            FileLoader.loadFiles(this, new FileResultCallback() {
                @Override
                public void onResult(ArrayList<MediaDirFile> filesResults) {
                    if (filesResults != null) {

                        mediaFiles.clear();
                        MediaDirFile dirFile = new MediaDirFile();
                        dirFile.setBucketId(FileUtils.ALL_MEDIA_BUCKET_ID);
                        if (configs.getMediaType() == Configurations.TYPE_MEDIA) {
                            dirFile.setBucketName("All Media files");
                        } else if (configs.getMediaType() == Configurations.TYPE_IMAGE) {
                            dirFile.setBucketName("All Images");
                        } else if (configs.getMediaType() == Configurations.TYPE_VIDEO) {
                            dirFile.setBucketName("All Videos");
                        } else if (configs.getMediaType() == Configurations.TYPE_FILE) {
                            dirFile.setBucketName("All Files");
                        } else if (configs.getMediaType() == Configurations.TYPE_AUDIO) {
                            dirFile.setBucketName("All Audios");
                        }

                        if (filesResults.size() > 0 && filesResults.get(0).getMedias() != null && filesResults.get(0).getMedias().size() > 0) {
                            dirFile.setDateAdded(filesResults.get(0).getDateAdded());
                            dirFile.setCoverPath(filesResults.get(0).getMedias().get(0).getPath());
                        }

                        for (MediaDirFile file : filesResults) {
                            dirFile.addPhotos(file.getMedias());
                        }

                        filesResults.add(0, dirFile);

                        if (!isCheckedCropped) {
                            isCheckedCropped = true;
                            ArrayList<MediaFile> mediaFiles1 = new ArrayList<>();
                            for (int i = 0; i < filesResults.get(0).getMedias().size(); i++) {

                                int findpos = -1;
                                if (PickerManager.getInstance().getSelectedItems() != null && PickerManager.getInstance().getSelectedItems().size() > 0) {

                                    for (int j = 0; j < PickerManager.getInstance().getSelectedItems().size(); j++) {
                                        if (PickerManager.getInstance().getSelectedItems().get(j).getName().contains("_cropped_mp_")) {

                                            String[] split = PickerManager.getInstance().getSelectedItems().get(j).getName().split("_cropped_mp_");
                                            String firstSubString = split[0];
                                            String ImageName = split[1];

                                            if (filesResults.get(0).getMedias().get(i).getName().equals(ImageName)) {
                                                mediaFiles1.add(filesResults.get(0).getMedias().get(i));
                                                findpos = j;
                                                break;
                                            }
                                        }
                                    }
                                }
                                if (findpos != -1) {
                                    PickerManager.getInstance().getSelectedItems().remove(findpos);
                                }
                            }
                            if (PickerManager.getInstance().getSelectedItems() != null && PickerManager.getInstance().getSelectedItems().size() > 0) {
                                mediaFiles1.addAll(PickerManager.getInstance().getSelectedItems());
                            }
                            fileGalleryAdapter.setSelectedItems(mediaFiles1);


                            mediaFiles.clear();
                            mediaFiles.addAll(filesResults.get(0).getMedias());
                            fileGalleryAdapter.notifyDataSetChanged();
                        } else {
                            mediaFiles.clear();
                            mediaFiles.addAll(filesResults.get(0).getMedias());
                            fileGalleryAdapter.notifyDataSetChanged();
                        }

                    }
                }
            }, configs, restart);
        } else {
            mediaFiles.clear();
            mediaFiles.addAll(dirFile.getMedias());
            fileGalleryAdapter.notifyDataSetChanged();
        }
    }

    /**
     * Request Permission result of storage and camera
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_WRITE_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                loadFiles(false);
            } else {
                Toast.makeText(this, R.string.permission_not_given, Toast.LENGTH_SHORT).show();
                finish();
            }
        } else if (requestCode == REQUEST_CAMERA_PERMISSION_FOR_CAMERA || requestCode == REQUEST_CAMERA_PERMISSION_FOR_VIDEO) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                fileGalleryAdapter.openCamera(requestCode == REQUEST_CAMERA_PERMISSION_FOR_VIDEO);
            } else {
                Toast.makeText(this, R.string.permission_not_given, Toast.LENGTH_SHORT).show();
            }
        }
    }


    /**
     * Result of Capture Image and CropImage
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == FileGalleryAdapter.CAPTURE_IMAGE_VIDEO) {
            File file = fileGalleryAdapter.getLastCapturedFile();
            if (resultCode == RESULT_OK) {
                MediaScannerConnection.scanFile(this, new String[]{file.getAbsolutePath()}, null,
                        new MediaScannerConnection.OnScanCompletedListener() {
                            @Override
                            public void onScanCompleted(String path, final Uri uri) {
                                if (uri != null) {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            loadFiles(true);
                                        }
                                    });
                                }
                            }
                        });
            } else {
                getContentResolver().delete(fileGalleryAdapter.getLastCapturedUri(),
                        null, null);
            }
        } else if (requestCode == CROP_REQUEST_CODE && resultCode == RESULT_OK && data != null) {
            Intent intent = new Intent();
            intent.putExtra(MEDIA_FILES, data.<MediaFile>getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES));
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.filegallery_menu, menu);
        menuItem = menu.findItem(R.id.done);
        if (configs.getMediaType() == Configurations.TYPE_IMAGE && configs.isCropEnabled()) {
            menuItem.setTitle(getString(R.string.next));
        } else if (configs.getMediaType() == Configurations.TYPE_VIDEO && configs.isCropEnabled()) {
            menuItem.setTitle(getString(R.string.next));
        } else {
            menuItem.setTitle(getString(R.string.done));
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.done) {

            /** Check Selected Item null, empty or not and workout according to conditions */

            if (fileGalleryAdapter != null && fileGalleryAdapter.getSelectedItems() != null && fileGalleryAdapter.getSelectedItems().size() > 0) {
                if (configs.getMediaType() == Configurations.TYPE_IMAGE && configs.isCropEnabled()) {
                    Intent intent = new Intent(this, ImageGalleryActivity.class);
                    intent.putExtra(MEDIA_FILES, fileGalleryAdapter.getSelectedItems());
                    startActivityForResult(intent, CROP_REQUEST_CODE);
                } else if (configs.getMediaType() == Configurations.TYPE_VIDEO && configs.isCropEnabled()) {
                    Intent intent = new Intent(this, VideoGalleryActivity.class);
                    intent.putExtra(MEDIA_FILES, fileGalleryAdapter.getSelectedItems());
                    intent.putExtra(FilePickerActivity.CONFIGS, configs);
                    startActivityForResult(intent, CROP_REQUEST_CODE);
                } else {
                    Intent intent = new Intent();
                    intent.putExtra(MEDIA_FILES, fileGalleryAdapter.getSelectedItems());
                    setResult(RESULT_OK, intent);
                    finish();
                }
            } else {
                if (configs.getMediaType() == Configurations.TYPE_MEDIA) {
                    Toast.makeText(this, R.string.select_any_one_media_file, Toast.LENGTH_SHORT).show();
                } else if (configs.getMediaType() == Configurations.TYPE_IMAGE) {
                    Toast.makeText(this, R.string.select_any_one_image, Toast.LENGTH_SHORT).show();
                } else if (configs.getMediaType() == Configurations.TYPE_VIDEO) {
                    Toast.makeText(this, R.string.select_any_one_video, Toast.LENGTH_SHORT).show();
                } else if (configs.getMediaType() == Configurations.TYPE_AUDIO) {
                    Toast.makeText(this, R.string.select_any_one_audio, Toast.LENGTH_SHORT).show();
                } else if (configs.getMediaType() == Configurations.TYPE_FILE) {
                    Toast.makeText(this, R.string.select_any_one_document, Toast.LENGTH_SHORT).show();
                }
            }
            return true;
        } else if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        File file = fileGalleryAdapter.getLastCapturedFile();
        if (file != null)
            outState.putString(PATH, file.getAbsolutePath());
        outState.putParcelable(URI, fileGalleryAdapter.getLastCapturedUri());
        outState.putParcelableArrayList(SELECTED_MEDIA_FILES, fileGalleryAdapter.getSelectedItems());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        String path = savedInstanceState.getString(PATH);
        if (path != null)
            fileGalleryAdapter.setLastCapturedFile(new File(path));

        Uri uri = savedInstanceState.getParcelable(URI);
        if (uri != null)
            fileGalleryAdapter.setLastCapturedUri(uri);

        ArrayList<MediaFile> mediaFiles = savedInstanceState.getParcelableArrayList(SELECTED_MEDIA_FILES);
        if (mediaFiles != null) {
            fileGalleryAdapter.setSelectedItems(mediaFiles);
            fileGalleryAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public boolean onCameraClick(boolean forVideo) {
        return requestPermission(
                Manifest.permission.CAMERA,
                forVideo ? REQUEST_CAMERA_PERMISSION_FOR_VIDEO : REQUEST_CAMERA_PERMISSION_FOR_CAMERA
        );
    }

    public boolean requestPermission(String permission, int requestCode) {
        if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
            if (configs.isCheckPermission()) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(new String[]{permission}, requestCode);
                }
            } else {
                Toast.makeText(this, R.string.permission_not_given, Toast.LENGTH_SHORT).show();
                finish();
            }
            return false;
        }
        return true;
    }

    @Override
    public void onSelectionBegin() {

    }

    @Override
    public void onSelected(FileGalleryAdapter.ViewHolder viewHolder, int position) {
        if (maxCount > 0) {
            setTitle(getResources().getString(R.string.selection_count, fileGalleryAdapter.getSelectedItemCount(), maxCount));

        }
    }

    @Override
    public void onUnSelected(FileGalleryAdapter.ViewHolder viewHolder, int position) {
        if (maxCount > 0) {
            setTitle(getResources().getString(R.string.selection_count, fileGalleryAdapter.getSelectedItemCount(), maxCount));
        }
    }

    @Override
    public void onSelectAll() {

    }

    @Override
    public void onUnSelectAll() {

    }

    @Override
    public void onSelectionEnd() {

    }

    @Override
    public void onMaxReached() {
    }


}
