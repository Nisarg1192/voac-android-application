

package com.library.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Point;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.library.R;
import com.library.adapter.FileDirAdapter;
import com.library.config.Configurations;
import com.library.config.PickerManager;
import com.library.loader.FileLoader;
import com.library.loader.FileResultCallback;
import com.library.model.MediaDirFile;
import com.library.model.MediaFile;
import com.library.utils.FileUtils;
import com.library.view.DividerItemDecoration;

import java.io.File;
import java.util.ArrayList;

public class FileDirsActivity extends AppCompatActivity
        implements FileDirAdapter.OnCameraClickListener, FileDirAdapter.OnItemClickListener {

    public static final String MEDIA_FILES = "MEDIA_FILES";
    public static final String SELECTED_MEDIA_FILES = "SELECTED_MEDIA_FILES";
    public static final String CONFIGS = "CONFIGS";
    public static final String TAG = "FilePicker";
    private static final String PATH = "PATH";
    private static final String URI = "URI";
    private static final int CROP_REQUEST_CODE = 1123;
    private static final int DETAIL_SCREEN_REQUEST_CODE = 1124;
    private static final int REQUEST_WRITE_PERMISSION = 1;
    private static final int REQUEST_CAMERA_PERMISSION_FOR_CAMERA = 2;
    private static final int REQUEST_CAMERA_PERMISSION_FOR_VIDEO = 3;
    private Configurations configs;
    private ArrayList<MediaDirFile> mediaFiles = new ArrayList<>();
    private FileDirAdapter fileGalleryAdapter;
    private int maxCount;
    MenuItem menuItem;


    /**
     * In FilePicker Activity get configuration and set data according to that
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.filepicker_gallery);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        /**
         * get configuration using intent data
         */

        configs = getIntent().getParcelableExtra(CONFIGS);
        if (configs == null) {
            configs = new Configurations.Builder().build();
        }

        int spanCount;
        if (getResources().getConfiguration().orientation
                == Configuration.ORIENTATION_LANDSCAPE) {
            spanCount = configs.getLandscapeSpanCount();
        } else {
            spanCount = configs.getPortraitSpanCount();
        }

        int imageSize = configs.getImageSize();
        if (imageSize <= 0) {
            Point point = new Point();
            getWindowManager().getDefaultDisplay().getSize(point);
            imageSize = Math.min(point.x, point.y) / configs.getPortraitSpanCount();
        }

        boolean isSingleChoice = configs.isSingleChoiceMode();

        PickerManager.getInstance().setSelectedItems(configs.getSelectedMediaFiles());

        fileGalleryAdapter = new FileDirAdapter(this, configs.getMediaType(), mediaFiles, imageSize,
                configs.isImageCaptureEnabled(),
                configs.isVideoCaptureEnabled());
        fileGalleryAdapter.setOnCameraClickListener(this);
        fileGalleryAdapter.setOnItemClickListener(this);
        RecyclerView recyclerView = findViewById(R.id.file_gallery);
        recyclerView.setLayoutManager(new GridLayoutManager(this, spanCount));
        recyclerView.setAdapter(fileGalleryAdapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(this));
        recyclerView.setItemAnimator(null);

        if (savedInstanceState == null) {
            if (requestPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, REQUEST_WRITE_PERMISSION)) {
                loadFiles(false);
            }
        } else {
            loadFiles(false);
        }

        maxCount = configs.getMaxSelection();
        if (maxCount > 0) {
            setTitle(getResources().getString(R.string.selection_count, PickerManager.getInstance().getSelectedItemCount(), maxCount));
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        maxCount = configs.getMaxSelection();
        if (maxCount > 0) {
            setTitle(getResources().getString(R.string.selection_count, PickerManager.getInstance().getSelectedItemCount(), maxCount));
        }
    }

    /**
     * Load Files From storage using loadFiles() method
     *
     * @param restart true : Refresh data when call method when after Capture image
     */
    private void loadFiles(boolean restart) {
        FileLoader.loadFiles(this, new FileResultCallback() {
            @Override
            public void onResult(ArrayList<MediaDirFile> filesResults) {
                if (filesResults != null) {

                    mediaFiles.clear();
                    MediaDirFile dirFile = new MediaDirFile();
                    dirFile.setBucketId(FileUtils.ALL_MEDIA_BUCKET_ID);
                    if (configs.getMediaType() == Configurations.TYPE_MEDIA) {
                        dirFile.setBucketName("All Media files");
                    } else if (configs.getMediaType() == Configurations.TYPE_IMAGE) {
                        dirFile.setBucketName("All Images");
                    } else if (configs.getMediaType() == Configurations.TYPE_VIDEO) {
                        dirFile.setBucketName("All Videos");
                    } else if (configs.getMediaType() == Configurations.TYPE_FILE) {
                        dirFile.setBucketName("All Files");
                    } else if (configs.getMediaType() == Configurations.TYPE_AUDIO) {
                        dirFile.setBucketName("All Audios");
                    }

                    if (filesResults.size() > 0 && filesResults.get(0).getMedias() != null && filesResults.get(0).getMedias().size() > 0) {
                        dirFile.setDateAdded(filesResults.get(0).getDateAdded());
                        dirFile.setCoverPath(filesResults.get(0).getMedias().get(0).getPath());
                    }

                    for (MediaDirFile file : filesResults) {
                        dirFile.addPhotos(file.getMedias());
                    }

                    filesResults.add(0, dirFile);

                    mediaFiles.addAll(filesResults);
                    fileGalleryAdapter.notifyDataSetChanged();

                }
            }
        }, configs, restart);
    }

    /**
     * Request Permission result of storage and camera
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_WRITE_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                loadFiles(false);
            } else {
                Toast.makeText(this, R.string.permission_not_given, Toast.LENGTH_SHORT).show();
                finish();
            }
        } else if (requestCode == REQUEST_CAMERA_PERMISSION_FOR_CAMERA || requestCode == REQUEST_CAMERA_PERMISSION_FOR_VIDEO) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                fileGalleryAdapter.openCamera(requestCode == REQUEST_CAMERA_PERMISSION_FOR_VIDEO);
            } else {
                Toast.makeText(this, R.string.permission_not_given, Toast.LENGTH_SHORT).show();
            }
        }
    }


    /**
     * Result of Capture Image and CropImage
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == FileDirAdapter.CAPTURE_IMAGE_VIDEO) {
            File file = fileGalleryAdapter.getLastCapturedFile();
            if (resultCode == RESULT_OK) {
                MediaScannerConnection.scanFile(this, new String[]{file.getAbsolutePath()}, null,
                        new MediaScannerConnection.OnScanCompletedListener() {
                            @Override
                            public void onScanCompleted(String path, final Uri uri) {
                                if (uri != null) {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            loadFiles(true);
                                        }
                                    });
                                }
                            }
                        });
            } else {
                getContentResolver().delete(fileGalleryAdapter.getLastCapturedUri(),
                        null, null);
            }
        } else if (requestCode == CROP_REQUEST_CODE && resultCode == RESULT_OK && data != null) {
            Intent intent = new Intent();
            intent.putExtra(MEDIA_FILES, data.<MediaFile>getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES));
            setResult(RESULT_OK, intent);
            finish();
        } else if (requestCode == DETAIL_SCREEN_REQUEST_CODE && resultCode == RESULT_OK && data != null) {
            Intent intent = new Intent();
            intent.putExtra(MEDIA_FILES, data.<MediaFile>getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES));
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.filegallery_menu, menu);
        menuItem = menu.findItem(R.id.done);
        if (configs.getMediaType() == Configurations.TYPE_IMAGE && configs.isCropEnabled()) {
            menuItem.setTitle(getString(R.string.next));
        } else if (configs.getMediaType() == Configurations.TYPE_VIDEO && configs.isCropEnabled()) {
            menuItem.setTitle(getString(R.string.next));
        } else {
            menuItem.setTitle(getString(R.string.done));
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.done) {

            /** Check Selected Item null, empty or not and workout according to conditions */

            if (PickerManager.getInstance() != null && PickerManager.getInstance().getSelectedItems() != null && PickerManager.getInstance().getSelectedItems().size() > 0) {
                if (configs.getMediaType() == Configurations.TYPE_IMAGE && configs.isCropEnabled()) {
                    Intent intent = new Intent(this, ImageGalleryActivity.class);
                    intent.putExtra(MEDIA_FILES, PickerManager.getInstance().getSelectedItems());
                    startActivityForResult(intent, CROP_REQUEST_CODE);
                } else if (configs.getMediaType() == Configurations.TYPE_VIDEO && configs.isCropEnabled()) {
                    Intent intent = new Intent(this, VideoGalleryActivity.class);
                    intent.putExtra(MEDIA_FILES, PickerManager.getInstance().getSelectedItems());
                    intent.putExtra(FileDirsActivity.CONFIGS, configs);
                    startActivityForResult(intent, CROP_REQUEST_CODE);
                } else {
                    Intent intent = new Intent();
                    intent.putExtra(MEDIA_FILES, PickerManager.getInstance().getSelectedItems());
                    setResult(RESULT_OK, intent);
                    finish();
                }
            } else {
                if (configs.getMediaType() == Configurations.TYPE_MEDIA) {
                    Toast.makeText(this, R.string.select_any_one_media_file, Toast.LENGTH_SHORT).show();
                } else if (configs.getMediaType() == Configurations.TYPE_IMAGE) {
                    Toast.makeText(this, R.string.select_any_one_image, Toast.LENGTH_SHORT).show();
                } else if (configs.getMediaType() == Configurations.TYPE_VIDEO) {
                    Toast.makeText(this, R.string.select_any_one_video, Toast.LENGTH_SHORT).show();
                } else if (configs.getMediaType() == Configurations.TYPE_AUDIO) {
                    Toast.makeText(this, R.string.select_any_one_audio, Toast.LENGTH_SHORT).show();
                } else if (configs.getMediaType() == Configurations.TYPE_FILE) {
                    Toast.makeText(this, R.string.select_any_one_document, Toast.LENGTH_SHORT).show();
                }
            }
            return true;
        } else if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        File file = fileGalleryAdapter.getLastCapturedFile();
        if (file != null)
            outState.putString(PATH, file.getAbsolutePath());
        outState.putParcelable(URI, fileGalleryAdapter.getLastCapturedUri());
        outState.putParcelableArrayList(SELECTED_MEDIA_FILES, PickerManager.getInstance().getSelectedItems());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        String path = savedInstanceState.getString(PATH);
        if (path != null)
            fileGalleryAdapter.setLastCapturedFile(new File(path));

        Uri uri = savedInstanceState.getParcelable(URI);
        if (uri != null)
            fileGalleryAdapter.setLastCapturedUri(uri);

        ArrayList<MediaFile> mediaFiles = savedInstanceState.getParcelableArrayList(SELECTED_MEDIA_FILES);
        if (mediaFiles != null) {
            fileGalleryAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public boolean onCameraClick(boolean forVideo) {
        return requestPermission(
                Manifest.permission.CAMERA,
                forVideo ? REQUEST_CAMERA_PERMISSION_FOR_VIDEO : REQUEST_CAMERA_PERMISSION_FOR_CAMERA
        );
    }

    public boolean requestPermission(String permission, int requestCode) {
        if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
            if (configs.isCheckPermission()) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(new String[]{permission}, requestCode);
                }
            } else {
                Toast.makeText(this, R.string.permission_not_given, Toast.LENGTH_SHORT).show();
                finish();
            }
            return false;
        }
        return true;
    }


    @Override
    public void onClick(View v, int position) {
        if (mediaFiles != null && mediaFiles.size() > 0 && mediaFiles.size() > position) {
            Intent intent = new Intent(this, FilePickerActivity.class);
            intent.putExtra(FilePickerActivity.CONFIGS, configs);
            if (position != 0) {
                intent.putExtra(FilePickerActivity.MEDIA_DIR, mediaFiles.get(position));
            }
            startActivityForResult(intent, DETAIL_SCREEN_REQUEST_CODE);
        }
    }
}
