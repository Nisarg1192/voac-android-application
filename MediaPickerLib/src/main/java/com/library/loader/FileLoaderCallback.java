

package com.library.loader;

import android.app.LoaderManager;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.Loader;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;

import com.library.R;
import com.library.config.Configurations;
import com.library.model.MediaDirFile;
import com.library.model.MediaFile;

import java.io.FileDescriptor;
import java.util.ArrayList;

import androidx.annotation.NonNull;

import static android.provider.BaseColumns._ID;
import static android.provider.MediaStore.Audio.AlbumColumns.ALBUM_ID;
import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE;
import static android.provider.MediaStore.Files.FileColumns.MIME_TYPE;
import static android.provider.MediaStore.Images.ImageColumns.BUCKET_DISPLAY_NAME;
import static android.provider.MediaStore.Images.ImageColumns.BUCKET_ID;
import static android.provider.MediaStore.MediaColumns.DATA;
import static android.provider.MediaStore.MediaColumns.DATE_ADDED;
import static android.provider.MediaStore.MediaColumns.HEIGHT;
import static android.provider.MediaStore.MediaColumns.SIZE;
import static android.provider.MediaStore.MediaColumns.TITLE;
import static android.provider.MediaStore.MediaColumns.WIDTH;
import static android.provider.MediaStore.Video.VideoColumns.DURATION;

class FileLoaderCallback implements LoaderManager.LoaderCallbacks<Cursor> {
    private Context context;
    private FileResultCallback fileResultCallback;
    private Configurations configs;
    ProgressDialog progress;

    FileLoaderCallback(@NonNull Context context,
                       @NonNull FileResultCallback fileResultCallback,
                       @NonNull Configurations configs) {
        this.context = context;
        this.fileResultCallback = fileResultCallback;
        this.configs = configs;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (context != null) {
            progress = new ProgressDialog(context);
            progress.setMessage("Fetching data...");
            progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progress.setIndeterminate(true);
            progress.setProgress(0);
            if (progress != null && !progress.isShowing()) {
                progress.show();
            }
        }

        return new FileLoader(context, configs);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        //ArrayList<MediaFile> mediaFiles = new ArrayList<>();
        ArrayList<MediaDirFile> mediaFiles = new ArrayList<>();
        if (data.moveToFirst())
            do {
                long size = data.getLong(data.getColumnIndex(SIZE));
                if (size == 0) {
                    //Check if File size is really zero
                    size = new java.io.File(data.getString(data.getColumnIndex(DATA))).length();
                    if (size <= 0 && configs.isSkipZeroSizeFiles())
                        continue;
                }
                MediaFile mediaFile = new MediaFile();
                mediaFile.setSize(size);
                mediaFile.setId(data.getLong(data.getColumnIndex(_ID)));
                mediaFile.setName(data.getString(data.getColumnIndex(TITLE)));
                mediaFile.setPath(data.getString(data.getColumnIndex(DATA)));
                mediaFile.setDate(data.getLong(data.getColumnIndex(DATE_ADDED)));
                mediaFile.setMimeType(data.getString(data.getColumnIndex(MIME_TYPE)));
                mediaFile.setMediaType(data.getInt(data.getColumnIndex(MEDIA_TYPE)));
                if (mediaFile.getMediaType() == MediaFile.TYPE_FILE
                        && mediaFile.getMimeType() != null) {
                    //Double check correct MediaType
                    mediaFile.setMediaType(getMediaType(mediaFile.getMimeType()));
                }
                mediaFile.setBucketId(data.getString(data.getColumnIndex(BUCKET_ID)));
                mediaFile.setBucketName(data.getString(data.getColumnIndex(BUCKET_DISPLAY_NAME)));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    mediaFile.setHeight(data.getLong(data.getColumnIndex(HEIGHT)));
                    mediaFile.setWidth(data.getLong(data.getColumnIndex(WIDTH)));
                }
                mediaFile.setDuration(data.getLong(data.getColumnIndex(DURATION)));
                int albumID = data.getInt(data.getColumnIndex(ALBUM_ID));
                if (albumID > 0) {
                    Uri sArtworkUri = Uri.parse("content://media/external/audio/albumart");
                    Uri uri = ContentUris.withAppendedId(sArtworkUri, albumID);
                    mediaFile.setThumbnail(uri);
                }

                MediaDirFile dirFile = new MediaDirFile();
                dirFile.setBucketId(data.getString(data.getColumnIndex(BUCKET_ID)));
                dirFile.setBucketName(data.getString(data.getColumnIndex(BUCKET_DISPLAY_NAME)));

                boolean isDirFound = false;
                int pos = -1;
                if (mediaFiles.size() > 0) {
                    for (int i = 0; i < mediaFiles.size(); i++) {
                        if (mediaFiles.get(i).getBucketId().equals(dirFile.getBucketId())) {
                            isDirFound = true;
                            pos = i;
                            break;
                        }
                    }
                }
                if (!isDirFound) {
                    dirFile.addPhoto(mediaFile);
                    dirFile.setDateAdded(data.getLong(data.getColumnIndex(DATE_ADDED)));
                    mediaFiles.add(dirFile);
                } else {
                    if (pos != -1) {
                        mediaFiles.get(pos).addPhoto(mediaFile);
                    }
                }


                //mediaFiles.add(mediaFile);
            } while (data.moveToNext());
        if (progress != null) {
            progress.dismiss();
        }
        fileResultCallback.onResult(mediaFiles);
    }

    private static @MediaFile.Type
    int getMediaType(String mime) {
        if (mime.startsWith("image/")) {
            return MediaFile.TYPE_IMAGE;
        } else if (mime.startsWith("video/")) {
            return MediaFile.TYPE_VIDEO;
        } else if (mime.startsWith("audio/")) {
            return MediaFile.TYPE_AUDIO;
        } else {
            return MediaFile.TYPE_FILE;
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        if (progress != null) {
            progress.dismiss();
        }
    }

    public Bitmap getAlbumart(Context context, Long album_id) {
        Bitmap albumArtBitMap = null;
        BitmapFactory.Options options = new BitmapFactory.Options();
        try {

            final Uri sArtworkUri = Uri
                    .parse("content://media/external/audio/albumart");

            Uri uri = ContentUris.withAppendedId(sArtworkUri, album_id);

            ParcelFileDescriptor pfd = context.getContentResolver()
                    .openFileDescriptor(uri, "r");

            if (pfd != null) {
                FileDescriptor fd = pfd.getFileDescriptor();
                albumArtBitMap = BitmapFactory.decodeFileDescriptor(fd, null,
                        options);
                pfd = null;
                fd = null;
            }
        } catch (Error ee) {
        } catch (Exception e) {
        }

        if (null != albumArtBitMap) {
            return albumArtBitMap;
        }
        return null;
    }

}
