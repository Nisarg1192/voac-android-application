

package com.library.loader;

import com.library.model.MediaDirFile;
import com.library.model.MediaFile;

import java.util.ArrayList;

import androidx.annotation.Nullable;

public interface FileResultCallback {
    //void onResult(@Nullable ArrayList<MediaFile> mediaFiles);
    void onResult(@Nullable ArrayList<MediaDirFile> mediaFiles);
}
