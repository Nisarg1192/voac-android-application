
package com.library.adapter;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.request.RequestOptions;
import com.googlecode.mp4parser.h264.Debug;
import com.library.R;
import com.library.config.Configurations;
import com.library.model.MediaDirFile;
import com.library.model.MediaFile;
import com.library.utils.FilePickerProvider;
import com.library.utils.FileUtils;
import com.library.view.SquareImage;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import static android.os.Environment.DIRECTORY_MOVIES;
import static android.os.Environment.DIRECTORY_PICTURES;
import static android.os.Environment.getExternalStoragePublicDirectory;

public class FileDirAdapter extends RecyclerView.Adapter<FileDirAdapter.ViewHolder> {
    public static final int CAPTURE_IMAGE_VIDEO = 1;
    private ArrayList<MediaDirFile> mediaFiles;
    private Activity activity;
    private RequestManager glideRequest;
    private OnCameraClickListener onCameraClickListener;
    private OnItemClickListener onItemClickListener;
    private boolean showCamera;
    private boolean showVideoCamera;
    private File lastCapturedFile;
    private Uri lastCapturedUri;
    private int MediaType;
    private SimpleDateFormat TimeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault());
    private int itemStartPosition = 0;

    public FileDirAdapter(Activity activity, int MediaType, ArrayList<MediaDirFile> mediaFiles, int imageSize, boolean showCamera, boolean showVideoCamera) {
        this.mediaFiles = mediaFiles;
        this.MediaType = MediaType;
        this.activity = activity;
        this.showCamera = showCamera;
        this.showVideoCamera = showVideoCamera;
        glideRequest = Glide.with(this.activity)
                .applyDefaultRequestOptions(RequestOptions
                        .sizeMultiplierOf(0.70f)
                        .optionalCenterCrop()
                        .override(imageSize));

        if (showCamera && showVideoCamera)
            itemStartPosition = 2;
        else if (showCamera || showVideoCamera)
            itemStartPosition = 1;
    }

    public File getLastCapturedFile() {
        return lastCapturedFile;
    }

    public void setLastCapturedFile(File file) {
        lastCapturedFile = file;
    }

    public Uri getLastCapturedUri() {
        return lastCapturedUri;
    }

    public void setLastCapturedUri(Uri uri) {
        lastCapturedUri = uri;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v;
        if (MediaType == Configurations.TYPE_FILE) {
            v = LayoutInflater.from(activity).inflate(R.layout.directory_item_files, parent, false);
        } else {
            v = LayoutInflater.from(activity).inflate(R.layout.directory_item, parent, false);
        }
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        if (showCamera) {
            if (position == 0) {
                handleCamera(holder.openCamera, false);
                return;
            }
            if (showVideoCamera) {
                if (position == 1) {
                    handleCamera(holder.openVideoCamera, true);
                    return;
                }
                holder.openVideoCamera.setVisibility(View.GONE);
                position--;
            }
            holder.openCamera.setVisibility(View.GONE);
            position--;
        } else if (showVideoCamera) {
            if (position == 0) {
                handleCamera(holder.openVideoCamera, true);
                return;
            }
            holder.openVideoCamera.setVisibility(View.GONE);
            position--;
        }
        MediaDirFile mediaFile = mediaFiles.get(position);
        if (MediaType == MediaFile.TYPE_VIDEO ||
                MediaType == MediaFile.TYPE_IMAGE || MediaType == MediaFile.TYPE_MEDIA) {
            glideRequest.load(mediaFile.getCoverPath())
                    .into(holder.fileThumbnail);
        } else if (MediaType == MediaFile.TYPE_AUDIO) {
            if (mediaFile.getCoverPath() != null) {
                Bitmap bitmap = FileUtils.getAlbumArt(activity, mediaFile.getMedias().get(0).getThumbnail());
                if (bitmap != null) {
                    bitmap = FileUtils.scaleDownBitmap(bitmap, 80, activity);
                    glideRequest.load(bitmap)
                            .apply(RequestOptions.placeholderOf(R.drawable.ic_audio))
                            .into(holder.fileThumbnail);
                } else {
                    holder.fileThumbnail.setImageResource(R.drawable.ic_audio);
                }
            } else {
                holder.fileThumbnail.setImageResource(R.drawable.ic_audio);
            }

        } else {
            holder.fileThumbnail.setImageResource(R.drawable.ic_folder);
        }

        holder.fileName.setText(mediaFile.getBucketName());
        holder.folder_count.setText("" + mediaFile.getMedias().size());
        final View view = holder.itemView;

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = holder.getAdapterPosition() - itemStartPosition;
                if (onItemClickListener != null)
                    onItemClickListener.onClick(v, position);
            }
        });
    }

    private void handleCamera(final ImageView openCamera, final boolean forVideo) {
        openCamera.setVisibility(View.VISIBLE);
        openCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onCameraClickListener != null && !onCameraClickListener.onCameraClick(forVideo))
                    return;
                openCamera(forVideo);
            }
        });
    }

    public void openCamera(boolean forVideo) {
        Intent intent;
        String fileName;
        File dir;
        Uri externalContentUri;
        if (forVideo) {
            intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
            intent.putExtra(android.provider.MediaStore.EXTRA_DURATION_LIMIT, 30);
            intent.putExtra(android.provider.MediaStore.EXTRA_VIDEO_QUALITY, 0);
            fileName = "/VID_" + getTimeStamp() + ".mp4";
            dir = getExternalStoragePublicDirectory(DIRECTORY_MOVIES);
            externalContentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
        } else {
            intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            dir = getExternalStoragePublicDirectory(DIRECTORY_PICTURES);
            fileName = "/IMG_" + getTimeStamp() + ".jpeg";
            externalContentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        }
        if (!dir.exists() && !dir.mkdir()) {
            Debug.trace("FileAdapter", "onClick: " +
                    (forVideo ? "MOVIES" : "PICTURES") + " Directory not exists");
            return;
        }
        lastCapturedFile = new File(dir.getAbsolutePath() + fileName);

        Uri fileUri = FilePickerProvider.getUriForFile(activity, lastCapturedFile);

        ContentValues values = new ContentValues();
        values.put(MediaStore.MediaColumns.DATA, lastCapturedFile.getAbsolutePath());
        values.put(MediaStore.Images.ImageColumns.DATE_TAKEN, System.currentTimeMillis());
        lastCapturedUri = activity.getContentResolver().insert(externalContentUri, values);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        activity.startActivityForResult(intent, CAPTURE_IMAGE_VIDEO);
    }

    private String getTimeStamp() {
        return TimeStamp.format(new Date());
    }

    public void setOnCameraClickListener(OnCameraClickListener onCameraClickListener) {
        this.onCameraClickListener = onCameraClickListener;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener {
        void onClick(View v, int position);
    }

    @Override
    public int getItemCount() {
        if (showCamera) {
            if (showVideoCamera)
                return mediaFiles.size() + 2;
            return mediaFiles.size() + 1;
        } else if (showVideoCamera) {
            return mediaFiles.size() + 1;
        }
        return mediaFiles.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView openCamera, openVideoCamera;
        private SquareImage fileThumbnail;
        private TextView fileName, folder_count;

        ViewHolder(View v) {
            super(v);
            openCamera = v.findViewById(R.id.file_open_camera);
            openVideoCamera = v.findViewById(R.id.file_open_video_camera);
            fileThumbnail = v.findViewById(R.id.file_thumbnail);
            fileName = v.findViewById(R.id.file_name);
            folder_count = v.findViewById(R.id.folder_count);
        }
    }

    public interface OnCameraClickListener {
        boolean onCameraClick(boolean forVideo);
    }

    public static Bitmap scaleDownBitmap(Bitmap photo, int newHeight, Context context) {

        final float densityMultiplier = context.getResources().getDisplayMetrics().density;

        int h = (int) (newHeight * densityMultiplier);
        int w = (int) (h * photo.getWidth() / ((double) photo.getHeight()));

        photo = Bitmap.createScaledBitmap(photo, w, h, true);

        return photo;
    }

}
