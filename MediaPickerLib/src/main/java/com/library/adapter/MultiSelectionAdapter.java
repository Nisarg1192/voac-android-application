

package com.library.adapter;

import android.content.Context;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.library.config.PickerManager;
import com.library.model.MediaFile;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@SuppressWarnings({"unused", "WeakerAccess"})
public abstract class MultiSelectionAdapter<VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH> {
    private static final String TAG = MultiSelectionAdapter.class.getSimpleName();
    private final Context context;
    //private ArrayList<MediaFile> selectedItems = new ArrayList<>();
    private ArrayList<MediaFile> mediaFiles;

    private OnItemClickListener onItemClickListener;
    private OnItemLongClickListener onItemLongClickListener;
    private OnSelectionListener<VH> customOnSelectionListener;
    private boolean isSelectionStarted = false;
    private boolean enabledSelection = false;
    private boolean isSingleClickSelection = false;
    private boolean singleChoiceMode = false;
    private int maxSelection = -1;
    private int itemStartPosition = 0;
    private int maxDuration = 1000 * 60;
    private OnSelectionListener<VH> onSelectionListener = new OnSelectionListener<VH>() {
        @Override
        public void onSelectionBegin() {
            isSelectionStarted = true;
            if (!singleChoiceMode && customOnSelectionListener != null)
                customOnSelectionListener.onSelectionBegin();
        }

        @Override
        public void onSelected(VH viewHolder, int position) {
            if (singleChoiceMode && PickerManager.getInstance().getSelectedItems().size() > 0) {
                int pos = mediaFiles.indexOf(PickerManager.getInstance().getSelectedItems().get(0));
                if (pos >= 0) {
                    removeSelection(pos);
                    handleItemChanged(pos);
                }
            }
            if (maxSelection > 0 && PickerManager.getInstance().getSelectedItems().size() >= maxSelection) {
                onMaxReached();
                return;
            }
            if (singleChoiceMode) {
                if (PickerManager.getInstance().getSelectedItems().size() == 0) {
                    setItemSelected(viewHolder, position, true, customOnSelectionListener);
                }
            } else {
                setItemSelected(viewHolder, position, true, customOnSelectionListener);
            }
        }

        @Override
        public void onUnSelected(VH viewHolder, int position) {
            setItemSelected(viewHolder, position, false, customOnSelectionListener);
        }

        @Override
        public void onSelectAll() {
            isSelectionStarted = true;
            PickerManager.getInstance().clear();
            PickerManager.getInstance().addAll(mediaFiles);
            notifyDataSetChanged();
            if (customOnSelectionListener != null)
                customOnSelectionListener.onSelectAll();
        }

        @Override
        public void onUnSelectAll() {
            for (int i = PickerManager.getInstance().getSelectedItems().size() - 1; i >= 0; i--) {
                int position = mediaFiles.indexOf(PickerManager.getInstance().getSelectedItems().get(i));
                if (position < 0) continue;
                removeSelection(position);
                handleItemChanged(position);
            }
            isSelectionStarted = false;
            if (customOnSelectionListener != null)
                customOnSelectionListener.onUnSelectAll();
        }

        @Override
        public void onSelectionEnd() {
            isSelectionStarted = false;
            if (!singleChoiceMode && customOnSelectionListener != null)
                customOnSelectionListener.onSelectionEnd();
        }

        @Override
        public void onMaxReached() {
            if (!singleChoiceMode && customOnSelectionListener != null)
                customOnSelectionListener.onMaxReached();
        }
    };

    public MultiSelectionAdapter(ArrayList<MediaFile> items, Context context) {
        this.mediaFiles = items;
        this.context = context;
    }

    public void setItemStartPosition(int itemStartPosition) {
        this.itemStartPosition = itemStartPosition;
    }

    public int getMaxSelection() {
        return maxSelection;
    }

    public void setMaxSelection(int maxSelection) {
        this.maxSelection = maxSelection;
    }

    @CallSuper
    @Override
    public void onBindViewHolder(@NonNull final VH holder, int position) {
        final View view = holder.itemView;

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = holder.getAdapterPosition() - itemStartPosition;
                if (enabledSelection && (isSelectionStarted || isSingleClickSelection)) {
                    if (PickerManager.getInstance().getSelectedItems().contains(mediaFiles.get(position))) {
                        onSelectionListener.onUnSelected(holder, position);
                        if (PickerManager.getInstance().getSelectedItems().isEmpty()) {
                            onSelectionListener.onSelectionEnd();
                        }
                    } else {
                        onSelectionListener.onSelected(holder, position);
                    }
                }
                if (onItemClickListener != null)
                    onItemClickListener.onClick(v, position);
            }
        });


        setItemSelected(holder, position, PickerManager.getInstance().getSelectedItems().contains(mediaFiles.get(position)), customOnSelectionListener);

        view.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                int position = holder.getAdapterPosition() - itemStartPosition;
                if (enabledSelection) {
                    if (!isSelectionStarted) {
                        onSelectionListener.onSelectionBegin();
                        onSelectionListener.onSelected(holder, position);
                    } else if (PickerManager.getInstance().getSelectedItems().size() <= 1
                            && PickerManager.getInstance().getSelectedItems().contains(mediaFiles.get(position))) {
                        onSelectionListener.onSelectionEnd();
                        onSelectionListener.onUnSelected(holder, position);
                    }
                }
                return onItemLongClickListener == null ||
                        onItemLongClickListener.onLongClick(view, position);
            }
        });
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position, @NonNull List<Object> payloads) {
        super.onBindViewHolder(holder, position, payloads);
    }

    public boolean isSelected(MediaFile mediaFile) {
        return PickerManager.getInstance().getSelectedItems().contains(mediaFile);
    }

    public void enableSelection(boolean enabledSelection) {
        this.enabledSelection = enabledSelection;
    }

    public void enableSingleClickSelection(boolean enableSingleClickSelection) {
        this.enabledSelection = enableSingleClickSelection || enabledSelection;
        this.isSingleClickSelection = enableSingleClickSelection;
    }

    public void setSingleChoiceMode(boolean singleChoiceMode) {
        this.singleChoiceMode = singleChoiceMode;
    }

    public void setOnItemClickListener(OnItemClickListener onClickListener) {
        this.onItemClickListener = onClickListener;
    }

    public void setOnItemLongClickListener(OnItemLongClickListener onItemLongClickListener) {
        this.onItemLongClickListener = onItemLongClickListener;
    }

    public void setOnSelectionListener(OnSelectionListener<VH> onSelectionListener) {
        this.customOnSelectionListener = onSelectionListener;
    }

    public ArrayList<MediaFile> getSelectedItems() {
        return PickerManager.getInstance().getSelectedItems();
    }

    public void setSelectedItems(ArrayList<MediaFile> selectedItems) {
        PickerManager.getInstance().setSelectedItems(selectedItems);
    }

    public int getSelectedItemCount() {
        return PickerManager.getInstance().getSelectedItemCount();
    }

    public void unSelectAll() {
        onSelectionListener.onUnSelectAll();
    }

    public void selectAll() {
        onSelectionListener.onSelectAll();
    }

    public void handleDataSetChanged() {
        notifyDataSetChanged();
    }

    public void handleItemChanged(int position) {
        notifyItemChanged(position);
    }

    public void handleItemInserted(int position) {
        notifyItemInserted(position);
    }

    public void handleItemRangeInserted(int positionStart, int itemCount) {
        notifyItemRangeInserted(positionStart, itemCount);
    }

    public void handleItemRemoved(int position) {
        if (enabledSelection) {
            removeSelection(position);
        }
        notifyItemRemoved(position);
    }

    public void handleItemRangeRemoved(int positionStart, int itemCount) {
        if (enabledSelection) {
            for (int i = positionStart; i < itemCount; i++) {
                removeSelection(i);
            }
        }
        notifyItemRangeRemoved(positionStart, itemCount);
    }

    private void setItemSelected(VH viewHolder, int position, boolean selected, OnSelectionListener<VH> customOnSelectionListener) {
        if (selected) {
            if (!PickerManager.getInstance().getSelectedItems().contains(mediaFiles.get(position)))
                if (mediaFiles.get(position).getDuration() > maxDuration) {
                    Toast.makeText(context, "Allowed only duration of file less than 1 minute", Toast.LENGTH_LONG).show();
                    return;
                }
            PickerManager.getInstance().add(mediaFiles.get(position));
            if (customOnSelectionListener != null)
                customOnSelectionListener.onSelected(viewHolder, position);
        } else {
            if (PickerManager.getInstance().remove(mediaFiles.get(position))
                    && PickerManager.getInstance().getSelectedItems().isEmpty()) {
                onSelectionListener.onSelectionEnd();
                if (customOnSelectionListener != null)
                    customOnSelectionListener.onUnSelected(viewHolder, position);
            }
        }
    }

    private void removeSelection(int position) {
        if (PickerManager.getInstance().remove(mediaFiles.get(position))
                && PickerManager.getInstance().getSelectedItems().isEmpty()) {
            onSelectionListener.onSelectionEnd();
        }
    }

    public boolean add(MediaFile mediaFile) {
        if (mediaFiles.add(mediaFile)) {
            handleItemInserted(mediaFiles.size() - 1);
            return true;
        }
        return false;
    }

    public void add(int position, MediaFile mediaFile) {
        mediaFiles.add(position, mediaFile);
        handleItemInserted(position);
    }

    public boolean addAll(Collection<MediaFile> itemSelection) {
        int lastPosition = mediaFiles.size();
        if (mediaFiles.addAll(itemSelection)) {
            notifyItemRangeInserted(lastPosition, itemSelection.size());
            return true;
        }
        return false;
    }

    public boolean addAll(int position, Collection<MediaFile> itemCollection) {
        int lastPosition = mediaFiles.size();
        if (mediaFiles.addAll(position, itemCollection)) {
            handleItemRangeInserted(lastPosition, mediaFiles.size());
            return true;
        }
        return false;
    }

    public void remove(MediaFile item) {
        int position = mediaFiles.indexOf(item);
        handleItemRemoved(position);
        mediaFiles.remove(position);
    }

    public MediaFile remove(int position) {
        handleItemRemoved(position);
        return mediaFiles.remove(position);
    }

    public void removeAll(Collection<MediaFile> itemCollection) {
        ArrayList<MediaFile> removeItems = new ArrayList<>(itemCollection);
        for (int i = itemCollection.size() - 1; i >= 0; i--) {
            remove(removeItems.get(i));
        }
    }

    public interface OnItemClickListener {
        void onClick(View v, int position);
    }

    public interface OnItemLongClickListener {
        boolean onLongClick(View v, int position);
    }

    public interface OnSelectionListener<VH> {
        void onSelectionBegin();

        void onSelected(VH viewHolder, int position);

        void onUnSelected(VH viewHolder, int position);

        void onSelectAll();

        void onUnSelectAll();

        void onSelectionEnd();

        void onMaxReached();
    }
}
