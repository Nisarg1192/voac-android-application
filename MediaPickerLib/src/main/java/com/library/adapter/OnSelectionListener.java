package com.library.adapter;

import android.view.View;

import com.library.model.MediaFile;


public interface OnSelectionListener {
    void onClick(MediaFile Img, View view, int position);

    void onLongClick(MediaFile img, View view, int position);
}
