

package com.library.adapter;

public interface Selectable {

    boolean isSelected();

    void setSelected(boolean selected);
}
