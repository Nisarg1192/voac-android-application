package com.library.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.library.utils.FileUtils;

import java.util.ArrayList;
import java.util.List;

public class MediaDirFile implements Parcelable {


    private String bucketId;
    private String coverPath;
    private String bucketName;
    private long dateAdded;
    private List<MediaFile> medias = new ArrayList<>();

    public MediaDirFile() {
        super();
    }

    protected MediaDirFile(Parcel in) {
        bucketId = in.readString();
        coverPath = in.readString();
        bucketName = in.readString();
        dateAdded = in.readLong();
        medias = in.createTypedArrayList(MediaFile.CREATOR);
    }

    public static final Creator<MediaDirFile> CREATOR = new Creator<MediaDirFile>() {
        @Override
        public MediaDirFile createFromParcel(Parcel in) {
            return new MediaDirFile(in);
        }

        @Override
        public MediaDirFile[] newArray(int size) {
            return new MediaDirFile[size];
        }
    };

    public void addPhoto(MediaFile media) {
        medias.add(media);
    }

    public void addPhotos(List<MediaFile> photosList) {
        medias.addAll(photosList);
    }

    public String getBucketId() {
        if (bucketId.equals(FileUtils.ALL_MEDIA_BUCKET_ID))
            return null;
        return bucketId;
    }


    public void setBucketId(String bucketId) {
        this.bucketId = bucketId;
    }

    public String getCoverPath() {
        if (medias != null && medias.size() > 0)
            return medias.get(0).getPath();
        else if (coverPath != null)
            return coverPath;
        else
            return "";
    }

    public void setCoverPath(String coverPath) {
        this.coverPath = coverPath;
    }

    public String getBucketName() {
        return bucketName;
    }

    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }

    public long getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(long dateAdded) {
        this.dateAdded = dateAdded;
    }

    public List<MediaFile> getMedias() {
        return medias;
    }

    public void setMedias(List<MediaFile> medias) {
        this.medias = medias;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(bucketId);
        dest.writeString(coverPath);
        dest.writeString(bucketName);
        dest.writeLong(dateAdded);
        dest.writeTypedList(medias);
    }
}
