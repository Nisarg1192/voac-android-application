

package com.library.utils;

import android.content.Context;
import android.net.Uri;

import java.io.File;

import androidx.annotation.NonNull;
import androidx.core.content.FileProvider;

public class FilePickerProvider extends FileProvider {
    private static final String FILE_PROVIDER = ".filepicker.provider";

    public static String getAuthority(@NonNull Context context) {
        return context.getPackageName() + FILE_PROVIDER;
    }

    public static Uri getUriForFile(@NonNull Context context, @NonNull File file) {
        return getUriForFile(context, getAuthority(context), file);
    }
}
